<?php

namespace AppBundle\Entity\Report;

use BusinessRules\Entities\Report\CommentFactory;
use BusinessRules\Entities\Report\Report;
use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CommentFactoryImpl implements CommentFactory
{

    /**
     * {@inheritdoc}
     */
    public function create($content, User $author, Report $report)
    {
        $comment = new CommentImpl();
        $comment->setContent($content);
        $comment->setReport($report);
        $comment->setAuthor($author);

        return $comment;
    }
}
