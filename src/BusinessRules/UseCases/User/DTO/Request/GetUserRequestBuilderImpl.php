<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\Exceptions\InvalidGetUserRequestParameterException;
use BusinessRules\Requestors\User\GetUserRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetUserRequestBuilderImpl implements GetUserRequestBuilder
{

    /**
     * @var GetUserRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetUserRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byId($id)
    {
        $this->request->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byEmail($email)
    {
        $this->request->email = $email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byLoginIdentifier($loginIdentifier)
    {
        $this->request->loginIdentifier = $loginIdentifier;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        if ($this->countNotEmptyParameter() > 1) {
            throw new InvalidGetUserRequestParameterException('You should requested only one parameter');
        }

        return $this->request;
    }

    /**
     * @return int
     */
    private function countNotEmptyParameter()
    {
        $arrayRequest = get_object_vars($this->request);

        $countNotEmptyParameter = 0;
        foreach ($arrayRequest as $parameter) {
            if (!empty($parameter)) {
                $countNotEmptyParameter++;
            }
        }

        return $countNotEmptyParameter;
    }
}
