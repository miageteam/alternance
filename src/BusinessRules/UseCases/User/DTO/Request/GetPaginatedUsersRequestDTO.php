<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\GetPaginatedUsersRequest;
use BusinessRules\Utils\Pagination;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedUsersRequestDTO implements GetPaginatedUsersRequest
{

    /**
     * @var string[]
     */
    public $sorts = array();

    /**
     * @var int
     */
    public $page = Pagination::PAGE;

    /**
     * @var int
     */
    public $itemsPerPage = Pagination::ITEMS_PER_PAGE;

    /**
     * @return string[]
     */
    public function getSorts()
    {
        return $this->sorts;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }
}
