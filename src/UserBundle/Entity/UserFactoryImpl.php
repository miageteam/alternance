<?php

namespace UserBundle\Entity;

use BusinessRules\Entities\User\Group;
use BusinessRules\Entities\User\User;
use BusinessRules\Entities\User\UserFactory;
use BusinessRules\Gateways\User\GroupGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\User\CreateUserRequest;
use BusinessRules\Requestors\User\ModifyUserRequest;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserFactoryImpl implements UserFactory
{

    /**
     * @var GroupGateway
     */
    private $groupGateway;

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var PasswordEncoderInterface
     */
    private $encoder;

    /**
     * {@inheritdoc}
     */
    public function createFromRequest(CreateUserRequest $request)
    {
        $group = $this->groupGateway->find($request->getGroupId());

        $user = new UserImpl();
        $user->setFirstName($request->getFirstName());
        $user->setLastName($request->getLastName());
        $user->setLoginIdentifier($this->generateLogin($request->getFirstName(), $request->getLastName()));
        $user->setEmail($request->getEmail());
        $user->setPhoneNumber($request->getPhoneNumber());
        $user->setPassword($this->encoder->encodePassword(substr(md5($request->getEmail()), 0, 8), $user->getSalt()));
        $user->setGroup($group);

        return $user;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     *
     * @return string
     */
    private function generateLogin($firstName, $lastName)
    {
        $login            = preg_replace('/\s+/', '', $firstName . $lastName);
        $existingUsers    = $this->userGateway->findAllLikeLogin($login);
        $existingSuffixes = array();

        /** @var User $existingUser */
        foreach ($existingUsers as $existingUser) {
            $existingSuffix = mb_substr($existingUser->getLoginIdentifier(), strlen($login));
            if (is_numeric($existingSuffix) || '' === $existingSuffix) {
                $existingSuffixes[$existingSuffix] = $existingSuffix;
            }
        }

        $notSet = true;
        if (!isset($existingSuffixes[''])) {
            $notSet = false;
        }

        $i = 0;
        while ($notSet) {
            if (!isset($existingSuffixes[++$i])) {
                $login .= $i;
                $notSet = false;
            }
        }

        return $login;
    }

    public function ModifyFromRequest(User $user, ModifyUserRequest $request)
    {
        $group = $this->getGroupOrNull($request->getGroupId());

        if (null !== $request->getFirstName()) {
            $user->setFirstName($request->getFirstName());
        }

        if(null !==$request->getLastName()){
            $user->setLastName($request->getLastName());
        }

        if(null !==$request->getEmail()){
            $user->setEmail($request->getEmail());
        }

        if(null !==$request->getPhoneNumber()){
            $user->setPhoneNumber($request->getPhoneNumber());
        }

        if(null !==$group){
            $user->setGroup($group);
        }

        if (null !== $request->getPassword()){
            $user->setPassword($this->encoder->encodePassword($request->getPassword(), $user->getSalt()));
        }

        return $user;
    }

    /**
     * @param int $groupId
     *
     * @return Group|null
     */
    private function getGroupOrNull($groupId)
    {
        if (empty($groupId)) {
            return null;
        }

        return $this->groupGateway->find($groupId);
    }

    /**
     * @param GroupGateway $groupGateway
     */
    public function setGroupGateway(GroupGateway $groupGateway)
    {
        $this->groupGateway = $groupGateway;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param PasswordEncoderInterface $encoder
     */
    public function setEncoder(PasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
}
