<?php

namespace AppBundle\Entity\Report;

use BusinessRules\Entities\Report\AttachmentFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class AttachmentFactoryImpl implements AttachmentFactory
{

    /**
     * {@inheritdoc}
     */
    public function create($attachmentPath)
    {
        $attachment = new AttachmentImpl();

        $attachment->setPath($attachmentPath);

        return $attachment;
    }
}
