<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\EditCommentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditCommentRequestDTO implements EditCommentRequest
{

    /**
     * @var int
     */
    public $commentId;

    /**
     * @var string
     */
    public $content;

    /**
     * {@inheritdoc}
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }
}
