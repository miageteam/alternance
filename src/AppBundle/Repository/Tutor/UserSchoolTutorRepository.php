<?php

namespace AppBundle\Repository\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Gateways\Tutor\Exceptions\UserSchoolTutorNotFoundException;
use BusinessRules\Gateways\Tutor\UserSchoolTutorGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserSchoolTutorRepository extends EntityRepository implements UserSchoolTutorGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(UserSchoolTutor $userSchoolTutor)
    {
        $this->_em->persist($userSchoolTutor);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        try {
            return $this->createQueryBuilder('ust')
                ->addSelect('u')
                ->leftJoin('ust.user', 'u')
                ->andWhere('ust.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new UserSchoolTutorNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        try {
            return $this->createQueryBuilder('ust')
                ->addSelect('u')
                ->leftJoin('ust.user', 'u')
                ->andWhere('u.id = :id')
                ->setParameter('id', $userId)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new UserSchoolTutorNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function update(UserSchoolTutor $userSchoolTutor)
    {
        // do nothing
    }
}
