<?php

namespace BusinessRules\Entities\User;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
abstract class Group
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var UserRole[]|ArrayCollection
     */
    protected $userRoles;

    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return UserRole[]|ArrayCollection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    /**
     * @param UserRole[]|ArrayCollection $userRoles
     */
    public function setUserRoles(array $userRoles)
    {
        $this->userRoles = $userRoles;
    }
}
