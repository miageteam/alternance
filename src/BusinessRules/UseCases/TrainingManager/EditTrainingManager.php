<?php

namespace BusinessRules\UseCases\TrainingManager;

use BusinessRules\Entities\TrainingManager\TrainingManagerFactory;
use BusinessRules\Gateways\TrainingManager\TrainingManagerGateway;
use BusinessRules\Requestors\TrainingManager\EditTrainingManagerRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class EditTrainingManager implements UseCase
{

    /**
     * @var TrainingManagerGateway
     */
    private $trainingManagerGateway;

    /**
     * @var TrainingManagerFactory
     */
    private $trainingManagerFactory;

    /**
     * @Transaction
     *
     * @param EditTrainingManagerRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $trainingManager = $this->trainingManagerGateway->findByUserId($request->getUserId());

        $updatedTrainingManager = $this->trainingManagerFactory->editFromRequest($trainingManager, $request);

        $this->trainingManagerGateway->update($updatedTrainingManager);
    }

    /**
     * @param TrainingManagerGateway $trainingManagerGateway
     */
    public function setTrainingManagerGateway($trainingManagerGateway)
    {
        $this->trainingManagerGateway = $trainingManagerGateway;
    }

    /**
     * @param TrainingManagerFactory $trainingManagerFactory
     */
    public function setTrainingManagerFactory($trainingManagerFactory)
    {
        $this->trainingManagerFactory = $trainingManagerFactory;
    }
}
