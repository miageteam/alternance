<?php

namespace BusinessRules\Responders\User;

use BusinessRules\Entities\User\UserRole;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GroupResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return UserRole[]
     */
    public function getUserRoles();
}
