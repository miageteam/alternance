<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Requestors\User\GetUserRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetUserRequestBuilderImplTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GetUserRequestBuilder
     */
    private $builder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\User\Exceptions\InvalidGetUserRequestParameterException
     * @expectedExceptionMessage You should requested only one parameter
     */
    public function moreOfOnceParameter_ThrowException()
    {
        $this->builder
            ->create()
            ->byId(UserStub1::ID)
            ->byEmail(UserStub1::EMAIL)
            ->build();
    }

    /**
     * @test
     */
    public function byId()
    {
        $request = $this->builder
            ->create()
            ->byId(UserStub1::ID)
            ->build();

        $this->assertEquals(UserStub1::ID, $request->getId());
        $this->assertNull($request->getEmail());
    }

    /**
     * @test
     */
    public function byEmail()
    {
        $request = $this->builder
            ->create()
            ->byEmail(UserStub1::EMAIL)
            ->build();

        $this->assertNull($request->getId());
        $this->assertEquals(UserStub1::EMAIL, $request->getEmail());
    }

    protected function setUp()
    {
        $this->builder = new GetUserRequestBuilderImpl();
    }
}
