<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('app_homepage');
        }

        $session = $request->getSession();

        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }

        if ($error) {
            $this->addFlash('error', 'app.form.authenticate.error.text');
        }

        return $this->render(
            'AppBundle:Security:login.html.twig',
            array(
                'last_username' => $session->get(Security::LAST_USERNAME),
            )
        );
    }
}
