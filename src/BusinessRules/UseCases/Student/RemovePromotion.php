<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\PromotionGateway;
use BusinessRules\Requestors\Student\RemovePromotionRequest;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class RemovePromotion implements UseCase
{

    /**
     * @var PromotionGateway
     */
    private $promotionGateway;

    /**
     *
     * @Transaction
     *
     * @param RemovePromotionRequest $useCaseRequest
     *
     */
    public function execute(UseCaseRequest $useCaseRequest)
    {
        $promotion = $this->promotionGateway->find($useCaseRequest->getPromotionId());

        $this->promotionGateway->remove($promotion);
    }

    /**
     * @param PromotionGateway $promotionGateway
     */
    public function setPromotionGateway(PromotionGateway $promotionGateway)
    {
        $this->promotionGateway = $promotionGateway;
    }


}
