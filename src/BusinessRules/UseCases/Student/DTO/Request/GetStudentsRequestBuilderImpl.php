<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 09/06/2015
 * Time: 16:44
 */

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\Exceptions\InvalidGetStudentRequestParameterException;
use BusinessRules\Requestors\Student\GetStudentsRequestBuilder;

class GetStudentsRequestBuilderImpl implements GetStudentsRequestBuilder
{

    /**
     * @var GetStudentsRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetStudentsRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bySchoolTutorId($schoolTutorId)
    {
        $this->request->schoolTutorId = $schoolTutorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        if ($this->countNotEmptyParameter() > 1) {
            throw new InvalidGetStudentRequestParameterException('You should requested only one parameter');
        }

        return $this->request;
    }

    /**
     * @return int
     */
    private function countNotEmptyParameter()
    {
        $arrayRequest = get_object_vars($this->request);

        $countNotEmptyParameter = 0;
        foreach ($arrayRequest as $parameter) {
            if (!empty($parameter)) {
                $countNotEmptyParameter++;
            }
        }

        return $countNotEmptyParameter;
    }

    /**
     * @param int $promotionId
     *
     * @return GetStudentsRequestBuilder
     */
    public function byPromotionId($promotionId)
    {
        $this->request->schoolTutorId = $promotionId;

        return $this;
    }
}
