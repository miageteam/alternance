<?php

namespace BusinessRules\Gateways\Student;

use BusinessRules\Entities\Student\Formation;
use BusinessRules\Gateways\Student\Exceptions\FormationAlreadyExistException;
use BusinessRules\Gateways\Student\Exceptions\FormationNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryFormationGateway implements FormationGateway
{

    /**
     * @var Formation[]
     */
    public static $formations = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @param Formation[] $formations
     */
    public function __construct(array $formations = array())
    {
        self::$formations = $formations;
        self::$id         = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(Formation $formation)
    {
        foreach (self::$formations as $localFormation) {
            if ($localFormation->getName() === $formation->getName()) {
                throw new FormationAlreadyExistException();
            }
        }

        self::$formations[++self::$id] = $formation;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (!self::$formations[$id]) {
            throw new FormationNotFoundException();
        }

        return self::$formations[$id];
    }

    /**
     * @return Formation[]
     */
    public function findAll()
    {
        // TODO: Implement this method.
    }

    /**
     * @param Formation $formation
     */
    public function remove(Formation $formation)
    {
        // TODO: Implement this method.
    }
}
