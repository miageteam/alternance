<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\EditStudentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditStudentRequestDTO implements EditStudentRequest
{

    /**
     * @return int
     */
    public $userId;

    /**
     * @return int
     */
    public $promotionId;

    /**
     * @return int
     */
    public $userSchoolTutorId;

    /**
     * @return int
     */
    public $userCompanyTutorId;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserSchoolTutorId()
    {
        return $this->userSchoolTutorId;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserCompanyTutorId()
    {
        return $this->userCompanyTutorId;
    }
}
