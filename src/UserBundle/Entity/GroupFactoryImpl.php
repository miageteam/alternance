<?php

namespace UserBundle\Entity;

use BusinessRules\Entities\User\Group;
use BusinessRules\Entities\User\GroupFactory;
use BusinessRules\Requestors\User\CreateGroupRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupFactoryImpl implements GroupFactory
{
    /**
     * @param CreateGroupRequest $request
     *
     * @return Group
     */
    public function createFromRequest(CreateGroupRequest $request)
    {
        $group = new GroupImpl();

        $group->setName($request->getName());
        $group->setUserRoles($request->getUserRoles());

        return $group;
    }
}
