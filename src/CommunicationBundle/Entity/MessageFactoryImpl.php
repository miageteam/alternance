<?php

namespace CommunicationBundle\Entity;

use BusinessRules\Entities\Communication\Flow;
use BusinessRules\Entities\Communication\MessageFactory;
use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MessageFactoryImpl implements MessageFactory
{
    /**
     * {@inheritdoc}
     */
    public function create($content, Flow $flow, User $author)
    {
        $message = new MessageImpl();
        $message->setContent($content);
        $message->setFlow($flow);
        $message->setAuthor($author);

        return $message;
    }
}
