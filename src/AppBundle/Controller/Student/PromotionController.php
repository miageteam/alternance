<?php

namespace AppBundle\Controller\Student;

use AppBundle\Form\Model\EditPromotionModel;
use AppBundle\Form\Model\PromotionModel;
use AppBundle\Form\Type\CreatePromotionType;
use AppBundle\Form\Type\EditPromotionType;
use BusinessRules\Gateways\Student\Exceptions\PromotionNotFoundException;
use BusinessRules\Requestors\Student\CreatePromotionRequest;
use BusinessRules\Requestors\Student\EditPromotionRequest;
use BusinessRules\Responders\Student\PromotionResponse;
use BusinessRules\UseCases\Student\DTO\Request\GetPromotionsRequestDTO;
use BusinessRules\UseCases\Student\DTO\Request\RemovePromotionRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PromotionController extends Controller
{
    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(new CreatePromotionType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var PromotionModel $model */
            $model = $form->getData();

            $model->upload();

            $this->get('use_cases.student.create_promotion')->execute($this->getUseCaseRequest($model));

            $this->addFlash('success', 'app.flash_message.create_promotion_success');

            return $this->redirectToRoute('app_user_student_show_list_promotion');
        }

        return $this->render('AppBundle:Student/Promotion:create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param PromotionModel $model
     *
     * @return CreatePromotionRequest
     */
    private function getUseCaseRequest(PromotionModel $model)
    {
        return $this->get('request.student.create_promotion_request_builder')
            ->create()
            ->withYear($model->year)
            ->withFormationId($model->formation)
            ->withAlternationCalendarPath($model->alternationCalendarPath)
            ->build();
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function removePromotionAction($promotionId)
    {
        $promotion = $this->get('use_cases.student.get_promotion')
            ->execute(
                $this->get('request.student.get_promotion_request_builder')
                    ->create()
                    ->byId($promotionId)
                    ->build()
            );


        if ($this->isPermittedToRemove($promotion)) {
            $this->get('use_cases.student.remove_promotion')
                ->execute(new RemovePromotionRequestDTO($promotionId));

            $this->addFlash('success', 'app.flash_message.promotion_remove_success');
        }

        return $this->redirectToRoute('app_user_student_show_list_promotion');
    }

    /**
     * @param PromotionResponse $promotion
     *
     * @return bool
     */
    private function isPermittedToRemove(PromotionResponse $promotion)
    {
        if ($this->hasStudents($promotion)) {
            $this->addFlash('error', 'app.flash_message.promotion_remove_fail_cause_have_student_in');

            return false;
        }

        if ($this->hasTrainingManager($promotion)) {
            $this->addFlash('error', 'app.flash_message.promotion_remove_fail_cause_have_training_manager');

            return false;
        }

        return true;
    }

    /**
     * @param PromotionResponse $promotion
     *
     * @return bool
     */
    private function hasStudents(PromotionResponse $promotion)
    {
        $students = $this->get('use_cases.student.get_students')->execute(
            $this->get('request.student.get_students_request_builder')
                ->create()
                ->byPromotionId($promotion->getId())
                ->build()
        );

        return count($students->getUserStudents()) > 0;
    }

    /**
     * @param PromotionResponse $promotion
     *
     * @return bool
     */
    private function hasTrainingManager(PromotionResponse $promotion)
    {
        $response = $this->get('use_cases.training_manager.get_training_managers')->execute(
            $this->get('request.training_manager.get_training_managers_request_builder')
                ->create()
                ->byPromotionId($promotion->getId())
                ->build()
        );

        foreach ($response->getTrainingManagers() as $trainingManager) {
            if ($trainingManager->getPromotion()->getId() === $promotion->getId()) {
                return true;
            }
        }

        return false;
    }


    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function showAllPromotionsAction()
    {
        $response = $this->get('use_cases.student.get_promotions')->execute(new GetPromotionsRequestDTO());

        return $this->render(
            'AppBundle:Student/Promotion:list.html.twig',
            array('promotions' => $response->getPromotions())
        );
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function showAction($promotionId)
    {
        try {
            $promotion = $this->get('use_cases.student.get_promotion')
                ->execute(
                    $this->get('request.student.get_promotion_request_builder')
                        ->create()
                        ->byId($promotionId)
                        ->build()
                );

            return $this->render('AppBundle:Student/Promotion:show.html.twig', array('promotion' => $promotion));
        } catch (PromotionNotFoundException $e) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function editPromotionAction(Request $request, $promotionId)
    {
        $promotion = $this->get('use_cases.student.get_promotion')
            ->execute(
                $this->get('request.student.get_promotion_request_builder')
                    ->create()
                    ->byId($promotionId)
                    ->build()
            );


        $model                          = new EditPromotionModel();
        $model->formation               = $promotion->getFormation();
        $model->year                    = $promotion->getYear();
        $model->alternationCalendarPath = $promotion->getAlternationCalendarPath();

        $form = $this->createForm(
            new EditPromotionType(),
            $model,
            array(
                'action' => $this->generateUrl(
                    'app_user_student_edit_promotion',
                    array('promotionId' => $promotionId)
                )
            )
        );


        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EditPromotionModel $model */
            $model = $form->getData();

            if ($model->alternationCalendar) {
                $model->upload();
            }

            $this->get('use_cases.student.edit_promotion')->execute(
                $this->getEditPromotionRequest($model, $promotionId)
            );

            $this->addFlash('success', 'app.flash_message.promotion_edit_success');

            return $this->redirectToRoute(
                'app_user_student_promotion_show',
                array('promotionId' => $promotionId)
            );
        }

        return $this->render('AppBundle:Student/Promotion:edit.html.twig', array('form' => $form->createView()));
    }


    /**
     * @param EditPromotionModel $model
     * @param int                $promotionId
     *
     * @return EditPromotionRequest
     */
    private function getEditPromotionRequest(EditPromotionModel $model, $promotionId)
    {
        return $this->get('request.student.edit_promotion_request_builder')
            ->create()
            ->withPromotionId($promotionId)
            ->withFormationId($model->formation)
            ->withYear($model->year)
            ->withAlternationCalendarPath($model->alternationCalendarPath)
            ->build();
    }
}
