<?php

namespace BusinessRules\Gateways\User\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserNotFoundException extends \Exception
{

}
