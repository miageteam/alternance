<?php

namespace BusinessRules\Entities\User;

use BusinessRules\Requestors\User\CreateGroupRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GroupFactory
{
    /**
     * @param CreateGroupRequest $request
     *
     * @return Group
     */
    public function createFromRequest(CreateGroupRequest $request);
}
