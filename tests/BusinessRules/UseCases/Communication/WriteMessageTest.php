<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Entities\Communication\AttachmentStub1;
use BusinessRules\Entities\Communication\FlowStub1;
use BusinessRules\Entities\Communication\MessageStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub2;
use BusinessRules\Gateways\Communication\InMemoryAttachmentGateway;
use BusinessRules\Gateways\Communication\InMemoryFlowGateway;
use BusinessRules\Gateways\Communication\InMemoryMessageGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Communication\WriteMessageRequestBuilder;
use BusinessRules\UseCases\Communication\DTO\Request\WriteMessageRequestBuilderImpl;
use CommunicationBundle\Entity\AttachmentFactoryImpl;
use CommunicationBundle\Entity\FlowFactoryImpl;
use CommunicationBundle\Entity\MessageFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteMessageTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteMessage
     */
    private $useCase;

    /**
     * @var WriteMessageRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function AuthorNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withContent(MessageStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->withTitle(FlowStub1::TITLE)
                ->withParticipantIds(array(UserStub1::ID, UserStub2::ID))
                ->build()
        );
    }

    /**
     * @test
     */
    public function WriteMessage_CreateFlowAndInsertNewMessage()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withContent(MessageStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->withTitle(FlowStub1::TITLE)
                ->withParticipantIds(array(UserStub1::ID, UserStub2::ID))
                ->build()
        );

        $this->assertCount(1, InMemoryFlowGateway::$flows);
        foreach (InMemoryFlowGateway::$flows as $flow) {
            $this->assertEquals(FlowStub1::TITLE, $flow->getTitle());
        }

        $this->assertCount(1, InMemoryMessageGateway::$messages);
        foreach (InMemoryMessageGateway::$messages as $message) {
            $this->assertEquals(MessageStub1::CONTENT, $message->getContent());
            $this->assertEquals(UserStub1::ID, $message->getAuthor()->getId());
        }
    }

    /**
     * @test
     */
    public function WithAttachment_SaveAttachment()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withContent(MessageStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->withTitle(FlowStub1::TITLE)
                ->withParticipantIds(array(UserStub1::ID, UserStub2::ID))
                ->withAttachmentPath(AttachmentStub1::PATH)
                ->build()
        );

        $this->assertCount(1, InMemoryAttachmentGateway::$attachments);
        foreach (InMemoryAttachmentGateway::$attachments as $attachment) {
            $this->assertEquals(AttachmentStub1::PATH, $attachment->getPath());
            $this->assertEquals(MessageStub1::CONTENT, $attachment->getMessage()->getContent());
        }
    }

    protected function setUp()
    {
        $this->useCase = new WriteMessage();
        $this->useCaseSetUserGateway();
        $this->useCase->setFlowFactory(new FlowFactoryImpl());
        $this->useCase->setFlowGateway(new InMemoryFlowGateway());
        $this->useCase->setMessageFactory(new MessageFactoryImpl());
        $this->useCase->setMessageGateway(new InMemoryMessageGateway());
        $this->useCase->setAttachmentGateway(new InMemoryAttachmentGateway());
        $this->useCase->setAttachmentFactory(new AttachmentFactoryImpl());

        $this->requestBuilder = new WriteMessageRequestBuilderImpl();
    }

    private function useCaseSetUserGateway()
    {
        $this->useCase->setUserGateway(
            new InMemoryUserGateway(
                array(
                    UserStub1::ID => new UserStub1(),
                    UserStub2::ID => new UserStub2()
                )
            )
        );
    }
}
