<?php

namespace CommunicationBundle\Entity;

use BusinessRules\Entities\Communication\FlowFactory;
use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowFactoryImpl implements FlowFactory
{

    /**
     * {@inheritdoc}
     */
    public function create($title, User $author, array $receivers)
    {
        $flow = new FlowImpl();
        $flow->setTitle($title);
        $flow->setParticipants(array_merge(array($author), $receivers));

        return $flow;
    }
}
