<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\GroupStub1;
use BusinessRules\Entities\User\PromotionStub1;
use BusinessRules\Entities\User\User;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\User\InMemoryGroupGateway;
use BusinessRules\Gateways\User\InMemoryPromotionGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\User\CreateUserRequestBuilder;
use BusinessRules\Services\Security\PasswordEncoder;
use BusinessRules\UseCases\User\DTO\Request\CreateUserRequestBuilderImpl;
use BusinessRules\UseCases\User\DTO\Response\UserResponseFactoryImpl;
use OpenClassrooms\UseCase\Application\Responder\PaginatedUseCaseResponseBuilderImpl;
use UserBundle\Entity\UserFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateUserTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CreateUser
     */
    private $useCase;

    /**
     * @var CreateUserRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\User\Exceptions\InvalidCreateUserRequestParameterException
     *
     * @expectedExceptionMessage The argument "Email" must be defined.
     */
    public function CreateWithoutEmail_ThrowException()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withFirstName(UserStub1::FIRSTNAME)
                ->withLastName(UserStub1::LASTNAME)
                ->build()
        );
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\User\Exceptions\InvalidCreateUserRequestParameterException
     *
     * @expectedExceptionMessage The argument "FirstName" must be defined.
     */
    public function CreateWithoutFirstName_ThrowException()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withLastName(UserStub1::LASTNAME)
                ->withEmail(UserStub1::EMAIL)
                ->build()
        );
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\User\Exceptions\InvalidCreateUserRequestParameterException
     *
     * @expectedExceptionMessage The argument "LastName" must be defined.
     */
    public function CreateWithoutLastName_ThrowException()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withFirstName(UserStub1::FIRSTNAME)
                ->withEmail(UserStub1::EMAIL)
                ->build()
        );
    }

    /**
     * @test
     */
    public function CreateUser()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withFirstName(UserStub1::FIRSTNAME)
                ->withLastName(UserStub1::LASTNAME)
                ->withEmail(UserStub1::EMAIL)
                ->withGroupId(GroupStub1::ID)
                ->build()
        );

        $this->assertCount(1, InMemoryUserGateway::$users);
        foreach (InMemoryUserGateway::$users as $user) {
            $this->assertUser($user);
        }
    }

    private function assertUser(User $user)
    {
        $this->assertEquals(UserStub1::FIRSTNAME, $user->getFirstName());
        $this->assertEquals(UserStub1::LASTNAME, $user->getLastName());
        $this->assertEquals(UserStub1::LOGIN_IDENTIFIER, $user->getLoginIdentifier());
        $this->assertEquals(UserStub1::EMAIL, $user->getEmail());
        $this->assertEquals(GroupStub1::ID, $user->getGroup()->getId());
    }

    protected function setUp()
    {
        $this->useCase = new CreateUser();
        $this->useCase->setUserFactory($this->getUserFactory());
        $this->useCase->setUserGateway(new InMemoryUserGateway());
        $this->useCase->setUserResponseFactory($this->getUserResponseFactory());

        $this->requestBuilder = new CreateUserRequestBuilderImpl();
        InMemoryUserGateway::$users = array();
    }

    private function getUserFactory()
    {
        $userFactory = new UserFactoryImpl();
        $userFactory->setGroupGateway(new InMemoryGroupGateway(array(GroupStub1::ID => new GroupStub1())));
        $userFactory->setEncoder(new PasswordEncoder());
        $userFactory->setUserGateway(new InMemoryUserGateway());

        return $userFactory;
    }

    private function getUserResponseFactory()
    {
        $userResponseFactory = new UserResponseFactoryImpl();
        $userResponseFactory->setPaginatedUseCaseResponseBuilder(new PaginatedUseCaseResponseBuilderImpl());

        return $userResponseFactory;
    }
}
