<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 15/06/2015
 * Time: 11:21
 */

namespace BusinessRules\UseCases\Student\DTO\Request;


use BusinessRules\Requestors\Student\GetPromotionRequest;

class GetPromotionRequestDTO implements GetPromotionRequest
{

    /**
     * @var int
     */
    public $id;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
}
