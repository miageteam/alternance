<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\ModifyUserRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ModifyUserRequestDTO implements ModifyUserRequest
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var String
     */
    public $firstName;

    /**
     * @var String
     */
    public $lastName;

    /**
     * @var String
     */
    public $email;

    /**
     * @var int
     */
    public $groupId;


    /**
     * @var int
     */
    public $phoneNumber;

    /**
     * @var string
     */
    public $password;


    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }
}
