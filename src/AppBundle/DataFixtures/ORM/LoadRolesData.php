<?php

namespace AppBundle\DataFixtures\ORM;

use BusinessRules\Entities\User\UserRole;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\UserRoleImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class LoadRolesData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $roles = array(
            'User Standard'    => 'ROLE_USER',
            'Student'          => 'ROLE_STUDENT',
            'Professor'        => 'ROLE_PROFESSOR',
            'Tutor'            => 'ROLE_TUTOR',
            'training manager' => 'ROLE_TRAINING_MANAGER',
            'Secretary'        => 'ROLE_SECRETARY',
            'Admin'            => 'ROLE_ADMIN',
        );

        $userRoles = $this->setRoles($roles);

        $this->insertAll($manager, $userRoles);
    }

    /**
     * @param array $roles
     *
     * @return array
     */
    private function setRoles(array $roles)
    {
        $userRoles = array();
        foreach ($roles as $label => $role) {
            $userRole = new UserRoleImpl();
            $userRole->setLabel($label);
            $userRole->setRole($role);
            $userRoles[] = $userRole;
        }

        return $userRoles;
    }

    /**
     * @param ObjectManager $manager
     * @param UserRole[]    $userRoles
     */
    private function insertAll(ObjectManager $manager, array $userRoles)
    {
        foreach ($userRoles as $key => $userRole) {
            $manager->persist($userRole);
            $manager->flush();

            $this->addReference($userRole->getRole(), $userRole);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
