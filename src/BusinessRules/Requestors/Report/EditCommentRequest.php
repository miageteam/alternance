<?php

namespace BusinessRules\Requestors\Report;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface EditCommentRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getCommentId();

    /**
     * @return string
     */
    public function getContent();
}
