<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\GetPaginatedStudentsRequest;
use BusinessRules\Utils\Pagination;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedStudentsRequestDTO implements GetPaginatedStudentsRequest
{

    /**
     * @var int
     */
    public $promotionId;

    /**
     * @var string[]
     */
    public $sorts = array();

    /**
     * @var int
     */
    public $page = Pagination::PAGE;

    /**
     * @var int
     */
    public $itemsPerPage = Pagination::ITEMS_PER_PAGE;

    /**
     * @return int
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }

    /**
     * @return string[]
     */
    public function getSorts()
    {
        return $this->sorts;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }
}
