<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 08/06/2015
 * Time: 12:18
 */

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

interface GetStudentsRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getSchoolTutorId();
}
