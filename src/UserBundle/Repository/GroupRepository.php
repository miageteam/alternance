<?php

namespace UserBundle\Repository;

use BusinessRules\Entities\User\Group;
use BusinessRules\Entities\User\GroupName;
use BusinessRules\Gateways\User\Exceptions\GroupNotFoundException;
use BusinessRules\Gateways\User\GroupGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupRepository extends EntityRepository implements GroupGateway
{
    /**
     * {@inheritdoc}
     */
    public function insert(Group $group)
    {
        $this->_em->persist($group);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        try {
            return $this->createQueryBuilder('g')
                ->where('g.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult();

        } catch (NoResultException $nre) {
            throw new GroupNotFoundException();
        }
    }


    /**
     * {@inheritdoc}
     */
    public function findByName($name)
    {
        try {
            return $this->createQueryBuilder('g')
                ->where('g.name = :name')
                ->setParameter('name', $name)
                ->getQuery()
                ->getSingleResult();

        } catch (NoResultException $nre) {
            throw new GroupNotFoundException();
        }
    }

    public function getAllWithoutAdminAndSecretaryQueryBuilder()
    {
        return $this->createQueryBuilder('g')
            ->where('g.name NOT IN (:names)')
            ->setParameter('names', array(GroupName::ADMIN, GroupName::SECRETARY));
    }
}
