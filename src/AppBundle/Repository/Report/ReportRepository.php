<?php

namespace AppBundle\Repository\Report;

use AppBundle\Service\Pagination\PaginatedCollectionFactory;
use BusinessRules\Entities\Report\Report;
use BusinessRules\Entities\Report\ReportFilter;
use BusinessRules\Entities\Report\ReportSort;
use BusinessRules\Gateways\Report\Exceptions\ReportNotFoundException;
use BusinessRules\Gateways\Report\ReportGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReportRepository extends EntityRepository implements ReportGateway
{

    /**
     * @var PaginatedCollectionFactory
     */
    private $paginatedCollectionFactory;

    /**
     * @param PaginatedCollectionFactory $paginatedCollectionFactory
     */
    public function setPaginatedCollectionFactory(PaginatedCollectionFactory $paginatedCollectionFactory)
    {
        $this->paginatedCollectionFactory = $paginatedCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(Report $report)
    {
        $this->_em->persist($report);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        try {
            return $this->createQueryBuilder('r')
                ->addSelect('att')
                ->addSelect('partial a.{id, firstName, lastName, email}')
                ->addSelect('c')
                ->leftJoin('r.attachment', 'att')
                ->leftJoin('r.author', 'a')
                ->leftJoin('r.comments', 'c')
                ->andWhere('r.id = :id')
                ->setParameter('id', $id)
                ->orderBy('c.createdAt', 'ASC')
                ->getQuery()
                ->getSingleResult();

        } catch (NoResultException $nre) {
            throw new ReportNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        $queryBuilder = $this->createQueryBuilder('r')
            ->addSelect('partial a.{id, firstName, lastName, email}')
            ->addSelect('c')
            ->leftJoin('r.author', 'a')
            ->leftJoin('r.comments', 'c');

        $filteredQueryBuilder       = $this->buildFilters($queryBuilder, $filters);
        $sortedFilteredQueryBuilder = $this->buildSorts($filteredQueryBuilder, $sorts);

        return $this->paginatedCollectionFactory->make($sortedFilteredQueryBuilder, $pagination);
    }

    /**
     * @return QueryBuilder
     */
    private function buildFilters(QueryBuilder $queryBuilder, array $filters = array())
    {
        if (isset($filters[ReportFilter::USER])) {
            $queryBuilder->andWhere('a = :user');
            $queryBuilder->setParameter('user', $filters[ReportFilter::USER]);
        }

        if (isset($filters[ReportFilter::VALIDATED])) {
            $queryBuilder->andWhere('r.validated = :validated');
            $queryBuilder->setParameter('validated', $filters[ReportFilter::VALIDATED]);
        }

        return $queryBuilder;
    }

    /**
     * @return QueryBuilder
     */
    private function buildSorts(QueryBuilder $queryBuilder, array $sorts = array())
    {
        if (isset($sorts[ReportSort::CREATED_AT])) {
            $queryBuilder->addOrderBy('r.' . ReportSort::CREATED_AT, $sorts[ReportSort::CREATED_AT]);
        }

        return $queryBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function update(Report $report)
    {
        // do nothing;
    }
}
