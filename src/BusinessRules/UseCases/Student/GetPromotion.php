<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 15/06/2015
 * Time: 11:04
 */

namespace BusinessRules\UseCases\Student;


use BusinessRules\Gateways\Student\PromotionGateway;
use BusinessRules\Requestors\Student\GetPromotionRequest;
use BusinessRules\Responders\Student\PromotionResponse;
use BusinessRules\Responders\Student\PromotionResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

class GetPromotion implements UseCase
{

    /**
     * @var PromotionGateway
     */
    private $promotionGateway;

    /**
     * @var PromotionResponseFactory
     */
    private $promotionResponseFactory;

    /**
     * @param GetPromotionRequest $request
     *
     * @return PromotionResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $promotion = $this->promotionGateway->find($request->getId());

        return $this->promotionResponseFactory->createFromPromotion($promotion);
    }

    /**
     * @param PromotionGateway $promotionGateway
     */
    public function setPromotionGateway(PromotionGateway $promotionGateway)
    {
        $this->promotionGateway = $promotionGateway;
    }

    /**
     * @param PromotionResponseFactory $promotionResponseFactory
     */
    public function setPromotionResponseFactory(PromotionResponseFactory $promotionResponseFactory)
    {
        $this->promotionResponseFactory = $promotionResponseFactory;
    }
}
