<?php

namespace BusinessRules\Gateways\Communication\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowNotFoundException extends \Exception
{

}
