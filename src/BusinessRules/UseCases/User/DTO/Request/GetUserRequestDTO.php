<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\GetUserRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetUserRequestDTO implements GetUserRequest
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $loginIdentifier;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function getLoginIdentifier()
    {
        return $this->loginIdentifier;
    }
}
