<?php

namespace BusinessRules\Gateways\Student;

use BusinessRules\Entities\Student\UserStudent;
use BusinessRules\Gateways\Student\Exceptions\UserStudentNotFoundException;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserStudentGateway
{
    /**
     * @param UserStudent $userStudent
     */
    public function insert(UserStudent $userStudent);

    /**
     * @param UserStudent $userStudent
     */
    public function update(UserStudent $userStudent);

    /**
     * @param int $userId
     *
     * @return UserStudent
     *
     * @throws UserStudentNotFoundException
     */
    public function findByUserId($userId);

    /**
     * @param int $CompanyTutorId
     *
     * @return UserStudent
     *
     * @throws UserStudentNotFoundException
     */
    public function findByCompanyTutorId($CompanyTutorId);

    /**
     * @param int $schoolTutorId
     *
     * @return UserStudent[]
     */
    public function findAllBySchoolTutor($schoolTutorId);

    /**
     * @param array $filters
     * @param array $sorts
     * @param array $pagination
     *
     * @return PaginatedCollection
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array());

    /**
     * @param int $promotionId
     *
     * @return UserStudent[]
     */
    public function findAllByPromotionId($promotionId);
}
