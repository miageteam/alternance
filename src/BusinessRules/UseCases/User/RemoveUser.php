<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\User\RemoveUserRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class RemoveUser implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @Transaction
     *
     * @param RemoveUserRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $user = $this->userGateway->find($request->getId());

        $this->userGateway->remove($user);
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }
}
