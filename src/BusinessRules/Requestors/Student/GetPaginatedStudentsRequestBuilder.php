<?php

namespace BusinessRules\Requestors\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetPaginatedStudentsRequestBuilder
{
    /**
     * @return GetPaginatedStudentsRequestBuilder
     */
    public function create();

    /**
     * @param int $promotionId
     *
     * @return GetPaginatedStudentsRequestBuilder
     */
    public function withPromotionId($promotionId);

    /**
     * @param array $sorts
     *
     * @return GetPaginatedStudentsRequestBuilder
     */
    public function withSorts(array $sorts);

    /**
     * @param int $page
     *
     * @return GetPaginatedStudentsRequestBuilder
     */
    public function withPage($page);

    /**
     * @param int $itemPerPage
     *
     * @return GetPaginatedStudentsRequestBuilder
     */
    public function withItemsPerPage($itemPerPage);

    /**
     * @return GetPaginatedStudentsRequest
     */
    public function build();
}
