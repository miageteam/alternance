<?php

namespace BusinessRules\Gateways\Student;

use BusinessRules\Entities\Student\Formation;
use BusinessRules\Gateways\Student\Exceptions\FormationAlreadyExistException;
use BusinessRules\Gateways\Student\Exceptions\FormationNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FormationGateway
{
    /**
     * @param Formation $formation
     *
     * @throws FormationAlreadyExistException
     */
    public function insert(Formation $formation);

    /**
     * @param int $id
     *
     * @return Formation
     *
     * @throws FormationNotFoundException
     */
    public function find($id);

    /**
     * @return Formation[]
     */
    public function findAll();

    /**
     * @param Formation $formation
     */
    public function remove(Formation $formation);
}
