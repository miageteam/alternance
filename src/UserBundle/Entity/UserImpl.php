<?php

namespace UserBundle\Entity;

use BusinessRules\Entities\User\User;
use BusinessRules\Entities\User\UserRole;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\Security\Core\Util\SecureRandomInterface;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserImpl extends User implements UserInterface
{

    /**
     * @var SecureRandomInterface
     */
    private $secureRandom;

    public function __construct()
    {
        $this->secureRandom = new SecureRandom();
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->getLoginIdentifier();
    }

    /**
     * {@inheritdoc}
     */
    protected function generateSalt()
    {
        return base64_encode($this->secureRandom->nextBytes(10))
        . '%s'
        . base64_encode($this->secureRandom->nextBytes(10));
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return array_map(
            function (UserRole $userRole) {
                return $userRole->getRole();
            },
            $this->group->getUserRoles()->toArray()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        return null;
    }
}