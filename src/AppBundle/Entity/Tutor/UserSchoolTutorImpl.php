<?php

namespace AppBundle\Entity\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserSchoolTutorImpl extends UserSchoolTutor
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUser()->getCompleteName();
    }

}
