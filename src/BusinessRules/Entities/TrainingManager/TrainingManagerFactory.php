<?php

namespace BusinessRules\Entities\TrainingManager;

use BusinessRules\Requestors\TrainingManager\CreateTrainingManagerRequest;
use BusinessRules\Requestors\TrainingManager\EditTrainingManagerRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface TrainingManagerFactory
{
    /**
     * @param CreateTrainingManagerRequest $request
     *
     * @return TrainingManager
     */
    public function createFromRequest(CreateTrainingManagerRequest $request);

    /**
     * @param TrainingManager        $trainingManager
     * @param EditTrainingManagerRequest $request
     *
     * @return TrainingManager
     */
    public function editFromRequest(TrainingManager $trainingManager, EditTrainingManagerRequest $request);
}
