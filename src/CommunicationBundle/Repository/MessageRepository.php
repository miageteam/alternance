<?php

namespace CommunicationBundle\Repository;

use BusinessRules\Entities\Communication\Message;
use BusinessRules\Gateways\Communication\Exceptions\MessageNotFoundException;
use BusinessRules\Gateways\Communication\MessageGateway;
use Doctrine\ORM\EntityRepository;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MessageRepository extends EntityRepository implements MessageGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(Message $message)
    {
        $this->_em->persist($message);
    }

    /**
     * {@inheritdoc}
     */
    public function update(Message $message)
    {
        //nothing
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $message = parent::find($id);
        if (empty($message)) {
            throw new MessageNotFoundException();
        }

        return $message;
    }
}
