<?php

namespace AppBundle\Form\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PromotionModel
{

    /**
     * @var string
     */
    public $year;

    /**
     * @var int
     */
    public $formation;

    /**
     * @var UploadedFile
     *
     * @Assert\File()
     */
    public $alternationCalendar;

    /**
     * @var string
     */
    public $alternationCalendarPath;

    /**
     * @var string
     */
    private $name;

    public function getAbsolutePath()
    {
        return null === $this->alternationCalendarPath ? null : $this->getUploadRootDir() . '/' . $this->name;
    }

    public function getWebPath()
    {
        return null === $this->alternationCalendarPath ? null : $this->getUploadDir() . '/' . $this->name;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/promotion/alternationCalendar';
    }

    public function upload()
    {
        if (null === $this->alternationCalendar) {
            return;
        }

        $this->name = sha1(uniqid(mt_rand(), true)) . '.' . $this->alternationCalendar->guessExtension();

        $this->alternationCalendar->move($this->getUploadRootDir(), $this->name);

        $this->alternationCalendarPath = $this->getUploadDir() . '/' . $this->name;

        $this->alternationCalendar = null;
    }
}
