<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Entities\Report\ReportFilter;
use BusinessRules\Gateways\Report\ReportGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Report\GetPaginatedReportsRequest;
use BusinessRules\Responders\Report\ReportResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\AbstractPaginatedUseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedReports implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var ReportGateway
     */
    private $reportGateway;

    /**
     * @var ReportResponseFactory
     */
    private $reportResponseFactory;

    /**
     * @param GetPaginatedReportsRequest $request
     *
     * @return AbstractPaginatedUseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $reports = $this->reportGateway->findAllPaginated(
            $this->getFilters($request),
            $this->getSorts($request),
            $this->getPagination($request)
        );

        return $this->reportResponseFactory->createFromPaginatedCollection($reports);
    }

    /**
     * @param GetPaginatedReportsRequest $useCaseRequest
     *
     * @return array
     */
    private function getFilters(GetPaginatedReportsRequest $useCaseRequest)
    {
        $filters = array();

        if (null !== $useCaseRequest->getUserId()) {
            $user                        = $this->userGateway->find($useCaseRequest->getUserId());
            $filters[ReportFilter::USER] = $user;
        }

        if (null !== $useCaseRequest->getIsValidated()) {
            $filters[ReportFilter::VALIDATED] = $useCaseRequest->getIsValidated();
        }

        return $filters;
    }

    /**
     * @param GetPaginatedReportsRequest $useCaseRequest
     *
     * @return array
     */
    private function getSorts(GetPaginatedReportsRequest $useCaseRequest)
    {
        return $useCaseRequest->getSorts();
    }

    /**
     * @param GetPaginatedReportsRequest $useCaseRequest
     *
     * @return array
     */
    private function getPagination(GetPaginatedReportsRequest $useCaseRequest)
    {
        return array(
            PaginatedCollection::PAGE           => $useCaseRequest->getPage(),
            PaginatedCollection::ITEMS_PER_PAGE => $useCaseRequest->getItemsPerPage(),
        );
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param ReportGateway $reportGateway
     */
    public function setReportGateway(ReportGateway $reportGateway)
    {
        $this->reportGateway = $reportGateway;
    }

    /**
     * @param ReportResponseFactory $reportResponseFactory
     */
    public function setReportResponseFactory(ReportResponseFactory $reportResponseFactory)
    {
        $this->reportResponseFactory = $reportResponseFactory;
    }
}
