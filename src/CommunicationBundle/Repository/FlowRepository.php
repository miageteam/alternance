<?php

namespace CommunicationBundle\Repository;

use AppBundle\Service\Pagination\PaginatedCollectionFactory;
use BusinessRules\Entities\Communication\Flow;
use BusinessRules\Entities\Communication\FlowFilter;
use BusinessRules\Entities\Communication\FlowSort;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Communication\Exceptions\FlowNotFoundException;
use BusinessRules\Gateways\Communication\FlowGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowRepository extends EntityRepository implements FlowGateway
{
    /**
     * @var PaginatedCollectionFactory
     */
    private $paginatedCollectionFactory;

    /**
     * @param PaginatedCollectionFactory $paginatedCollectionFactory
     */
    public function setPaginatedCollectionFactory(PaginatedCollectionFactory $paginatedCollectionFactory)
    {
        $this->paginatedCollectionFactory = $paginatedCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(Flow $flow)
    {
        $this->_em->persist($flow);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $flow = parent::find($id);
        if (empty($flow)) {
            throw new FlowNotFoundException();
        }

        return $flow;
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        $queryBuilder = $this->createQueryBuilder('f')
            ->addSelect('m')
            ->leftJoin('f.participants', 'p')
            ->leftJoin('f.messages', 'm');

        $filteredQueryBuilder       = $this->buildFilters($queryBuilder, $filters);
        $sortedFilteredQueryBuilder = $this->buildSorts($filteredQueryBuilder, $sorts);

        return $this->paginatedCollectionFactory->make($sortedFilteredQueryBuilder, $pagination);
    }

    /**
     * @return QueryBuilder
     */
    private function buildFilters(QueryBuilder $queryBuilder, array $filters = array())
    {
        if (isset($filters[FlowFilter::USER])) {
            $queryBuilder->andWhere('p = :user');
            $queryBuilder->setParameter('user', $filters[FlowFilter::USER]);
        }

        return $queryBuilder;
    }

    /**
     * @return QueryBuilder
     */
    private function buildSorts(QueryBuilder $queryBuilder, array $sorts = array())
    {
        if (isset($sorts[FlowSort::CREATED_AT])) {
            $queryBuilder->addOrderBy('f.' . FlowSort::CREATED_AT, $sorts[FlowSort::CREATED_AT]);
        }

        return $queryBuilder;
    }
}
