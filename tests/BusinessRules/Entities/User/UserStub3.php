<?php

namespace BusinessRules\Entities\User;

use UserBundle\Entity\UserImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserStub3 extends UserImpl
{
    const ID = 3;

    const FIRSTNAME = 'David';

    const LASTNAME = 'Dieu';

    const LOGIN_IDENTIFIER = 'daviddieu';

    const EMAIL = 'david.dieu@mail.com';

    public function __construct()
    {
        $this->group = new GroupStub1();
    }

    protected $id = self::ID;

    protected $firstName = self::FIRSTNAME;

    protected $lastName = self::LASTNAME;

    protected $loginIdentifier = self::LOGIN_IDENTIFIER;

    protected $email = self::EMAIL;

}
