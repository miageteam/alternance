<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\GetPaginatedStudentsRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedStudentsRequestBuilderImpl implements GetPaginatedStudentsRequestBuilder
{

    /**
     * @var GetPaginatedStudentsRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetPaginatedStudentsRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPromotionId($promotionId)
    {
        $this->request->promotionId = $promotionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withSorts(array $sorts)
    {
        $this->request->sorts = $sorts;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPage($page)
    {
        $this->request->page = $page;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withItemsPerPage($itemPerPage)
    {
        $this->request->itemsPerPage = $itemPerPage;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
