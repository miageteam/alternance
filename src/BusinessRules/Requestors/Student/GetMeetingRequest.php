<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetMeetingRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();
}
