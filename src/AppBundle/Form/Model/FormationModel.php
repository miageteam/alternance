<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationModel
{

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $name;

}
