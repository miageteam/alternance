<?php

namespace BusinessRules\Requestors\User;

use BusinessRules\Requestors\User\Exceptions\InvalidGetGroupRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetGroupRequestBuilder
{
    /**
     * @return GetGroupRequestBuilder
     */
    public function create();

    /**
     * @param int $id
     *
     * @return GetGroupRequestBuilder
     */
    public function byId($id);

    /**
     * @param string $name
     *
     * @return GetGroupRequestBuilder
     */
    public function byName($name);

    /**
     * @return GetGroupRequest
     *
     * @throws InvalidGetGroupRequestParameterException
     */
    public function build();
}
