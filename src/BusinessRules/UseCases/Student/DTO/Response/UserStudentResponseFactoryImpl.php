<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Entities\Student\UserStudent;
use BusinessRules\Responders\Student\UserStudentResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserStudentResponseFactoryImpl implements UserStudentResponseFactory
{

    /**
     * @var PaginatedUseCaseResponseBuilder
     */
    private $paginatedUseCaseResponseBuilder;

    /**
     * {@inheritdoc}
     */
    public function createFromUserStudent(UserStudent $userStudent)
    {
        $response                   = new UserStudentResponseDTO();
        $response->id               = $userStudent->getId();
        $response->user             = $userStudent->getUser();
        $response->promotion        = $userStudent->getPromotion();
        $response->userSchoolTutor  = $userStudent->getUserSchoolTutor();
        $response->userCompanyTutor = $userStudent->getUserCompanyTutor();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromUserStudents($userStudents)
    {
        $userStudentsResponses = array();
        foreach ($userStudents as $userStudent) {
            $userStudentsResponses[] = $this->createFromUserStudent($userStudent);
        }

        return $userStudentsResponses;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromPaginatedCollection(PaginatedCollection $paginatedCollection)
    {
        return $this->paginatedUseCaseResponseBuilder
            ->create()
            ->withItems($this->createFromUserStudents($paginatedCollection->getItems()))
            ->withItemsPerPage($paginatedCollection->getItemsPerPage())
            ->withPage($paginatedCollection->getPage())
            ->withTotalItems($paginatedCollection->getTotalItems())
            ->build();
    }

    /**
     * @param PaginatedUseCaseResponseBuilder $paginatedUseCaseResponseBuilder
     */
    public function setPaginatedUseCaseResponseBuilder(PaginatedUseCaseResponseBuilder $paginatedUseCaseResponseBuilder)
    {
        $this->paginatedUseCaseResponseBuilder = $paginatedUseCaseResponseBuilder;
    }
}
