<?php

namespace BusinessRules\Responders\TrainingManager;

use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface TrainingManagersResponse extends UseCaseResponse
{
    /**
     * @return TrainingManagerResponse[]
     */
    public function getTrainingManagers();
}
