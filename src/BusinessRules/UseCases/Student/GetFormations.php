<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\FormationGateway;
use BusinessRules\Requestors\Student\GetFormationsRequest;
use BusinessRules\Responders\Student\FormationResponseFactory;
use BusinessRules\Responders\Student\FormationsResponse;
use BusinessRules\UseCases\Student\DTO\Response\FormationsResponseDTO;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetFormations implements UseCase
{

    /**
     * @var FormationGateway
     */
    private $formationGateway;

    /**
     * @var FormationResponseFactory
     */
    private $formationResponseFactory;

    /**
     * @param GetFormationsRequest $request
     *
     * @return FormationsResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $formations = $this->formationGateway->findAll();

        return new FormationsResponseDTO($this->formationResponseFactory->createFromFormations($formations));
    }

    /**
     * @param FormationGateway $formationGateway
     */
    public function setFormationGateway(FormationGateway $formationGateway)
    {
        $this->formationGateway = $formationGateway;
    }

    /**
     * @param FormationResponseFactory $formationResponseFactory
     */
    public function setFormationResponseFactory(FormationResponseFactory $formationResponseFactory)
    {
        $this->formationResponseFactory = $formationResponseFactory;
    }
}
