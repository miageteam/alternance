<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreatePromotionRequest extends UseCaseRequest
{
    /**
     * @return string
     */
    public function getYear();

    /**
     * @return int
     */
    public function getFormationId();

    /**
     * @return string
     */
    public function getAlternationCalendarPath();
}
