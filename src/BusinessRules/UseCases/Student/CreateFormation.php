<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\FormationFactory;
use BusinessRules\Gateways\Student\FormationGateway;
use BusinessRules\Requestors\Student\CreateFormationRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateFormation implements UseCase
{

    /**
     * @var FormationGateway
     */
    private $formationGateway;

    /**
     * @var FormationFactory
     */
    private $formationFactory;

    /**
     * @Transaction
     *
     * @param CreateFormationRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $formation = $this->formationFactory->createFromRequest($request);
        $this->formationGateway->insert($formation);
    }

    /**
     * @param FormationGateway $formationGateway
     */
    public function setFormationGateway(FormationGateway $formationGateway)
    {
        $this->formationGateway = $formationGateway;
    }

    /**
     * @param FormationFactory $formationFactory
     */
    public function setFormationFactory(FormationFactory $formationFactory)
    {
        $this->formationFactory = $formationFactory;
    }
}
