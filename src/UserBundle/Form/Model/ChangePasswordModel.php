<?php

namespace UserBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use UserBundle\Validator\Constraints as MyAssert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ChangePasswordModel
{

    /**
     * @var String
     *
     * @Assert\NotBlank()
     * @MyAssert\CheckPassword()
     */
    public $password;

    /**
     * @var String
     *
     * @Assert\NotBlank()
     */
    public $newPassword;

    /**
     * @var String
     *
     * @Assert\NotBlank()
     */
    public $confirmNewPassword;

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->newPassword !== $this->confirmNewPassword) {
            $context->addViolationAt('confirmNewPassword', 'user.password_not_identical');
        }
    }

    /**
     * @return String
     */
    public function getPassword()
    {
        return $this->password;
    }
}
