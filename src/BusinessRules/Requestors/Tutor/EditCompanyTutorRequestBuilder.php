<?php

namespace BusinessRules\Requestors\Tutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface EditCompanyTutorRequestBuilder
{
    /**
     * @return EditCompanyTutorRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return EditCompanyTutorRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param string $companyName
     *
     * @return EditCompanyTutorRequestBuilder
     */
    public function withCompanyName($companyName);

    /**
     * @param string $street
     *
     * @return EditCompanyTutorRequestBuilder
     */
    public function withStreet($street);

    /**
     * @param string $zipCode
     *
     * @return EditCompanyTutorRequestBuilder
     */
    public function withZipCode($zipCode);

    /**
     * @param string $city
     *
     * @return EditCompanyTutorRequestBuilder
     */
    public function withCity($city);

    /**
     * @param string $country
     *
     * @return EditCompanyTutorRequestBuilder
     */
    public function withCountry($country);

    /**
     * @return EditCompanyTutorRequest
     */
    public function build();
}
