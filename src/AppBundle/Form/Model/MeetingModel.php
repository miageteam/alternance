<?php

namespace AppBundle\Form\Model;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MeetingModel
{

    /**
     * @var DateTime
     *
     * @Assert\NotBlank()
     */
    public $plannedAt;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $place;

}
