<?php

namespace CommunicationBundle\Controller;

use BusinessRules\Entities\Communication\FlowSort;
use BusinessRules\Entities\User\User;
use BusinessRules\UseCases\Communication\DTO\Request\GetPaginatedFlowsRequestDTO;
use BusinessRules\UseCases\Communication\DTO\Request\ReadFlowRequestDTO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowController extends Controller
{
    public function listAction(Request $request)
    {
        $response = $this->get('use_cases.communication.get_paginated_flows')
            ->execute($this->getPaginatedFlowsRequest($request));

        $pagination = array(
            'page'         => $response->getPage(),
            'route'        => 'communication_bundle_list_flow',
            'pages_count'  => $response->getTotalPages(),
            'route_params' => array()
        );

        return $this->render('CommunicationBundle:Flow:list.html.twig', array(
            'flows' => $response->getItems(),
            'pagination' => $pagination
        ));
    }

    /**
     * @param Request $request
     *
     * @return GetPaginatedFlowsRequestDTO
     */
    private function getPaginatedFlowsRequest(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->get('request.communication.get_paginated_flows_request_builder')
            ->create()
            ->withUserId($user->getId())
            ->withPage($request->get('page'))
            ->withSorts(array(FlowSort::CREATED_AT => FlowSort::DESC))
            ->build();
    }

    public function readAction($id)
    {
        $flow = $this->get('use_cases.communication.read_flow')->execute(new ReadFlowRequestDTO($id));

        return $this->render('CommunicationBundle:Flow:read.html.twig', array('flow' => $flow));
    }
}
