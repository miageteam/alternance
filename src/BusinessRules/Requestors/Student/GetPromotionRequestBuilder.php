<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 15/06/2015
 * Time: 11:17
 */

namespace BusinessRules\Requestors\Student;


interface GetPromotionRequestBuilder
{

    /**
     * @return GetPromotionRequestBuilder
     */
    public function create();

    /**
     * @param int $id
     *
     * @return GetPromotionRequestBuilder
     */
    public function byId($id);

    /**
     * @return GetPromotionRequest
     */
    public function build();
}
