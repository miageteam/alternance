<?php

namespace AppBundle\Repository\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Gateways\Tutor\Exceptions\UserCompanyTutorNotFoundException;
use BusinessRules\Gateways\Tutor\UserCompanyTutorGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserCompanyTutorRepository extends EntityRepository implements UserCompanyTutorGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(UserCompanyTutor $userCompanyTutor)
    {
        $this->_em->persist($userCompanyTutor);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        try {
            return $this->createQueryBuilder('uct')
                ->addSelect('u')
                ->leftJoin('uct.user', 'u')
                ->andWhere('uct.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new UserCompanyTutorNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        try {
            return $this->createQueryBuilder('uct')
                ->addSelect('u')
                ->leftJoin('uct.user', 'u')
                ->andWhere('u.id = :id')
                ->setParameter('id', $userId)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new UserCompanyTutorNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function update(UserCompanyTutor $userCompanyTutor)
    {
        // do nothing
    }
}
