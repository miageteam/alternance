<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\PromotionFactory;
use BusinessRules\Gateways\Student\PromotionGateway;
use BusinessRules\Requestors\Student\EditPromotionRequest;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class EditPromotion implements UseCase
{

    /**
     * @var PromotionGateway
     */
    private $promotionGateway;

    /**
     * @var PromotionFactory
     */
    private $promotionFactory;

    /**
     * @Transaction
     *
     * @param EditPromotionRequest $useCaseRequest
     */
    public function execute(UseCaseRequest $useCaseRequest)
    {
        $promotion = $this->promotionGateway->find($useCaseRequest->getPromotionId());

        $updatedPromotion = $this->promotionFactory->editFromRequest($promotion, $useCaseRequest);

        $this->promotionGateway->update($updatedPromotion);
    }

    /**
     * @param PromotionGateway $promotionGateway
     */
    public function setPromotionGateway(PromotionGateway $promotionGateway)
    {
        $this->promotionGateway = $promotionGateway;
    }

    /**
     * @param PromotionFactory $promotionFactory
     */
    public function setPromotionFactory(PromotionFactory $promotionFactory)
    {
        $this->promotionFactory = $promotionFactory;
    }



}
