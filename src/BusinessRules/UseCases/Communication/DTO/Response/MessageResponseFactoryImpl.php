<?php

namespace BusinessRules\UseCases\Communication\DTO\Response;

use BusinessRules\Entities\Communication\Message;
use BusinessRules\Responders\Communication\MessageResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MessageResponseFactoryImpl implements MessageResponseFactory
{
    /**
     * {@inheritdoc}
     */
    public function createFromMessage(Message $message)
    {
        $response              = new MessageResponseDTO();
        $response->id          = $message->getId();
        $response->content     = $message->getContent();
        $response->author      = $message->getAuthor();
        $response->attachments = $message->getAttachments();
        $response->createdAt   = $message->getCreatedAt();
        $response->updatedAt   = $message->getUpdatedAt();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromMessages($messages)
    {
        $messagesResponse = array();
        foreach ($messages as $message) {
            $messagesResponse[] = $this->createFromMessage($message);
        }

        return $messagesResponse;
    }
}
