<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Request;

use BusinessRules\Requestors\TrainingManager\CreateTrainingManagerRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateTrainingManagerRequestBuilderImpl implements CreateTrainingManagerRequestBuilder
{

    /**
     * @var CreateTrainingManagerRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new CreateTrainingManagerRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPromotionId($promotionId)
    {
        $this->request->promotionId = $promotionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
