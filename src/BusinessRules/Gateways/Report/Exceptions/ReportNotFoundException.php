<?php

namespace BusinessRules\Gateways\Report\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReportNotFoundException extends \Exception
{

}
