<?php

namespace BusinessRules\Requestors\Report;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetPaginatedReportsRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return bool
     */
    public function getIsValidated();

    /**
     * @return string[]
     */
    public function getSorts();

    /**
     * @return int
     */
    public function getPage();

    /**
     * @return int
     */
    public function getItemsPerPage();
}
