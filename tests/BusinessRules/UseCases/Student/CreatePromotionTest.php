<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\FormationStub1;
use BusinessRules\Entities\Student\PromotionStub1;
use BusinessRules\Gateways\Student\InMemoryFormationGateway;
use BusinessRules\Gateways\Student\InMemoryPromotionGateway;
use BusinessRules\Requestors\Student\CreatePromotionRequestBuilder;
use BusinessRules\UseCases\Student\DTO\Request\CreatePromotionRequestBuilderImpl;
use AppBundle\Entity\Student\PromotionFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreatePromotionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CreatePromotion
     */
    private $useCase;

    /**
     * @var CreatePromotionRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\Student\Exceptions\PromotionAlreadyExistException
     */
    public function alreadyExist_ThrowException()
    {
        InMemoryPromotionGateway::$promotions = array(PromotionStub1::ID => new PromotionStub1());
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withYear(PromotionStub1::YEAR)
                ->withFormationId(FormationStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     */
    public function createPromotion()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withYear(PromotionStub1::YEAR)
                ->withFormationId(FormationStub1::ID)
                ->build()
        );

        $this->assertCount(1, InMemoryPromotionGateway::$promotions);
        foreach (InMemoryPromotionGateway::$promotions as $promotion) {
            $this->assertEquals(PromotionStub1::YEAR, $promotion->getYear());
            $this->assertEquals(FormationStub1::ID, $promotion->getFormationId());
        }
    }

    protected function setUp()
    {
        $this->useCase    = new CreatePromotion();
        $promotionFactory = new PromotionFactoryImpl();
        $promotionFactory->setFormationGateway(
            new InMemoryFormationGateway(array(FormationStub1::ID => new FormationStub1()))
        );
        $this->useCase->setPromotionFactory($promotionFactory);
        $this->useCase->setPromotionGateway(new InMemoryPromotionGateway());
        $this->requestBuilder = new CreatePromotionRequestBuilderImpl();
    }
}
