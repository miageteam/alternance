<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Entities\Communication\AttachmentStub1;
use BusinessRules\Entities\Communication\FlowStub1;
use BusinessRules\Entities\Communication\MessageStub1;
use BusinessRules\Entities\User\UserStub1;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteResponseRequestBuilderImplTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteResponseRequestBuilderImpl
     */
    private $builder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Communication\Exceptions\InvalidWriteResponseRequestParameterException
     * @expectedExceptionMessage The argument "flowId" must be defined.
     */
    public function WithoutFlowId_ThrowException()
    {
        $this->builder
            ->create()
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Communication\Exceptions\InvalidWriteResponseRequestParameterException
     * @expectedExceptionMessage The argument "Content" must be defined.
     */
    public function WithoutContent_ThrowException()
    {
        $this->builder
            ->create()
            ->withFlowId(FlowStub1::ID)
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Communication\Exceptions\InvalidWriteResponseRequestParameterException
     * @expectedExceptionMessage The argument "AuthorId" must be defined.
     */
    public function WithoutAuthorId_ThrowException()
    {
        $this->builder
            ->create()
            ->withFlowId(FlowStub1::ID)
            ->withContent(MessageStub1::CONTENT)
            ->build();
    }

    /**
     * @test
     */
    public function BuildStandard()
    {
        $this->builder
            ->create()
            ->withFlowId(FlowStub1::ID)
            ->withContent(MessageStub1::CONTENT)
            ->withAuthorId(UserStub1::ID)
            ->build();
    }

    /**
     * @test
     */
    public function BuildWithAttachment()
    {
        $this->builder
            ->create()
            ->withFlowId(FlowStub1::ID)
            ->withContent(MessageStub1::CONTENT)
            ->withAuthorId(UserStub1::ID)
            ->withAttachmentPath(AttachmentStub1::PATH)
            ->build();
    }

    protected function setUp()
    {
        $this->builder = new WriteResponseRequestBuilderImpl();
    }
}
