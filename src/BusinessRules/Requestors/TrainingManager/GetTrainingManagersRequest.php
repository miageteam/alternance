<?php

namespace BusinessRules\Requestors\TrainingManager;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetTrainingManagersRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getPromotionId();
}
