<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Gateways\Report\CommentGateway;
use BusinessRules\Gateways\Report\ReportGateway;
use BusinessRules\Requestors\Report\GetReportRequest;
use BusinessRules\Responders\Report\ReportResponse;
use BusinessRules\Responders\Report\ReportResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetReport implements UseCase
{

    /**
     * @var ReportGateway
     */
    private $reportGateway;

    /**
     * @var ReportResponseFactory
     */
    private $reportResponseFactory;

    /**
     * @param GetReportRequest $request
     *
     * @return ReportResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $report = $this->reportGateway->find($request->getId());

        return $this->reportResponseFactory->createFromReport($report);
    }

    /**
     * @param ReportGateway $reportGateway
     */
    public function setReportGateway(ReportGateway $reportGateway)
    {
        $this->reportGateway = $reportGateway;
    }

    /**
     * @param ReportResponseFactory $reportResponseFactory
     */
    public function setReportResponseFactory(ReportResponseFactory $reportResponseFactory)
    {
        $this->reportResponseFactory = $reportResponseFactory;
    }
}
