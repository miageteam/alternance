<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\Exceptions\InvalidWriteReportRequestParameterException;
use BusinessRules\Requestors\Report\WriteReportRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteReportRequestBuilderImpl implements WriteReportRequestBuilder
{

    /**
     * @var WriteReportRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new WriteReportRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withTitle($title)
    {
        $this->request->title = $title;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAuthorId($authorId)
    {
        $this->request->authorId = $authorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAttachmentPath($attachmentPath)
    {
        $this->request->attachmentPath = $attachmentPath;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $this->CheckParametersAreDefined();

        return $this->request;
    }

    private function CheckParametersAreDefined()
    {
        if (empty($this->request->title)) {
            throw new InvalidWriteReportRequestParameterException('The argument "Title" must be defined.');
        }

        if (empty($this->request->authorId)) {
            throw new InvalidWriteReportRequestParameterException('The argument "AuthorId" must be defined.');
        }
    }
}
