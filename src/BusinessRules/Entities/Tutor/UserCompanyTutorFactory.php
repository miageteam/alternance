<?php

namespace BusinessRules\Entities\Tutor;

use BusinessRules\Requestors\Tutor\CreateCompanyTutorRequest;
use BusinessRules\Requestors\Tutor\EditCompanyTutorRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserCompanyTutorFactory
{
    /**
     * @param CreateCompanyTutorRequest $request
     *
     * @return UserCompanyTutor
     */
    public function createFromRequest(CreateCompanyTutorRequest $request);

    /**
     * @param EditCompanyTutorRequest $request
     *
     * @return UserCompanyTutor
     */
    public function editFromRequest(UserCompanyTutor $userCompanyTutor, EditCompanyTutorRequest $request);
}
