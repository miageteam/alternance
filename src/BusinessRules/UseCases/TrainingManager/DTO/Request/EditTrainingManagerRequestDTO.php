<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Request;

use BusinessRules\Requestors\TrainingManager\EditTrainingManagerRequest;

/**
* @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
*/

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class EditTrainingManagerRequestDTO implements EditTrainingManagerRequest
{

    /**
     * @return int
     */
    public $userId;

    /**
     * @return int
     */
    public $promotionId;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }
}
