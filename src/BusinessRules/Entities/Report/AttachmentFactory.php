<?php

namespace BusinessRules\Entities\Report;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface AttachmentFactory
{
    /**
     * @param string $attachmentPath
     *
     * @return Attachment
     */
    public function create($attachmentPath);
}
