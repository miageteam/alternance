<?php

namespace BusinessRules\Utils;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class Pagination
{
    const PAGE = 1;

    const ITEMS_PER_PAGE = 20;

    /** @codeCoverageIgnore */
    public function __construct(){}
}
