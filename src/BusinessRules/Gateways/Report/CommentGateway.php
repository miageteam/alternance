<?php

namespace BusinessRules\Gateways\Report;

use BusinessRules\Entities\Report\Comment;
use BusinessRules\Gateways\Report\Exceptions\CommentNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CommentGateway
{
    /**
     * @param Comment $comment
     */
    public function insert(Comment $comment);

    /**
     * @param int $id
     *
     * @return Comment
     *
     * @throws CommentNotFoundException
     */
    public function find($id);

    /**
     * @param Comment $comment
     */
    public function remove(Comment $comment);

    /**
     * @param Comment $comment
     */
    public function update(Comment $comment);
}
