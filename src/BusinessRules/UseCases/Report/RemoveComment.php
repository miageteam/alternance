<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Gateways\Report\CommentGateway;
use BusinessRules\Requestors\Student\RemoveCommentRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class RemoveComment implements UseCase
{

    /**
     * @var CommentGateway
     */
    private $commentGateway;

    /**
     * @Transaction
     *
     * @param RemoveCommentRequest $request
     *
     * @return UseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $comment = $this->commentGateway->find($request->getCommentId());

        $this->commentGateway->remove($comment);
    }

    /**
     * @param CommentGateway $commentGateway
     */
    public function setCommentGateway(CommentGateway $commentGateway)
    {
        $this->commentGateway = $commentGateway;
    }
}
