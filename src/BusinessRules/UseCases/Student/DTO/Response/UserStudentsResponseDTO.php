<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 11/06/2015
 * Time: 01:39
 */

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Responders\Student\UserStudentResponse;
use BusinessRules\Responders\Student\UserStudentsResponse;

class UserStudentsResponseDTO implements UserStudentsResponse
{

    /**
     * @var UserStudentResponse[]
     */
    public $userStudents;

    /**
     * @param UserStudentResponse[] $userStudents
     */
    public function __construct($userStudents)
    {
        $this->userStudents = $userStudents;
    }

    /**
     * @return UserStudentResponse[]
     */
    public function getUserStudents()
    {
        return $this->userStudents;
    }
}
