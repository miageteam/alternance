<?php

namespace BusinessRules\UseCases\Tutor\DTO\Response;

use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Responders\Tutor\UserCompanyTutorResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserCompanyTutorResponseFactoryImpl implements UserCompanyTutorResponseFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromUserCompanyTutor(UserCompanyTutor $userCompanyTutor)
    {
        $response              = new UserCompanyTutorResponseDTO();
        $response->id          = $userCompanyTutor->getId();
        $response->user        = $userCompanyTutor->getUser();
        $response->companyName = $userCompanyTutor->getCompanyName();
        $response->street      = $userCompanyTutor->getStreet();
        $response->zipCode     = $userCompanyTutor->getZipCode();
        $response->city        = $userCompanyTutor->getCity();
        $response->country     = $userCompanyTutor->getCountry();

        return $response;
    }
}
