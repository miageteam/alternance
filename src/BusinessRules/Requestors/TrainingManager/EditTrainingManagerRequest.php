<?php

namespace BusinessRules\Requestors\TrainingManager;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
interface EditTrainingManagerRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return int
     */
    public function getPromotionId();
}
