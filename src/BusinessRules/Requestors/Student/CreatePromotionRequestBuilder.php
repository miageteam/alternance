<?php

namespace BusinessRules\Requestors\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreatePromotionRequestBuilder
{
    /**
     * @return CreatePromotionRequestBuilder
     */
    public function create();

    /**
     * @param string $year
     *
     * @return CreatePromotionRequestBuilder
     */
    public function withYear($year);

    /**
     * @param int $formationId
     *
     * @return CreatePromotionRequestBuilder
     */
    public function withFormationId($formationId);

    /**
     * @param string $alternationCalendarPath
     *
     * @return CreatePromotionRequestBuilder
     */
    public function withAlternationCalendarPath($alternationCalendarPath);

    /**
     * @return CreatePromotionRequest
     */
    public function build();
}
