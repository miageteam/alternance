<?php

namespace BusinessRules\Gateways\Student;

use BusinessRules\Entities\Student\UserStudent;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryUserStudentGateway implements UserStudentGateway
{

    /**
     * @var UserStudent[]
     */
    public static $userStudents = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @param UserStudent[] $userStudents
     */
    public function __construct(array $userStudents = array())
    {
        self::$userStudents = $userStudents;
        self::$id           = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(UserStudent $userStudent)
    {
        self::$userStudents[++self::$id] = $userStudent;
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        // Later: Implement this method.
    }

    /**
     * {@inheritdoc}
     */
    public function findByCompanyTutorId($CompanyTutorId)
    {
        // Later: Implement this method.
    }

    /**
     * {@inheritdoc}
     */
    public function findAllBySchoolTutor($schoolTutorId)
    {
        // Later: Implement this method.
    }

    /**
     * {@inheritdoc}
     */
    public function update(UserStudent $userStudent)
    {
        // Later: Implement this method.
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        // Later: Implement this method.
    }

    /**
     * @param int $promotionId
     *
     * @return UserStudent[]
     */
    public function findAllByPromotionId($promotionId)
    {
        // TODO: Implement this method.
    }
}
