<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\CreateUserRequestBuilder;
use BusinessRules\Requestors\User\Exceptions\InvalidCreateUserRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateUserRequestBuilderImpl implements CreateUserRequestBuilder
{

    /**
     * @var CreateUserRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new CreateUserRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withFirstName($firstName)
    {
        $this->request->firstName = $firstName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withLastName($lastName)
    {
        $this->request->lastName = $lastName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withEmail($email)
    {
        $this->request->email = $email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPhoneNumber($phoneNumber)
    {
        $this->request->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withGroupId($groupId)
    {
        $this->request->groupId = $groupId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $this->CheckParametersAreDefined();

        return $this->request;
    }

    private function CheckParametersAreDefined()
    {
        if (empty($this->request->email)) {
            throw new InvalidCreateUserRequestParameterException('The argument "Email" must be defined.');
        }
        if (empty($this->request->firstName)) {
            throw new InvalidCreateUserRequestParameterException('The argument "FirstName" must be defined.');
        }
        if (empty($this->request->lastName)) {
            throw new InvalidCreateUserRequestParameterException('The argument "LastName" must be defined.');
        }

    }
}
