<?php

namespace BusinessRules\Entities\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
abstract class Promotion
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $year;

    /**
     * @var Formation
     */
    protected $formation;

    /**
     * @var string
     */
    protected $alternationCalendarPath;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return Formation
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * @return int
     */
    public function getFormationId()
    {
        return $this->formation->getId();
    }

    /**
     * @return string
     */
    public function getFormationName()
    {
        return $this->formation->getName();
    }

    /**
     * @param Formation $formation
     */
    public function setFormation($formation)
    {
        $this->formation = $formation;
    }

    /**
     * @return string
     */
    public function getAlternationCalendarPath()
    {
        return $this->alternationCalendarPath;
    }

    /**
     * @param string $alternationCalendarPath
     */
    public function setAlternationCalendarPath($alternationCalendarPath)
    {
        $this->alternationCalendarPath = $alternationCalendarPath;
    }
}
