<?php

namespace BusinessRules\Gateways\Communication;

use BusinessRules\Entities\Communication\Flow;
use BusinessRules\Gateways\Communication\Exceptions\FlowNotFoundException;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FlowGateway
{
    /**
     * @param Flow $flow
     */
    public function insert(Flow $flow);

    /**
     * @param int $id
     *
     * @return Flow
     *
     * @throws FlowNotFoundException
     */
    public function find($id);

    /**
     * @param array $filters
     * @param array $sorts
     * @param array $pagination
     *
     * @return PaginatedCollection
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array());
}
