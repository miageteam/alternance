<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\GetMessageRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class GetMessageRequestDTO implements GetMessageRequest
{

    /**
     * @var int
     */
    public $id;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
}
