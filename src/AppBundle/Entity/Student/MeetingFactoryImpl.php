<?php

namespace AppBundle\Entity\Student;

use BusinessRules\Entities\Student\Meeting;
use BusinessRules\Entities\Student\MeetingFactory;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Student\CreateMeetingRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MeetingFactoryImpl implements MeetingFactory
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @param CreateMeetingRequest $request
     *
     * @return Meeting
     */
    public function createFromRequest(CreateMeetingRequest $request)
    {
        $participants = $this->userGateway->findByIds($request->getParticipantIds());

        $meeting = new MeetingImpl();
        $meeting->setPlannedAt($request->getPlannedAt());
        $meeting->setPlace($request->getPlace());
        $meeting->setParticipants($participants);

        return $meeting;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }
}
