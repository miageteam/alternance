<?php

namespace CommunicationBundle\Repository;

use BusinessRules\Entities\Communication\Attachment;
use BusinessRules\Gateways\Communication\AttachmentGateway;
use Doctrine\ORM\EntityRepository;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class AttachmentRepository extends EntityRepository implements AttachmentGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(Attachment $attachment)
    {
        $this->_em->persist($attachment);
    }
}
