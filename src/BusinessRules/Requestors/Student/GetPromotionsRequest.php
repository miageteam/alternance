<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author David Dieu <ddieu80@gmail.com>
 */
interface GetPromotionsRequest extends UseCaseRequest
{

}
