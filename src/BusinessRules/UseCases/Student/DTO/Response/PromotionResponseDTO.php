<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Entities\Student\Formation;
use BusinessRules\Responders\Student\PromotionResponse;

/**
 * @author Kevin Leconte <leconte.kevink@gmail.com>
 * @author David Dieu <daviddieu80@gmail.com>
 */
class PromotionResponseDTO implements PromotionResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var Formation
     */
    public $formation;

    /**
     * @var string
     */
    public $year;

    /**
     * @var String
     */
    public $alternationCalendarPath;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Formation
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * @return String
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return String
     */
    public function getAlternationCalendarPath()
    {
        return $this->alternationCalendarPath;
    }
}
