<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Entities\User\UserRole;
use BusinessRules\Requestors\User\CreateGroupRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateGroupRequestDTO implements CreateGroupRequest
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var UserRole[]
     */
    public $userRoles;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return UserRole[]
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }
}
