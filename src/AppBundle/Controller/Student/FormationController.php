<?php

namespace AppBundle\Controller\Student;

use AppBundle\Form\Model\FormationModel;
use AppBundle\Form\Type\CreateFormationType;
use BusinessRules\Responders\Student\FormationResponse;
use BusinessRules\UseCases\Student\DTO\Request\CreateFormationRequestDTO;
use BusinessRules\UseCases\Student\DTO\Request\GetFormationRequestDTO;
use BusinessRules\UseCases\Student\DTO\Request\GetFormationsRequestDTO;
use BusinessRules\UseCases\Student\DTO\Request\GetPromotionsRequestDTO;
use BusinessRules\UseCases\Student\DTO\Request\RemoveFormationRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationController extends Controller
{
    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(new CreateFormationType());

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var FormationModel $model */
            $model = $form->getData();
            $this->get('use_cases.student.create_formation')->execute(new CreateFormationRequestDTO($model->name));

            $this->addFlash('success', 'app.flash_message.create_formation_success');

            return $this->redirectToRoute('app_user_student_show_formations');
        }

        return $this->render('AppBundle:Student/Formation:create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function showFormationsAction()
    {
        $response = $this->get('use_cases.student.get_formations')->execute(new GetFormationsRequestDTO());

        return $this->render(
            'AppBundle:Student/Formation:list.html.twig',
            array('formations' => $response->getFormations())
        );
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function removeFormationAction($formationId)
    {
        $formation = $this->get('use_cases.student.get_formation')
            ->execute(new GetFormationRequestDTO($formationId));

        if ($this->isPermittedToRemove($formation)) {
            $this->get('use_cases.student.remove_formation')
                ->execute(new RemoveFormationRequestDTO($formationId));

            $this->addFlash('success', 'app.flash_message.formation_remove_success');
        }

        return $this->redirectToRoute('app_user_student_show_formations');
    }

    /**
     * @param FormationResponse $formation
     *
     * @return bool
     */
    private function isPermittedToRemove(FormationResponse $formation)
    {
        if ($this->isUsedByPromotion($formation)) {
            $this->addFlash('error', 'app.flash_message.remove_formation_fail_cause_is_used_by_promotion');

            return false;
        };

        return true;
    }

    /**
     * @param FormationResponse $formation
     *
     * @return bool
     */
    private function isUsedByPromotion(FormationResponse $formation)
    {
        $response = $this->get('use_cases.student.get_promotions')
            ->execute(new GetPromotionsRequestDTO());

        foreach ($response->getPromotions() as $promotion) {
            if ($promotion->getFormation()->getId() === $formation->getId()) {
                return true;
            }
        }

        return false;
    }
}
