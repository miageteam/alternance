<?php

namespace BusinessRules\Requestors\Report;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetReportRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getId();
}
