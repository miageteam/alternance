<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 11/06/2015
 * Time: 01:33
 */

namespace BusinessRules\Responders\Student;

use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

interface UserStudentsResponse extends UseCaseResponse
{
    /**
     * @return UserStudentResponse[]
     */
    public function getUserStudents();
}
