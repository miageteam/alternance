<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\GetPromotionsRequest;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class GetPromotionsRequestDTO implements GetPromotionsRequest
{

}
