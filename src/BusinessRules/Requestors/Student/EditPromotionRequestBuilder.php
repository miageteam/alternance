<?php

namespace BusinessRules\Requestors\Student;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
interface EditPromotionRequestBuilder
{

    /**
     * @return EditStudentRequestBuilder
     */
    public function create();

    /**
     * @param int $formationId
     *
     * @return EditPromotionRequestBuilder
     */
    public function withFormationId($formationId);

    /**
     * @param string $year
     *
     * @return EditPromotionRequestBuilder
     */
    public function withYear($year);

    /**
     * @param string $alternationCalendar
     *
     * @return EditPromotionRequestBuilder
     */
    public function withAlternationCalendarPath($alternationCalendar);

    /**
     * @return EditPromotionRequest
     */
    public function build();

}
