<?php

namespace BusinessRules\UseCases\TrainingManager;

use BusinessRules\Entities\TrainingManager\TrainingManagerFactory;
use BusinessRules\Gateways\TrainingManager\TrainingManagerGateway;
use BusinessRules\Requestors\TrainingManager\CreateTrainingManagerRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateTrainingManager implements UseCase
{

    /**
     * @var TrainingManagerFactory
     */
    private $trainingManagerFactory;

    /**
     * @var TrainingManagerGateway
     */
    private $trainingManagerGateway;

    /**
     * @Transaction
     *
     * @param CreateTrainingManagerRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $trainingManager = $this->trainingManagerFactory->createFromRequest($request);

        $this->trainingManagerGateway->insert($trainingManager);
    }

    /**
     * @param TrainingManagerFactory $trainingManagerFactory
     */
    public function setTrainingManagerFactory(TrainingManagerFactory $trainingManagerFactory)
    {
        $this->trainingManagerFactory = $trainingManagerFactory;
    }

    /**
     * @param TrainingManagerGateway $trainingManagerGateway
     */
    public function setTrainingManagerGateway(TrainingManagerGateway $trainingManagerGateway)
    {
        $this->trainingManagerGateway = $trainingManagerGateway;
    }
}
