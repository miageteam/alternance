<?php

namespace BusinessRules\Gateways\Communication;

use BusinessRules\Entities\Communication\Attachment;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryAttachmentGateway implements AttachmentGateway
{

    /**
     * @var Attachment[]
     */
    public static $attachments = array();

    /**
     * @var int
     */
    public static $id;

    /**
     * @param Attachment[] $attachments
     */
    public function __construct(array $attachments = array())
    {
        self::$attachments = $attachments;
        self::$id    = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(Attachment $attachment)
    {
        self::$attachments[++self::$id] = $attachment;
    }
}
