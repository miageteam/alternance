<?php

namespace BusinessRules\Requestors\Communication;

use BusinessRules\Requestors\Communication\Exceptions\InvalidWriteResponseRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteResponseRequestBuilder
{
    /**
     * @return WriteResponseRequestBuilder
     */
    public function create();

    /**
     * @param int $flowId
     *
     * @return WriteResponseRequestBuilder
     */
    public function withFlowId($flowId);

    /**
     * @param string $content
     *
     * @return WriteResponseRequestBuilder
     */
    public function withContent($content);

    /**
     * @param int $authorId
     *
     * @return WriteResponseRequestBuilder
     */
    public function withAuthorId($authorId);

    /**
     * @param string $attachmentPath
     *
     * @return WriteResponseRequestBuilder
     */
    public function withAttachmentPath($attachmentPath);

    /**
     * @return WriteResponseRequest
     *
     * @throws InvalidWriteResponseRequestParameterException
     */
    public function build();
}
