<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 08/06/2015
 * Time: 12:29
 */

namespace BusinessRules\Requestors\Student;

interface GetStudentsRequestBuilder
{

    /**
     * @return GetStudentsRequestBuilder
     */
    public function create();

    /**
     * @param int $schoolTutorId
     *
     * @return GetStudentsRequestBuilder
     */
    public function bySchoolTutorId($schoolTutorId);

    /**
     * @param int $promotionId
     *
     * @return GetStudentsRequestBuilder
     */
    public function byPromotionId($promotionId);

    /**
     * @return GetStudentsRequest
     */
    public function build();
}
