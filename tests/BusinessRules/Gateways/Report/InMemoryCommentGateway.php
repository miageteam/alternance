<?php

namespace BusinessRules\Gateways\Report;

use BusinessRules\Entities\Report\Comment;
use BusinessRules\Gateways\Report\Exceptions\CommentNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryCommentGateway implements CommentGateway
{

    /**
     * @var Comment[]
     */
    public static $comments = array();

    /**
     * @var int
     */
    public static $id;

    /**
     * @param Comment[] $comments
     */
    public function __construct(array $comments = array())
    {
        self::$comments = $comments;
        self::$id       = 0;
    }

    /**
     * @param Comment $comment
     */
    public function insert(Comment $comment)
    {
        self::$comments[++self::$id] = $comment;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$comments[$id])) {
            throw new CommentNotFoundException();
        }

        return self::$comments[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Comment $comment)
    {
        // TODO: Implement this method.
    }

    /**
     * {@inheritdoc}
     */
    public function update(Comment $comment)
    {
        // TODO: Implement this method.
    }
}
