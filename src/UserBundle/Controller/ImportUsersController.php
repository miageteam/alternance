<?php

namespace UserBundle\Controller;

use BusinessRules\Entities\User\GroupName;
use BusinessRules\Gateways\User\Exceptions\EmailAlreadyExistsException;
use BusinessRules\Models\CSVFile;
use BusinessRules\Models\ImportUser;
use BusinessRules\Requestors\User\CreateUserRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\Model\ImportUsersModel;
use UserBundle\Form\Type\ImportUsersType;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ImportUsersController extends Controller
{
    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function importAction(Request $request)
    {
        $form = $this->createForm(new ImportUsersType());

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var ImportUsersModel $model */
            $model = $form->getData();

            $model->upload();

            $csvFile = new CSVFile($model->getAbsolutePath());

            $notCreatedStudents = array();
            foreach ($csvFile->toArray() as $row) {

                try {
                    $user = $this->get('use_cases.user.create_user')->execute($this->getCreateUserRequest($row));

                    $user = $this->get('use_cases.user.get_user')->execute(
                        $this->get('request.user.get_user_request_builder')
                            ->create()
                            ->byEmail($user->getEmail())
                            ->build()
                    );
                    
                    $this->get('use_cases.student.create_student')->execute(
                        $this->get('request.student.create_student_request_builder')
                            ->create()
                            ->withUserId($user->getId())
                            ->withPromotionId($model->promotion)
                            ->build()
                    );
                } catch (EmailAlreadyExistsException $e) {
                    $notCreatedStudents[] = $this->get('entities.user.user_factory')
                        ->createFromRequest($this->getCreateUserRequest($row));
                }
            }

            $model->removeFile();

            return $this->render(
                'UserBundle:ImportUsers:not_created_students.html.twig',
                array('users' => $notCreatedStudents)
            );
        }

        return $this->render('UserBundle:ImportUsers:import.html.twig', array('form' => $form->createView(),));
    }

    /**
     * @param $row
     *
     * @return CreateUserRequest
     */
    private function getCreateUserRequest($row)
    {
        $group = $this->get('use_cases.user.get_group')
            ->execute(
                $this->get('request.user.get_group_request_builder')
                    ->create()
                    ->byName(GroupName::STUDENT)
                    ->build()
            );

        return $this->get('request.user.create_user_request_builder')
            ->create()
            ->withFirstName($row[ImportUser::FIRST_NAME])
            ->withLastName($row[ImportUser::LAST_NAME])
            ->withEmail(strtolower($row[ImportUser::EMAIL]))
            ->withPhoneNumber($row[ImportUser::PHONE_NUMBER])
            ->withGroupId($group->getId())
            ->build();
    }
}
