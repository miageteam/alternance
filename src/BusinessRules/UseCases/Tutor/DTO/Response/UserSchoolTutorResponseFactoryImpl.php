<?php

namespace BusinessRules\UseCases\Tutor\DTO\Response;

use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Responders\Tutor\UserSchoolTutorResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserSchoolTutorResponseFactoryImpl implements UserSchoolTutorResponseFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromUserSchoolTutor(UserSchoolTutor $userSchoolTutor)
    {
        $response              = new UserSchoolTutorResponseDTO();
        $response->id          = $userSchoolTutor->getId();
        $response->user        = $userSchoolTutor->getUser();
        $response->companyName = $userSchoolTutor->getCompanyName();
        $response->street      = $userSchoolTutor->getStreet();
        $response->zipCode     = $userSchoolTutor->getZipCode();
        $response->city        = $userSchoolTutor->getCity();
        $response->country     = $userSchoolTutor->getCountry();

        return $response;
    }
}
