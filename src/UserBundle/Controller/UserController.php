<?php

namespace UserBundle\Controller;

use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Student\Exceptions\UserStudentNotFoundException;
use BusinessRules\Gateways\TrainingManager\Exceptions\TrainingManagerNotFoundException;
use BusinessRules\Gateways\Tutor\Exceptions\UserCompanyTutorNotFoundException;
use BusinessRules\Gateways\Tutor\Exceptions\UserSchoolTutorNotFoundException;
use BusinessRules\Requestors\User\CreateUserRequest;
use BusinessRules\Requestors\User\GetPaginatedUsersRequest;
use BusinessRules\Requestors\User\ModifyUserRequest;
use BusinessRules\Responders\User\UserResponse;
use BusinessRules\UseCases\Tutor\DTO\Request\GetCompanyTutorRequestDTO;
use BusinessRules\UseCases\Tutor\DTO\Request\GetSchoolTutorRequestDTO;
use BusinessRules\UseCases\User\DTO\Request\GetUserRequestDTO;
use BusinessRules\UseCases\User\DTO\Request\RemoveUserRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\Model\EditUserModel;
use UserBundle\Form\Model\UserModel;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserController extends Controller
{
    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('user');

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var UserModel $model */
            $model = $form->getData();

            $user = $this->get('use_cases.user.create_user')->execute($this->getCreateUserRequest($model));

            $this->addFlash('success', 'app.flash_message.create_user_success');

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $user->getLoginIdentifier())
            );
        }

        return $this->render('UserBundle:User:create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param UserModel $model
     *
     * @return CreateUserRequest
     */
    private function getCreateUserRequest(UserModel $model)
    {
        return $this->get('request.user.create_user_request_builder')
            ->create()
            ->withFirstName(ucfirst(strtolower($model->firstName)))
            ->withLastName(ucfirst(strtolower($model->lastName)))
            ->withEmail(strtolower($model->email))
            ->withPhoneNumber($model->phoneNumber)
            ->withGroupId($model->group)
            ->build();
    }

    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function listAction(Request $request)
    {
        $response = $this->get('use_cases.user.get_paginated_users')
            ->execute($this->getPaginatedUserRequest($request));

        $pagination = array(
            'page'         => $response->getPage(),
            'route'        => 'user_bundle_list_user',
            'pages_count'  => $response->getTotalPages(),
            'route_params' => array()
        );

        return $this->render(
            'UserBundle:User:list.html.twig',
            array(
                'users'      => $response->getItems(),
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return GetPaginatedUsersRequest
     */
    private function getPaginatedUserRequest(Request $request)
    {
        return $this->get('request.user.get_paginated_users_request_builder')
            ->create()
            ->withPage($request->get('page'))
            ->build();
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function showAction($userIdentifier)
    {
        $user = $this->get('use_cases.user.get_user')
            ->execute($this->getUserRequest($userIdentifier));

        $defaultPassword = substr(md5($user->getEmail()), 0, 8);

        return $this->render(
            'UserBundle:User:show.html.twig',
            array(
                'user'            => $user,
                'defaultPassword' => $defaultPassword
            )
        );
    }

    /**
     * @param $id
     *
     * @return GetUserRequestDTO
     */
    private function getUserRequest($userIdentifier)
    {
        return $this->get('request.user.get_user_request_builder')
            ->create()
            ->byLoginIdentifier($userIdentifier)
            ->build();
    }

    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function removeAction($id)
    {

        $user = $this->get('use_cases.user.get_user')->execute(
            $this->get('request.user.get_user_request_builder')
                ->create()
                ->byId($id)
                ->build()
        );

        if ($this->isPermittedToRemove($user)) {
            $this->get('use_cases.user.remove_user')->execute(new RemoveUserRequestDTO($id));

            $this->addFlash('success', 'app.flash_message.remove_user_success');
        }

        return $this->redirectToRoute('user_bundle_list_user');
    }

    /**
     * @param UserResponse $user
     *
     * @return bool
     */
    private function isPermittedToRemove(UserResponse $user)
    {
        if ($this->isConnectedUser($user)) {
            $this->addFlash('error', 'app.flash_message.remove_user_fail_cause_is_you');

            return false;
        };

        if ($this->isAdminAndYouAreNot($user)) {
            $this->addFlash('error', 'app.flash_message.remove_user_fail_cause_is_admin_and_you_are_not');

            return false;
        }

        if ($this->isStudentAndHasReports($user)) {
            $this->addFlash('error', 'app.flash_message.remove_user_fail_cause_is_student');

            return false;
        }

        if ($this->isTutorAndHasStudent($user)) {
            $this->addFlash('error', 'app.flash_message.remove_user_fail_cause_is_tutor');

            return false;
        }

        if ($this->isProfessorAndHasStudents($user)) {
            $this->addFlash('error', 'app.flash_message.remove_user_fail_cause_is_professor');

            return false;
        }

        if ($this->isTrainingManagerAndHasTrainingResponsibility($user)) {
            $this->addFlash('error', 'app.flash_message.remove_user_fail_cause_is_training_manager');

            return false;
        }

        return true;
    }

    /**
     * @param UserResponse $user
     *
     * @return bool
     */
    private function isStudentAndHasReports(UserResponse $user)
    {
        if (!$user->isStudent()) {
            return false;
        }

        $reports = $this->get('use_cases.report.get_paginated_reports')->execute(
            $this->get('request.report.get_paginated_reports_request_builder')
                ->create()
                ->withUserId($user->getId())
                ->build()
        );

        return $reports->getTotalItems() > 0;
    }

    /**
     * @param UserResponse $user
     *
     * @return bool
     */
    private function isTutorAndHasStudent(UserResponse $user)
    {
        if (!$user->isTutor()) {
            return false;
        }

        try {
            $tutor   = $this->get('use_cases.tutor.get_company_tutor')
                ->execute(new GetCompanyTutorRequestDTO($user->getId()));
            $student = $this->get('use_cases.student.get_student')->execute(
                $this->get('request.student.get_student_request_builder')
                    ->create()
                    ->byCompanyTutorId($tutor->getId())
                    ->build()
            );

            return !empty($student);
        } catch (UserCompanyTutorNotFoundException $e) {
            return false;
        } catch (UserStudentNotFoundException $e) {
            return false;
        }
    }

    /**
     * @param UserResponse $user
     *
     * @return bool
     */
    private function isProfessorAndHasStudents(UserResponse $user)
    {
        if (!$user->isProfessor()) {
            return false;
        }

        try {
            $professor = $this->get('use_cases.tutor.get_school_tutor')
                ->execute(new GetSchoolTutorRequestDTO($user->getId()));
            $response  = $this->get('use_cases.student.get_students')->execute(
                $this->get('request.student.get_students_request_builder')
                    ->create()
                    ->bySchoolTutorId($professor->getId())
                    ->build()
            );

            return count($response->getUserStudents()) > 0;
        } catch (UserSchoolTutorNotFoundException $e) {
            return false;
        }
    }

    /**
     * @param UserResponse $user
     *
     * @return bool
     */
    private function isTrainingManagerAndHasTrainingResponsibility(UserResponse $user)
    {
        if (!$user->isTrainingManager()) {
            return false;
        }

        try {
            $trainingManager = $this->get('use_cases.training_manager.get_training_manager')
                ->execute(
                    $this->get('request.training_manager.get_training_manager_request_builder')
                        ->create()
                        ->byUserId($user->getId())
                        ->build()
                );

            return null !== $trainingManager->getPromotion();
        } catch (TrainingManagerNotFoundException $e) {
            return false;
        }
    }

    /**
     * @param UserResponse $user
     *
     * @return bool
     */
    private function isConnectedUser(UserResponse $user)
    {
        return $this->getUser()->getId() === $user->getId();
    }

    /**
     * @param UserResponse $user
     *
     * @return bool
     */
    private function isAdminAndYouAreNot(UserResponse $user)
    {
        return ($user->isAdmin() && !$this->getUser()->isAdmin());
    }


    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function modifyAction(Request $request, $userId)
    {
        $user = $this->get('use_cases.user.get_user')->execute(
            $this->get('request.user.get_user_request_builder')
                ->create()
                ->byId($userId)
                ->build()
        );

        $model              = new EditUserModel();
        $model->firstName   = $user->getFirstName();
        $model->lastName    = $user->getLastName();
        $model->email       = $user->getEmail();
        $model->group       = $user->getGroup();
        $model->phoneNumber = $user->getPhoneNumber();

        $form = $this->createForm(
            'edit_user',
            $model,
            array(
                'action' => $this->generateUrl('user_bundle_modify_user', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EditUserModel $model */
            $model = $form->getData();

            $user = $this->get('use_cases.user.modify_user')->execute(
                $this->getModifyUserRequest($user, $model)
            );

            $this->addFlash('success', 'app.flash_message.edit_user_success');

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $user->getLoginIdentifier())
            );
        }

        return $this->render('UserBundle:User:modify.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param UserResponse  $user
     * @param EditUserModel $model
     *
     * @return ModifyUserRequest
     */
    private function getModifyUserRequest(UserResponse $user, EditUserModel $model)
    {
        return $this->get('request.user.modify_user_request_builder')
            ->create()
            ->withId($user->getId())
            ->withFirstName($model->firstName)
            ->withLastName($model->lastName)
            ->withEmail($model->email)
            ->withPhoneNumber($model->phoneNumber)
            ->withGroupId($model->group)
            ->withPassword($model->password)
            ->build();
    }
}
