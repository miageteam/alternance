<?php

namespace BusinessRules\Requestors\Tutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface EditSchoolTutorRequestBuilder
{
    /**
     * @return EditSchoolTutorRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return EditSchoolTutorRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param string $companyName
     *
     * @return EditSchoolTutorRequestBuilder
     */
    public function withCompanyName($companyName);

    /**
     * @param string $street
     *
     * @return EditSchoolTutorRequestBuilder
     */
    public function withStreet($street);

    /**
     * @param string $zipCode
     *
     * @return EditSchoolTutorRequestBuilder
     */
    public function withZipCode($zipCode);

    /**
     * @param string $city
     *
     * @return EditSchoolTutorRequestBuilder
     */
    public function withCity($city);

    /**
     * @param string $country
     *
     * @return EditSchoolTutorRequestBuilder
     */
    public function withCountry($country);

    /**
     * @return EditSchoolTutorRequest
     */
    public function build();
}
