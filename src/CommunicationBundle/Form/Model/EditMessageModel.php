<?php

namespace CommunicationBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class EditMessageModel
{

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $content;

}
