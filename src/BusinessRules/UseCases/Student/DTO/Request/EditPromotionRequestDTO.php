<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\EditPromotionRequest;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class EditPromotionRequestDTO implements EditPromotionRequest
{

    /**
     * @var int
     */
    public $promotionId;

    /**
     * @var string
     */
    public $year;

    /**
     * @var int
     */
    public $formationId;

    /**
     * @var string
     */
    public $alternationCalendarPath;


    /**
     * {@inheritdoc}
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormationId()
    {
        return $this->formationId;
    }

    /**
     * {@inheritdoc}
     */
    public function getAlternationCalendarPath()
    {
        return $this->alternationCalendarPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }
}
