<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Entities\Student\Meeting;
use BusinessRules\Responders\Student\MeetingResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MeetingResponseFactoryImpl implements MeetingResponseFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromMeeting(Meeting $meeting)
    {
        $response = new MeetingResponseDTO();

        $response->id        = $meeting->getId();
        $response->plannedAt = $meeting->getPlannedAt();
        $response->place     = $meeting->getPlace();

        return $response;
    }
}
