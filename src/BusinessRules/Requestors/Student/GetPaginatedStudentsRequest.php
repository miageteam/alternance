<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetPaginatedStudentsRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getPromotionId();

    /**
     * @return string[]
     */
    public function getSorts();

    /**
     * @return int
     */
    public function getPage();

    /**
     * @return int
     */
    public function getItemsPerPage();
}
