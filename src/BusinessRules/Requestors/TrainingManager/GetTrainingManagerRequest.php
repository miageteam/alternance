<?php

namespace BusinessRules\Requestors\TrainingManager;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetTrainingManagerRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();
}
