<?php

namespace BusinessRules\Requestors\Communication;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
interface GetMessageRequestBuilder
{
    /**
     * @return GetMessageRequestBuilder
     */
    public function create();

    /**
     * @param int $id
     *
     * @return GetMessageRequestBuilder
     */
    public function byId($id);

    /**
     * @return GetMessageRequest
     */
    public function build();
}
