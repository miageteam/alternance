<?php

namespace AppBundle\Entity\Student;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Entities\Student\UserStudent;
use BusinessRules\Entities\Student\UserStudentFactory;
use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Gateways\Student\PromotionGateway;
use BusinessRules\Gateways\Tutor\UserCompanyTutorGateway;
use BusinessRules\Gateways\Tutor\UserSchoolTutorGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Student\CreateStudentRequest;
use BusinessRules\Requestors\Student\EditStudentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserStudentFactoryImpl implements UserStudentFactory
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var PromotionGateway
     */
    private $promotionGateway;

    /**
     * @var UserCompanyTutorGateway
     */
    private $userCompanyTutorGateway;

    /**
     * @var UserSchoolTutorGateway
     */
    private $userSchoolTutorGateway;

    /**
     * {@inheritdoc}
     */
    public function createFromRequest(CreateStudentRequest $request)
    {
        $user             = $this->userGateway->find($request->getUserId());
        $promotion        = $this->promotionGateway->find($request->getPromotionId());
        $userCompanyTutor = $this->getCompanyTutorOrNull($request->getUserCompanyTutorId());
        $userSchoolTutor  = $this->getSchoolTutorOrNull($request->getUserSchoolTutorId());

        $userStudent = new UserStudentImpl();
        $userStudent->setUser($user);
        $userStudent->setPromotion($promotion);
        if (null !== $userCompanyTutor) {
            $userStudent->setUserCompanyTutor($userCompanyTutor);
        }
        if (null !== $userSchoolTutor) {
            $userStudent->setUserSchoolTutor($userSchoolTutor);
        }

        return $userStudent;
    }

    /**
     * {@inheritdoc}
     */
    public function editFromRequest(UserStudent $userStudent, EditStudentRequest $request)
    {
        $promotion        = $this->getPromotionOrNull($request->getPromotionId());
        $userCompanyTutor = $this->getCompanyTutorOrNull($request->getUserCompanyTutorId());
        $userSchoolTutor  = $this->getSchoolTutorOrNull($request->getUserSchoolTutorId());


        if (null !== $promotion) {
            $userStudent->setPromotion($promotion);
        }
        if (null !== $userCompanyTutor) {
            $userStudent->setUserCompanyTutor($userCompanyTutor);
        }
        if (null !== $userSchoolTutor) {
            $userStudent->setUserSchoolTutor($userSchoolTutor);
        }

        return $userStudent;
    }

    /**
     * @param int $promotionId
     *
     * @return Promotion|null
     */
    private function getPromotionOrNull($promotionId)
    {
        if (empty($promotionId)) {
            return null;
        }

        return $this->promotionGateway->find($promotionId);
    }

    /**
     * @param int $companyTutorId
     *
     * @return UserCompanyTutor|null
     */
    private function getCompanyTutorOrNull($companyTutorId)
    {
        if (empty($companyTutorId)) {
            return null;
        }

        return $this->userCompanyTutorGateway->find($companyTutorId);
    }

    /**
     * @param int $professorId
     *
     * @return UserSchoolTutor|null
     */
    private function getSchoolTutorOrNull($professorId)
    {
        if (empty($professorId)) {
            return null;
        }

        return $this->userSchoolTutorGateway->find($professorId);
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param PromotionGateway $promotionGateway
     */
    public function setPromotionGateway(PromotionGateway $promotionGateway)
    {
        $this->promotionGateway = $promotionGateway;
    }

    /**
     * @param UserCompanyTutorGateway $userCompanyTutorGateway
     */
    public function setUserCompanyTutorGateway(UserCompanyTutorGateway $userCompanyTutorGateway)
    {
        $this->userCompanyTutorGateway = $userCompanyTutorGateway;
    }

    /**
     * @param UserSchoolTutorGateway $userSchoolTutorGateway
     */
    public function setUserSchoolTutorGateway(UserSchoolTutorGateway $userSchoolTutorGateway)
    {
        $this->userSchoolTutorGateway = $userSchoolTutorGateway;
    }
}
