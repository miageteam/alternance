<?php

namespace BusinessRules\Entities\User;

use BusinessRules\Requestors\User\CreateUserRequest;
use BusinessRules\Requestors\User\ModifyUserRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserFactory
{
    /**
     * @param CreateUserRequest $request
     *
     * @return User
     */
    public function createFromRequest(CreateUserRequest $request);

    /**
     * @param User              $user
     * @param ModifyUserRequest $request
     *
     * @return User
     */
    public function ModifyFromRequest(User $user, ModifyUserRequest $request);
}
