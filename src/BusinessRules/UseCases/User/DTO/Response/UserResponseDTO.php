<?php

namespace BusinessRules\UseCases\User\DTO\Response;

use BusinessRules\Entities\User\Group;
use BusinessRules\Entities\User\GroupName;
use BusinessRules\Entities\User\Promotion;
use BusinessRules\Responders\User\UserResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserResponseDTO implements UserResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $loginIdentifier;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phoneNumber;

    /**
     * @var string
     */
    public $password;

    /**
     * @var Group
     */
    public $group;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function getLoginIdentifier()
    {
        return $this->loginIdentifier;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * {@inheritdoc}
     */
    public function isStudent()
    {
        return $this->getGroup()->getName() === GroupName::STUDENT;
    }

    /**
     * {@inheritdoc}
     */
    public function isTutor()
    {
        return $this->getGroup()->getName() === GroupName::TUTOR;
    }

    /**
     * {@inheritdoc}
     */
    public function isProfessor()
    {
        return $this->getGroup()->getName() === GroupName::PROFESSOR;
    }

    /**
     * {@inheritdoc}
     */
    public function isTrainingManager()
    {
        return $this->getGroup()->getName() === GroupName::TRAINING_MANAGER;
    }

    /**
     * {@inheritdoc}
     */
    public function isSecretary()
    {
        return $this->getGroup()->getName() === GroupName::SECRETARY;
    }

    /**
     * {@inheritdoc}
     */
    public function isAdmin()
    {
        return $this->getGroup()->getName() === GroupName::ADMIN;
    }
}
