<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\GetMessageRequestBuilder;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class GetMessageRequestBuilderImpl implements GetMessageRequestBuilder
{

    /**
     * @var GetMessageRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetMessageRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byId($id)
    {
        $this->request->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
