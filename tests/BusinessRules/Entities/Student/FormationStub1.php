<?php

namespace BusinessRules\Entities\Student;

use AppBundle\Entity\Student\FormationImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationStub1 extends FormationImpl
{
    const ID   = 1;
    const NAME = 'MIAGE';

    protected $id = self::ID;

    protected $name = self::NAME;

}
