<?php

namespace BusinessRules\Responders\User;

use BusinessRules\Entities\User\User;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserResponseFactory extends PaginatedUseCaseResponseFactory
{
    /**
     * @param User $user
     *
     * @return UserResponse;
     */
    public function createFromUser(User $user);

    /**
     * @param User[] $users
     *
     * @return UserResponse[]
     */
    public function createFromUsers($users);
}
