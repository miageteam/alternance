<?php

namespace BusinessRules\Entities\User;

use UserBundle\Entity\UserImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserStub1 extends UserImpl
{
    const ID = 1;

    const FIRSTNAME = 'Arnaud';

    const LASTNAME = 'Lefevre';

    const LOGIN_IDENTIFIER = 'ArnaudLefevre';

    const EMAIL = 'arnaud.lefevre@mail.com';

    const SALT = 'SALT1234';

    public function __construct()
    {
        $this->group      = new GroupStub1();
    }

    protected $id = self::ID;

    protected $firstName = self::FIRSTNAME;

    protected $lastName = self::LASTNAME;

    protected $loginIdentifier = self::LOGIN_IDENTIFIER;

    protected $email = self::EMAIL;

    protected $salt = self::SALT;

}
