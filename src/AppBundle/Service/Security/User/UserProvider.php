<?php

namespace AppBundle\Service\Security\User;

use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use BusinessRules\Gateways\User\UserGateway;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use UserBundle\Entity\UserImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserProvider implements UserProviderInterface
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        try {
            return $this->userGateway->findByLogin($username);
        } catch (UserNotFoundException $uscnfe) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        try {
            if (!$user instanceof UserImpl) {
                throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
            }

            return $this->userGateway->find($user->getId());
        } catch (UserNotFoundException $unfe) {
            throw new UsernameNotFoundException(sprintf('User id "%s" does not exist.', $user->getId()));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === 'UserBundle\Entity\UserImpl';
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }
}
