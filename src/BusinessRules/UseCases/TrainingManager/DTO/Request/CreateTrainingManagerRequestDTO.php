<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Request;

use BusinessRules\Requestors\TrainingManager\CreateTrainingManagerRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateTrainingManagerRequestDTO implements CreateTrainingManagerRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $promotionId;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }
}
