<?php

namespace UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CheckPassword extends Constraint
{

    /**
     * @var string
     */
    public $message = 'user.enter_your_password';

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'check_password';
    }
}
