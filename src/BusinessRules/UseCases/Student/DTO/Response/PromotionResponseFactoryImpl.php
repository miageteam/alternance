<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Responders\Student\PromotionResponseFactory;

/**
 * @author Kevin Leconte <leconte.kevink@gmail.com>
 * @author David Dieu <daviddieu80@gmail.com>
 */
class PromotionResponseFactoryImpl implements PromotionResponseFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromPromotion(Promotion $promotion)
    {
        $response                          = new PromotionResponseDTO();
        $response->id                      = $promotion->getId();
        $response->year                    = $promotion->getYear();
        $response->formation               = $promotion->getFormation();
        $response->alternationCalendarPath = $promotion->getAlternationCalendarPath();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromPromotions($promotions)
    {
        $promotionsResponses = array();
        foreach ($promotions as $promotion) {
            $promotionsResponses[] = $this->createFromPromotion($promotion);
        }

        return $promotionsResponses;
    }
}
