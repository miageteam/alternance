<?php

namespace BusinessRules\Responders\TrainingManager;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Entities\User\User;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface TrainingManagerResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return User
     */
    public function getUser();

    /**
     * @return Promotion
     */
    public function getPromotion();
}
