<?php

namespace BusinessRules\Gateways\Communication;

use BusinessRules\Entities\Communication\Message;
use BusinessRules\Gateways\Communication\Exceptions\MessageNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryMessageGateway implements MessageGateway
{

    /**
     * @var Message[]
     */
    public static $messages = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @param Message[] $messages
     */
    public function __construct(array $messages = array())
    {
        self::$messages = $messages;
        self::$id       = 0;
    }

    /**
     * @param Message $message
     */
    public function insert(Message $message)
    {
        self::$messages[++self::$id] = $message;
    }

    /**
     * @param int $id
     *
     * @return Message
     *
     * @throws MessageNotFoundException
     */
    public function find($id)
    {
        // TODO: Implement this method.
    }

    /**
     * @param Message $message
     */
    public function update(Message $message)
    {
        // TODO: Implement this method.
    }
}
