<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Response;

use BusinessRules\Responders\TrainingManager\TrainingManagerResponse;
use BusinessRules\Responders\TrainingManager\TrainingManagersResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class TrainingManagersResponseDTO implements TrainingManagersResponse
{

    /**
     * @return TrainingManagerResponse[]
     */
    public $trainingManagers;

    /**
     * @param TrainingManagerResponse[] $trainingManagers
     */
    public function __construct($trainingManagers)
    {
        $this->trainingManagers = $trainingManagers;
    }


    /**
     * {@inheritdoc}
     */
    public function getTrainingManagers()
    {
        return $this->trainingManagers;
    }
}
