<?php

namespace UserBundle\Repository;

use AppBundle\Service\Pagination\PaginatedCollectionFactory;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use BusinessRules\Gateways\User\UserGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserRepository extends EntityRepository implements UserGateway
{

    /**
     * @var PaginatedCollectionFactory
     */
    private $paginatedCollectionFactory;

    /**
     * @param PaginatedCollectionFactory $paginatedCollectionFactory
     */
    public function setPaginatedCollectionFactory(PaginatedCollectionFactory $paginatedCollectionFactory)
    {
        $this->paginatedCollectionFactory = $paginatedCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(User $user)
    {
        $this->_em->persist($user);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        try {
            return $this->createQueryBuilder('u')
                ->addSelect('g')
                ->leftJoin('u.group', 'g')
                ->where('u.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult();

        } catch (NoResultException $nre) {
            throw new UserNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail($email)
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('u.email = :email')
                ->setParameter('email', $email)
                ->getQuery()
                ->getSingleResult();

        } catch (NoResultException $nre) {
            throw new UserNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByIds(array $ids)
    {
        $query = $this->createQueryBuilder('u')
            ->where('u.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findByLogin($loginIdentifier)
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('lower(u.email)= lower(:email)')
                ->setParameter('email', $loginIdentifier)
                ->orWhere('lower(u.loginIdentifier) = lower(:loginIdentifier)')
                ->setParameter('loginIdentifier', $loginIdentifier)
                ->getQuery()
                ->getSingleResult();

        } catch (NoResultException $nre) {
            throw new UserNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findAllLikeLogin($loginIdentifier)
    {
        return $this->createQueryBuilder('u')
            ->where('lower(u.loginIdentifier) Like lower(:loginIdentifier)')
            ->setParameter('loginIdentifier', $loginIdentifier . '%')
            ->orderBy('u.loginIdentifier', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        $qb = $this->createQueryBuilder('u');

        return $this->paginatedCollectionFactory->make($qb, $pagination);
    }

    /**
     * {@inheritdoc}
     */
    public function update(User $user)
    {
        // Do nothing
    }

    /**
     * {@inheritdoc}
     */
    public function remove(User $user)
    {
        $this->_em->remove($user);
    }
}
