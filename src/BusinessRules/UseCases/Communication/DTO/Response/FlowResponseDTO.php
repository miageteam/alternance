<?php

namespace BusinessRules\UseCases\Communication\DTO\Response;

use BusinessRules\Entities\Communication\Message;
use BusinessRules\Entities\User\User;
use BusinessRules\Responders\Communication\FlowResponse;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowResponseDTO implements FlowResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var Message[]
     */
    public $messages;

    /**
     * @var Message
     */
    public $firstMessage;

    /**
     * @var Message
     */
    public $lastMessage;

    /**
     * @var DateTime
     */
    public $createdAt;

    /**
     * @var User{]
     */
    public $participants;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstMessage()
    {
        return $this->firstMessage;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getParticipants()
    {
        return $this->participants;
    }
}
