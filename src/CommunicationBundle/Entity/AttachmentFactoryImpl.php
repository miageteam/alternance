<?php

namespace CommunicationBundle\Entity;

use BusinessRules\Entities\Communication\AttachmentFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class AttachmentFactoryImpl implements AttachmentFactory
{

    /**
     * {@inheritdoc}
     */
    public function create($attachmentPath, $message)
    {
        $attachment = new AttachmentImpl();

        $attachment->setPath($attachmentPath);
        $attachment->setMessage($message);

        return $attachment;
    }
}
