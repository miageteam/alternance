<?php

namespace AppBundle\Controller\TrainingManager;

use AppBundle\Form\Model\TrainingManagerModel;
use AppBundle\Form\Type\TrainingManagerType;
use BusinessRules\Gateways\TrainingManager\Exceptions\TrainingManagerNotFoundException;
use BusinessRules\Requestors\TrainingManager\CreateTrainingManagerRequest;
use BusinessRules\Requestors\TrainingManager\EditTrainingManagerRequest;
use BusinessRules\Requestors\User\GetUserRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class TrainingManagerController extends Controller
{
    public function showAction($userId)
    {
        try {
            $trainingManager = $this->get('use_cases.training_manager.get_training_manager')
                ->execute(
                    $this->get('request.training_manager.get_training_manager_request_builder')
                        ->create()
                        ->byUserId($userId)
                        ->build()
                );

            return $this->render(
                'AppBundle:TrainingManager/TrainingManager:show.html.twig',
                array('trainingManager' => $trainingManager)
            );
        } catch (TrainingManagerNotFoundException $e) {
            return $this->forward('AppBundle:TrainingManager/TrainingManager:create', array('userId' => $userId));
        }
    }

    public function createAction(Request $request, $userId)
    {
        $form = $this->createForm(
            new TrainingManagerType(),
            null,
            array('action' => $this->generateUrl('app_user_training_manager_create', array('userId' => $userId)))
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var TrainingManagerModel $model */
            $model = $form->getData();

            $this->get('use_cases.training_manager.create_training_manager')
                ->execute($this->getCreateTrainingManagerRequest($userId, $model));

            $user = $this->get('use_cases.user.get_user')->execute($this->getUserRequest($userId));

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $user->getLoginIdentifier())
            );
        }

        return $this->render(
            'AppBundle:TrainingManager/TrainingManager:create.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @param int                  $userId
     * @param TrainingManagerModel $model
     *
     * @return CreateTrainingManagerRequest
     */
    private function getCreateTrainingManagerRequest($userId, $model)
    {
        return $this->get('request.training_manager.create_training_manager_request_builder')
            ->create()
            ->withUserId($userId)
            ->withPromotionId($model->promotion)
            ->build();
    }

    /**
     * @param int $userId
     *
     * @return GetUserRequest
     */
    private function getUserRequest($userId)
    {
        return $this->get('request.user.get_user_request_builder')
            ->create()
            ->byId($userId)
            ->build();
    }

    public function listStudentsAction(Request $request)
    {
        $response = $this->get('use_cases.student.get_paginated_students')->execute(
            $this->get('request.student.get_paginated_students_request_builder')
                ->create()
                ->withPage($request->get('page'))
                ->build()
        );

        $pagination = array(
            'page'         => $response->getPage(),
            'route'        => 'app_user_training_manager_list_students',
            'pages_count'  => $response->getTotalPages(),
            'route_params' => array()
        );

        return $this->render(
            'AppBundle:TrainingManager/TrainingManager:list_student.html.twig',
            array(
                'students'   => $response->getItems(),
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function editAction(Request $request, $userId)
    {
        $trainingManager = $this->get('use_cases.training_manager.get_training_manager')
            ->execute(
                $this->get('request.training_manager.get_training_manager_request_builder')
                    ->create()
                    ->byUserId($userId)
                    ->build()
            );

        $model                   = new TrainingManagerModel();
        $model->promotion        = $trainingManager->getPromotion();

        $form = $this->createForm(
            new TrainingManagerType(),
            $model,
            array(
                'action' => $this->generateUrl('app_training_manager_edit', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var TrainingManagerModel $model */
            $model = $form->getData();

            $this->get('use_cases.training_manager.edit_training_manager')->execute($this->getEditTrainingManagerRequest($model, $userId));

            $this->addFlash('success', 'app.flash_message.edit_training_manager_success');

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $trainingManager->getUser()->getLoginIdentifier())
            );
        }

        return $this->render('AppBundle:TrainingManager/TrainingManager:edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param TrainingManagerModel $model
     * @param int                  $userId
     *
     * @return EditTrainingManagerRequest
     */
    private function getEditTrainingManagerRequest(TrainingManagerModel $model, $userId)
    {
        return $this->get('request.training_manager.edit_training_manager_request_builder')
            ->create()
            ->withUserId($userId)
            ->withPromotionId($model->promotion)
            ->build();
    }

    public function getPromotionAction()
    {
        try {
            $trainingManager = $this->get('use_cases.training_manager.get_training_manager')->execute(
                $this->get('request.training_manager.get_training_manager_request_builder')
                    ->create()
                    ->byUserId($this->getUser()->getId())
                    ->build()
            );

            $promotion = $trainingManager->getPromotion();
        } catch (TrainingManagerNotFoundException $e) {
            $promotion = null;
        }

        return $this->render('AppBundle:TrainingManager/TrainingManager/partials:get_promotion.html.twig', array('promotion' => $promotion));
    }
}
