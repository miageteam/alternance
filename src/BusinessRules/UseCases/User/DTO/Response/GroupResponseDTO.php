<?php

namespace BusinessRules\UseCases\User\DTO\Response;

use BusinessRules\Entities\User\UserRole;
use BusinessRules\Responders\User\GroupResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupResponseDTO implements GroupResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var UserRole[]
     */
    public $userRoles;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return UserRole[]
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }
}
