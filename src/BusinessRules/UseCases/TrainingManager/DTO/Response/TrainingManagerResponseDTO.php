<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Response;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Entities\User\User;
use BusinessRules\Responders\TrainingManager\TrainingManagerResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class TrainingManagerResponseDTO implements TrainingManagerResponse
{

    /**
     * @return int
     */
    public $id;

    /**
     * @return User
     */
    public $user;

    /**
     * @return Promotion
     */
    public $promotion;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotion()
    {
        return $this->promotion;
    }
}
