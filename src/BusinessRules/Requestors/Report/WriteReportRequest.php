<?php

namespace BusinessRules\Requestors\Report;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteReportRequest extends UseCaseRequest
{
    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return int
     */
    public function getAuthorId();

    /**
     * @return string
     */
    public function getAttachmentPath();
}
