<?php

namespace UserBundle\Controller;

use BusinessRules\Entities\User\Group;
use BusinessRules\UseCases\User\DTO\Request\GetGroupsRequestDTO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\GroupImpl;
use UserBundle\Form\Type\CreateGroupType;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupController extends Controller
{
    public function createAction(Request $request)
    {
        $form = $this->createForm(new CreateGroupType());

        $form->handleRequest($request);

        if ($form->isValid()) {
            $model = $form->getData();
            $request = $this->get('request.user.create_group_request_builder')
                            ->create()
                            ->withName($model->name)
                            ->withUserRoles($model->userRoles->toArray())
                            ->build();
            $this->get('use_cases.user.create_group')->execute($request);

            $this->addFlash('success', 'app.flash_message.create_group_success');

            return $this->redirectToRoute('user_bundle_list_group');
        }

        return $this->render('UserBundle:Group:create.html.twig', array('form' => $form->createView(),));
    }

    public function listAction()
    {
        $response = $this->get('use_cases.user.get_groups')
                         ->execute(new GetGroupsRequestDTO());

        return $this->render('UserBundle:Group:list.html.twig', array('groups' => $response->getGroups()));
    }
}
