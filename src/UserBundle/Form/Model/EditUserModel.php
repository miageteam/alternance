<?php

namespace UserBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Validator\Constraints as MyAssert;

/**
 * @author Kevin LECONTE <arnaud.h.lefevre@gmail.com>
 */
class EditUserModel
{

    /**
     * @var String
     *
     * @Assert\NotBlank()
     */
    public $firstName;

    /**
     * @var String
     *
     * @Assert\NotBlank()
     */
    public $lastName;

    /**
     * @var String
     *
     * @Assert\NotBlank()
     */
    public $email;

    /**
     * @var String
     */
    public $phoneNumber;

    /**
     * @var String
     */
    public $password;

    /**
     * @var int
     */
    public $group;

}
