<?php

namespace BusinessRules\Entities\Communication;

use BusinessRules\Entities\User\UserStub1;
use CommunicationBundle\Entity\MessageImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MessageStub1 extends MessageImpl
{
    const ID      = 1;
    const CONTENT = 'My first Message, Oh it\'s funny !';

    public function __construct()
    {
        $this->author = new UserStub1();
        $this->flow   = new FlowStub1();
        parent::__construct();
    }

    protected $id = self::ID;

    protected $content = self::CONTENT;

}
