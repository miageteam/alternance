<?php

namespace BusinessRules\UseCases\Tutor;

use AppBundle\Entity\Tutor\UserCompanyTutorFactoryImpl;
use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Entities\Tutor\UserCompanyTutorStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\Tutor\InMemoryUserCompanyTutorGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Tutor\CreateCompanyTutorRequestBuilder;
use BusinessRules\UseCases\Tutor\DTO\Request\CreateCompanyTutorRequestBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateCompanyTutorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CreateCompanyTutor
     */
    private $useCase;

    /**
     * @var CreateCompanyTutorRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     */
    public function createCompanyTutor()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withUserId(UserStub1::ID)
                ->withCompanyName(UserCompanyTutorStub1::COMPANYNAME)
                ->withStreet(UserCompanyTutorStub1::STREET)
                ->withZipCode(UserCompanyTutorStub1::ZIPCODE)
                ->withCity(UserCompanyTutorStub1::CITY)
                ->withCountry(UserCompanyTutorStub1::COUNTRY)
                ->build()
        );

        $this->assertCount(1, InMemoryUserCompanyTutorGateway::$userCompanyTutors);
        foreach (InMemoryUserCompanyTutorGateway::$userCompanyTutors as $userCompanyTutor) {
            $this->assertUserCompanyTutor($userCompanyTutor);
        }

    }

    /**
     * @param UserCompanyTutor $userCompanyTutor
     */
    private function assertUserCompanyTutor(UserCompanyTutor $userCompanyTutor)
    {
        $this->assertEquals(UserStub1::ID, $userCompanyTutor->getUser()->getId());
        $this->assertEquals(UserCompanyTutorStub1::COMPANYNAME, $userCompanyTutor->getCompanyName());
        $this->assertEquals(UserCompanyTutorStub1::STREET, $userCompanyTutor->getStreet());
        $this->assertEquals(UserCompanyTutorStub1::ZIPCODE, $userCompanyTutor->getZipCode());
        $this->assertEquals(UserCompanyTutorStub1::CITY, $userCompanyTutor->getCity());
        $this->assertEquals(UserCompanyTutorStub1::COUNTRY, $userCompanyTutor->getCountry());
    }

    protected function setUp()
    {
        $this->useCase = new CreateCompanyTutor();
        $this->useCase->setUserCompanyTutorFactory($this->getUserCompanyTutorFactoryGateway());
        $this->useCase->setUserCompanyTutorGateway(new InMemoryUserCompanyTutorGateway());

        $this->requestBuilder = new CreateCompanyTutorRequestBuilderImpl();
    }

    /**
     * @return UserCompanyTutorFactoryImpl
     */
    private function getUserCompanyTutorFactoryGateway()
    {
        $userCompanyTutorFactory = new UserCompanyTutorFactoryImpl();
        $userCompanyTutorFactory->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));

        return $userCompanyTutorFactory;
    }
}
