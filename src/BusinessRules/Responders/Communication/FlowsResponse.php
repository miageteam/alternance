<?php

namespace BusinessRules\Responders\Communication;

use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FlowsResponse extends UseCaseResponse
{
    /**
     * @return FlowResponse[]
     */
    public function getFlows();
}
