<?php

namespace BusinessRules\Entities\Tutor;

use BusinessRules\Requestors\Tutor\CreateSchoolTutorRequest;
use BusinessRules\Requestors\Tutor\EditSchoolTutorRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserSchoolTutorFactory
{
    /**
     * @param CreateSchoolTutorRequest $request
     *
     * @return UserSchoolTutor
     */
    public function createFromRequest(CreateSchoolTutorRequest $request);

    /**
     * @param EditSchoolTutorRequest $request
     *
     * @return UserSchoolTutor
     */
    public function editFromRequest(UserSchoolTutor $userSchoolTutor, EditSchoolTutorRequest $request);
}
