<?php

namespace BusinessRules\Requestors\TrainingManager;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateTrainingManagerRequestBuilder
{
    /**
     * @return CreateTrainingManagerRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return CreateTrainingManagerRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param int $promotionId
     *
     * @return CreateTrainingManagerRequestBuilder
     */
    public function withPromotionId($promotionId);

    /**
     * @return CreateTrainingManagerRequest
     */
    public function build();
}
