<?php

namespace BusinessRules\Responders\Report;

use BusinessRules\Entities\Report\Comment;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CommentResponseFactory
{
    /**
     * @param Comment $comment
     *
     * @return CommentResponse
     */
    public function createFromComment(Comment $comment);

    /**
     * @param Comment[] $comments
     *
     * @return CommentResponse[]
     */
    public function createFromComments($comments);
}
