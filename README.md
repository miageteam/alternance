Plateform d'alternance
========================

Pré-requis
----------
* installer git
* installer composer
* installer une serveur PHP/Apache/Mysql


# How to install?

Ouvrir un terminal de commande git et lancer les commandes suivantes :

```bash

    git clone https://arnaud-23@bitbucket.org/miageteam/alternance.git
    
    cd alternance/
    
    composer install
    
    git submodule init
    
    git submodule update
```

# Donner l'accès au repertoire de cache et de logs

**Sous Linux et Mac**
Executer :

```bash
    HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
    sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
    sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs

```

**Sous Windows**
Il ne devrait pas y avoir de problème en particulier à ce niveau la.

# Remplir la base de donnée
Vérifiez que vous avez bien configuré les accès à votre base de données MySQL 
dans le fichier **app/config/parameter.yml**

dans votre terminal de commande git executez les commandes suivantes:

```bash
    # SI VOTRE BASE DE DONNÉES N'EST PAS ENCORE CRÉÉE
    php app/console doctrine:database:create
    
    # SI LE SCHEMA DE BASE DE DONNÉES N'EST PAS CRÉÉ 
    php app/console doctrine:schema:create
    
    # QUAND TOUTES LES ETAPES PRÉCÉDENTES SONT FAITES 
    # INSERER LES DONNÉES PAR DÉFAUT
    php app/console doctrine:fixtures:load --append
```

> les données par défaut comportent les différents Roles d'accéssibilité, 
> les groupes par défaut et un compte administrateur.

les identifiants du compte administrateur par défaut sont :
login    : admin
password : admin
