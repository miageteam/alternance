<?php

namespace BusinessRules\Requestors\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetStudentRequestBuilder
{
    /**
     * @return GetStudentRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return GetStudentRequestBuilder
     */
    public function byUserId($userId);

    /**
     * @param int $companyTutorId
     *
     * @return GetStudentRequestBuilder
     */
    public function byCompanyTutorId($companyTutorId);

    /**
     * @return GetStudentRequest
     */
    public function build();
}
