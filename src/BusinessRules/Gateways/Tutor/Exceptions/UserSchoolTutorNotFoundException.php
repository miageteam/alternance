<?php

namespace BusinessRules\Gateways\Tutor\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserSchoolTutorNotFoundException extends \Exception
{

}
