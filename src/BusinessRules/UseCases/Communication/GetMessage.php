<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Gateways\Communication\MessageGateway;
use BusinessRules\Requestors\Communication\GetMessageRequest;
use BusinessRules\Responders\Communication\MessageResponse;
use BusinessRules\Responders\Communication\MessageResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class GetMessage implements UseCase
{

    /**
     * @var MessageGateway
     */
    private $messageGateway;

    /**
     * @var MessageResponseFactory
     */
    private $messageResponseFactory;

    /**
     * @param GetMessageRequest $request
     *
     * @return MessageResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $message = $this->messageGateway->find($request->getId());

        return $this->messageResponseFactory->createFromMessage($message);
    }

    /**
     * @param MessageGateway $messageGateway
     */
    public function setMessageGateway(MessageGateway $messageGateway)
    {
        $this->messageGateway = $messageGateway;
    }

    /**
     * @param MessageResponseFactory $messageResponseFactory
     */
    public function setMessageResponseFactory(MessageResponseFactory $messageResponseFactory)
    {
        $this->messageResponseFactory = $messageResponseFactory;
    }
}
