<?php

namespace BusinessRules\Requestors\Report;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface EditCommentRequestBuilder
{
    /**
     * @return EditCommentRequestBuilder
     */
    public function create();

    /**
     * @param int $commentId
     *
     * @return EditCommentRequestBuilder
     */
    public function withCommentId($commentId);

    /**
     * @param string $content
     *
     * @return EditCommentRequestBuilder
     */
    public function withContent($content);

    /**
     * @return EditCommentRequest
     */
    public function build();
}
