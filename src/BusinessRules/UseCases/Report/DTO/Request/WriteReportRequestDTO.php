<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\WriteReportRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteReportRequestDTO implements WriteReportRequest
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var int
     */
    public $authorId;

    /**
     * @var string
     */
    public $attachmentPath;

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttachmentPath()
    {
        return $this->attachmentPath;
    }
}
