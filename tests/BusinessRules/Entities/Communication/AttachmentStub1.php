<?php

namespace BusinessRules\Entities\Communication;

use CommunicationBundle\Entity\AttachmentImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class AttachmentStub1 extends AttachmentImpl
{
    const ID   = 1;
    const PATH = 'www.monserveur.de/stockage';

    public function __construct()
    {
        $this->message = new MessageStub1();
    }

    protected $id = self::ID;

    protected $path = self::PATH;

}
