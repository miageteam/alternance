<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\MeetingGateway;
use BusinessRules\Requestors\Student\GetMeetingRequest;
use BusinessRules\Responders\Student\MeetingResponse;
use BusinessRules\Responders\Student\MeetingResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetMeeting implements UseCase
{

    /**
     * @var MeetingGateway
     */
    private $meetingGateway;

    /**
     * @var MeetingResponseFactory
     */
    private $meetingResponseFactory;

    /**
     * @param GetMeetingRequest $request
     *
     * @return MeetingResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $meeting = $this->meetingGateway->findByUserId($request->getUserId());

        return $this->meetingResponseFactory->createFromMeeting($meeting);
    }

    /**
     * @param MeetingGateway $meetingGateway
     */
    public function setMeetingGateway(MeetingGateway $meetingGateway)
    {
        $this->meetingGateway = $meetingGateway;
    }

    /**
     * @param MeetingResponseFactory $meetingResponseFactory
     */
    public function setMeetingResponseFactory(MeetingResponseFactory $meetingResponseFactory)
    {
        $this->meetingResponseFactory = $meetingResponseFactory;
    }
}
