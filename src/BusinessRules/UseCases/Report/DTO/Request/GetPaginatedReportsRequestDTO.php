<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\GetPaginatedReportsRequest;
use BusinessRules\Utils\Pagination;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedReportsRequestDTO implements GetPaginatedReportsRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * @var bool
     */
    public $isValidated;

    /**
     * @var string[]
     */
    public $sorts = array();

    /**
     * @var int
     */
    public $page = Pagination::PAGE;

    /**
     * @var int
     */
    public $itemsPerPage = Pagination::ITEMS_PER_PAGE;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getIsValidated()
    {
        return $this->isValidated;
    }

    /**
     * {@inheritdoc}
     */
    public function getSorts()
    {
        return $this->sorts;
    }

    /**
     * {@inheritdoc}
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * {@inheritdoc}
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }
}
