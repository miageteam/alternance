<?php

namespace BusinessRules\Requestors\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetPaginatedUsersRequestBuilder
{
    /**
     * @return GetPaginatedUsersRequestBuilder
     */
    public function create();

    /**
     * @param array $sorts
     *
     * @return GetPaginatedUsersRequestBuilder
     */
    public function withSorts(array $sorts);

    /**
     * @param int $page
     *
     * @return GetPaginatedUsersRequestBuilder
     */
    public function withPage($page);

    /**
     * @param int $itemsPerPage
     *
     * @return GetPaginatedUsersRequestBuilder
     */
    public function withItemsPerPage($itemsPerPage);

    /**
     * @return GetPaginatedUsersRequest
     */
    public function build();
}
