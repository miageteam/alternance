<?php

namespace BusinessRules\Requestors\Report\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InvalidWriteCommentRequestParameterException extends \Exception
{

}
