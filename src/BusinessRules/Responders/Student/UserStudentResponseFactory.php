<?php

namespace BusinessRules\Responders\Student;

use BusinessRules\Entities\Student\UserStudent;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserStudentResponseFactory extends PaginatedUseCaseResponseFactory
{
    /**
     * @param UserStudent $userStudent
     *
     * @return UserStudentResponse
     */
    public function createFromUserStudent(UserStudent $userStudent);

    /**
     * @param UserStudent[] $userStudents
     *
     * @return UserStudentResponse[]
     */
    public function createFromUserStudents($userStudents);
}
