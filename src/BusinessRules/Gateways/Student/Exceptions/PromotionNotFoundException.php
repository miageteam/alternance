<?php

namespace BusinessRules\Gateways\Student\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PromotionNotFoundException extends \Exception
{

}
