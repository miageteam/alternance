<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Entities\Report\AttachmentStub1;
use BusinessRules\Entities\Report\ReportStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Requestors\Report\WriteReportRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteReportRequestBuilderImplTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteReportRequestBuilder
     */
    private $builder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Report\Exceptions\InvalidWriteReportRequestParameterException
     * @expectedExceptionMessage The argument "AuthorId" must be defined.
     */
    public function WithoutAuthor_ThrowException()
    {
        $this->builder
            ->create()
            ->withTitle(ReportStub1::TITLE)
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Report\Exceptions\InvalidWriteReportRequestParameterException
     * @expectedExceptionMessage The argument "Title" must be defined.
     */
    public function WithoutTitle_ThrowException()
    {
        $this->builder
            ->create()
            ->withAuthorId(UserStub1::ID)
            ->build();
    }

    /**
     * @test
     */
    public function BuildStandard()
    {
        $this->builder
            ->create()
            ->withAuthorId(UserStub1::ID)
            ->withTitle(ReportStub1::TITLE)
            ->build();
    }

    /**
     * @test
     */
    public function BuildWithAttachments()
    {
        $this->builder
            ->create()
            ->withAuthorId(UserStub1::ID)
            ->withTitle(ReportStub1::TITLE)
            ->withAttachmentPath(AttachmentStub1::PATH)
            ->build();
    }

    protected function setUp()
    {
        $this->builder = new WriteReportRequestBuilderImpl();
    }
}
