<?php

namespace BusinessRules\UseCases\User\DTO\Response;

use BusinessRules\Entities\User\User;
use BusinessRules\Responders\User\UserResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserResponseFactoryImpl implements UserResponseFactory
{

    /**
     * @var PaginatedUseCaseResponseBuilder
     */
    private $paginatedUseCaseResponseBuilder;

    /**
     * {@inheritdoc}
     */
    public function createFromPaginatedCollection(PaginatedCollection $paginatedCollection)
    {
        return $this->paginatedUseCaseResponseBuilder
            ->create()
            ->withItems($this->createFromUsers($paginatedCollection->getItems()))
            ->withItemsPerPage($paginatedCollection->getItemsPerPage())
            ->withPage($paginatedCollection->getPage())
            ->withTotalItems($paginatedCollection->getTotalItems())
            ->build();
    }

    /**
     * {@inheritdoc}
     */
    public function createFromUsers($users)
    {
        $userResponses = array();
        foreach ($users as $user) {
            $userResponses[] = $this->createFromUser($user);
        }

        return $userResponses;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromUser(User $user)
    {
        $response                  = new UserResponseDTO();
        $response->id              = $user->getId();
        $response->firstName       = $user->getFirstName();
        $response->lastName        = $user->getLastName();
        $response->loginIdentifier = $user->getLoginIdentifier();
        $response->email           = $user->getEmail();
        $response->phoneNumber     = $user->getPhoneNumber();
        $response->password        = $user->getPassword();
        $response->group           = $user->getGroup();

        return $response;
    }

    /**
     * @param PaginatedUseCaseResponseBuilder $builder
     */
    public function setPaginatedUseCaseResponseBuilder(PaginatedUseCaseResponseBuilder $builder)
    {
        $this->paginatedUseCaseResponseBuilder = $builder;
    }
}
