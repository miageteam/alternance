<?php

namespace BusinessRules\Requestors\Communication;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
interface EditMessageRequestBuilder
{
    /**
     * @return EditMessageRequestBuilder
     */
    public function create();

    /**
     * @param int $messageId
     *
     * @return EditMessageRequestBuilder
     */
    public function withMessageId($messageId);

    /**
     * @param string $content
     *
     * @return EditMessageRequestBuilder
     */
    public function withContent($content);

    /**
     * @return EditMessageRequest
     */
    public function build();
}
