<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\UserStudentGateway;
use BusinessRules\Requestors\Student\GetStudentRequest;
use BusinessRules\Responders\Student\UserStudentResponse;
use BusinessRules\Responders\Student\UserStudentResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetStudent implements UseCase
{

    /**
     * @var UserStudentGateway
     */
    private $userStudentGateway;

    /**
     * @var UserStudentResponseFactory
     */
    private $userStudentResponseFactory;

    /**
     * @param GetStudentRequest $request
     *
     * @return UserStudentResponse
     */
    public function execute(UseCaseRequest $request)
    {
        if ($request->getCompanyTutorId()) {
            $userStudent = $this->userStudentGateway->findByCompanyTutorId($request->getCompanyTutorId());
        } else {
            $userStudent = $this->userStudentGateway->findByUserId($request->getUserId());
        }

        return $this->userStudentResponseFactory->createFromUserStudent($userStudent);
    }

    /**
     * @param UserStudentGateway $userStudentGateway
     */
    public function setUserStudentGateway(UserStudentGateway $userStudentGateway)
    {
        $this->userStudentGateway = $userStudentGateway;
    }

    /**
     * @param UserStudentResponseFactory $userStudentResponseFactory
     */
    public function setUserStudentResponseFactory(UserStudentResponseFactory $userStudentResponseFactory)
    {
        $this->userStudentResponseFactory = $userStudentResponseFactory;
    }
}
