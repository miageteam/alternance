<?php

namespace BusinessRules\Gateways\Student;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Gateways\Student\Exceptions\PromotionAlreadyExistException;
use BusinessRules\Gateways\Student\Exceptions\PromotionNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryPromotionGateway implements PromotionGateway
{

    /**
     * @var Promotion[]
     */
    public static $promotions = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @param Promotion[] $promotions
     */
    public function __construct(array $promotions = array())
    {
        self::$promotions = $promotions;
        self::$id         = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$promotions[$id])) {
            throw new PromotionNotFoundException();
        }

        return self::$promotions[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function insert(Promotion $promotion)
    {
        foreach (self::$promotions as $localPromotion) {
            if ($localPromotion->getYear() === $promotion->getYear() &&
                $localPromotion->getFormationId() === $promotion->getFormationId()
            ) {
                throw new PromotionAlreadyExistException();
            }
        }

        self::$promotions[++self::$id] = $promotion;
    }

    /**
     * @return Promotion[]
     */
    public function findAll()
    {
        // TODO: Implement this method.
    }

    /**
     * @param Promotion $promotion
     */
    public function remove(Promotion $promotion)
    {
        // TODO: Implement this method.
    }

    /**
     * @param Promotion $promotion
     */
    public function update(Promotion $promotion)
    {
        // TODO: Implement this method.
    }
}
