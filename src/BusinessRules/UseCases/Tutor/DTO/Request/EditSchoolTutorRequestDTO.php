<?php

namespace BusinessRules\UseCases\Tutor\DTO\Request;

use BusinessRules\Requestors\Tutor\EditSchoolTutorRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditSchoolTutorRequestDTO implements EditSchoolTutorRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $companyName;

    /**
     * @var string
     */
    public $street;

    /**
     * @var int
     */
    public $zipCode;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $country;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * {@inheritdoc}
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * {@inheritdoc}
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        return $this->country;
    }
}
