<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\GetReportRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetReportRequestDTO implements GetReportRequest
{

    /**
     * @var int
     */
    public $id;

    /**
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
}
