<?php

namespace BusinessRules\Gateways\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Gateways\Tutor\Exceptions\UserSchoolTutorNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserSchoolTutorGateway
{
    /**
     * @param UserSchoolTutor $userSchoolTutor
     */
    public function insert(UserSchoolTutor $userSchoolTutor);

    /**
     * @param int $id
     *
     * @return UserSchoolTutor
     *
     * @throws UserSchoolTutorNotFoundException
     */
    public function find($id);

    /**
     * @param int $userId
     *
     * @return UserSchoolTutor
     *
     * @throws UserSchoolTutorNotFoundException
     */
    public function findByUserId($userId);

    /**
     * @param UserSchoolTutor $userSchoolTutor
     */
    public function update(UserSchoolTutor $userSchoolTutor);
}
