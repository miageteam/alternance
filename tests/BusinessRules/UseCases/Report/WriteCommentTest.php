<?php

namespace BusinessRules\UseCases\Report;

use AppBundle\Entity\Report\CommentFactoryImpl;
use BusinessRules\Entities\Report\CommentStub1;
use BusinessRules\Entities\Report\ReportStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\Report\InMemoryCommentGateway;
use BusinessRules\Gateways\Report\InMemoryReportGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Report\WriteCommentRequestBuilder;
use BusinessRules\UseCases\Report\DTO\Request\WriteCommentRequestBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteCommentTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteComment
     */
    private $useCase;

    /**
     * @var WriteCommentRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\Report\Exceptions\ReportNotFoundException
     */
    public function ReportNotFound_ThrowException()
    {
        InMemoryReportGateway::$reports = array();

        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withReportId(ReportStub1::ID)
                ->withContent(CommentStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function AuthorNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();

        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withReportId(ReportStub1::ID)
                ->withContent(CommentStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     */
    public function WriteComment()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withReportId(ReportStub1::ID)
                ->withContent(CommentStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );

        foreach (InMemoryCommentGateway::$comments as $comment) {
            $this->assertEquals(ReportStub1::ID, $comment->getReportId());
            $this->assertEquals(CommentStub1::CONTENT, $comment->getContent());
            $this->assertEquals(UserStub1::ID, $comment->getAuthorId());
        }
    }

    protected function setup()
    {
        $this->useCase = new WriteComment();
        $this->useCase->setReportGateway(new InMemoryReportGateway(array(ReportStub1::ID => new ReportStub1())));
        $this->useCase->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));
        $this->useCase->setCommentFactory(new CommentFactoryImpl());
        $this->useCase->setCommentGateway(new InMemoryCommentGateway());

        $this->requestBuilder = new WriteCommentRequestBuilderImpl();
    }
}
