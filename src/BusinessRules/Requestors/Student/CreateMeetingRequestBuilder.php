<?php

namespace BusinessRules\Requestors\Student;

use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateMeetingRequestBuilder
{
    /**
     * @return CreateMeetingRequestBuilder
     */
    public function create();

    /**
     * @param DateTime $plannedAt
     *
     * @return CreateMeetingRequestBuilder
     */
    public function withPlannedAt(DateTime $plannedAt);

    /**
     * @param string $place
     *
     * @return CreateMeetingRequestBuilder
     */
    public function withPlace($place);

    /**
     * @param int[] $participantIds
     *
     * @return CreateMeetingRequestBuilder
     */
    public function withParticipantIds(array $participantIds);

    /**
     * @return CreateMeetingRequest
     */
    public function build();
}
