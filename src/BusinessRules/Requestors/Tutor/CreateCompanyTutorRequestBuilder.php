<?php

namespace BusinessRules\Requestors\Tutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateCompanyTutorRequestBuilder
{
    /**
     * @return CreateCompanyTutorRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return CreateCompanyTutorRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param string $companyName
     *
     * @return CreateCompanyTutorRequestBuilder
     */
    public function withCompanyName($companyName);

    /**
     * @param string $street
     *
     * @return CreateCompanyTutorRequestBuilder
     */
    public function withStreet($street);

    /**
     * @param string $zipCode
     *
     * @return CreateCompanyTutorRequestBuilder
     */
    public function withZipCode($zipCode);

    /**
     * @param string $city
     *
     * @return CreateCompanyTutorRequestBuilder
     */
    public function withCity($city);

    /**
     * @param string $country
     *
     * @return CreateCompanyTutorRequestBuilder
     */
    public function withCountry($country);

    /**
     * @return CreateCompanyTutorRequest
     */
    public function build();
}
