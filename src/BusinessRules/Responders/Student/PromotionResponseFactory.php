<?php

namespace BusinessRules\Responders\Student;

use BusinessRules\Entities\Student\Promotion;

/**
 * @author Kevin Leconte <leconte.kevink@gmail.com>
 * @author David Dieu <daviddieu80@gmail.com>
 */
interface PromotionResponseFactory
{
    /**
     * @param Promotion $promotion
     *
     * @return PromotionResponse
     */
    public function createFromPromotion(Promotion $promotion);

    /**
     * @param Promotion[] $promotions
     *
     * @return PromotionResponse[]
     */
    public function createFromPromotions($promotions);
}
