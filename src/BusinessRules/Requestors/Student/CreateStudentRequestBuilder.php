<?php

namespace BusinessRules\Requestors\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateStudentRequestBuilder
{
    /**
     * @return CreateStudentRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return CreateStudentRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param int $promotionId
     *
     * @return CreateStudentRequestBuilder
     */
    public function withPromotionId($promotionId);

    /**
     * @param int $userSchoolTutorId
     *
     * @return CreateStudentRequestBuilder
     */
    public function withUserSchoolTutorId($userSchoolTutorId);

    /**
     * @param int $userCompanyTutorId
     *
     * @return CreateStudentRequestBuilder
     */
    public function withUserCompanyTutorId($userCompanyTutorId);

    /**
     * @return CreateStudentRequest
     */
    public function build();
}
