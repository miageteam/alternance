<?php

namespace BusinessRules\Entities\Student;

use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
abstract class UserStudent
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Promotion
     */
    protected $promotion;

    /**
     * @var UserSchoolTutor
     */
    protected $userSchoolTutor;

    /**
     * @var UserCompanyTutor
     */
    protected $userCompanyTutor;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Promotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * @param Promotion $promotion
     */
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;
    }

    /**
     * @return UserSchoolTutor
     */
    public function getUserSchoolTutor()
    {
        return $this->userSchoolTutor;
    }

    /**
     * @param UserSchoolTutor $userSchoolTutor
     */
    public function setUserSchoolTutor(UserSchoolTutor $userSchoolTutor)
    {
        $this->userSchoolTutor = $userSchoolTutor;
    }

    /**
     * @return UserCompanyTutor
     */
    public function getUserCompanyTutor()
    {
        return $this->userCompanyTutor;
    }

    /**
     * @param UserCompanyTutor $userCompanyTutor
     */
    public function setUserCompanyTutor(UserCompanyTutor $userCompanyTutor)
    {
        $this->userCompanyTutor = $userCompanyTutor;
    }
}
