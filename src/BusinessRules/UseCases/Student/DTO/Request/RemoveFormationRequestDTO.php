<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\RemoveFormationRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class RemoveFormationRequestDTO implements RemoveFormationRequest
{

    /**
     * @var
     */
    public $formationId;

    /**
     * @param int $formationId
     */
    public function __construct($formationId)
    {
        $this->formationId = $formationId;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormationId()
    {
        return $this->formationId;
    }
}
