<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Gateways\User\GroupGateway;
use BusinessRules\Requestors\User\GetGroupRequest;
use BusinessRules\Responders\User\GroupResponse;
use BusinessRules\Responders\User\GroupResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetGroup implements UseCase
{

    /**
     * @var GroupGateway
     */
    private $groupGateway;

    /**
     * @var GroupResponseFactory
     */
    private $groupResponseFactory;

    /**
     * @param GetGroupRequest $request
     *
     * @return GroupResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $name = $request->getName();
        if (!empty($name)) {
            $group = $this->groupGateway->findByName($request->getName());
        } else {
            $group = $this->groupGateway->find($request->getId());
        }

        return $this->groupResponseFactory->createFromGroup($group);
    }

    /**
     * @param GroupGateway $groupGateway
     */
    public function setGroupGateway(GroupGateway $groupGateway)
    {
        $this->groupGateway = $groupGateway;
    }

    /**
     * @param GroupResponseFactory $groupResponseFactory
     */
    public function setGroupResponseFactory(GroupResponseFactory $groupResponseFactory)
    {
        $this->groupResponseFactory = $groupResponseFactory;
    }
}
