<?php

namespace BusinessRules\Entities\User;

use UserBundle\Entity\UserRoleImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserRoleStub1 extends UserRoleImpl
{
    const ID    = 1;
    const LABEL = 'Droit standard';
    const ROLE  = 'ROLE_USER';

    protected $id = self::ID;

    protected $label = self::LABEL;

    protected $role = self::ROLE;

}
