<?php

namespace BusinessRules\Responders\Student;

use BusinessRules\Entities\Student\Formation;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FormationResponseFactory
{
    /**
     * @param Formation $formation
     *
     * @return FormationResponse
     */
    public function createFromFormation(Formation $formation);

    /**
     * @param Formation[] $formations
     *
     * @return FormationResponse[]
     */
    public function createFromFormations($formations);
}
