<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\MeetingFactory;
use BusinessRules\Gateways\Student\MeetingGateway;
use BusinessRules\Requestors\Student\CreateMeetingRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateMeeting implements UseCase
{

    /**
     * @var MeetingFactory
     */
    private $meetingFactory;

    /**
     * @var MeetingGateway
     */
    private $meetingGateway;

    /**
     * @Transaction
     *
     * @param CreateMeetingRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $meeting = $this->meetingFactory->createFromRequest($request);

        $this->meetingGateway->insert($meeting);
    }

    /**
     * @param MeetingFactory $meetingFactory
     */
    public function setMeetingFactory(MeetingFactory $meetingFactory)
    {
        $this->meetingFactory = $meetingFactory;
    }

    /**
     * @param MeetingGateway $meetingGateway
     */
    public function setMeetingGateway(MeetingGateway $meetingGateway)
    {
        $this->meetingGateway = $meetingGateway;
    }
}
