<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Responders\Student\MeetingResponse;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MeetingResponseDTO implements MeetingResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var DateTime
     */
    public $plannedAt;

    /**
     * @var string
     */
    public $place;

    /**e
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlannedAt()
    {
        return $this->plannedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlace()
    {
        return $this->place;
    }
}
