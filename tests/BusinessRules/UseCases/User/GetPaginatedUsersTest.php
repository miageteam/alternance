<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub2;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\User\GetPaginatedUsersRequestBuilder;
use BusinessRules\UseCases\User\DTO\Request\GetPaginatedUsersRequestBuilderImpl;
use BusinessRules\UseCases\User\DTO\Response\UserResponseFactoryImpl;
use OpenClassrooms\UseCase\Application\Responder\PaginatedUseCaseResponseBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedUsersTest extends \PHPUnit_Framework_TestCase
{
    use AssertUserTrait;

    /**
     * @var GetPaginatedUsers
     */
    private $useCase;

    /**
     * @var GetPaginatedUsersRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     */
    public function WithoutUser_ReturnEmpty()
    {
        InMemoryUserGateway::$users = array();

        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->build()
        );

        $this->assertEmpty($response->getItems());
        $this->assertCount(0, $response->getItems());
        $this->assertEquals(0, $response->getTotalItems());
    }

    /**
     * @test
     */
    public function WithSomeUser_ReturnList()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->build()
        );

        $this->assertCount(1, $response->getItems());
        $this->assertEquals(1, $response->getTotalItems());
        $userStub = 'BusinessRules\Entities\User\UserStub';
        foreach ($response->getItems() as $key => $userResponse) {
            $this->assertUser($userStub . ($key + 1), $userResponse);
        }
    }

    /**
     * @test
     */
    public function WithPagination_ReturnPaginatedList()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withPage(1)
                ->withItemsPerPage(20)
                ->build()
        );

        $this->assertCount(1, $response->getItems());
        $this->assertEquals(1, $response->getTotalItems());
        foreach ($response->getItems() as $key => $userResponse) {
            $userStub = 'BusinessRules\Entities\User\UserStub';
            $this->assertUser($userStub . ($key + 1), $userResponse);
        }
    }

    protected function setUp()
    {
        $this->useCase = new GetPaginatedUsers();
        $this->useCase->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));
        $this->useCaseSetPaginatedUseCaseResponseBuilder();

        $this->requestBuilder = new GetPaginatedUsersRequestBuilderImpl();
    }

    /**
     * @return UserResponseFactoryImpl
     */
    private function useCaseSetPaginatedUseCaseResponseBuilder()
    {
        $userResponseFactory = new UserResponseFactoryImpl();
        $userResponseFactory->setPaginatedUseCaseResponseBuilder(new PaginatedUseCaseResponseBuilderImpl());
        $this->useCase->setUserResponseFactory($userResponseFactory);
    }
}
