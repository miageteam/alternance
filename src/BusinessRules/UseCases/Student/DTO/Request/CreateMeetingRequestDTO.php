<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\CreateMeetingRequest;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateMeetingRequestDTO implements CreateMeetingRequest
{

    /**
     * @var DateTime
     */
    public $plannedAt;

    /**
     * @var string
     */
    public $place;

    /**
     * @var int[]
     */
    public $participantIds;

    /**
     * @var DateTime
     */
    public $createdAt;

    /**
     * @var DateTime
     */
    public $updatedAt;

    /**
     * {@inheritdoc}
     */
    public function getPlannedAt()
    {
        return $this->plannedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * {@inheritdoc}
     */
    public function getParticipantIds()
    {
        return $this->participantIds;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
