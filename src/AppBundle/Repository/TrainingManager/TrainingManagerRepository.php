<?php

namespace AppBundle\Repository\TrainingManager;

use BusinessRules\Entities\TrainingManager\TrainingManager;
use BusinessRules\Gateways\TrainingManager\Exceptions\TrainingManagerNotFoundException;
use BusinessRules\Gateways\TrainingManager\TrainingManagerGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class TrainingManagerRepository extends EntityRepository implements TrainingManagerGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(TrainingManager $trainingManager)
    {
        $this->_em->persist($trainingManager);
    }

    /**
     * {@inheritdoc}
     */
    public function update(TrainingManager $trainingManager)
    {
        // do nothing
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        try {
            return $this->createQueryBuilder('tm')
                ->addSelect('u, p')
                ->leftJoin('tm.user', 'u')
                ->leftJoin('tm.promotion', 'p')
                ->andWhere('u.id = :id')
                ->setParameter('id', $userId)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new TrainingManagerNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByPromotionId($promotionId)
    {
        return $this->createQueryBuilder('tm')
            ->addSelect('u, p')
            ->leftJoin('tm.user', 'u')
            ->leftJoin('tm.promotion', 'p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $promotionId)
            ->getQuery()
            ->getResult();
    }
}
