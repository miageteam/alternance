<?php

namespace AppBundle\Entity\TrainingManager;

use BusinessRules\Entities\TrainingManager\TrainingManager;
use BusinessRules\Entities\TrainingManager\TrainingManagerFactory;
use BusinessRules\Gateways\Student\PromotionGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\TrainingManager\CreateTrainingManagerRequest;
use BusinessRules\Requestors\TrainingManager\EditTrainingManagerRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class TrainingManagerFactoryImpl implements TrainingManagerFactory
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var PromotionGateway
     */
    private $promotionGateway;

    /**
     * @param CreateTrainingManagerRequest $request
     *
     * @return TrainingManager
     */
    public function createFromRequest(CreateTrainingManagerRequest $request)
    {
        $user      = $this->userGateway->find($request->getUserId());
        $promotion = $this->promotionGateway->find($request->getPromotionId());

        $trainingManager = new TrainingManagerImpl();
        $trainingManager->setUser($user);
        $trainingManager->setPromotion($promotion);

        return $trainingManager;
    }

    /**
     * {@inheritdoc}
     */
    public function editFromRequest(TrainingManager $trainingManager, EditTrainingManagerRequest $request)
    {
        $promotion        = $this->getPromotionOrNull($request->getPromotionId());

        if (null !== $promotion) {
            $trainingManager->setPromotion($promotion);
        }

        return $trainingManager;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param PromotionGateway $promotionGateway
     */
    public function setPromotionGateway(PromotionGateway $promotionGateway)
    {
        $this->promotionGateway = $promotionGateway;
    }

    /**
     * @param int $promotionId
     *
     * @return Promotion|null
     */
    private function getPromotionOrNull($promotionId)
    {
        if (empty($promotionId)) {
            return null;
        }

        return $this->promotionGateway->find($promotionId);
    }
}
