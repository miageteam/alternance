<?php

namespace BusinessRules\Responders\TrainingManager;

use BusinessRules\Entities\TrainingManager\TrainingManager;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface TrainingManagerResponseFactory
{

    /**
     * @param TrainingManager $trainingManager
     *
     * @return TrainingManagerResponse
     */
    public function createFromTrainingManager(TrainingManager $trainingManager);

    /**
     * @param TrainingManager[] $trainingManagers
     *
     * @return TrainingManagerResponse[]
     */
    public function createFromTrainingManagers($trainingManagers);
}
