<?php

namespace BusinessRules\Responders\Student;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Entities\User\User;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserStudentResponse extends UseCaseResponse
{

    /**
     * @return int
     */
    public function getId();

    /**
     * @return User
     */
    public function getUser();

    /**
     * @return Promotion
     */
    public function getPromotion();

    /**
     * @return UserSchoolTutor
     */
    public function getUserSchoolTutor();

    /**
     * @return UserCompanyTutor
     */
    public function getUserCompanyTutor();
}
