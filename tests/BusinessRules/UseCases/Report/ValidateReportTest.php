<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Entities\Report\ReportStub1;
use BusinessRules\Gateways\Report\InMemoryReportGateway;
use BusinessRules\UseCases\Report\DTO\Request\ValidateReportRequestDTO;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ValidateReportTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var ValidateReport
     */
    private $useCase;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\Report\Exceptions\ReportNotFoundException
     */
    public function reportNotFound_ThrowException()
    {
        InMemoryReportGateway::$reports = array();
        $this->useCase->execute(new ValidateReportRequestDTO(ReportStub1::ID));
    }

    /**
     * @test
     */
    public function validateReport()
    {
        $this->useCase->execute(new ValidateReportRequestDTO(ReportStub1::ID));

        foreach (InMemoryReportGateway::$reports as $report) {
            $this->assertEquals(ReportStub1::TITLE, $report->getTitle());
            $this->assertTrue($report->isValidated());
        }

    }

    protected function setUp()
    {
        $this->useCase = new ValidateReport();
        $this->useCase->setReportGateway(new InMemoryReportGateway(array(ReportStub1::ID => new ReportStub1())));
    }
}
