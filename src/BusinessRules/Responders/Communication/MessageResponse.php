<?php

namespace BusinessRules\Responders\Communication;

use BusinessRules\Entities\Communication\Attachment;
use BusinessRules\Entities\User\User;
use DateTime;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MessageResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return User
     */
    public function getAuthor();

    /**
     * @return Attachment[]
     */
    public function getAttachments();

    /**
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * @return DateTime
     */
    public function getUpdatedAt();

}
