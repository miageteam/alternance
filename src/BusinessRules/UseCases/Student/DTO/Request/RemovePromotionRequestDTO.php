<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\RemovePromotionRequest;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class RemovePromotionRequestDTO implements RemovePromotionRequest
{

    /**
     * @var int
     */
    public $promotionId;

    /**
     * @param int $promotionId
     */
    public function __construct($promotionId)
    {
        $this->promotionId = $promotionId;
    }

    /**
     * @return int
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }
}
