<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Entities\Report\CommentFactory;
use BusinessRules\Gateways\Report\CommentGateway;
use BusinessRules\Gateways\Report\ReportGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Report\WriteCommentRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteComment implements UseCase
{

    /**
     * @var ReportGateway
     */
    private $reportGateway;

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var CommentFactory
     */
    private $commentFactory;

    /**
     * @var CommentGateway
     */
    private $commentGateway;

    /**
     * @Transaction
     *
     * @param WriteCommentRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $report = $this->reportGateway->find($request->getReportId());
        $author = $this->userGateway->find($request->getAuthorId());

        $comment = $this->commentFactory->create($request->getContent(), $author, $report);

        $this->commentGateway->insert($comment);
    }

    /**
     * @param ReportGateway $reportGateway
     */
    public function setReportGateway(ReportGateway $reportGateway)
    {
        $this->reportGateway = $reportGateway;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param CommentFactory $commentFactory
     */
    public function setCommentFactory(CommentFactory $commentFactory)
    {
        $this->commentFactory = $commentFactory;
    }

    /**
     * @param CommentGateway $commentGateway
     */
    public function setCommentGateway(CommentGateway $commentGateway)
    {
        $this->commentGateway = $commentGateway;
    }
}
