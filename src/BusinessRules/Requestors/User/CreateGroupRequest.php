<?php

namespace BusinessRules\Requestors\User;

use BusinessRules\Entities\User\UserRole;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateGroupRequest extends UseCaseRequest
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return UserRole[]
     */
    public function getUserRoles();
}
