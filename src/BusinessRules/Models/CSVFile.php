<?php

namespace BusinessRules\Models;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CSVFile extends \SplFileObject
{

    /**
     * @var string
     */
    private $filename;

    /**
     * @param string $filename
     *
     * @throws \Exception
     */
    public function __construct($filename)
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            throw new \Exception("Unable to read the file '$filename'");
        }

        parent::__construct($filename);
        $this->setFlags(FILE_SKIP_EMPTY_LINES);

        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $this->rewind();
        $data   = array();
        $header = null;
        while (!$this->eof()) {
            $currentLine = $this->getCurrentLine();
            if (null === $header) {
                $header = str_getcsv($currentLine);
            } else {
                if (!empty($currentLine)) {
                    $data[] = array_combine($header, str_getcsv($currentLine));
                }
            }
        }

        return $data;
    }
}
