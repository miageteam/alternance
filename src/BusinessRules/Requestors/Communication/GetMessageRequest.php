<?php

namespace BusinessRules\Requestors\Communication;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
interface GetMessageRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getId();
}
