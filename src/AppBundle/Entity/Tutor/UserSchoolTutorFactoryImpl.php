<?php

namespace AppBundle\Entity\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Entities\Tutor\UserSchoolTutorFactory;
use BusinessRules\Gateways\User\SchoolAddressGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Tutor\CreateSchoolTutorRequest;
use BusinessRules\Requestors\Tutor\EditSchoolTutorRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserSchoolTutorFactoryImpl implements UserSchoolTutorFactory
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * {@inheritdoc}
     */
    public function createFromRequest(CreateSchoolTutorRequest $request)
    {
        $user = $this->userGateway->find($request->getUserId());

        $userSchoolTutor = new UserSchoolTutorImpl();
        $userSchoolTutor->setUser($user);
        $userSchoolTutor->setCompanyName($request->getCompanyName());
        $userSchoolTutor->setStreet($request->getStreet());
        $userSchoolTutor->setZipCode($request->getZipCode());
        $userSchoolTutor->setCity($request->getCity());
        $userSchoolTutor->setCountry($request->getCountry());

        return $userSchoolTutor;
    }

    /**
     * {@inheritdoc}
     */
    public function editFromRequest(UserSchoolTutor $userSchoolTutor, EditSchoolTutorRequest $request)
    {
        $userSchoolTutor->setCompanyName($request->getCompanyName());
        $userSchoolTutor->setStreet($request->getStreet());
        $userSchoolTutor->setZipCode($request->getZipCode());
        $userSchoolTutor->setCity($request->getCity());
        $userSchoolTutor->setCountry($request->getCountry());

        return $userSchoolTutor;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }
}
