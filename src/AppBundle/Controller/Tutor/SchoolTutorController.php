<?php

namespace AppBundle\Controller\Tutor;

use AppBundle\Form\Model\EditSchoolTutorModel;
use AppBundle\Form\Model\UserSchoolTutorModel;
use AppBundle\Form\Type\EditSchoolTutorType;
use AppBundle\Form\Type\UserSchoolTutorType;
use BusinessRules\Gateways\Tutor\Exceptions\UserSchoolTutorNotFoundException;
use BusinessRules\Requestors\Tutor\EditSchoolTutorRequest;
use BusinessRules\Requestors\User\GetUserRequest;
use BusinessRules\UseCases\Tutor\DTO\Request\CreateSchoolTutorRequestDTO;
use BusinessRules\UseCases\Tutor\DTO\Request\GetSchoolTutorRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class SchoolTutorController extends Controller
{
    public function showAction($userId)
    {
        try {
            $schoolTutor = $this->get('use_cases.tutor.get_school_tutor')
                ->execute(new GetSchoolTutorRequestDTO($userId));

            return $this->render('AppBundle:Tutor\SchoolTutor:show.html.twig', array('schoolTutor' => $schoolTutor));
        } catch (UserSchoolTutorNotFoundException $e) {
            return $this->forward('AppBundle:Tutor/SchoolTutor:create', array('userId' => $userId));
        }
    }

    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function createAction(Request $request, $userId)
    {
        $form = $this->createForm(
            new UserSchoolTutorType(),
            null,
            array(
                'action' => $this->generateUrl('app_user_school_tutor_create', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var UserSchoolTutorModel $model */
            $model = $form->getData();

            $this->get('use_cases.tutor.create_school_tutor')
                ->execute($this->getCreateSchoolTutorRequest($model, $userId));

            $user = $this->get('use_cases.user.get_user')->execute($this->getUserRequest($userId));

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $user->getLoginIdentifier())
            );
        }

        return $this->render('AppBundle:Tutor/SchoolTutor:create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param UserSchoolTutorModel $model
     *
     * @return CreateSchoolTutorRequestDTO
     */
    private function getCreateSchoolTutorRequest(UserSchoolTutorModel $model, $userId)
    {
        return $this->get('request.tutor.create_school_tutor_request_builder')
            ->create()
            ->withUserId($userId)
            ->withCompanyName($model->companyName)
            ->withStreet($model->street)
            ->withZipCode($model->zipCode)
            ->withCity($model->city)
            ->withCountry($model->country)
            ->build();
    }

    /**
     * @param int $userId
     *
     * @return GetUserRequest
     */
    private function getUserRequest($userId)
    {
        return $this->get('request.user.get_user_request_builder')
            ->create()
            ->byId($userId)
            ->build();
    }


    public function getStudentsAction()
    {
        try {
            $schoolTutor = $this->get('use_cases.tutor.get_school_tutor')
                ->execute(new GetSchoolTutorRequestDTO($this->getUser()->getId()));

            $response = $this->get('use_cases.student.get_students')->execute(
                $this->get('request.student.get_students_request_builder')
                    ->create()
                    ->bySchoolTutorId($schoolTutor->getId())
                    ->build()
            );

            $students = $response->getUserStudents();
        } catch (UserSchoolTutorNotFoundException $e) {
            $students = array();
        }


        return $this->render(
            'AppBundle:Tutor/SchoolTutor/partials:get_students.html.twig',
            array('students' => $students)
        );
    }

    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function editAction(Request $request, $userId)
    {
        $schoolTutor = $this->get('use_cases.tutor.get_school_tutor')
            ->execute(new GetSchoolTutorRequestDTO($userId));

        $model              = new EditSchoolTutorModel();
        $model->companyName = $schoolTutor->getCompanyName();
        $model->street      = $schoolTutor->getStreet();
        $model->zipCode     = $schoolTutor->getZipCode();
        $model->city        = $schoolTutor->getCity();
        $model->country     = $schoolTutor->getCountry();

        $form = $this->createForm(
            new EditSchoolTutorType(),
            $model,
            array(
                'action' => $this->generateUrl('app_school_tutor_edit', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EditSchoolTutorModel $model */
            $model = $form->getData();

            $this->get('use_cases.tutor.edit_school_tutor')
                ->execute($this->getEditSchoolTutorRequest($model, $userId));

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $schoolTutor->getUser()->getLoginIdentifier())
            );
        }

        return $this->render('AppBundle:Tutor/SchoolTutor:edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param EditSchoolTutorModel $model
     * @param int                  $userId
     *
     * @return EditSchoolTutorRequest
     */
    private function getEditSchoolTutorRequest(EditSchoolTutorModel $model, $userId)
    {
        return $this->get('request.tutor.edit_school_tutor_request_builder')
            ->create()
            ->withUserId($userId)
            ->withCompanyName($model->companyName)
            ->withStreet($model->street)
            ->withZipCode($model->zipCode)
            ->withCity($model->city)
            ->withCountry($model->country)
            ->build();
    }
}