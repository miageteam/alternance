<?php

namespace BusinessRules\Requestors\Communication;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
interface EditMessageRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getMessageId();

    /**
     * @return string
     */
    public function getContent();

}
