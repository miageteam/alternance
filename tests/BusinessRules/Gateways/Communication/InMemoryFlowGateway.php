<?php

namespace BusinessRules\Gateways\Communication;

use BusinessRules\Entities\Communication\Flow;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Communication\Exceptions\FlowNotFoundException;
use OpenClassrooms\UseCase\Application\Entity\PaginatedCollectionBuilderImpl;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollectionBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryFlowGateway implements FlowGateway
{

    /**
     * @var Flow[]
     */
    public static $flows = array();

    /**
     * @var int
     */
    public static $id;

    /**
     * @var PaginatedCollectionBuilder
     */
    private $paginatedCollectionBuilder;

    /**
     * @param Flow[] $flows
     */
    public function __construct(array $flows = array())
    {
        self::$flows = $flows;
        self::$id    = 0;
        $this->paginatedCollectionBuilder = new PaginatedCollectionBuilderImpl();
    }

    /**
     * @param Flow $flow
     */
    public function insert(Flow $flow)
    {
        self::$flows[++self::$id] = $flow;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$flows[$id])) {
            throw new FlowNotFoundException();
        }

        return self::$flows[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        $items = array();
        foreach (self::$flows as $flow) {
                $items[] = $flow;
        }

        return $this->paginatedCollectionBuilder
            ->create()
            ->withItems($items)
            ->withTotalItems(count($items))
            ->build();
    }
}
