<?php

namespace BusinessRules\Requestors\User;

use BusinessRules\Requestors\User\Exceptions\InvalidCreateUserRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateUserRequestBuilder
{
    /**
     * @return CreateUserRequestBuilder
     */
    public function create();

    /**
     * @param string $firstName
     *
     * @return CreateUserRequestBuilder
     */
    public function withFirstName($firstName);

    /**
     * @param string $lastName
     *
     * @return CreateUserRequestBuilder
     */
    public function withLastName($lastName);

    /**
     * @param string $email
     *
     * @return CreateUserRequestBuilder
     */
    public function withEmail($email);

    /**
     * @param string $phoneNumber
     *
     * @return CreateUserRequestBuilder
     */
    public function withPhoneNumber($phoneNumber);

    /**
     * @param int $groupId
     *
     * @return CreateUserRequestBuilder
     */
    public function withGroupId($groupId);

    /**
     * @return CreateUserRequest
     *
     * @throws InvalidCreateUserRequestParameterException
     */
    public function build();
}
