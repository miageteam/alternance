<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Gateways\Report\ReportGateway;
use BusinessRules\Requestors\Report\ValidateReportRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ValidateReport implements UseCase
{

    /**
     * @var ReportGateway
     */
    private $reportGateway;

    /**
     * @Transaction
     *
     * @param ValidateReportRequest $useCaseRequest
     *
     * @return UseCaseResponse
     */
    public function execute(UseCaseRequest $useCaseRequest)
    {
        $report = $this->reportGateway->find($useCaseRequest->getReportId());

        $report->setValidated(true);

        $this->reportGateway->update($report);
    }

    /**
     * @param ReportGateway $reportGateway
     */
    public function setReportGateway(ReportGateway $reportGateway)
    {
        $this->reportGateway = $reportGateway;
    }
}
