<?php

namespace BusinessRules\UseCases\Report\DTO\Response;

use BusinessRules\Entities\Report\Attachment;
use BusinessRules\Entities\User\User;
use BusinessRules\Responders\Report\CommentResponse;
use BusinessRules\Responders\Report\CommentsResponse;
use BusinessRules\Responders\Report\ReportResponse;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReportResponseDTO implements ReportResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var CommentResponse[]
     */
    public $comments;

    /**
     * @var int
     */
    public $totalComment;

    /**
     * @var User
     */
    public $author;

    /**
     * @var Attachment
     */
    public $attachment;

    /**
     * @var DateTime
     */
    public $createdAt;

    /**
     * @var DateTime
     */
    public $updatedAt;

    /**
     * @var bool
     */
    public $validated;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return int
     */
    public function getTotalComment()
    {
        return $this->totalComment;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function isValidated()
    {
        return $this->validated;
    }
}
