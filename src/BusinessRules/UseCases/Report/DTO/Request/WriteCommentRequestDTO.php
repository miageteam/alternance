<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\WriteCommentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteCommentRequestDTO implements WriteCommentRequest
{

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $authorId;

    /**
     * @var int
     */
    public $reportId;

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * {@inheritdoc}
     */
    public function getReportId()
    {
        return $this->reportId;
    }
}
