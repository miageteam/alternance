<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Entities\Student\Formation;
use BusinessRules\Responders\Student\FormationResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationResponseFactoryImpl implements FormationResponseFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromFormation(Formation $formation)
    {
        $response       = new FormationResponseDTO();
        $response->id   = $formation->getId();
        $response->name = $formation->getName();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromFormations($formations)
    {
        $formationsResponses = array();
        foreach ($formations as $formation) {
            $formationsResponses[] = $this->createFromFormation($formation);
        }

        return $formationsResponses;
    }
}
