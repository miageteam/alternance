<?php

namespace BusinessRules\UseCases\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutorFactory;
use BusinessRules\Gateways\Tutor\UserCompanyTutorGateway;
use BusinessRules\Requestors\Tutor\CreateCompanyTutorRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateCompanyTutor implements UseCase
{

    /**
     * @var UserCompanyTutorFactory
     */
    private $userCompanyTutorFactory;

    /**
     * @var UserCompanyTutorGateway
     */
    private $userCompanyTutorGateway;

    /**
     * @Transaction
     *
     * @param CreateCompanyTutorRequest $request
     *
     * @return UseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $userCompanyTutor = $this->userCompanyTutorFactory->createFromRequest($request);

        $this->userCompanyTutorGateway->insert($userCompanyTutor);
    }

    /**
     * @param UserCompanyTutorFactory $userCompanyTutorFactory
     */
    public function setUserCompanyTutorFactory(UserCompanyTutorFactory $userCompanyTutorFactory)
    {
        $this->userCompanyTutorFactory = $userCompanyTutorFactory;
    }

    /**
     * @param UserCompanyTutorGateway $userCompanyTutorGateway
     */
    public function setUserCompanyTutorGateway(UserCompanyTutorGateway $userCompanyTutorGateway)
    {
        $this->userCompanyTutorGateway = $userCompanyTutorGateway;
    }
}
