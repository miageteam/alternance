<?php

namespace BusinessRules\Entities\Tutor;

use AppBundle\Entity\Tutor\UserSchoolTutorImpl;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub3;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserSchoolTutorStub1 extends UserSchoolTutorImpl
{
    const ID          = 1;
    const COMPANYNAME = 'Miage';
    const STREET      = '20 rue du moulin';
    const ZIPCODE     = '80000';
    const CITY        = 'Amiens';
    const COUNTRY     = 'France';

    public function __construct()
    {
        $this->user     = new UserStub3();
        $this->students = array(new UserStub1());
    }

    protected $id = self::ID;

    protected $companyName = self::COMPANYNAME;

    protected $street = self::STREET;

    protected $zipCode = self::ZIPCODE;

    protected $city = self::CITY;

    protected $country = self::COUNTRY;

}
