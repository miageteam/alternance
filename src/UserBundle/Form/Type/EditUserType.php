<?php

namespace UserBundle\Form\Type;

use BusinessRules\Entities\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use UserBundle\Form\Model\EditUserModel;
use UserBundle\Repository\GroupRepository;

/**
 * @author Kevin LECONTE <arnaud.h.lefevre@gmail.com>
 */
class EditUserType extends AbstractType
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('email', 'email')
            ->add('phoneNumber')
            ->add('password', 'password', array('required' => false));

        if ($user->isAdmin()) {
            $builder->add(
                'group',
                'entity',
                array(
                    'class'       => 'UserBundle\Entity\GroupImpl',
                    'placeholder' => ''
                )
            );
        } else {
            $builder->add(
                'group',
                'entity',
                array(
                    'class'         => 'UserBundle\Entity\GroupImpl',
                    'query_builder' => function (GroupRepository $er) {
                        return $er->getAllWithoutAdminAndSecretaryQueryBuilder();
                    },
                    'placeholder'   => ''
                )
            );
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Form\Model\EditUserModel',
                'intention'  => 'edit_user',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'edit_user';
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
}
