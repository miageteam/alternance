<?php

namespace BusinessRules\Gateways\Report;

use BusinessRules\Entities\Report\Report;
use BusinessRules\Gateways\Report\Exceptions\ReportNotFoundException;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface ReportGateway
{
    /**
     * @param Report $report
     */
    public function insert(Report $report);

    /**
     * @param int $id
     *
     * @return Report
     *
     * @throws ReportNotFoundException
     */
    public function find($id);

    /**
     * @param array $filters
     * @param array $sorts
     * @param array $pagination
     *
     * @return PaginatedCollection
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array());

    /**
     * @param Report $report
     */
    public function update(Report $report);
}
