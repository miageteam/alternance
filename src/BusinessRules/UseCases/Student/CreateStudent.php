<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\UserStudentFactory;
use BusinessRules\Gateways\Student\UserStudentGateway;
use BusinessRules\Requestors\Student\CreateStudentRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateStudent implements UseCase
{

    /**
     * @var UserStudentFactory
     */
    private $userStudentFactory;

    /**
     * @var UserStudentGateway
     */
    private $userStudentGateway;

    /**
     * @Transaction
     *
     * @param CreateStudentRequest $request
     *
     * @return UseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $userStudent = $this->userStudentFactory->createFromRequest($request);

        $this->userStudentGateway->insert($userStudent);
    }

    /**
     * @param UserStudentFactory $userStudentFactory
     */
    public function setUserStudentFactory(UserStudentFactory $userStudentFactory)
    {
        $this->userStudentFactory = $userStudentFactory;
    }

    /**
     * @param UserStudentGateway $userStudentGateway
     */
    public function setUserStudentGateway(UserStudentGateway $userStudentGateway)
    {
        $this->userStudentGateway = $userStudentGateway;
    }
}
