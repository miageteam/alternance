<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Request;

use BusinessRules\Requestors\TrainingManager\GetTrainingManagerRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetTrainingManagerRequestBuilderImpl implements GetTrainingManagerRequestBuilder
{

    /**
     * @var GetTrainingManagerRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetTrainingManagerRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
