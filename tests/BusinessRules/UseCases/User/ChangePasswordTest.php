<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Services\Security\PasswordEncoder;
use BusinessRules\UseCases\User\DTO\Request\ChangePasswordRequestDTO;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ChangePasswordTest extends \PHPUnit_Framework_TestCase
{
    const NEW_PASSWORD = 'New password';

    /**
     * @var ChangePassword
     */
    private $useCase;

    /**
     * @var PasswordEncoder
     */
    private $encoder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function userNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();
        $this->useCase->execute(new ChangePasswordRequestDTO(UserStub1::ID, self::NEW_PASSWORD));
    }

    /**
     * @test
     */
    public function ChangePassword()
    {
        $this->useCase->execute(new ChangePasswordRequestDTO(UserStub1::ID, self::NEW_PASSWORD));

        $actual = InMemoryUserGateway::$users[UserStub1::ID];
        $this->assertNotEquals(UserStub1::PASSWORD, $actual->getPassword());
        $this->assertEquals($this->getNewPasswordEncoded(), $actual->getPassword());
    }

    /**
     * @return string
     */
    private function getNewPasswordEncoded()
    {
        return $this->encoder->encodePassword(self::NEW_PASSWORD, UserStub1::SALT);
    }

    protected function setUp()
    {
        $this->useCase = new ChangePassword();
        $this->useCase->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));
        $this->encoder = new PasswordEncoder();
        $this->useCase->setEncoder($this->encoder);
    }
}
