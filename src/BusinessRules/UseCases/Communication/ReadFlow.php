<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Gateways\Communication\FlowGateway;
use BusinessRules\Requestors\Communication\ReadFlowRequest;
use BusinessRules\Responders\Communication\FlowResponse;
use BusinessRules\Responders\Communication\FlowResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReadFlow implements UseCase
{

    /**
     * @var FlowGateway
     */
    private $flowGateway;

    /**
     * @var FlowResponseFactory
     */
    private $flowResponseFactory;

    /**
     * @param ReadFlowRequest $request
     *
     * @return FlowResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $flow = $this->flowGateway->find($request->getFlowId());

        return $this->flowResponseFactory->createFromFlow($flow);
    }

    /**
     * @param FlowGateway $flowGateway
     */
    public function setFlowGateway(FlowGateway $flowGateway)
    {
        $this->flowGateway = $flowGateway;
    }

    /**
     * @param FlowResponseFactory $flowResponseFactory
     */
    public function setFlowResponseFactory(FlowResponseFactory $flowResponseFactory)
    {
        $this->flowResponseFactory = $flowResponseFactory;
    }
}
