<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\Exceptions\InvalidWriteCommentRequestParameterException;
use BusinessRules\Requestors\Report\WriteCommentRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteCommentRequestBuilderImpl implements WriteCommentRequestBuilder
{

    /**
     * @var WriteCommentRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new WriteCommentRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withReportId($reportId)
    {
        $this->request->reportId = $reportId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withContent($content)
    {
        $this->request->content = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAuthorId($authorId)
    {
        $this->request->authorId = $authorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $this->CheckIfParameterIsValid();

        return $this->request;
    }

    private function CheckIfParameterIsValid()
    {
        if (empty($this->request->reportId)) {
            throw new InvalidWriteCommentRequestParameterException('The argument "ReportId" must be defined.');
        }

        if (empty($this->request->content)) {
            throw new InvalidWriteCommentRequestParameterException('The argument "Content" must be defined.');
        }

        if (empty($this->request->authorId)) {
            throw new InvalidWriteCommentRequestParameterException('The argument "AuthorId" must be defined.');
        }
    }
}
