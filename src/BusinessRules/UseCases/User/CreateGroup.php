<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\GroupFactory;
use BusinessRules\Gateways\User\GroupGateway;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateGroup implements UseCase
{

    /**
     * @var GroupFactory
     */
    private $groupFactory;

    /**
     * @var GroupGateway
     */
    private $groupGateway;

    /**
     * @Transaction
     *
     * @param UseCaseRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $group = $this->groupFactory->createFromRequest($request);

        $this->groupGateway->insert($group);
    }

    /**
     * @param GroupFactory $groupFactory
     */
    public function setGroupFactory(GroupFactory $groupFactory)
    {
        $this->groupFactory = $groupFactory;
    }

    /**
     * @param GroupGateway $groupGateway
     */
    public function setGroupGateway(GroupGateway $groupGateway)
    {
        $this->groupGateway = $groupGateway;
    }
}
