<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface EditStudentRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return int
     */
    public function getPromotionId();

    /**
     * @return int
     */
    public function getUserSchoolTutorId();

    /**
     * @return int
     */
    public function getUserCompanyTutorId();
}
