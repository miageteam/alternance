<?php

namespace BusinessRules\Requestors\Tutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateSchoolTutorRequestBuilder
{
    /**
     * @return CreateSchoolTutorRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return CreateSchoolTutorRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param string $companyName
     *
     * @return CreateSchoolTutorRequestBuilder
     */
    public function withCompanyName($companyName);

    /**
     * @param string $street
     *
     * @return CreateSchoolTutorRequestBuilder
     */
    public function withStreet($street);

    /**
     * @param string $zipCode
     *
     * @return CreateSchoolTutorRequestBuilder
     */
    public function withZipCode($zipCode);

    /**
     * @param string $city
     *
     * @return CreateSchoolTutorRequestBuilder
     */
    public function withCity($city);

    /**
     * @param string $country
     *
     * @return CreateSchoolTutorRequestBuilder
     */
    public function withCountry($country);

    /**
     * @return CreateSchoolTutorRequest
     */
    public function build();
}
