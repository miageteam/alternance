<?php

namespace CommunicationBundle\Form\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateResponseModel
{

    /**
     * @var UploadedFile
     *
     * @Assert\File()
     */
    public $attachment;

    /**
     * @var string
     */
    public $attachmentPath;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $content;

    public function getAbsolutePath()
    {
        return null === $this->attachmentPath ? null : $this->getUploadRootDir() . '/' . $this->attachmentPath;
    }

    public function getWebPath()
    {
        return null === $this->attachmentPath ? null : $this->getUploadDir() . '/' . $this->attachmentPath;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/communication/attachments';
    }

    public function upload()
    {
        if (null === $this->attachment) {
            return;
        }

        $name = sha1(uniqid(mt_rand(), true)).'.'.$this->attachment->guessExtension();

        $this->attachment->move($this->getUploadRootDir(), $name);

        $this->attachmentPath = $name;

        $this->attachment = null;
    }
}
