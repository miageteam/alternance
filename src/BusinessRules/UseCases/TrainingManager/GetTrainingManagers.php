<?php

namespace BusinessRules\UseCases\TrainingManager;

use BusinessRules\Gateways\TrainingManager\TrainingManagerGateway;
use BusinessRules\Requestors\TrainingManager\GetTrainingManagersRequest;
use BusinessRules\Responders\TrainingManager\TrainingManagerResponseFactory;
use BusinessRules\Responders\TrainingManager\TrainingManagersResponse;
use BusinessRules\UseCases\TrainingManager\DTO\Response\TrainingManagersResponseDTO;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetTrainingManagers implements UseCase
{

    /**
     * @var TrainingManagerGateway
     */
    private $trainingManagerGateway;

    /**
     * @var TrainingManagerResponseFactory
     */
    private $trainingManagerResponseFactory;

    /**
     * @param GetTrainingManagersRequest $request
     *
     * @return TrainingManagersResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $trainingManagers = $this->trainingManagerGateway->findAllByPromotionId($request->getPromotionId());

        return new TrainingManagersResponseDTO(
            $this->trainingManagerResponseFactory->createFromTrainingManagers($trainingManagers)
        );
    }

    /**
     * @param TrainingManagerGateway $trainingManagerGateway
     */
    public function setTrainingManagerGateway(TrainingManagerGateway $trainingManagerGateway)
    {
        $this->trainingManagerGateway = $trainingManagerGateway;
    }

    /**
     * @param TrainingManagerResponseFactory $trainingManagerResponseFactory
     */
    public function setTrainingManagerResponseFactory(TrainingManagerResponseFactory $trainingManagerResponseFactory)
    {
        $this->trainingManagerResponseFactory = $trainingManagerResponseFactory;
    }
}
