<?php

namespace UserBundle\Validator\Constraints;

use BusinessRules\Gateways\User\Exceptions\GroupNotFoundException;
use BusinessRules\Requestors\User\GetGroupRequestBuilder;
use BusinessRules\UseCases\User\GetGroup;
use BusinessRules\UseCases\User\GetUser;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupNameUniqueValidator extends ConstraintValidator
{

    /**
     * @var GetGroup
     */
    private $getGroup;

    /**
     * @var GetGroupRequestBuilder
     */
    private $getGroupRequestBuilder;

    /**
     * @param GetUser                $getGroup
     * @param GetGroupRequestBuilder $getGroupRequestBuilder
     */
    public function __construct(UseCase $getGroup, GetGroupRequestBuilder $getGroupRequestBuilder)
    {
        $this->getGroup               = $getGroup;
        $this->getGroupRequestBuilder = $getGroupRequestBuilder;
    }


    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof GroupNameUnique) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\GroupNameUnique');
        }

        try {
            $request = $this->getGroupRequestBuilder
                ->create()
                ->byName(ucfirst($value))
                ->build();

            $group = $this->getGroup->execute($request);

            if (!empty($group)) {
                $this->context->addViolation($constraint->message);
            }
        } catch (GroupNotFoundException $unfe) {
        }
    }
}
