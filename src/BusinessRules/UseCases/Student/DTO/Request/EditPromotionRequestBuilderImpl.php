<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\EditPromotionRequest;
use BusinessRules\Requestors\Student\EditPromotionRequestBuilder;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class EditPromotionRequestBuilderImpl implements EditPromotionRequestBuilder
{

    /**
     * @var EditPromotionRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new EditPromotionRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPromotionId($promotionId)
    {
        $this->request->promotionId = $promotionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withFormationId($formationId)
    {
        $this->request->formationId = $formationId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withYear($year)
    {
        $this->request->year = $year;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAlternationCalendarPath($alternationCalendar)
    {
        $this->request->alternationCalendarPath = $alternationCalendar;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
