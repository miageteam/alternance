<?php

namespace BusinessRules\Responders\Report;

use BusinessRules\Entities\Report\Attachment;
use BusinessRules\Entities\User\User;
use DateTime;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface ReportResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return CommentResponse[]
     */
    public function getComments();

    /**
     * @return User
     */
    public function getAuthor();

    /**
     * @return Attachment
     */
    public function getAttachment();

    /**
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * @return DateTime
     */
    public function getUpdatedAt();

    /**
     * @return bool
     */
    public function isValidated();
}
