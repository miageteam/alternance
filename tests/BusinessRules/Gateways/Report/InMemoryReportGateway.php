<?php

namespace BusinessRules\Gateways\Report;

use BusinessRules\Entities\Report\Report;
use BusinessRules\Gateways\Report\Exceptions\ReportNotFoundException;
use OpenClassrooms\UseCase\Application\Entity\PaginatedCollectionBuilderImpl;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollectionBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryReportGateway implements ReportGateway
{

    /**
     * @var Report[]
     */
    public static $reports = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @var PaginatedCollectionBuilder
     */
    private $paginatedCollectionBuilder;

    /**
     * @param Report[] $report
     */
    public function __construct(array $report = array())
    {
        self::$reports                    = $report;
        self::$id                         = 0;
        $this->paginatedCollectionBuilder = new PaginatedCollectionBuilderImpl();
    }

    /**
     * {@inheritdoc}
     */
    public function insert(Report $report)
    {
        self::$reports[++self::$id] = $report;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$reports[$id])) {
            throw new ReportNotFoundException();
        }

        return self::$reports[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        $items = array();

        foreach (self::$reports as $report) {
            $items[] = $report;
        }

        return $this->paginatedCollectionBuilder
            ->create()
            ->withItems($items)
            ->withTotalItems(count($items))
            ->build();
    }

    /**
     * @param Report $report
     */
    public function update(Report $report)
    {
        self::$reports[$report->getId()] = $report;
    }
}
