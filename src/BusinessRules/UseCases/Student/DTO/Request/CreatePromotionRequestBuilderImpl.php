<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\CreatePromotionRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreatePromotionRequestBuilderImpl implements CreatePromotionRequestBuilder
{

    /**
     * @var CreatePromotionRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new CreatePromotionRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withYear($year)
    {
        $this->request->year = $year;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withFormationId($formationId)
    {
        $this->request->formationId = $formationId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAlternationCalendarPath($alternationCalendarPath)
    {
        $this->request->alternationCalendarPath = $alternationCalendarPath;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
