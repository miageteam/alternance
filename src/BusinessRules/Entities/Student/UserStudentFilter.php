<?php

namespace BusinessRules\Entities\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class UserStudentFilter
{
    const PROMOTION_ID = 'promotion';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
