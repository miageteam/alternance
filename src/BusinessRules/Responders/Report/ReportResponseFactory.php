<?php

namespace BusinessRules\Responders\Report;

use BusinessRules\Entities\Report\Report;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface ReportResponseFactory extends PaginatedUseCaseResponseFactory
{
    /**
     * @param Report $report
     *
     * @return ReportResponse
     */
    public function createFromReport(Report $report);

    /**
     * @param Report[] $reports
     *
     * @return ReportResponse[]
     */
    public function createFromReports($reports);
}
