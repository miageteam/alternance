<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Gateways\Report\CommentGateway;
use BusinessRules\Requestors\Report\EditCommentRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditComment implements UseCase
{

    /**
     * @var CommentGateway
     */
    private $commentGateway;

    /**
     * @Transaction
     *
     * @param EditCommentRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $comment = $this->commentGateway->find($request->getCommentId());

        $comment->setContent($request->getContent());
        $comment->setUpdatedAt(new \DateTime());

        $this->commentGateway->update($comment);
    }

    /**
     * @param CommentGateway $commentGateway
     */
    public function setCommentGateway(CommentGateway $commentGateway)
    {
        $this->commentGateway = $commentGateway;
    }
}
