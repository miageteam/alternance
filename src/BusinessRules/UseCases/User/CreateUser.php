<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\UserFactory;
use BusinessRules\Gateways\User\Exceptions\EmailAlreadyExistsException;
use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\User\CreateUserRequest;
use BusinessRules\Responders\User\UserResponse;
use BusinessRules\Responders\User\UserResponseFactory;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateUser implements UseCase
{

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var UserResponseFactory
     */
    private $userResponseFactory;

    /**
     * @Transaction
     *
     * @param CreateUserRequest $request
     *
     * @return UserResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $this->checkEmailIsAvailable($request);
        $user = $this->userFactory->createFromRequest($request);

        $this->userGateway->insert($user);

        return $this->userResponseFactory->createFromUser($user);
    }

    /**
     * @param CreateUserRequest $request
     */
    private function checkEmailIsAvailable(CreateUserRequest $request)
    {
        try {
            $this->userGateway->findByEmail($request->getEmail());

            throw new EmailAlreadyExistsException();
        } catch (UserNotFoundException $e) {
        }
    }

    /**
     * @param UserFactory $userFactory
     */
    public function setUserFactory(UserFactory $userFactory)
    {
        $this->userFactory = $userFactory;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param UserResponseFactory $userResponseFactory
     */
    public function setUserResponseFactory(UserResponseFactory $userResponseFactory)
    {
        $this->userResponseFactory = $userResponseFactory;
    }
}
