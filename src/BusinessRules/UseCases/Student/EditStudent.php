<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\UserStudentFactory;
use BusinessRules\Gateways\Student\UserStudentGateway;
use BusinessRules\Requestors\Student\EditStudentRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditStudent implements UseCase
{

    /**
     * @var UserStudentGateway
     */
    private $userStudentGateway;

    /**
     * @var UserStudentFactory
     */
    private $userStudentFactory;

    /**
     * @Transaction
     *
     * @param EditStudentRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $student = $this->userStudentGateway->findByUserId($request->getUserId());

        $updatedStudent = $this->userStudentFactory->editFromRequest($student, $request);

        $this->userStudentGateway->update($updatedStudent);
    }

    /**
     * @param UserStudentGateway $userStudentGateway
     */
    public function setUserStudentGateway(UserStudentGateway $userStudentGateway)
    {
        $this->userStudentGateway = $userStudentGateway;
    }

    /**
     * @param UserStudentFactory $userStudentFactory
     */
    public function setUserStudentFactory(UserStudentFactory $userStudentFactory)
    {
        $this->userStudentFactory = $userStudentFactory;
    }
}
