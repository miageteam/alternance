<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\ValidateReportRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ValidateReportRequestDTO implements ValidateReportRequest
{

    /**
     * @var int
     */
    public $reportId;

    /**
     * @param int $reportId
     */
    public function __construct($reportId)
    {
        $this->reportId = $reportId;
    }

    /**
     * @return int
     */
    public function getReportId()
    {
        return $this->reportId;
    }
}
