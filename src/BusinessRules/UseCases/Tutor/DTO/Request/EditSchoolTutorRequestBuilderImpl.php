<?php

namespace BusinessRules\UseCases\Tutor\DTO\Request;

use BusinessRules\Requestors\Tutor\EditSchoolTutorRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditSchoolTutorRequestBuilderImpl implements EditSchoolTutorRequestBuilder
{

    /**
     * @var EditSchoolTutorRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new EditSchoolTutorRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withCompanyName($companyName)
    {
        $this->request->companyName = $companyName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withStreet($street)
    {
        $this->request->street = $street;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withZipCode($zipCode)
    {
        $this->request->zipCode = $zipCode;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withCity($city)
    {
        $this->request->city = $city;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withCountry($country)
    {
        $this->request->country = $country;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
