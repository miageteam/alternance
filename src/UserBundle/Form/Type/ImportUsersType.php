<?php

namespace UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ImportUsersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('csvFile')
            ->add('promotion', 'entity', array(
                'empty_value' => '',
                'class' => 'AppBundle:Student\PromotionImpl'
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Form\Model\ImportUsersModel',
                'intention'  => 'import_users',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'import_users';
    }
}
