<?php

namespace BusinessRules\Responders\Student;

use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FormationsResponse extends UseCaseResponse
{
    /**
     * @return FormationResponse[]
     */
    public function getFormations();
}
