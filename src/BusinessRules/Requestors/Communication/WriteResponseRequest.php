<?php

namespace BusinessRules\Requestors\Communication;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteResponseRequest extends MessageRequestInterface
{
    /**
     * @return int
     */
    public function getFlowId();
}
