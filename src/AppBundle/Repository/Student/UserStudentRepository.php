<?php

namespace AppBundle\Repository\Student;

use AppBundle\Service\Pagination\PaginatedCollectionFactory;
use BusinessRules\Entities\Student\UserStudent;
use BusinessRules\Entities\Student\UserStudentFilter;
use BusinessRules\Entities\Student\UserStudentSort;
use BusinessRules\Gateways\Student\Exceptions\UserStudentNotFoundException;
use BusinessRules\Gateways\Student\UserStudentGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserStudentRepository extends EntityRepository implements UserStudentGateway
{

    /**
     * @var PaginatedCollectionFactory
     */
    private $paginatedCollectionFactory;

    /**
     * @param PaginatedCollectionFactory $paginatedCollectionFactory
     */
    public function setPaginatedCollectionFactory(PaginatedCollectionFactory $paginatedCollectionFactory)
    {
        $this->paginatedCollectionFactory = $paginatedCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(UserStudent $userStudent)
    {
        $this->_em->persist($userStudent);
    }

    /**
     * {@inheritdoc}
     */
    public function update(UserStudent $userStudent)
    {
        // do nothing
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        try {
            return $this->createQueryBuilder('us')
                ->addSelect('u, p, uct, ust')
                ->leftJoin('us.user', 'u')
                ->leftJoin('us.promotion', 'p')
                ->leftJoin('us.userCompanyTutor', 'uct')
                ->leftJoin('us.userSchoolTutor', 'ust')
                ->andWhere('u.id = :id')
                ->setParameter('id', $userId)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new UserStudentNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findByCompanyTutorId($CompanyTutorId)
    {
        try {
            return $this->createQueryBuilder('us')
                ->addSelect('u, p, uct, ust')
                ->leftJoin('us.user', 'u')
                ->leftJoin('us.promotion', 'p')
                ->leftJoin('us.userCompanyTutor', 'uct')
                ->leftJoin('us.userSchoolTutor', 'ust')
                ->andWhere('uct.id = :id')
                ->setParameter('id', $CompanyTutorId)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new UserStudentNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findAllBySchoolTutor($schoolTutorId)
    {
        return $this->createQueryBuilder('us')
            ->addSelect('u, p, uct, ust')
            ->leftJoin('us.user', 'u')
            ->leftJoin('us.promotion', 'p')
            ->leftJoin('us.userCompanyTutor', 'uct')
            ->leftJoin('us.userSchoolTutor', 'ust')
            ->andWhere('ust.id = :id')
            ->setParameter('id', $schoolTutorId)
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        $queryBuilder = $this->createQueryBuilder('us')
            ->addSelect('u, p')
            ->leftJoin('us.user', 'u')
            ->leftJoin('us.promotion', 'p');

        $filteredQueryBuilder       = $this->buildFilters($queryBuilder, $filters);
        $sortedFilteredQueryBuilder = $this->buildSorts($filteredQueryBuilder, $sorts);

        return $this->paginatedCollectionFactory->make($sortedFilteredQueryBuilder, $pagination);
    }

    /**
     * @return QueryBuilder
     */
    private function buildFilters(QueryBuilder $queryBuilder, array $filters = array())
    {
        if (isset($filters[UserStudentFilter::PROMOTION_ID])) {
            $queryBuilder->andWhere('a = :user');
            $queryBuilder->setParameter('user', $filters[UserStudentFilter::PROMOTION_ID]);
        }

        return $queryBuilder;
    }

    /**
     * @return QueryBuilder
     */
    private function buildSorts(QueryBuilder $queryBuilder, array $sorts = array())
    {
        if (isset($sorts[UserStudentSort::LAST_NAME])) {
            $queryBuilder->addOrderBy('u.' . UserStudentSort::LAST_NAME, $sorts[UserStudentSort::LAST_NAME]);
        }

        return $queryBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByPromotionId($promotionId)
    {
        return $this->createQueryBuilder('us')
            ->addSelect('u, p')
            ->leftJoin('us.user', 'u')
            ->leftJoin('us.promotion', 'p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $promotionId)
            ->getQuery()
            ->getResult();
    }
}
