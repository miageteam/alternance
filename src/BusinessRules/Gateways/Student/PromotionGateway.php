<?php

namespace BusinessRules\Gateways\Student;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Gateways\Student\Exceptions\PromotionAlreadyExistException;
use BusinessRules\Gateways\Student\Exceptions\PromotionNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface PromotionGateway
{
    /**
     * @param Promotion $promotion
     *
     * @throws PromotionAlreadyExistException
     */
    public function insert(Promotion $promotion);

    /**
     * @param int $id
     *
     * @return Promotion
     *
     * @throws PromotionNotFoundException
     */
    public function find($id);

    /**
     * @param Promotion $promotion
     */
    public function remove(Promotion $promotion);

    /**
     * @return Promotion[]
     */
    public function findAll();

    /**
     * @param Promotion $promotion
     */
    public function update(Promotion $promotion);
}
