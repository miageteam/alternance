<?php

namespace BusinessRules\Entities\Communication;

use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FlowFactory
{
    /**
     * @param string $title
     * @param User   $author
     * @param User[]  $receivers
     *
     * @return Flow
     */
    public function create($title, User $author, array $receivers);
}
