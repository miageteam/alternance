<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\PromotionGateway;
use BusinessRules\Requestors\Student\GetPromotionsRequest;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use BusinessRules\Responders\Student\PromotionResponseFactory;
use BusinessRules\UseCases\Student\DTO\Response\PromotionsResponseDTO;


/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class GetPromotions implements UseCase
{

    /**
     * @var PromotionGateway
     */
    private $promotionGateway;

    /**
     * @var PromotionResponseFactory
     */
    private $promotionResponseFactory;

    /**
     * @param GetPromotionsRequest $useCaseRequest
     *
     * @return PromotionsResponseDTO
     */
    public function execute(UseCaseRequest $useCaseRequest)
    {
        $promotions = $this->promotionGateway->findAll();

        return new PromotionsResponseDTO($this->promotionResponseFactory->createFromPromotions($promotions));
    }

    /**
     * @param PromotionGateway $promotionGateway
     */
    public function setPromotionGateway(PromotionGateway $promotionGateway)
    {
        $this->promotionGateway = $promotionGateway;
    }

    /**
     * @param PromotionResponseFactory $promotionResponseFactory
     */
    public function setPromotionResponseFactory(PromotionResponseFactory $promotionResponseFactory)
    {
        $this->promotionResponseFactory = $promotionResponseFactory;
    }


}
