<?php

namespace BusinessRules\Requestors\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface EditStudentRequestBuilder
{
    /**
     * @return EditStudentRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return EditStudentRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param int $promotionId
     *
     * @return EditStudentRequestBuilder
     */
    public function withPromotionId($promotionId);

    /**
     * @param int $userSchoolTutorId
     *
     * @return EditStudentRequestBuilder
     */
    public function withUserSchoolTutorId($userSchoolTutorId);

    /**
     * @param int $userCompanyTutorId
     *
     * @return EditStudentRequestBuilder
     */
    public function withUserCompanyTutorId($userCompanyTutorId);

    /**
     * @return EditStudentRequest
     */
    public function build();
}
