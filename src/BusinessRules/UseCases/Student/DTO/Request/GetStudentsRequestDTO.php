<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 09/06/2015
 * Time: 16:22
 */

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\GetStudentsRequest;

class GetStudentsRequestDTO implements GetStudentsRequest
{

    /**
     * @var int
     */
    public $schoolTutorId;

    /**
     * {@inheritdoc}
     */
    public function getSchoolTutorId()
    {
        return $this->schoolTutorId;
    }
}
