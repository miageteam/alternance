<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\GetMeetingRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetMeetingRequestDTO implements GetMeetingRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
