<?php

namespace BusinessRules\UseCases\Report\DTO\Response;

use BusinessRules\Entities\Report\Comment;
use BusinessRules\Responders\Report\CommentResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CommentResponseFactoryImpl implements CommentResponseFactory
{
    /**
     * {@inheritdoc}
     */
    public function createFromComment(Comment $comment)
    {
        $response            = new CommentResponseDTO();
        $response->id        = $comment->getId();
        $response->content   = $comment->getContent();
        $response->author    = $comment->getAuthor();
        $response->createdAt = $comment->getCreatedAt();
        $response->updatedAt = $comment->getUpdatedAt();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromComments($comments)
    {

        $commentsResponse = array();
        foreach ($comments as $comment) {
            $commentsResponse[] = $this->createFromComment($comment);
        }

        return $commentsResponse;
    }
}
