<?php

namespace AppBundle\Controller\Tutor;

use AppBundle\Form\Model\EditCompanyTutorModel;
use AppBundle\Form\Model\UserCompanyTutorModel;
use AppBundle\Form\Type\EditCompanyTutorType;
use AppBundle\Form\Type\UserCompanyTutorType;
use BusinessRules\Gateways\Student\Exceptions\MeetingNotFoundException;
use BusinessRules\Gateways\Tutor\Exceptions\UserCompanyTutorNotFoundException;
use BusinessRules\Requestors\Tutor\CreateCompanyTutorRequest;
use BusinessRules\Requestors\Tutor\EditCompanyTutorRequest;
use BusinessRules\Requestors\User\GetUserRequest;
use BusinessRules\UseCases\Tutor\DTO\Request\GetCompanyTutorRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CompanyTutorController extends Controller
{
    public function showAction($userId)
    {
        try {
            $companyTutor = $this->get('use_cases.tutor.get_company_tutor')
                ->execute(new GetCompanyTutorRequestDTO($userId));

            return $this->render('AppBundle:Tutor\CompanyTutor:show.html.twig', array('companyTutor' => $companyTutor));
        } catch (UserCompanyTutorNotFoundException $e) {
            return $this->forward('AppBundle:Tutor/CompanyTutor:create', array('userId' => $userId));
        }
    }

    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function createAction(Request $request, $userId)
    {
        $form = $this->createForm(
            new UserCompanyTutorType(),
            null,
            array(
                'action' => $this->generateUrl('app_user_company_tutor_create', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var UserCompanyTutorModel $model */
            $model = $form->getData();

            $this->get('use_cases.tutor.create_company_tutor')
                ->execute($this->getCreateCompanyTutorRequest($model, $userId));

            $user = $this->get('use_cases.user.get_user')->execute($this->getUserRequest($userId));

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $user->getLoginIdentifier())
            );
        }

        return $this->render('AppBundle:Tutor/CompanyTutor:create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param UserCompanyTutorModel $model
     * @param int                   $userId
     *
     * @return CreateCompanyTutorRequest
     */
    private function getCreateCompanyTutorRequest(UserCompanyTutorModel $model, $userId)
    {
        return $this->get('request.tutor.create_company_tutor_request_builder')
            ->create()
            ->withUserId($userId)
            ->withCompanyName($model->companyName)
            ->withStreet($model->street)
            ->withZipCode($model->zipCode)
            ->withCity($model->city)
            ->withCountry($model->country)
            ->build();
    }

    /**
     * @param int $userId
     *
     * @return GetUserRequest
     */
    private function getUserRequest($userId)
    {
        return $this->get('request.user.get_user_request_builder')
            ->create()
            ->byId($userId)
            ->build();
    }

    public function getStudentAction()
    {
        try {
            $companyTutor = $this->get('use_cases.tutor.get_company_tutor')
                ->execute(new GetCompanyTutorRequestDTO($this->getUser()->getId()));

            $student = $this->get('use_cases.student.get_student')->execute(
                $this->get('request.student.get_student_request_builder')
                    ->create()
                    ->byCompanyTutorId($companyTutor->getId())
                    ->build()
            );

        } catch (UserCompanyTutorNotFoundException $e) {
            $student = null;
        }

        return $this->render(
            'AppBundle:Tutor/CompanyTutor/partials:get_student.html.twig',
            array('student' => $student)
        );
    }

    public function getStudentAlternationCalendarAction()
    {
        try {
            $companyTutor = $this->get('use_cases.tutor.get_company_tutor')
                ->execute(new GetCompanyTutorRequestDTO($this->getUser()->getid()));

            $student = $this->get('use_cases.student.get_student')
                ->execute(
                    $this->get('request.student.get_student_request_builder')
                        ->create()
                        ->byCompanyTutorId($companyTutor->getId())
                        ->build()
                );

            $alternationCalendarPath = $student->getPromotion()->getAlternationCalendarPath();
        } catch (UserCompanyTutorNotFoundException $e) {
            $alternationCalendarPath = null;
        }

        return $this->render(
            'AppBundle:Tutor/CompanyTutor/partials:get_student_alternation_calendar.html.twig',
            array('alternationCalendarPath' => $alternationCalendarPath)
        );
    }

    public function getMeetingAction()
    {
        try {
            $meeting = $this->get('use_cases.student.get_meeting')
                ->execute(
                    $this->get('request.student.get_meeting_request_builder')
                        ->create()
                        ->byUserId($this->getUser()->getId())
                        ->build()
                );

            return $this->render(
                'AppBundle:Tutor/CompanyTutor/partials:get_meeting.html.twig',
                array('meeting' => $meeting)
            );
        } catch (MeetingNotFoundException $e) {
            return $this->render('AppBundle:Tutor/CompanyTutor/partials:get_meeting.html.twig');
        }
    }

    /**
     * @Security("is_granted('ROLE_SECRETARY')")
     */
    public function editAction(Request $request, $userId)
    {
        $companyTutor = $this->get('use_cases.tutor.get_company_tutor')
            ->execute(new GetCompanyTutorRequestDTO($userId));

        $model              = new EditCompanyTutorModel();
        $model->companyName = $companyTutor->getCompanyName();
        $model->street      = $companyTutor->getStreet();
        $model->zipCode     = $companyTutor->getZipCode();
        $model->city        = $companyTutor->getCity();
        $model->country     = $companyTutor->getCountry();

        $form = $this->createForm(
            new EditCompanyTutorType(),
            $model,
            array(
                'action' => $this->generateUrl('app_company_tutor_edit', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EditCompanyTutorModel $model */
            $model = $form->getData();

            $this->get('use_cases.tutor.edit_company_tutor')
                ->execute($this->getEditCompanyTutorRequest($model, $userId));

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $companyTutor->getUser()->getLoginIdentifier())
            );
        }

        return $this->render('AppBundle:Tutor/CompanyTutor:edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param EditCompanyTutorModel $model
     * @param int                   $userId
     *
     * @return EditCompanyTutorRequest
     */
    private function getEditCompanyTutorRequest(EditCompanyTutorModel $model, $userId)
    {
        return $this->get('request.tutor.edit_company_tutor_request_builder')
            ->create()
            ->withUserId($userId)
            ->withCompanyName($model->companyName)
            ->withStreet($model->street)
            ->withZipCode($model->zipCode)
            ->withCity($model->city)
            ->withCountry($model->country)
            ->build();
    }
}
