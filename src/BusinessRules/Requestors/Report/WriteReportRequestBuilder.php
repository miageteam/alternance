<?php

namespace BusinessRules\Requestors\Report;

use BusinessRules\Requestors\Report\Exceptions\InvalidWriteReportRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteReportRequestBuilder
{
    /**
     * @return WriteReportRequestBuilder
     */
    public function create();

    /**
     * @param string $title
     *
     * @return WriteReportRequestBuilder
     */
    public function withTitle($title);

    /**
     * @param int $authorId
     *
     * @return WriteReportRequestBuilder
     */
    public function withAuthorId($authorId);

    /**
     * @param string $attachmentPath
     *
     * @return WriteReportRequestBuilder
     */
    public function withAttachmentPath($attachmentPath);

    /**
     * @return WriteReportRequest
     *
     * @throws InvalidWriteReportRequestParameterException
     */
    public function build();
}
