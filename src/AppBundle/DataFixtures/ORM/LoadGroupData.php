<?php

namespace AppBundle\DataFixtures\ORM;

use BusinessRules\Entities\User\Group;
use BusinessRules\Entities\User\GroupName;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\GroupImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class LoadGroupData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $items = array(
            GroupName::ADMIN            => array('ROLE_ADMIN', 'ROLE_SECRETARY', 'ROLE_TRAINING_MANAGER'),
            GroupName::SECRETARY        => array('ROLE_SECRETARY', 'ROLE_TRAINING_MANAGER'),
            GroupName::TRAINING_MANAGER => array('ROLE_TRAINING_MANAGER'),
            GroupName::PROFESSOR        => array('ROLE_PROFESSOR'),
            GroupName::TUTOR            => array('ROLE_TUTOR'),
            GroupName::STUDENT          => array('ROLE_STUDENT'),
        );


        $groups = $this->setGroups($items);

        $this->insertAll($manager, $groups);
    }

    /**
     * @param array $userRoles
     *
     * @return array
     */
    private function getReferences(array $userRoles)
    {
        $roles = array();
        foreach ($userRoles as $userRole) {
            $roles[] = $this->getReference($userRole);
        }

        return $roles;
    }

    /**
     * @param $items
     *
     * @return array
     */
    private function setGroups($items)
    {
        $groups = array();
        foreach ($items as $itemName => $itemRoles) {
            $group = new GroupImpl();
            $group->setName($itemName);
            $group->setUserRoles($this->getReferences($itemRoles));

            $groups[] = $group;
        }

        return $groups;
    }

    /**
     * @param ObjectManager $manager
     * @param Group[]       $groups
     */
    private function insertAll(ObjectManager $manager, array $groups)
    {
        foreach ($groups as $group) {
            $manager->persist($group);
            $manager->flush();

            $this->addReference($group->getName(), $group);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
