<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Entities\Report\CommentStub1;
use BusinessRules\Entities\Report\ReportStub1;
use BusinessRules\Entities\User\UserStub1;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteCommentRequestBuilderImplTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteCommentRequestBuilderImpl
     */
    private $builder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Report\Exceptions\InvalidWriteCommentRequestParameterException
     * @expectedExceptionMessage The argument "ReportId" must be defined.
     */
    public function WithoutReportId_ThrowException()
    {
        $this->builder
            ->create()
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Report\Exceptions\InvalidWriteCommentRequestParameterException
     * @expectedExceptionMessage The argument "Content" must be defined.
     */
    public function WithoutContent_ThrowException()
    {
        $this->builder
            ->create()
            ->withReportId(ReportStub1::ID)
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Report\Exceptions\InvalidWriteCommentRequestParameterException
     * @expectedExceptionMessage The argument "AuthorId" must be defined.
     */
    public function WithoutAuthorId_ThrowException()
    {
        $this->builder
            ->create()
            ->withReportId(ReportStub1::ID)
            ->withContent(CommentStub1::CONTENT)
            ->build();
    }

    /**
     * @test
     */
    public function BuildStandard()
    {
        $this->builder
            ->create()
            ->withReportId(ReportStub1::ID)
            ->withContent(CommentStub1::CONTENT)
            ->withAuthorId(UserStub1::ID)
            ->build();
    }

    protected function setUp()
    {
        $this->builder = new WriteCommentRequestBuilderImpl();
    }
}
