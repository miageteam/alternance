<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Entities\Communication\AttachmentFactory;
use BusinessRules\Entities\Communication\Flow;
use BusinessRules\Entities\Communication\FlowFactory;
use BusinessRules\Entities\Communication\Message;
use BusinessRules\Entities\Communication\MessageFactory;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Communication\AttachmentGateway;
use BusinessRules\Gateways\Communication\FlowGateway;
use BusinessRules\Gateways\Communication\MessageGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Communication\WriteMessageRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteMessage implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var FlowFactory
     */
    private $flowFactory;

    /**
     * @var FlowGateway
     */
    private $flowGateway;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var MessageGateway
     */
    private $messageGateway;

    /**
     * @var AttachmentGateway
     */
    private $attachmentGateway;

    /**
     * @var AttachmentFactory
     */
    private $attachmentFactory;

    /**
     * @Transaction
     *
     * @param WriteMessageRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $author    = $this->userGateway->find($request->getAuthorId());
        $receivers = $this->userGateway->findByIds($request->getParticipantIds());

        $flow = $this->createAndInsertFlow($request, $author, $receivers);

        $message = $this->createAndInsertMessage($request, $flow, $author);

        $attachmentPath = $request->getAttachmentPath();
        if (!empty($attachmentPath)) {
            $this->createAndInsertAttachments($request, $message);
        }
    }

    private function createAndInsertFlow(WriteMessageRequest $request, User $author, array $receivers)
    {
        $flow = $this->flowFactory->create($request->getTitle(), $author, $receivers);
        $this->flowGateway->insert($flow);

        return $flow;
    }

    private function createAndInsertMessage(WriteMessageRequest $request, Flow $flow, User $author)
    {
        $message = $this->messageFactory->create($request->getContent(), $flow, $author);
        $this->messageGateway->insert($message);

        return $message;
    }

    private function createAndInsertAttachments(WriteMessageRequest $request, Message $message)
    {
        $attachment = $this->attachmentFactory->create($request->getAttachmentPath(), $message);
        $this->attachmentGateway->insert($attachment);
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param FlowFactory $flowFactory
     */
    public function setFlowFactory(FlowFactory $flowFactory)
    {
        $this->flowFactory = $flowFactory;
    }

    /**
     * @param FlowGateway $flowGateway
     */
    public function setFlowGateway(FlowGateway $flowGateway)
    {
        $this->flowGateway = $flowGateway;
    }

    /**
     * @param MessageFactory $messageFactory
     */
    public function setMessageFactory(MessageFactory $messageFactory)
    {
        $this->messageFactory = $messageFactory;
    }

    /**
     * @param MessageGateway $messageGateway
     */
    public function setMessageGateway(MessageGateway $messageGateway)
    {
        $this->messageGateway = $messageGateway;
    }

    /**
     * @param AttachmentGateway $attachmentGateway
     */
    public function setAttachmentGateway(AttachmentGateway $attachmentGateway)
    {
        $this->attachmentGateway = $attachmentGateway;
    }

    /**
     * @param AttachmentFactory $attachmentFactory
     */
    public function setAttachmentFactory(AttachmentFactory $attachmentFactory)
    {
        $this->attachmentFactory = $attachmentFactory;
    }
}
