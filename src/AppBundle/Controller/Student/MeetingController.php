<?php

namespace AppBundle\Controller\Student;

use AppBundle\Form\Model\MeetingModel;
use AppBundle\Form\Type\MeetingType;
use BusinessRules\Gateways\Student\Exceptions\MeetingNotFoundException;
use BusinessRules\Requestors\Student\CreateMeetingRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MeetingController extends Controller
{
    public function createAction(Request $request, $userId)
    {
        $form = $this->createForm(new MeetingType());

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var MeetingModel $model */
            $model = $form->getData();

            $this->get('use_cases.student.create_meeting')
                ->execute($this->getCreateMeetingRequest($userId, $model));

            $this->addFlash('success', 'app.flash_message.create_meeting_success');

            return $this->redirectToRoute('app_homepage');
        }

        return $this->render('AppBundle:Student/Meeting:create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param int          $userId
     * @param MeetingModel $model
     *
     * @return CreateMeetingRequest
     */
    private function getCreateMeetingRequest($userId, MeetingModel $model)
    {
        return $this->get('request.student.create_meeting_request_builder')
            ->create()
            ->withPlannedAt($model->plannedAt)
            ->withPlace($model->place)
            ->withParticipantIds($this->getParticipantsId($userId))
            ->build();
    }

    /**
     * @param int $userId
     *
     * @return int[]
     */
    private function getParticipantsId($userId)
    {
        $student = $this->get('use_cases.student.get_student')
            ->execute(
                $this->get('request.student.get_student_request_builder')
                    ->create()
                    ->byUserId($userId)
                    ->build()
            );

        return array(
            $student->getUser()->getId(),
            $student->getUserCompanyTutor()->getUser()->getId(),
            $student->getUserSchoolTutor()->getUser()->getId()
        );
    }

    public function getMeetingAction($userId)
    {
        try {
            $meeting = $this->get('use_cases.student.get_meeting')
                ->execute(
                    $this->get('request.student.get_meeting_request_builder')
                        ->create()
                        ->byUserId($userId)
                        ->build()
                );

            return $this->render(
                'AppBundle:Student/Meeting/partials:get_meeting.html.twig',
                array('meeting' => $meeting)
            );
        } catch (MeetingNotFoundException $e) {
            return $this->render(
                'AppBundle:Student/Meeting/partials:get_meeting.html.twig',
                array('userId' => $userId)
            );
        }
    }
}
