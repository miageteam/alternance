<?php

namespace BusinessRules\UseCases\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutorFactory;
use BusinessRules\Gateways\Tutor\UserSchoolTutorGateway;
use BusinessRules\Requestors\Tutor\CreateSchoolTutorRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateSchoolTutor implements UseCase
{

    /**
     * @var UserSchoolTutorFactory
     */
    private $userSchoolTutorFactory;

    /**
     * @var UserSchoolTutorGateway
     */
    private $userSchoolTutorGateway;

    /**
     * @Transaction
     *
     * @param CreateSchoolTutorRequest $request
     *
     * @return UseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $userSchoolTutor = $this->userSchoolTutorFactory->createFromRequest($request);

        $this->userSchoolTutorGateway->insert($userSchoolTutor);
    }

    /**
     * @param UserSchoolTutorFactory $userSchoolTutorFactory
     */
    public function setUserSchoolTutorFactory(UserSchoolTutorFactory $userSchoolTutorFactory)
    {
        $this->userSchoolTutorFactory = $userSchoolTutorFactory;
    }

    /**
     * @param UserSchoolTutorGateway $userSchoolTutorGateway
     */
    public function setUserSchoolTutorGateway(UserSchoolTutorGateway $userSchoolTutorGateway)
    {
        $this->userSchoolTutorGateway = $userSchoolTutorGateway;
    }
}
