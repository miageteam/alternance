<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\WriteResponseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteResponseRequestDTO implements WriteResponseRequest
{

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $authorId;

    /**
     * @var string
     */
    public $attachmentPath;

    /**
     * @var int
     */
    public $flowId;

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttachmentPath()
    {
        return $this->attachmentPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getFlowId()
    {
        return $this->flowId;
    }
}
