<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Request;

use BusinessRules\Requestors\TrainingManager\EditTrainingManagerRequestBuilder;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class EditTrainingManagerRequestBuilderImpl implements EditTrainingManagerRequestBuilder
{

    /**
     * @var EditTrainingManagerRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new EditTrainingManagerRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPromotionId($promotionId)
    {
        $this->request->promotionId = $promotionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
