<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\GetPaginatedReportsRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedReportsRequestBuilderImpl implements GetPaginatedReportsRequestBuilder
{

    /**
     * @var GetPaginatedReportsRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetPaginatedReportsRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withIsValidated($isValidated)
    {
        $this->request->isValidated = $isValidated;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withSorts(array $sorts)
    {
        $this->request->sorts = $sorts;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPage($page)
    {
        $this->request->page = $page;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withItemsPerPage($itemPerPage)
    {
        $this->request->itemsPerPage = $itemPerPage;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
