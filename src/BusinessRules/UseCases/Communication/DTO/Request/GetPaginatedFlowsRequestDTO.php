<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\GetPaginatedFlowsRequest;
use BusinessRules\Utils\Pagination;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedFlowsRequestDTO implements GetPaginatedFlowsRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string[]
     */
    public $sorts = array();

    /**
     * @var int
     */
    public $page = Pagination::PAGE;

    /**
     * @var int
     */
    public $itemsPerPage = Pagination::ITEMS_PER_PAGE;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getSorts()
    {
        return $this->sorts;
    }

    /**
     * {@inheritdoc}
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * {@inheritdoc}
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }
}
