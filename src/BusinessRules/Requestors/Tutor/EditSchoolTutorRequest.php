<?php

namespace BusinessRules\Requestors\Tutor;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface EditSchoolTutorRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return string
     */
    public function getCompanyName();

    /**
     * @return string
     */
    public function getStreet();

    /**
     * @return int
     */
    public function getZipCode();

    /**
     * @return string
     */
    public function getCity();

    /**
     * @return string
     */
    public function getCountry();
}
