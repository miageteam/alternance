<?php

namespace BusinessRules\Requestors\Student;

use DateTime;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateMeetingRequest extends UseCaseRequest
{
    /**
     * @return DateTime
     */
    public function getPlannedAt();

    /**
     * @return string
     */
    public function getPlace();

    /**
     * @return int[]
     */
    public function getParticipantIds();

    /**
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * @return DateTime
     */
    public function getUpdatedAt();
}
