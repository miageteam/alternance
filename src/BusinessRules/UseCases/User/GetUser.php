<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\User\GetUserRequest;
use BusinessRules\Responders\User\UserResponse;
use BusinessRules\Responders\User\UserResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetUser implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var UserResponseFactory
     */
    private $userResponseFactory;

    /**
     * @param GetUserRequest $request
     *
     * @return UserResponse
     *
     * @throws UserNotFoundException
     */
    public function execute(UseCaseRequest $request)
    {
        $email = $request->getEmail();
        if (!empty($email)) {
            $user = $this->userGateway->findByEmail($request->getEmail());
        } else {
            $loginIdentifier = $request->getLoginIdentifier();
            if (!empty($loginIdentifier)) {
                $user = $this->userGateway->findByLogin($request->getLoginIdentifier());
            } else {
                $user = $this->userGateway->find($request->getId());
            }
        }

        return $this->userResponseFactory->createFromUser($user);
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param UserResponseFactory $userResponseFactory
     */
    public function setUserResponseFactory(UserResponseFactory $userResponseFactory)
    {
        $this->userResponseFactory = $userResponseFactory;
    }
}
