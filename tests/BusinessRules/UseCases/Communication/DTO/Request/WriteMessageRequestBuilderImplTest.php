<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Entities\Communication\AttachmentStub1;
use BusinessRules\Entities\Communication\FlowStub1;
use BusinessRules\Entities\Communication\MessageStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub2;
use BusinessRules\Requestors\Communication\WriteMessageRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteMessageRequestBuilderImplTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteMessageRequestBuilder
     */
    private $builder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Communication\Exceptions\InvalidWriteMessageRequestParameterException
     * @expectedExceptionMessage The argument "Content" must be defined.
     */
    public function WithoutContent_ThrowException()
    {
        $this->builder
            ->create()
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Communication\Exceptions\InvalidWriteMessageRequestParameterException
     * @expectedExceptionMessage The argument "AuthorId" must be defined.
     */
    public function WithoutAuthor_ThrowException()
    {
        $this->builder
            ->create()
            ->withContent(MessageStub1::CONTENT)
            ->withTitle(FlowStub1::TITLE)
            ->withParticipantIds(array(UserStub1::ID, UserStub2::ID))
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Communication\Exceptions\InvalidWriteMessageRequestParameterException
     * @expectedExceptionMessage The argument "Title" must be defined.
     */
    public function WithoutTitle_ThrowException()
    {
        $this->builder
            ->create()
            ->withContent(MessageStub1::CONTENT)
            ->withAuthorId(UserStub1::ID)
            ->withParticipantIds(array(UserStub1::ID, UserStub2::ID))
            ->build();
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\Communication\Exceptions\InvalidWriteMessageRequestParameterException
     * @expectedExceptionMessage The argument "ParticipantIds" must be defined.
     */
    public function WithoutParticipants_ThrowException()
    {
        $this->builder
            ->create()
            ->withContent(MessageStub1::CONTENT)
            ->withAuthorId(UserStub1::ID)
            ->withTitle(FlowStub1::TITLE)
            ->build();
    }

    /**
     * @test
     */
    public function BuildStandard()
    {
        $this->builder
            ->create()
            ->withContent(MessageStub1::CONTENT)
            ->withAuthorId(UserStub1::ID)
            ->withTitle(FlowStub1::TITLE)
            ->withParticipantIds(array(UserStub1::ID, UserStub2::ID))
            ->build();
    }

    /**
     * @test
     */
    public function BuildWithAttachments()
    {
        $this->builder
            ->create()
            ->withContent(MessageStub1::CONTENT)
            ->withAuthorId(UserStub1::ID)
            ->withTitle(FlowStub1::TITLE)
            ->withParticipantIds(array(UserStub1::ID, UserStub2::ID))
            ->withAttachmentPath(AttachmentStub1::PATH)
            ->build();
    }

    protected function setUp()
    {
        $this->builder = new WriteMessageRequestBuilderImpl();
    }
}
