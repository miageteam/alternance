<?php

namespace AppBundle\Entity\Student;

use BusinessRules\Entities\Student\Formation;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationImpl extends Formation
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}
