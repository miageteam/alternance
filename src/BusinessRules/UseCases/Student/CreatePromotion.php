<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\PromotionFactory;
use BusinessRules\Gateways\Student\PromotionGateway;
use BusinessRules\Requestors\Student\CreatePromotionRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreatePromotion implements UseCase
{

    /**
     * @var PromotionFactory
     */
    private $promotionFactory;

    /**
     * @var PromotionGateway
     */
    private $promotionGateway;

    /**
     * @Transaction
     *
     * @param CreatePromotionRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $promotion = $this->promotionFactory->createFromRequest($request);

        $this->promotionGateway->insert($promotion);
    }

    /**
     * @param PromotionFactory $promotionFactory
     */
    public function setPromotionFactory(PromotionFactory $promotionFactory)
    {
        $this->promotionFactory = $promotionFactory;
    }

    /**
     * @param PromotionGateway $promotionGateway
     */
    public function setPromotionGateway(PromotionGateway $promotionGateway)
    {
        $this->promotionGateway = $promotionGateway;
    }
}
