<?php

namespace BusinessRules\Entities\Report;

use AppBundle\Entity\Report\CommentImpl;
use BusinessRules\Entities\User\UserStub1;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CommentStub1 extends CommentImpl
{
    const ID         = 1;
    const CONTENT    = 'My first comment, Ooooh yeaaaah !';
    const CREATED_AT = '2015-04-20';
    const UPDATED_AT = '2015-04-20';

    public function __construct()
    {
        $this->author    = new UserStub1();
        $this->createdAt = new DateTime(self::CREATED_AT);
        $this->updatedAt = new DateTime(self::UPDATED_AT);
    }

    protected $id = self::ID;

    protected $content = self::CONTENT;

}
