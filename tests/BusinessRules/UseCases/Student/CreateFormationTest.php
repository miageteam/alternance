<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\FormationStub1;
use BusinessRules\Gateways\Student\InMemoryFormationGateway;
use BusinessRules\UseCases\Student\DTO\Request\CreateFormationRequestDTO;
use AppBundle\Entity\Student\FormationFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateFormationTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CreateFormation
     */
    private $useCase;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\Student\Exceptions\FormationAlreadyExistException
     */
    public function alreadyExist_ThrowException()
    {
        InMemoryFormationGateway::$formations = array(FormationStub1::ID => new FormationStub1());
        $this->useCase->execute(new CreateFormationRequestDTO(FormationStub1::NAME));
    }

    /**
     * @test
     */
    public function createFormation()
    {
        $this->useCase->execute(new CreateFormationRequestDTO(FormationStub1::NAME));

        foreach (InMemoryFormationGateway::$formations as $formation) {
            $this->assertEquals($formation->getName(), FormationStub1::NAME);
        }
    }

    protected function setUp()
    {
        $this->useCase = new CreateFormation();
        $this->useCase->setFormationGateway(new InMemoryFormationGateway());
        $this->useCase->setFormationFactory(new FormationFactoryImpl());
    }
}
