<?php

namespace UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupNameUnique extends Constraint
{

    /**
     * @var string
     */
    public $message = 'user.group_name_already_exist';

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'group_name_unique';
    }
}
