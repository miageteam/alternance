<?php

namespace BusinessRules\Gateways\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Gateways\Tutor\Exceptions\UserCompanyTutorNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryUserCompanyTutorGateway implements UserCompanyTutorGateway
{

    /**
     * @var UserCompanyTutor[]
     */
    public static $userCompanyTutors = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @param UserCompanyTutor[] $userCompanyTutors
     */
    public function __construct(array $userCompanyTutors = array())
    {
        self::$userCompanyTutors = $userCompanyTutors;
        self::$id                = 0;
    }


    /**
     * {@inheritdoc}
     */
    public function insert(UserCompanyTutor $userCompanyTutor)
    {
        self::$userCompanyTutors[++self::$id] = $userCompanyTutor;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$userCompanyTutors[$id])) {
            throw new UserCompanyTutorNotFoundException();
        }

        return self::$userCompanyTutors[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        // TODO: Implement this method.
    }

    /**
     * @param UserCompanyTutor $userCompanyTutor
     */
    public function update(UserCompanyTutor $userCompanyTutor)
    {
        // TODO: Implement this method.
    }
}
