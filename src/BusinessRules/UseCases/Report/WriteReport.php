<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Entities\Report\Attachment;
use BusinessRules\Entities\Report\AttachmentFactory;
use BusinessRules\Entities\Report\ReportFactory;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Report\AttachmentGateway;
use BusinessRules\Gateways\Report\ReportGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Report\WriteReportRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteReport implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var ReportFactory
     */
    private $ReportFactory;

    /**
     * @var ReportGateway
     */
    private $ReportGateway;

    /**
     * @var AttachmentGateway
     */
    private $attachmentGateway;

    /**
     * @var AttachmentFactory
     */
    private $attachmentFactory;

    /**
     * @Transaction
     *
     * @param WriteReportRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $author = $this->userGateway->find($request->getAuthorId());

        $attachment = $this->createAndInsertAttachments($request);

        $this->createAndInsertReport($request, $author, $attachment);

    }

    private function createAndInsertAttachments(WriteReportRequest $request)
    {
        $attachmentPath = $request->getAttachmentPath();
        if (!empty($attachmentPath)) {
            $attachment = $this->attachmentFactory->create($request->getAttachmentPath());
            $this->attachmentGateway->insert($attachment);

            return $attachment;
        }

        return null;
    }

    private function createAndInsertReport(WriteReportRequest $request, User $author, Attachment $attachment = null)
    {
        $report = $this->ReportFactory->create($request->getTitle(), $author, $attachment);
        $this->ReportGateway->insert($report);

        return $report;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param ReportFactory $ReportFactory
     */
    public function setReportFactory(ReportFactory $ReportFactory)
    {
        $this->ReportFactory = $ReportFactory;
    }

    /**
     * @param ReportGateway $ReportGateway
     */
    public function setReportGateway(ReportGateway $ReportGateway)
    {
        $this->ReportGateway = $ReportGateway;
    }

    /**
     * @param AttachmentGateway $attachmentGateway
     */
    public function setAttachmentGateway(AttachmentGateway $attachmentGateway)
    {
        $this->attachmentGateway = $attachmentGateway;
    }

    /**
     * @param AttachmentFactory $attachmentFactory
     */
    public function setAttachmentFactory(AttachmentFactory $attachmentFactory)
    {
        $this->attachmentFactory = $attachmentFactory;
    }
}
