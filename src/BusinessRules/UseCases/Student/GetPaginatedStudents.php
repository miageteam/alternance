<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Entities\Student\UserStudentFilter;
use BusinessRules\Gateways\Student\UserStudentGateway;
use BusinessRules\Requestors\Student\GetPaginatedStudentsRequest;
use BusinessRules\Responders\Student\UserStudentResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\AbstractPaginatedUseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedStudents implements UseCase
{

    /**
     * @var UserStudentGateway
     */
    private $userStudentGateway;

    /**
     * @var UserStudentResponseFactory
     */
    private $userStudentResponseFactory;

    /**
     * @param GetPaginatedStudentsRequest $request
     *
     * @return AbstractPaginatedUseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $userStudent = $this->userStudentGateway->findAllPaginated(
            $this->getFilters($request),
            $this->getSorts($request),
            $this->getPagination($request)
        );

        return $this->userStudentResponseFactory->createFromPaginatedCollection($userStudent);
    }

    /**
     * @param GetPaginatedStudentsRequest $useCaseRequest
     *
     * @return array
     */
    private function getFilters(GetPaginatedStudentsRequest $useCaseRequest)
    {
        $filters = array();

        if (null !== $useCaseRequest->getPromotionId()) {
            $filters[UserStudentFilter::PROMOTION_ID] = $useCaseRequest->getPromotionId();
        }

        return $filters;
    }

    /**
     * @param GetPaginatedStudentsRequest $useCaseRequest
     *
     * @return array
     */
    private function getSorts(GetPaginatedStudentsRequest $useCaseRequest)
    {
        return $useCaseRequest->getSorts();
    }

    /**
     * @param GetPaginatedStudentsRequest $useCaseRequest
     *
     * @return array
     */
    private function getPagination(GetPaginatedStudentsRequest $useCaseRequest)
    {
        return array(
            PaginatedCollection::PAGE           => $useCaseRequest->getPage(),
            PaginatedCollection::ITEMS_PER_PAGE => $useCaseRequest->getItemsPerPage(),
        );
    }

    /**
     * @param UserStudentGateway $userStudentGateway
     */
    public function setUserStudentGateway(UserStudentGateway $userStudentGateway)
    {
        $this->userStudentGateway = $userStudentGateway;
    }

    /**
     * @param UserStudentResponseFactory $userStudentResponseFactory
     */
    public function setUserStudentResponseFactory(UserStudentResponseFactory $userStudentResponseFactory)
    {
        $this->userStudentResponseFactory = $userStudentResponseFactory;
    }
}
