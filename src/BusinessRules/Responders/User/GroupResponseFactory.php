<?php

namespace BusinessRules\Responders\User;

use BusinessRules\Entities\User\Group;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GroupResponseFactory
{
    /**
     * @param Group $group
     *
     * @return GroupResponse
     */
    public function createFromGroup(Group $group);

    /**
     * @param array $groups
     *
     * @return GroupResponse[]
     */
    public function createFromGroups($groups);
}
