<?php

namespace BusinessRules\Responders\User;

use BusinessRules\Entities\User\Group;
use BusinessRules\Entities\User\Promotion;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @return string
     */
    public function getLoginIdentifier();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getPhoneNumber();

    /**
     * @return string
     */
    public function getPassword();

    /**
     * @return Group
     */
    public function getGroup();

    /**
     * @return bool
     */
    public function isStudent();

    /**
     * @return bool
     */
    public function isTutor();

    /**
     * @return bool
     */
    public function isProfessor();

    /**
     * @return bool
     */
    public function isTrainingManager();

    /**
     * @return bool
     */
    public function isSecretary();

    /**
     * @return bool
     */
    public function isAdmin();
}
