<?php

namespace AppBundle\Repository\Student;

use BusinessRules\Entities\Student\Meeting;
use BusinessRules\Gateways\Student\Exceptions\MeetingNotFoundException;
use BusinessRules\Gateways\Student\MeetingGateway;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MeetingRepository extends EntityRepository implements MeetingGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(Meeting $meeting)
    {
        $this->_em->persist($meeting);
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        try {
            return $this->createQueryBuilder('m')
                ->leftJoin('m.participants', 'p')
                ->andWhere('m.plannedAt >= CURRENT_TIMESTAMP()')
                ->andWhere('p.id = :id')
                ->setParameter('id', $userId)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $nre) {
            throw new MeetingNotFoundException();
        }
    }
}
