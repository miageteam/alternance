<?php

namespace BusinessRules\Requestors\Report;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetPaginatedReportsRequestBuilder
{
    /**
     * @return GetPaginatedReportsRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return GetPaginatedReportsRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param bool $isValidated
     *
     * @return GetPaginatedReportsRequestBuilder
     */
    public function withIsValidated($isValidated);

    /**
     * @param array $sorts
     *
     * @return GetPaginatedReportsRequestBuilder
     */
    public function withSorts(array $sorts);

    /**
     * @param int $page
     *
     * @return GetPaginatedReportsRequestBuilder
     */
    public function withPage($page);

    /**
     * @param int $itemPerPage
     *
     * @return GetPaginatedReportsRequestBuilder
     */
    public function withItemsPerPage($itemPerPage);

    /**
     * @return GetPaginatedReportsRequest
     */
    public function build();
}
