<?php

namespace BusinessRules\Requestors\Report;

use BusinessRules\Requestors\Report\Exceptions\InvalidWriteCommentRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteCommentRequestBuilder
{
    /**
     * @return WriteCommentRequestBuilder
     */
    public function create();

    /**
     * @param int $reportId
     *
     * @return WriteCommentRequestBuilder
     */
    public function withReportId($reportId);

    /**
     * @param string $content
     *
     * @return WriteCommentRequestBuilder
     */
    public function withContent($content);

    /**
     * @param int $authorId
     *
     * @return WriteCommentRequestBuilder
     */
    public function withAuthorId($authorId);

    /**
     * @return WriteCommentRequest
     *
     * @throws InvalidWriteCommentRequestParameterException
     */
    public function build();
}
