<?php

namespace BusinessRules\UseCases\User\DTO\Response;

use BusinessRules\Entities\User\Group;
use BusinessRules\Responders\User\GroupResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupResponseFactoryImpl implements GroupResponseFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromGroup(Group $group)
    {
        $response            = new GroupResponseDTO();
        $response->id        = $group->getId();
        $response->name      = $group->getName();
        $response->userRoles = $group->getUserRoles();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromGroups($groups)
    {
        $groupResponses = array();
        foreach ($groups as $group) {
            $groupResponses[] = $this->createFromGroup($group);
        }

        return $groupResponses;
    }
}
