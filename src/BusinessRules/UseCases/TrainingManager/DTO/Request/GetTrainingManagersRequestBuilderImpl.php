<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Request;

use BusinessRules\Requestors\TrainingManager\GetTrainingManagersRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetTrainingManagersRequestBuilderImpl implements GetTrainingManagersRequestBuilder
{

    /**
     * @var GetTrainingManagersRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetTrainingManagersRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byPromotionId($promotionId)
    {
        $this->request->promotionId = $promotionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
