<?php

namespace BusinessRules\UseCases\Communication\DTO\Response;

use BusinessRules\Responders\Communication\FlowResponse;
use BusinessRules\Responders\Communication\FlowsResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowsResponseDTO implements FlowsResponse
{

    /**
     * @var FlowResponse[]
     */
    public $flows;

    /**
     * @param FlowResponse[] $flows
     */
    public function __construct(array $flows)
    {
        $this->flows = $flows;
    }

    /**
     * {@inheritdoc}
     */
    public function getFlows()
    {
        return $this->flows;
    }
}
