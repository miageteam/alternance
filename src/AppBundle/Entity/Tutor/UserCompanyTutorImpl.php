<?php

namespace AppBundle\Entity\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserCompanyTutorImpl extends UserCompanyTutor
{

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUser()->getCompleteName();
    }

}
