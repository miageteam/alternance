<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\User\ChangePasswordRequest;
use BusinessRules\Services\Security\PasswordEncoder;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ChangePassword implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var PasswordEncoder
     */
    private $encoder;

    /**
     * @Transaction
     *
     * @param ChangePasswordRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $user = $this->userGateway->find($request->getUserId());

        $newPassword = $this->encoder->encodePassword($request->getNewPassword(), $user->getSalt());
        $user->setPassword($newPassword);

        $this->userGateway->update($user);
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param PasswordEncoder $encoder
     */
    public function setEncoder(PasswordEncoder $encoder)
    {
        $this->encoder = $encoder;
    }
}
