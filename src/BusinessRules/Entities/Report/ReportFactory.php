<?php

namespace BusinessRules\Entities\Report;

use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface ReportFactory
{
    /**
     * @param string          $title
     * @param User            $author
     * @param Attachment|null $attachment
     *
     * @return Report
     */
    public function create($title, User $author, Attachment $attachment = null);
}
