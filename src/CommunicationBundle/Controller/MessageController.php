<?php

namespace CommunicationBundle\Controller;

use BusinessRules\Entities\User\User;
use BusinessRules\Requestors\Communication\EditMessageRequest;
use BusinessRules\Requestors\Communication\WriteMessageRequest;
use BusinessRules\Responders\Communication\MessageResponse;
use BusinessRules\Responders\Student\UserStudentResponse;
use BusinessRules\UseCases\Tutor\DTO\Request\GetCompanyTutorRequestDTO;
use CommunicationBundle\Form\Model\CreateMessageModel;
use CommunicationBundle\Form\Model\CreateResponseModel;
use CommunicationBundle\Form\Model\EditMessageModel;
use CommunicationBundle\Form\Type\CreateMessageType;
use CommunicationBundle\Form\Type\CreateResponseType;
use CommunicationBundle\Form\Type\EditMessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller
{
    public function writeStudentToTutorAndProfessorAction(Request $request)
    {
        $form = $this->createForm(new CreateMessageType());

        $form->handleRequest($request);

        /** @var UserStudentResponse $student */
        $student = $this->get('use_cases.student.get_student')->execute(
            $this->get('request.student.get_student_request_builder')
                ->create()
                ->byUserId($this->getUser()->getId())
                ->build()
        );

        if ($form->isValid()) {
            $participantIds = array(
                $student->getUserSchoolTutor()->getUser()->getId(),
                $student->getUserCompanyTutor()->getUser()->getId()
            );

            $this->writeMessage($form, $participantIds);

            $this->addFlash('success', 'app.flash_message.add_message_success');

            return $this->redirectToRoute('communication_bundle_list_flow');
        }

        return $this->render(
            'CommunicationBundle:Message:create.html.twig',
            array(
                'form'      => $form->createView(),
                'author'    => $this->getUser(),
                'receivers' => array($student->getUserSchoolTutor(), $student->getUserCompanyTutor())
            )
        );
    }

    /**
     * @param Form  $form
     * @param int[] $participantIds
     */
    private function writeMessage(Form $form, array $participantIds)
    {
        /** @var CreateMessageModel $model */
        $model = $form->getData();

        $model->upload();

        $this->get('use_cases.communication.write_message')
            ->execute($this->getWriteMessageRequest($model, $participantIds));
    }

    /**
     * @param CreateMessageModel $model
     *
     * @return WriteMessageRequest
     */
    private function getWriteMessageRequest(CreateMessageModel $model, $participants)
    {
        return $this->get('request.communication.write_message_request_builder')
            ->create()
            ->withTitle($model->title)
            ->withAuthorId($this->getUser()->getId())
            ->withContent($model->content)
            ->withParticipantIds($participants)
            ->withAttachmentPath($model->attachmentPath)
            ->build();
    }

    public function writeStudentToProfessorAction(Request $request)
    {
        $form = $this->createForm(new CreateMessageType());

        $form->handleRequest($request);

        $student = $this->get('use_cases.student.get_student')->execute(
            $this->get('request.student.get_student_request_builder')
                ->create()
                ->byUserId($this->getUser()->getId())
                ->build()
        );

        if ($form->isValid()) {
            $participantIds = array(
                $student->getUserSchoolTutor()->getUser()->getId()
            );

            $this->writeMessage($form, $participantIds);

            $this->addFlash('success', 'app.flash_message.add_message_success');

            return $this->redirectToRoute('communication_bundle_list_flow');
        }

        return $this->render(
            'CommunicationBundle:Message:create.html.twig',
            array(
                'form'      => $form->createView(),
                'author'    => $this->getUser(),
                'receivers' => array($student->getUserSchoolTutor())
            )
        );
    }

    public function writeTutorToProfessorAction(Request $request)
    {
        $form = $this->createForm(new CreateMessageType());

        $form->handleRequest($request);

        $companyTutor = $this->get('use_cases.tutor.get_company_tutor')
            ->execute(new GetCompanyTutorRequestDTO($this->getUser()->getId()));

        $student = $this->get('use_cases.student.get_student')->execute(
            $this->get('request.student.get_student_request_builder')
                ->create()
                ->byCompanyTutorId($companyTutor->getId())
                ->build()
        );

        if ($form->isValid()) {
            $participantIds = array(
                $student->getUserSchoolTutor()->getUser()->getId()
            );

            $this->writeMessage($form, $participantIds);

            $this->addFlash('success', 'app.flash_message.add_message_success');

            return $this->redirectToRoute('communication_bundle_list_flow');
        }

        return $this->render(
            'CommunicationBundle:Message:create.html.twig',
            array(
                'form'      => $form->createView(),
                'author'    => $this->getUser(),
                'receivers' => array($student->getUserSchoolTutor(), $student)
            )
        );
    }

    public function writeProfessorToTutorAction(Request $request, $studentUserId)
    {
        $form = $this->createForm(new CreateMessageType());

        $form->handleRequest($request);

        $student = $this->get('use_cases.student.get_student')->execute(
            $this->get('request.student.get_student_request_builder')
                ->create()
                ->byUserId($studentUserId)
                ->build()
        );

        if ($form->isValid()) {
            $participantIds = array($student->getUserCompanyTutor()->getUser()->getId());

            $this->writeMessage($form, $participantIds);

            $this->addFlash('success', 'app.flash_message.add_message_success');

            return $this->redirectToRoute('communication_bundle_list_flow');
        }

        return $this->render(
            'CommunicationBundle:Message:create.html.twig',
            array(
                'form'      => $form->createView(),
                'author'    => $this->getUser(),
                'receivers' => array($student->getUserCompanyTutor())
            )
        );
    }

    public function writeProfessorToStudentAction(Request $request, $studentUserId)
    {
        $form = $this->createForm(new CreateMessageType());

        $form->handleRequest($request);

        $student = $this->get('use_cases.student.get_student')->execute(
            $this->get('request.student.get_student_request_builder')
                ->create()
                ->byUserId($studentUserId)
                ->build()
        );

        if ($form->isValid()) {
            $participantIds = array($student->getUser()->getId());

            $this->writeMessage($form, $participantIds);

            $this->addFlash('success', 'app.flash_message.add_message_success');

            return $this->redirectToRoute('communication_bundle_list_flow');
        }

        return $this->render(
            'CommunicationBundle:Message:create.html.twig',
            array(
                'form'      => $form->createView(),
                'author'    => $this->getUser(),
                'receivers' => array($student)
            )
        );
    }

    public function writeProfessorToStudentAndTutorAction(Request $request, $studentUserId)
    {
        $form = $this->createForm(new CreateMessageType());

        $form->handleRequest($request);

        $student = $this->get('use_cases.student.get_student')->execute(
            $this->get('request.student.get_student_request_builder')
                ->create()
                ->byUserId($studentUserId)
                ->build()
        );

        if ($form->isValid()) {
            $participantIds = array(
                $student->getUserCompanyTutor()->getUser()->getId(),
                $student->getUser()->getId()
            );

            $this->writeMessage($form, $participantIds);

            $this->addFlash('success', 'app.flash_message.add_message_success');

            return $this->redirectToRoute('communication_bundle_list_flow');
        }

        return $this->render(
            'CommunicationBundle:Message:create.html.twig',
            array(
                'form'      => $form->createView(),
                'author'    => $this->getUser(),
                'receivers' => array($student->getUserCompanyTutor(), $student)
            )
        );
    }

    public function writeResponseAction(Request $request, $flowId)
    {
        $form = $this->createForm(
            new CreateResponseType(),
            null,
            array(
                'action' => $this->generateUrl('communication_bundle_write_response', array('flowId' => $flowId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var CreateResponseModel $model */
            $model = $form->getData();
            $model->upload();

            $this->get('use_cases.communication.write_response')
                ->execute($this->getWriteResponseRequest($model, $flowId));

            $this->addFlash('success', 'app.flash_message.add_response_success');

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render(
            'CommunicationBundle:Message:write_response.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @param CreateResponseModel $model
     * @param int                 $flowId
     */
    private function getWriteResponseRequest(CreateResponseModel $model, $flowId)
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->get('request.communication.write_response_request_builder')
            ->create()
            ->withFlowId($flowId)
            ->withAuthorId($user->getId())
            ->withContent($model->content)
            ->withAttachmentPath($model->attachmentPath)
            ->build();
    }

    public function editMessageAction(Request $request, $flowId, $messageId)
    {
        $message = $this->get('use_cases.communication.get_message')
            ->execute(
                $this->get('request.communication.get_message_request_builder')
                    ->create()
                    ->byId($messageId)
                    ->build()
            );

        if (!$this->isAuthor($message->getAuthor())) {
            $this->addFlash('error', 'app.flash_message.message_edit_author_fail');

            return $this->redirectToRoute('communication_bundle_read_flow', array('id' => $flowId));
        }

        $model          = new EditMessageModel();
        $model->content = $message->getContent();

        $form = $this->createForm(
            new EditMessageType(),
            $model,
            array(
                'action' => $this->generateUrl(
                    'communication_bundle_edit_message',
                    array('flowId' => $flowId, 'messageId' => $messageId)
                )
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EditMessageModel $model */
            $model = $form->getData();

            $this->get('use_cases.communication.edit_message')
                ->execute($this->getEditMessageRequest($message, $model));

            $this->addFlash('success', 'app.flash_message.message_edit_success');

            return $this->redirectToRoute('communication_bundle_read_flow', array('id' => $flowId));
        }

        return new Response(
            $this->renderView(
                'CommunicationBundle:Message/partials:edit_message.html.twig',
                array('form' => $form->createView())
            )
        );
    }

    /**
     * @param MessageResponse  $message
     * @param EditMessageModel $model
     *
     * @return EditMessageRequest
     */
    private function getEditMessageRequest(MessageResponse $message, EditMessageModel $model)
    {
        return $this->get('request.communication.edit_message_request_builder')
            ->create()
            ->withMessageId($message->getId())
            ->withContent($model->content)
            ->build();
    }

    /**
     * @param User $author
     *
     * @return bool
     */
    private function isAuthor(User $author)
    {
        return $author->getId() === $this->getUser()->getId();
    }
}

