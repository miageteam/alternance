<?php

namespace BusinessRules\Gateways\Report;

use BusinessRules\Entities\Report\Attachment;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface AttachmentGateway
{
    /**
     * @param Attachment $attachment
     */
    public function insert(Attachment $attachment);
}
