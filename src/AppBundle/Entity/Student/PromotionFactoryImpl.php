<?php

namespace AppBundle\Entity\Student;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Entities\Student\PromotionFactory;
use BusinessRules\Gateways\Student\FormationGateway;
use BusinessRules\Requestors\Student\CreatePromotionRequest;
use BusinessRules\Requestors\Student\EditPromotionRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PromotionFactoryImpl implements PromotionFactory
{

    /**
     * @var FormationGateway
     */
    private $formationGateway;

    /**
     * {@inheritdoc}
     */
    public function createFromRequest(CreatePromotionRequest $request)
    {
        $formation = $this->formationGateway->find($request->getFormationId());
        $promotion = new PromotionImpl();

        $promotion->setYear($request->getYear());
        $promotion->setFormation($formation);
        $promotion->setAlternationCalendarPath($request->getAlternationCalendarPath());

        return $promotion;
    }

    /**
     * @param FormationGateway $formationGateway
     */
    public function setFormationGateway(FormationGateway $formationGateway)
    {
        $this->formationGateway = $formationGateway;
    }

    /**
     * @param Promotion            $promotion
     * @param EditPromotionRequest $request
     *
     * @return Promotion
     */
    public function editFromRequest(Promotion $promotion, EditPromotionRequest $request)
    {
        $formation = $this->getFormationOrNull($request->getFormationId());

        if (null !== $formation) {
            $promotion->setFormation($formation);
        }
        if (null !== $request->getYear()) {
            $promotion->setYear($request->getYear());
        }
        if (null !== $request->getAlternationCalendarPath()) {
            $promotion->setAlternationCalendarPath($request->getAlternationCalendarPath());
        }

        return $promotion;
    }

    /**
     * @param int $formationId
     *
     * @return Formation|null
     */
    private function getFormationOrNull($formationId)
    {
        if (empty($formationId)) {
            return null;
        }

        return $this->formationGateway->find($formationId);
    }
}
