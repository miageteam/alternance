<?php

namespace BusinessRules\Gateways\Student;

use BusinessRules\Entities\Student\Meeting;
use BusinessRules\Gateways\Student\Exceptions\MeetingNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MeetingGateway
{
    /**
     * @param Meeting $meeting
     */
    public function insert(Meeting $meeting);

    /**
     * @param int $userId
     *
     * @return Meeting
     *
     * @throws MeetingNotFoundException
     */
    public function findByUserId($userId);
}
