<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\EditMessageRequestBuilder;

class EditMessageRequestBuilderImpl implements EditMessageRequestBuilder
{

    /**
     * @var EditMessageRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new EditMessageRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withMessageId($messageId)
    {
        $this->request->messageId = $messageId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withContent($content)
    {
        $this->request->content = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
