<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\Exceptions\InvalidWriteMessageRequestParameterException;
use BusinessRules\Requestors\Communication\WriteMessageRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteMessageRequestBuilderImpl implements WriteMessageRequestBuilder
{

    /**
     * @var WriteMessageRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new WriteMessageRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withContent($content)
    {
        $this->request->content = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAuthorId($authorId)
    {
        $this->request->authorId = $authorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withTitle($title)
    {
        $this->request->title = $title;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withParticipantIds(array $participantIds)
    {
        $this->request->participantIds = $participantIds;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAttachmentPath($attachmentPath)
    {
        $this->request->attachmentPath = $attachmentPath;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $this->CheckParametersAreDefined();

        return $this->request;
    }

    private function CheckParametersAreDefined()
    {
        if (empty($this->request->content)) {
            throw new InvalidWriteMessageRequestParameterException('The argument "Content" must be defined.');
        }

        if (empty($this->request->authorId)) {
            throw new InvalidWriteMessageRequestParameterException('The argument "AuthorId" must be defined.');
        }

        if (empty($this->request->title)) {
            throw new InvalidWriteMessageRequestParameterException('The argument "Title" must be defined.');
        }

        if (empty($this->request->participantIds)) {
            throw new InvalidWriteMessageRequestParameterException('The argument "ParticipantIds" must be defined.');
        }
    }
}
