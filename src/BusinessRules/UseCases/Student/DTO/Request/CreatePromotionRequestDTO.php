<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\CreatePromotionRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreatePromotionRequestDTO implements CreatePromotionRequest
{

    /**
     * @var string
     */
    public $year;

    /**
     * @var int
     */
    public $formationId;

    /**
     * @var string
     */
    public $alternationCalendarPath;

    /**
     * {@inheritdoc}
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormationId()
    {
        return $this->formationId;
    }

    /**
     * {@inheritdoc}
     */
    public function getAlternationCalendarPath()
    {
        return $this->alternationCalendarPath;
    }
}
