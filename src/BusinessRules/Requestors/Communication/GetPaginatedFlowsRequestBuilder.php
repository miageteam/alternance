<?php

namespace BusinessRules\Requestors\Communication;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetPaginatedFlowsRequestBuilder
{
    /**
     * @return GetPaginatedFlowsRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return GetPaginatedFlowsRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param array $sorts
     *
     * @return GetPaginatedFlowsRequestBuilder
     */
    public function withSorts(array $sorts);

    /**
     * @param int $page
     *
     * @return GetPaginatedFlowsRequestBuilder
     */
    public function withPage($page);

    /**
     * @param int $itemPerPage
     *
     * @return GetPaginatedFlowsRequestBuilder
     */
    public function withItemsPerPage($itemPerPage);

    /**
     * @return GetPaginatedFlowsRequest
     */
    public function build();
}
