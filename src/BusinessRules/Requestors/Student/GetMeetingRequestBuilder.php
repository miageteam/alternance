<?php

namespace BusinessRules\Requestors\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetMeetingRequestBuilder
{
    /**
     * @return GetMeetingRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return GetMeetingRequestBuilder
     */
    public function byUserId($userId);

    /**
     * @return GetMeetingRequest
     */
    public function build();
}
