<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Entities\Communication\FlowStub1;
use BusinessRules\Entities\Communication\MessageStub1;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Communication\InMemoryFlowGateway;
use BusinessRules\Requestors\Communication\ReadFlowRequest;
use BusinessRules\UseCases\Communication\DTO\Request\ReadFlowRequestDTO;
use BusinessRules\UseCases\Communication\DTO\Response\FlowResponseFactoryImpl;
use BusinessRules\UseCases\Communication\DTO\Response\MessageResponseFactoryImpl;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReadFlowTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var ReadFlow
     */
    private $useCase;

    /**
     * @var ReadFlowRequest
     */
    private $request;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\Communication\Exceptions\FlowNotFoundException
     */
    public function FlowNotFound_ThrowException()
    {
        InMemoryFlowGateway::$flows = array();
        $this->useCase->execute($this->request);
    }

    /**
     * @test
     */
    public function ReadFlow()
    {
        $response = $this->useCase->execute($this->request);

        $this->assertEquals(FlowStub1::ID, $response->getId());
        $this->assertEquals(FlowStub1::TITLE, $response->getTitle());
        $this->assertEquals(new DateTime(FlowStub1::CREATED_AT), $response->getCreatedAt());
        $this->assertEquals(MessageStub1::ID, $response->getFirstMessage()->getId());

        $userStub = 'BusinessRules\Entities\User\UserStub';
        foreach ($response->getParticipants() as $key => $participant) {
            $this->assertParticipant($userStub . ($key + 1), $participant);
        }
    }

    private function assertParticipant($userStub, User $participant)
    {
        $this->assertEquals($userStub::ID, $participant->getId());
    }

    protected function setUp()
    {
        $this->useCase = new ReadFlow();
        $flowStub1     = new FlowStub1();
        $flowStub1->setMessages(array(new MessageStub1()));
        $this->useCase->setFlowGateway(new InMemoryFlowGateway(array(FlowStub1::ID => $flowStub1)));
        $this->useCase->setFlowResponseFactory($this->getFlowResponseFactory());

        $this->request = new ReadFlowRequestDTO(FlowStub1::ID);
    }

    /**
     * @return FlowResponseFactoryImpl
     */
    protected function getFlowResponseFactory()
    {
        $flowResponseFactory = new FlowResponseFactoryImpl();
        $flowResponseFactory->setMessageResponseFactory(new MessageResponseFactoryImpl());

        return $flowResponseFactory;
    }
}
