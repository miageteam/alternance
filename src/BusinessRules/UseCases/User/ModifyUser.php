<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\User;
use BusinessRules\Entities\User\UserFactory;
use BusinessRules\Gateways\User\Exceptions\EmailAlreadyExistsException;
use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\User\ModifyUserRequest;
use BusinessRules\Responders\User\UserResponse;
use BusinessRules\Responders\User\UserResponseFactory;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ModifyUser implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var UserResponseFactory
     */
    private $userResponseFactory;

    /**
     * @Transaction
     *
     * @param ModifyUserRequest $request
     *
     * @return UserResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $user = $this->userGateway->find($request->getId());

        if (!$this->IsIdenticalEmail($request, $user)) {
            $this->checkEmailIsAvailable($request);
        }

        $updatedUser = $this->userFactory->ModifyFromRequest($user, $request);

        $this->userGateway->update($updatedUser);

        return $this->userResponseFactory->createFromUser($user);
    }

    /**
     * @param ModifyUserRequest $request
     * @param User              $user
     *
     * @return bool
     */
    private function IsIdenticalEmail(ModifyUserRequest $request, User $user)
    {
        return $user->getEmail() === $request->getEmail();
    }

    /**
     * @param ModifyUserRequest $request
     */
    private function checkEmailIsAvailable(ModifyUserRequest $request)
    {
        try {
            $this->userGateway->findByEmail($request->getEmail());

            throw new EmailAlreadyExistsException();
        } catch (UserNotFoundException $e) {
        }
    }

    public function setUserFactory(UserFactory $userFactory)
    {
        $this->userFactory = $userFactory;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param UserResponseFactory $userResponseFactory
     */
    public function setUserResponseFactory(UserResponseFactory $userResponseFactory)
    {
        $this->userResponseFactory = $userResponseFactory;
    }
}
