<?php

namespace BusinessRules\Services\Security;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PasswordEncoder implements PasswordEncoderInterface
{
    /**
     * {@inheritDoc}
     */
    public function encodePassword($raw, $salt)
    {
        return sha1(sprintf($salt, sha1($raw)));
    }

    /**
     * {@inheritDoc}
     */
    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded === $this->encodePassword($raw, $salt);
    }
}
