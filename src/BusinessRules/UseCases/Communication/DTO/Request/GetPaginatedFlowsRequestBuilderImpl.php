<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\GetPaginatedFlowsRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedFlowsRequestBuilderImpl implements GetPaginatedFlowsRequestBuilder
{

    /**
     * @var GetPaginatedFlowsRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetPaginatedFlowsRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withSorts(array $sorts)
    {
        $this->request->sorts = $sorts;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPage($page)
    {
        $this->request->page = $page;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withItemsPerPage($itemPerPage)
    {
        $this->request->itemsPerPage = $itemPerPage;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
