<?php

namespace BusinessRules\UseCases\Student;

use AppBundle\Entity\Student\UserStudentFactoryImpl;
use BusinessRules\Entities\Tutor\UserCompanyTutorStub1;
use BusinessRules\Entities\Tutor\UserSchoolTutorStub1;
use BusinessRules\Entities\Student\PromotionStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub2;
use BusinessRules\Entities\User\UserStub3;
use BusinessRules\Gateways\Student\InMemoryUserStudentGateway;
use BusinessRules\Gateways\Tutor\InMemoryUserCompanyTutorGateway;
use BusinessRules\Gateways\Tutor\InMemoryUserSchoolTutorGateway;
use BusinessRules\Gateways\Student\InMemoryPromotionGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Student\CreateStudentRequestBuilder;
use BusinessRules\UseCases\Student\DTO\Request\CreateStudentRequestBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateStudentTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CreateStudent
     */
    private $useCase;

    /**
     * @var CreateStudentRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     */
    public function createStudent()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withUserId(UserStub1::ID)
                ->withPromotionId(PromotionStub1::ID)
                ->withUserSchoolTutorId(UserStub2::ID)
                ->withUserCompanyTutorId(UserStub3::ID)
                ->build()
        );
    }

    protected function setUp()
    {
        $this->useCase = new CreateStudent();
        $this->useCase->setUserStudentFactory($this->getUserStudentFactory());
        $this->useCase->setUserStudentGateway(new InMemoryUserStudentGateway());

        $this->requestBuilder = new CreateStudentRequestBuilderImpl();
    }

    /**
     * @return UserStudentFactoryImpl
     */
    private function getUserStudentFactory()
    {
        $userStudentFactory = new UserStudentFactoryImpl();
        $userStudentFactory->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));
        $userStudentFactory->setPromotionGateway(
            new InMemoryPromotionGateway(array(PromotionStub1::ID => new PromotionStub1()))
        );
        $userStudentFactory->setUserSchoolTutorGateway(
            new InMemoryUserSchoolTutorGateway(array(UserStub2::ID => new UserSchoolTutorStub1()))
        );
        $userStudentFactory->setUserCompanyTutorGateway(
            new InMemoryUserCompanyTutorGateway(array(UserStub3::ID => new UserCompanyTutorStub1()))
        );

        return $userStudentFactory;
    }
}
