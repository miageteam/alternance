<?php

namespace BusinessRules\Responders\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserSchoolTutorResponseFactory
{
    /**
     * @param UserSchoolTutor $userSchoolTutor
     *
     * @return UserSchoolTutorResponse
     */
    public function createFromUserSchoolTutor(UserSchoolTutor $userSchoolTutor);
}
