<?php

namespace BusinessRules\Requestors\User\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InvalidCreateUserRequestParameterException extends \Exception
{

}
