<?php

namespace BusinessRules\Requestors\Communication\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InvalidWriteMessageRequestParameterException extends \Exception
{

}
