<?php

namespace BusinessRules\Gateways\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Gateways\Tutor\Exceptions\UserSchoolTutorNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryUserSchoolTutorGateway implements UserSchoolTutorGateway
{

    /**
     * @var UserSchoolTutor[]
     */
    public static $userSchoolTutors = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @param UserSchoolTutor[] $userSchoolTutors
     */
    public function __construct(array $userSchoolTutors = array())
    {
        self::$userSchoolTutors = $userSchoolTutors;
        self::$id               = 0;
    }


    /**
     * {@inheritdoc}
     */
    public function insert(UserSchoolTutor $userSchoolTutor)
    {
        self::$userSchoolTutors[++self::$id] = $userSchoolTutor;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$userSchoolTutors[$id])) {
            throw new UserSchoolTutorNotFoundException();
        }

        return self::$userSchoolTutors[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserId($userId)
    {
        // TODO: Implement this method.
    }

    /**
     * @param UserSchoolTutor $userSchoolTutor
     */
    public function update(UserSchoolTutor $userSchoolTutor)
    {
        // TODO: Implement this method.
    }
}
