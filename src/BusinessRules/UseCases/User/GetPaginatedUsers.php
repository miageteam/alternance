<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\User\GetPaginatedUsersRequest;
use BusinessRules\Responders\User\UserResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\AbstractPaginatedUseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedUsers implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var UserResponseFactory
     */
    private $userResponseFactory;

    /**
     * @param GetPaginatedUsersRequest $request
     *
     * @return AbstractPaginatedUseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $users = $this->userGateway->findAllPaginated(
            $this->getFilters(),
            $this->getSorts($request),
            $this->getPagination($request)
        );

        return $this->userResponseFactory->createFromPaginatedCollection($users);
    }

    /**
     * @return array
     */
    private function getFilters()
    {
        return array();
    }

    /**
     * @param GetPaginatedUsersRequest $useCaseRequest
     *
     * @return array
     */
    private function getSorts(GetPaginatedUsersRequest $useCaseRequest)
    {
        return $useCaseRequest->getSorts();
    }

    /**
     * @param GetPaginatedUsersRequest $useCaseRequest
     *
     * @return array
     */
    private function getPagination(GetPaginatedUsersRequest $useCaseRequest)
    {
        return array(
            PaginatedCollection::PAGE => $useCaseRequest->getPage(),
            PaginatedCollection::ITEMS_PER_PAGE => $useCaseRequest->getItemsPerPage(),
        );
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param UserResponseFactory $userResponseFactory
     */
    public function setUserResponseFactory(UserResponseFactory $userResponseFactory)
    {
        $this->userResponseFactory = $userResponseFactory;
    }
}
