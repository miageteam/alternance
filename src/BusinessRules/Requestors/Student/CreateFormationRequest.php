<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateFormationRequest extends UseCaseRequest
{
    /**
     * @return String
     */
    public function getName();
}
