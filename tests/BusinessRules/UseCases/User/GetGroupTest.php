<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\GroupStub1;
use BusinessRules\Gateways\User\InMemoryGroupGateway;
use BusinessRules\Requestors\User\GetGroupRequestBuilder;
use BusinessRules\Responders\User\GroupResponse;
use BusinessRules\UseCases\User\DTO\Request\GetGroupRequestBuilderImpl;
use BusinessRules\UseCases\User\DTO\Response\GroupResponseFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetGroupTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GetGroup
     */
    private $useCase;

    /**
     * @var GetGroupRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\GroupNotFoundException
     */
    public function GroupNotFoundById_ThrowException()
    {
        InMemoryGroupGateway::$groups = array();
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->byId(GroupStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\GroupNotFoundException
     */
    public function GroupNotFoundByName_ThrowException()
    {
        InMemoryGroupGateway::$groups = array();
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->byName(GroupStub1::NAME)
                ->build()
        );
    }

    /**
     * @test
     */
    public function GroupFoundById_ReturnResponse()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->byId(GroupStub1::ID)
                ->build()
        );

        $this->assetGroup('BusinessRules\Entities\User\GroupStub1', $response);
    }

    /**
     * @test
     */
    public function GroupFoundByEmail_ReturnResponse()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->byName(GroupStub1::NAME)
                ->build()
        );

        $this->assetGroup('BusinessRules\Entities\User\GroupStub1', $response);
    }

    private function assetGroup($groupStub, GroupResponse $group)
    {
        $this->assertEquals($groupStub::ID, $group->getId());
        $this->assertEquals($groupStub::NAME, $group->getName());
    }

    protected function setUp()
    {
        $this->useCase = new GetGroup();
        $this->useCase->setGroupGateway(new InMemoryGroupGateway(array(GroupStub1::ID => new GroupStub1())));
        $this->useCase->setGroupResponseFactory(new GroupResponseFactoryImpl());

        $this->requestBuilder = new GetGroupRequestBuilderImpl();
    }
}
