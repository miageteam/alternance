<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\CreateStudentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateStudentRequestDTO implements CreateStudentRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $promotionId;

    /**
     * @var int
     */
    public $userSchoolTutorId;

    /**
     * @var int
     */
    public $userCompanyTutorId;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserSchoolTutorId()
    {
        return $this->userSchoolTutorId;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserCompanyTutorId()
    {
        return $this->userCompanyTutorId;
    }
}
