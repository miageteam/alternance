<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Entities\User\User;
use BusinessRules\Responders\Student\UserStudentResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserStudentResponseDTO implements UserStudentResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var User
     */
    public $user;

    /**
     * @var Promotion
     */
    public $promotion;

    /**
     * @var UserSchoolTutor
     */
    public $userSchoolTutor;

    /**
     * @var UserCompanyTutor
     */
    public $userCompanyTutor;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserSchoolTutor()
    {
        return $this->userSchoolTutor;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserCompanyTutor()
    {
        return $this->userCompanyTutor;
    }
}
