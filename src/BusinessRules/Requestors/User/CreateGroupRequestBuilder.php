<?php

namespace BusinessRules\Requestors\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CreateGroupRequestBuilder
{
    /**
     * @return CreateGroupRequestBuilder
     */
    public function create();

    /**
     * @param $name
     *
     * @return CreateGroupRequestBuilder
     */
    public function withName($name);

    /**
     * @param array $userRoles
     *
     * @return CreateGroupRequestBuilder
     */
    public function withUserRoles(array $userRoles);

    /**
     * @return CreateGroupRequest
     */
    public function build();
}
