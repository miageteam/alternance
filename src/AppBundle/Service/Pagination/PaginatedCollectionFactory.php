<?php

namespace AppBundle\Service\Pagination;

use Doctrine\ORM\QueryBuilder;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface PaginatedCollectionFactory
{
    /**
     * @param QueryBuilder $qb
     * @param array        $pagination
     *
     * @return PaginatedCollection
     */
    public function make(QueryBuilder $qb, array $pagination = array());
}
