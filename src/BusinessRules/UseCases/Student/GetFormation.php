<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\FormationGateway;
use BusinessRules\Requestors\Student\GetFormationRequest;
use BusinessRules\Responders\Student\FormationResponse;
use BusinessRules\Responders\Student\FormationResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetFormation implements UseCase
{

    /**
     * @var FormationGateway
     */
    private $formationGateway;

    /**
     * @var FormationResponseFactory
     */
    private $formationResponseFactory;

    /**
     * @param GetFormationRequest $request
     *
     * @return FormationResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $formation = $this->formationGateway->find($request->getId());

        return $this->formationResponseFactory->createFromFormation($formation);
    }

    /**
     * @param FormationGateway $formationGateway
     */
    public function setFormationGateway(FormationGateway $formationGateway)
    {
        $this->formationGateway = $formationGateway;
    }

    /**
     * @param FormationResponseFactory $formationResponseFactory
     */
    public function setFormationResponseFactory(FormationResponseFactory $formationResponseFactory)
    {
        $this->formationResponseFactory = $formationResponseFactory;
    }
}
