<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
interface RemovePromotionRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getPromotionId();
}
