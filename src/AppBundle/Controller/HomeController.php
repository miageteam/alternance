<?php

namespace AppBundle\Controller;

use BusinessRules\Entities\User\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class HomeController extends Controller
{
    public function homeAction()
    {
        return $this->render('AppBundle:Home:home.html.twig');
    }
}
