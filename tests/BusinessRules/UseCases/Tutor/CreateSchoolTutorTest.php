<?php

namespace BusinessRules\UseCases\Tutor;

use AppBundle\Entity\Tutor\UserSchoolTutorFactoryImpl;
use BusinessRules\Entities\Tutor\UserSchoolTutor;
use BusinessRules\Entities\Tutor\UserSchoolTutorStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\Tutor\InMemoryUserSchoolTutorGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Tutor\CreateSchoolTutorRequestBuilder;
use BusinessRules\UseCases\Tutor\DTO\Request\CreateSchoolTutorRequestBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateSchoolTutorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CreateCompanyTutor
     */
    private $useCase;

    /**
     * @var CreateSchoolTutorRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     */
    public function createSchoolTutor()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withUserId(UserStub1::ID)
                ->withCompanyName(UserSchoolTutorStub1::COMPANYNAME)
                ->withStreet(UserSchoolTutorStub1::STREET)
                ->withZipCode(UserSchoolTutorStub1::ZIPCODE)
                ->withCity(UserSchoolTutorStub1::CITY)
                ->withCountry(UserSchoolTutorStub1::COUNTRY)
                ->build()
        );

        $this->assertCount(1, InMemoryUserSchoolTutorGateway::$userSchoolTutors);
        foreach (InMemoryUserSchoolTutorGateway::$userSchoolTutors as $userSchoolTutor) {
            $this->assertUserSchoolTutor($userSchoolTutor);
        }

    }

    /**
     * @param UserSchoolTutor $userSchoolTutor
     */
    private function assertUserSchoolTutor(UserSchoolTutor $userSchoolTutor)
    {
        $this->assertEquals(UserStub1::ID, $userSchoolTutor->getUser()->getId());
        $this->assertEquals(UserSchoolTutorStub1::COMPANYNAME, $userSchoolTutor->getCompanyName());
        $this->assertEquals(UserSchoolTutorStub1::STREET, $userSchoolTutor->getStreet());
        $this->assertEquals(UserSchoolTutorStub1::ZIPCODE, $userSchoolTutor->getZipCode());
        $this->assertEquals(UserSchoolTutorStub1::CITY, $userSchoolTutor->getCity());
        $this->assertEquals(UserSchoolTutorStub1::COUNTRY, $userSchoolTutor->getCountry());
    }

    protected function setUp()
    {
        $this->useCase = new CreateSchoolTutor();
        $this->useCase->setUserSchoolTutorFactory($this->getUserSchoolTutorFactoryGateway());
        $this->useCase->setUserSchoolTutorGateway(new InMemoryUserSchoolTutorGateway());

        $this->requestBuilder = new CreateSchoolTutorRequestBuilderImpl();
    }

    /**
     * @return UserSchoolTutorFactoryImpl
     */
    private function getUserSchoolTutorFactoryGateway()
    {
        $userSchoolTutorFactory = new UserSchoolTutorFactoryImpl();
        $userSchoolTutorFactory->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));

        return $userSchoolTutorFactory;
    }
}
