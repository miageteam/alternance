<?php

namespace BusinessRules\Responders\Student;

use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
interface PromotionsResponse extends UseCaseResponse
{
    /**
     * @return PromotionResponse[]
     */
    public function getPromotions();
}
