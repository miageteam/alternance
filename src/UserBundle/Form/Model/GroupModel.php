<?php

namespace UserBundle\Form\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Validator\Constraints as MyAssert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupModel
{

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @MyAssert\GroupNameUnique()
     */
    public $name;

    /**
     * @var int[]|ArrayCollection
     *
     * @Assert\Count(min="1")
     */
    public $userRoles;

}
