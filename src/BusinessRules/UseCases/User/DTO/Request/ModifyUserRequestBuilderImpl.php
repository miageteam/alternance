<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\ModifyUserRequestBuilder;

/**
 * @author Kevin LECONTE <arnaud.h.lefevre@gmail.com>
 */
class ModifyUserRequestBuilderImpl implements ModifyUserRequestBuilder
{

    /**
     * @var ModifyUserRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new ModifyUserRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withId($id)
    {
        $this->request->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withFirstName($firstName)
    {
        $this->request->firstName = $firstName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withLastName($lastName)
    {
        $this->request->lastName = $lastName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withEmail($email)
    {
        $this->request->email = $email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPhoneNumber($phoneNumber)
    {
        $this->request->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withGroupId($groupId)
    {
        $this->request->groupId = $groupId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPassword($password)
    {
        $this->request->password = $password;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
