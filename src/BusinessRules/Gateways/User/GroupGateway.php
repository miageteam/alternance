<?php

namespace BusinessRules\Gateways\User;

use BusinessRules\Entities\User\Group;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GroupGateway
{
    /**
     * @param Group $group
     */
    public function insert(Group $group);

    /**
     * @param int $id
     *
     * @return Group
     *
     * @throw GroupNotFoundException
     */
    public function find($id);

    /**
     * @param string $name
     *
     * @return Group
     *
     * @throw GroupNotFoundException
     */
    public function findByName($name);

    /**
     * @return Group[]
     */
    public function findAll();
}
