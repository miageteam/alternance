<?php

namespace AppBundle\Entity\Student;

use BusinessRules\Entities\Student\FormationFactory;
use BusinessRules\Requestors\Student\CreateFormationRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationFactoryImpl implements FormationFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromRequest(CreateFormationRequest $request)
    {
        $formation = new FormationImpl();

        $formation->setName($request->getName());

        return $formation;
    }
}
