<?php

namespace BusinessRules\Gateways\User;

use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserGateway
{
    /**
     * @param User $user
     */
    public function insert(User $user);

    /**
     * @param int $id
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function find($id);

    /**
     * @param string $email
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function findByEmail($email);

    /**
     * @param int[] $ids
     *
     * @return User[]
     */
    public function findByIds(array $ids);

    /**
     * @param string $loginIdentifier
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function findByLogin($loginIdentifier);

    /**
     * @param string $loginIdentifier
     *
     * @return User[]
     */
    public function findAllLikeLogin($loginIdentifier);

    /**
     * @param array $filters
     * @param array $sorts
     * @param array $pagination
     *
     * @return PaginatedCollection
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array());

    /**
     * @param User $user
     */
    public function update(User $user);

    /**
     * @param User $user
     */
    public function remove(User $user);
}
