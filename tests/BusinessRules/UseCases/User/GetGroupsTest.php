<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\GroupStub1;
use BusinessRules\Gateways\User\InMemoryGroupGateway;
use BusinessRules\Responders\User\GroupResponse;
use BusinessRules\UseCases\User\DTO\Request\GetGroupsRequestDTO;
use BusinessRules\UseCases\User\DTO\Response\GroupResponseFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetGroupsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GetGroups
     */
    private $useCase;

    /**
     * @test
     */
    public function WithoutGroup_ReturnEmpy()
    {
        InMemoryGroupGateway::$groups = array();

        $response = $this->useCase->execute(new GetGroupsRequestDTO());

        $this->assertEmpty($response->getGroups());
    }

    /**
     * @test
     */
    public function WithSomeGroup_ReturnList()
    {
        $response = $this->useCase->execute(new GetGroupsRequestDTO());

        $groupStub = 'BusinessRules\Entities\User\GroupStub';

        $this->assertCount(1, $response->getGroups());
        foreach ($response->getGroups() as $key => $group) {
            $this->assetGroup($groupStub . ($key + 1), $group);
        }
    }

    /**
     * @param               $groupStub
     * @param GroupResponse $group
     */
    private function assetGroup($groupStub, GroupResponse $group)
    {
        $this->assertEquals($groupStub::ID, $group->getId());
        $this->assertEquals($groupStub::NAME, $group->getName());
    }

    protected function setUp()
    {
        $this->useCase = new GetGroups();
        $this->useCase->setGroupGateway(new InMemoryGroupGateway(array(GroupStub1::ID => new GroupStub1())));
        $this->useCase->setGroupResponseFactory(new GroupResponseFactoryImpl());
    }
}
