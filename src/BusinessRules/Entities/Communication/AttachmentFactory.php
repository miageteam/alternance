<?php

namespace BusinessRules\Entities\Communication;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface AttachmentFactory
{
    /**
     * @param string  $attachmentPath
     * @param Message $message
     *
     * @return Attachment
     */
    public function create($attachmentPath, $message);
}
