<?php

namespace BusinessRules\Entities\Communication;

use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MessageFactory
{
    /**
     * @param string $content
     * @param Flow   $flow
     * @param User   $author
     *
     * @return Message
     */
    public function create($content, Flow $flow, User $author);
}
