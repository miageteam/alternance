<?php

namespace BusinessRules\Requestors\User;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetPaginatedUsersRequest extends UseCaseRequest
{
    /**
     * @return string[]
     */
    public function getSorts();

    /**
     * @return int
     */
    public function getPage();

    /**
     * @return int
     */
    public function getItemsPerPage();
}
