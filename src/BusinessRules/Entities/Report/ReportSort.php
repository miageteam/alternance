<?php

namespace BusinessRules\Entities\Report;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class ReportSort
{
    const DESC = 'desc';

    const ASC = 'asc';

    const CREATED_AT = 'createdAt';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
