<?php

namespace BusinessRules\Entities\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
abstract class User
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $loginIdentifier;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $phoneNumber;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $salt;

    /**
     * @var Group
     */
    protected $group;

    public function __construct()
    {
        $this->salt = $this->generateSalt();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getCompleteName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    /**
     * @return string
     */
    public function getLoginIdentifier()
    {
        return $this->loginIdentifier;
    }

    /**
     * @param string $loginIdentifier
     */
    public function setLoginIdentifier($loginIdentifier)
    {
        $this->loginIdentifier = $loginIdentifier;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return string
     */
    abstract protected function generateSalt();

    /**
     * @return bool
     */
    public function isStudent()
    {
        return $this->getGroup()->getName() === GroupName::STUDENT;
    }

    /**
     * @return bool
     */
    public function isTutor()
    {
        return $this->getGroup()->getName() === GroupName::TUTOR;
    }

    /**
     * @return bool
     */
    public function isProfessor()
    {
        return $this->getGroup()->getName() === GroupName::PROFESSOR;
    }

    /**
     * @return bool
     */
    public function isTrainingManager()
    {
        return $this->getGroup()->getName() === GroupName::TRAINING_MANAGER;
    }

    /**
     * @return bool
     */
    public function isSecretary()
    {
        return $this->getGroup()->getName() === GroupName::SECRETARY;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->getGroup()->getName() === GroupName::ADMIN;
    }
}