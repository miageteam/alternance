<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\ChangePasswordRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ChangePasswordRequestDTO implements ChangePasswordRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $newPassword;

    /**
     * @param int    $userId
     * @param string $newPassword
     */
    public function __construct($userId, $newPassword)
    {
        $this->userId      = $userId;
        $this->newPassword = $newPassword;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }
}
