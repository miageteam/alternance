<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\CreateMeetingRequestBuilder;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateMeetingRequestBuilderImpl implements CreateMeetingRequestBuilder
{

    /**
     * @var CreateMeetingRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new CreateMeetingRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPlannedAt(DateTime $plannedAt)
    {
        $this->request->plannedAt = $plannedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPlace($place)
    {
        $this->request->place = $place;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withParticipantIds(array $participantIds)
    {
        $this->request->participantIds = $participantIds;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
