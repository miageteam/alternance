<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Responders\Student\FormationResponse;
use BusinessRules\Responders\Student\FormationsResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationsResponseDTO implements FormationsResponse
{

    /**
     * @var FormationResponse[]
     */
    public $formations;

    /**
     * @param FormationResponse[] $formations
     */
    public function __construct($formations)
    {
        $this->formations = $formations;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormations()
    {
        return $this->formations;
    }
}
