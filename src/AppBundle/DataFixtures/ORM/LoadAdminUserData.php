<?php

namespace AppBundle\DataFixtures\ORM;

use BusinessRules\Entities\User\User;
use BusinessRules\Services\Security\PasswordEncoder;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use UserBundle\Entity\UserImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class LoadAdminUserData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * @var PasswordEncoderInterface
     */
    private $encoder;

    public function __construct()
    {
        $this->encoder = new PasswordEncoder();
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $admin = new UserImpl();
        $admin->setEmail('admin@mail.com');
        $admin->setFirstName('admin');
        $admin->setLastName('admin');
        $admin->setLoginIdentifier('admin');
        $admin->setPassword($this->encoder->encodePassword('admin', $admin->getSalt()));
        $admin->setGroup($this->getReference('Administrateur'));

        $this->insert($manager, $admin);
    }

    /**
     * @param ObjectManager $manager
     * @param User          $admin
     */
    private function insert(ObjectManager $manager, User $admin)
    {
        $manager->persist($admin);
        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
