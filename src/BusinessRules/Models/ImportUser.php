<?php

namespace BusinessRules\Models;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ImportUser
{
    const FIRST_NAME   = 'PRENOM';
    const LAST_NAME    = 'NOM';
    const EMAIL        = 'EMAIL';
    const PHONE_NUMBER = 'TELEPHONE';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
