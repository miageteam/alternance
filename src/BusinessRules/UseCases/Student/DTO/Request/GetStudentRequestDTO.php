<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\GetStudentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetStudentRequestDTO implements GetStudentRequest
{

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $companyTutorId;

    /**
     * @var int
     */
    public $promotionId;

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompanyTutorId()
    {
        return $this->companyTutorId;
    }

    /**
     * @return int
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }


}
