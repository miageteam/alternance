<?php

namespace BusinessRules\Entities\Student;

use AppBundle\Entity\Student\PromotionImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PromotionStub1 extends PromotionImpl
{
    const ID   = 1;
    const YEAR = '2014/2015';

    protected $id = self::ID;

    protected $year = self::YEAR;

    public function __construct()
    {
        $this->formation = new FormationStub1();
    }
}
