<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 08/06/2015
 * Time: 12:01
 */

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\UserStudentGateway;
use BusinessRules\Requestors\Student\GetStudentsRequest;
use BusinessRules\Responders\Student\UserStudentResponseFactory;
use BusinessRules\Responders\Student\UserStudentsResponse;
use BusinessRules\UseCases\Student\DTO\Response\UserStudentsResponseDTO;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

class GetStudents implements UseCase
{

    /**
     * @var UserStudentGateway
     */
    private $userStudentGateway;

    /**
     * @var UserStudentResponseFactory
     */
    private $userStudentResponseFactory;

    /**
     * @param GetStudentsRequest $request
     *
     * @return UserStudentsResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $allUserStudents = $this->userStudentGateway->findAllBySchoolTutor($request->getSchoolTutorId());

        return new UserStudentsResponseDTO($this->userStudentResponseFactory->createFromUserStudents($allUserStudents));
    }

    /**
     * @param UserStudentGateway $userStudentGateway
     */
    public function setUserStudentGateway(UserStudentGateway $userStudentGateway)
    {
        $this->userStudentGateway = $userStudentGateway;
    }

    /**
     * @param UserStudentResponseFactory $userStudentResponseFactory
     */
    public function setUserStudentResponseFactory(UserStudentResponseFactory $userStudentResponseFactory)
    {
        $this->userStudentResponseFactory = $userStudentResponseFactory;
    }
}
