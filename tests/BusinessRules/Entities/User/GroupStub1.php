<?php

namespace BusinessRules\Entities\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupStub1 extends Group
{
    const ID   = 1;
    const NAME = 'admin';

    public function __construct()
    {
        $this->userRoles = array(UserRoleStub1::ID => new UserRoleStub1());
    }

    protected $id = self::ID;

    protected $name = self::NAME;

}
