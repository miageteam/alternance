<?php

namespace BusinessRules\Entities\Student;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class UserStudentSort
{
    const DESC = 'desc';

    const ASC = 'asc';

    const LAST_NAME = 'lastName';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
