<?php

namespace BusinessRules\UseCases\Report\DTO\Response;

use BusinessRules\Entities\Report\Report;
use BusinessRules\Responders\Report\CommentResponse;
use BusinessRules\Responders\Report\CommentResponseFactory;
use BusinessRules\Responders\Report\ReportResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReportResponseFactoryImpl implements ReportResponseFactory
{

    /**
     * @var CommentResponseFactory
     */
    public $commentResponseFactory;

    /**
     * @var PaginatedUseCaseResponseBuilder
     */
    private $paginatedUseCaseResponseBuilder;

    /**
     * {@inheritdoc}
     */
    public function createFromReport(Report $report)
    {
        $response               = new ReportResponseDTO();
        $response->id           = $report->getId();
        $response->title        = $report->getTitle();
        $response->author       = $report->getAuthor();
        $response->comments     = $this->getReportComments($report);
        $response->totalComment = $report->getTotalComment();
        $response->attachment   = $report->getAttachment();
        $response->createdAt    = $report->getCreatedAt();
        $response->updatedAt    = $report->getUpdatedAt();
        $response->validated    = $report->isValidated();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromReports($reports)
    {
        $reportsResponse = array();
        foreach ($reports as $report) {
            $reportsResponse[] = $this->createFromReport($report);
        }

        return $reportsResponse;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromPaginatedCollection(PaginatedCollection $paginatedCollection)
    {
        return $this->paginatedUseCaseResponseBuilder
            ->create()
            ->withItems($this->createFromReports($paginatedCollection->getItems()))
            ->withItemsPerPage($paginatedCollection->getItemsPerPage())
            ->withPage($paginatedCollection->getPage())
            ->withTotalItems($paginatedCollection->getTotalItems())
            ->build();
    }

    /**
     * @param CommentResponseFactory $commentResponseFactory
     */
    public function setCommentResponseFactory(CommentResponseFactory $commentResponseFactory)
    {
        $this->commentResponseFactory = $commentResponseFactory;
    }

    /**
     * @param PaginatedUseCaseResponseBuilder $builder
     */
    public function setPaginatedUseCaseResponseBuilder(PaginatedUseCaseResponseBuilder $builder)
    {
        $this->paginatedUseCaseResponseBuilder = $builder;
    }

    /**
     * @param Report $report
     *
     * @return CommentResponse[]
     */
    private function getReportComments(Report $report)
    {
        return $this->commentResponseFactory->createFromComments($report->getComments());
    }
}
