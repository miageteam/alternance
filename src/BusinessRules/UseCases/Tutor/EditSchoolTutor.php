<?php

namespace BusinessRules\UseCases\Tutor;

use BusinessRules\Entities\Tutor\UserSchoolTutorFactory;
use BusinessRules\Gateways\Tutor\UserSchoolTutorGateway;
use BusinessRules\Requestors\Tutor\EditSchoolTutorRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditSchoolTutor implements UseCase
{

    /**
     * @var UserSchoolTutorGateway
     */
    private $userSchoolTutorGateway;

    /**
     * @var UserSchoolTutorFactory
     */
    private $userSchoolTutorFactory;

    /**
     * @Transaction
     *
     * @param EditSchoolTutorRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $schoolTutor = $this->userSchoolTutorGateway->findByUserId($request->getUserId());

        $updatedSchoolTutor = $this->userSchoolTutorFactory->editFromRequest($schoolTutor, $request);

        $this->userSchoolTutorGateway->update($updatedSchoolTutor);
    }

    /**
     * @param UserSchoolTutorGateway $userSchoolTutorGateway
     */
    public function setUserSchoolTutorGateway(UserSchoolTutorGateway $userSchoolTutorGateway)
    {
        $this->userSchoolTutorGateway = $userSchoolTutorGateway;
    }

    /**
     * @param UserSchoolTutorFactory $userSchoolTutorFactory
     */
    public function setUserSchoolTutorFactory(UserSchoolTutorFactory $userSchoolTutorFactory)
    {
        $this->userSchoolTutorFactory = $userSchoolTutorFactory;
    }
}
