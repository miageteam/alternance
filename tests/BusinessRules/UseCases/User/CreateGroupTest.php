<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\GroupStub1;
use BusinessRules\Entities\User\UserRoleStub1;
use BusinessRules\Gateways\User\InMemoryGroupGateway;
use BusinessRules\Requestors\User\CreateGroupRequestBuilder;
use BusinessRules\UseCases\User\DTO\Request\CreateGroupRequestBuilderImpl;
use UserBundle\Entity\GroupFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateGroupTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CreateGroup
     */
    private $useCase;

    /**
     * @var CreateGroupRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\GroupAlreadyExistException
     */
    public function ExistingGroup_ThrowException()
    {
        InMemoryGroupGateway::$groups = array(GroupStub1::ID => new GroupStub1());

        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withName(GroupStub1::NAME)
                ->withUserRoles(array(UserRoleStub1::ID => new UserRoleStub1()))
                ->build()
        );
    }

    /**
     * @test
     */
    public function CreateGroup()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withName(GroupStub1::NAME)
                ->withUserRoles(array(UserRoleStub1::ID => new UserRoleStub1()))
                ->build()
        );

        $this->assertCount(1, InMemoryGroupGateway::$groups);
        $this->assertEquals(GroupStub1::NAME, InMemoryGroupGateway::$groups[1]->getName());
        $this->assertEquals(UserRoleStub1::ID, InMemoryGroupGateway::$groups[1]->getUserRoles()[1]->getId());
    }

    protected function setUp()
    {
        $this->useCase = new CreateGroup();
        $this->useCase->setGroupFactory(new GroupFactoryImpl());
        $this->useCase->setGroupGateway(new InMemoryGroupGateway());
        $this->requestBuilder = new CreateGroupRequestBuilderImpl();
    }
}
