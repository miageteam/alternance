<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 15/06/2015
 * Time: 11:15
 */

namespace BusinessRules\Requestors\Student;


use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

interface GetPromotionRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getId();
}
