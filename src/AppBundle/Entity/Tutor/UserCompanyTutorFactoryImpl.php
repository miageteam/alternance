<?php

namespace AppBundle\Entity\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Entities\Tutor\UserCompanyTutorFactory;
use BusinessRules\Gateways\User\CompanyAddressGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Tutor\CreateCompanyTutorRequest;
use BusinessRules\Requestors\Tutor\EditCompanyTutorRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserCompanyTutorFactoryImpl implements UserCompanyTutorFactory
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * {@inheritdoc}
     */
    public function createFromRequest(CreateCompanyTutorRequest $request)
    {
        $user = $this->userGateway->find($request->getUserId());

        $userCompanyTutor = new UserCompanyTutorImpl();
        $userCompanyTutor->setUser($user);
        $userCompanyTutor->setCompanyName($request->getCompanyName());
        $userCompanyTutor->setStreet($request->getStreet());
        $userCompanyTutor->setZipCode($request->getZipCode());
        $userCompanyTutor->setCity($request->getCity());
        $userCompanyTutor->setCountry($request->getCountry());

        return $userCompanyTutor;
    }

    /**
     * {@inheritdoc}
     */
    public function editFromRequest(UserCompanyTutor $userCompanyTutor, EditCompanyTutorRequest $request)
    {
        $userCompanyTutor->setCompanyName($request->getCompanyName());
        $userCompanyTutor->setStreet($request->getStreet());
        $userCompanyTutor->setZipCode($request->getZipCode());
        $userCompanyTutor->setCity($request->getCity());
        $userCompanyTutor->setCountry($request->getCountry());

        return $userCompanyTutor;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }
}
