<?php

namespace BusinessRules\Responders\Student;

use BusinessRules\Entities\Student\Formation;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Kevin Leconte <leconte.kevink@gmail.com>
 * @author David Dieu <daviddieu80@gmail.com>
 */
interface PromotionResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return Formation
     */
    public function getFormation();

    /**
     * @return String
     */
    public function getYear();

    /**
     * @return String
     */
    public function getAlternationCalendarPath();
}
