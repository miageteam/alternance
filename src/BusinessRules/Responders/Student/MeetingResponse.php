<?php

namespace BusinessRules\Responders\Student;

use DateTime;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MeetingResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return DateTime
     */
    public function getPlannedAt();

    /**
     * @return string
     */
    public function getPlace();
}
