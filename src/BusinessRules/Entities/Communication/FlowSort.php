<?php

namespace BusinessRules\Entities\Communication;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class FlowSort
{
    const DESC = 'desc';

    const ASC = 'asc';

    const CREATED_AT = 'createdAt';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
