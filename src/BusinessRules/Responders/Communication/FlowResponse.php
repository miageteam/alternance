<?php

namespace BusinessRules\Responders\Communication;

use BusinessRules\Entities\Communication\Message;
use BusinessRules\Entities\User\User;
use DateTime;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FlowResponse extends UseCaseResponse
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return Message[]
     */
    public function getMessages();

    /**
     * @return Message
     */
    public function getFirstMessage();

    /**
     * @return Message
     */
    public function getLastMessage();

    /**
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * @return User[]
     */
    public function getParticipants();
}
