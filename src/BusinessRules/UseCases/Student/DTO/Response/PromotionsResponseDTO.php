<?php

namespace BusinessRules\UseCases\Student\DTO\Response;

use BusinessRules\Entities\Student\Formation;
use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Responders\Student\PromotionResponse;
use BusinessRules\Responders\Student\PromotionsResponse;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class PromotionsResponseDTO implements PromotionsResponse
{

    /**
     * @var Promotion[]
     */
    public $promotions;

    /**
     * @param Promotion[]
     */
    public function __construct($promotions)
    {
        $this->promotions = $promotions;
    }

    /**
     * @return PromotionResponse[]
     */
    public function getPromotions()
    {
        return $this->promotions;
    }


}
