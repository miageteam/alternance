<?php

namespace AppBundle\Repository\Student;

use BusinessRules\Entities\Student\Formation;
use BusinessRules\Gateways\Student\Exceptions\FormationNotFoundException;
use BusinessRules\Gateways\Student\FormationGateway;
use Doctrine\ORM\EntityRepository;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FormationRepository extends EntityRepository implements FormationGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(Formation $formation)
    {
        $this->_em->persist($formation);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $formation = parent::find($id);

        if (empty($formation)) {
            throw new FormationNotFoundException();
        }

        return $formation;
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Formation $formation)
    {
        $this->_em->remove($formation);
    }
}
