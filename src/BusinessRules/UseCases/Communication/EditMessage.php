<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Gateways\Communication\MessageGateway;
use BusinessRules\Requestors\Communication\EditMessageRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class EditMessage implements UseCase
{

    /**
     * @var MessageGateway
     */
    private $messageGateway;

    /**
     * @Transaction
     *
     * @param EditMessageRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $message = $this->messageGateway->find($request->getMessageId());

        $message->setContent($request->getContent());
        $message->setUpdatedAt(new \DateTime());

        $this->messageGateway->update($message);
    }

    /**
     * @param MessageGateway $messageGateway
     */
    public function setMessageGateway(MessageGateway $messageGateway)
    {
        $this->messageGateway = $messageGateway;
    }


}
