<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Request;

use BusinessRules\Requestors\TrainingManager\GetTrainingManagersRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetTrainingManagersRequestDTO implements GetTrainingManagersRequest
{

    /**
     * @var int
     */
    public $promotionId;

    /**
     * @return int
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }
}
