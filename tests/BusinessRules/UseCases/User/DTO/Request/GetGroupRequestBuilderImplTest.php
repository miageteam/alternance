<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Entities\User\GroupStub1;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetGroupRequestBuilderImplTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GetGroupRequestBuilderImpl
     */
    private $builder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Requestors\User\Exceptions\InvalidGetGroupRequestParameterException
     * @expectedExceptionMessage You should requested only one parameter
     */
    public function moreOfOnceParameter_ThrowException()
    {
        $this->builder
            ->create()
            ->byId(GroupStub1::ID)
            ->byName(GroupStub1::NAME)
            ->build();
    }

    /**
     * @test
     */
    public function byId()
    {
        $request = $this->builder
            ->create()
            ->byId(GroupStub1::ID)
            ->build();

        $this->assertEquals(GroupStub1::ID, $request->getId());
        $this->assertNull($request->getName());
    }

    /**
     * @test
     */
    public function byEmail()
    {
        $request = $this->builder
            ->create()
            ->byName(GroupStub1::NAME)
            ->build();

        $this->assertNull($request->getId());
        $this->assertEquals(GroupStub1::NAME, $request->getName());
    }

    protected function setUp()
    {
        $this->builder = new GetGroupRequestBuilderImpl();
    }
}
