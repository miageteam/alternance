<?php

namespace BusinessRules\Entities\Report;

use AppBundle\Entity\Report\AttachmentImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class AttachmentStub1 extends AttachmentImpl
{
    const ID   = 1;
    const PATH = 'www.monserveur.de/stockage';

    protected $id = self::ID;

    protected $path = self::PATH;

}
