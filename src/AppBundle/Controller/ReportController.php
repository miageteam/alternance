<?php

namespace AppBundle\Controller;

use AppBundle\Form\Model\CreateCommentModel;
use AppBundle\Form\Model\CreateReportModel;
use AppBundle\Form\Model\EditCommentModel;
use AppBundle\Form\Type\CreateCommentType;
use AppBundle\Form\Type\CreateReportType;
use AppBundle\Form\Type\EditCommentType;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Tutor\Exceptions\UserCompanyTutorNotFoundException;
use BusinessRules\Requestors\Report\EditCommentRequest;
use BusinessRules\Requestors\Report\GetPaginatedReportsRequest;
use BusinessRules\Requestors\Report\WriteCommentRequest;
use BusinessRules\Requestors\Report\WriteReportRequest;
use BusinessRules\Responders\Report\CommentResponse;
use BusinessRules\UseCases\Report\DTO\Request\GetCommentRequestDTO;
use BusinessRules\UseCases\Report\DTO\Request\GetReportRequestDTO;
use BusinessRules\UseCases\Report\DTO\Request\RemoveCommentRequestDTO;
use BusinessRules\UseCases\Report\DTO\Request\ValidateReportRequestDTO;
use BusinessRules\UseCases\Tutor\DTO\Request\GetCompanyTutorRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReportController extends Controller
{
    public function listAction(Request $request)
    {
        $response = $this->get('use_cases.report.get_paginated_reports')
            ->execute($this->getPaginatedReportsRequest($request, $this->getUser()->getId()));

        $pagination = array(
            'page'         => $response->getPage(),
            'route'        => 'app_user_report',
            'pages_count'  => $response->getTotalPages(),
            'route_params' => array()
        );

        return $this->render(
            'AppBundle:Report:list.html.twig',
            array(
                'reports'    => $response->getItems(),
                'pagination' => $pagination
            )
        );
    }

    /**
     * @param int  $page
     * @param User $user
     *
     * @return GetPaginatedReportsRequest
     */
    private function getPaginatedReportsRequest(Request $request, $userId)
    {
        return $this->get('request.report.get_paginated_reports_request_builder')
            ->create()
            ->withUserId($userId)
            ->withPage($request->get('page'))
            ->build();
    }

    /**
     * @Security("is_granted('ROLE_STUDENT')")
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(new CreateReportType());

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var CreateReportModel $model */
            $model = $form->getData();

            $model->upload();

            $this->get('use_cases.report.write_report')->execute($this->getWriteReportRequest($model));

            $this->addFlash('success', 'app.flash_message.add_report_success');

            return $this->redirectToRoute('app_user_report');
        }

        return $this->render(
            'AppBundle:Report:create.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @return WriteReportRequest
     */
    private function getWriteReportRequest(CreateReportModel $model)
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->get('request.report.write_report_request_builder')
            ->create()
            ->withTitle($model->title)
            ->withAuthorId($user->getId())
            ->withAttachmentPath($model->attachmentPath)
            ->build();
    }

    public function showAction($reportId)
    {
        $report = $this->get('use_cases.report.get_report')
            ->execute(new GetReportRequestDTO($reportId));

        return $this->render(
            'AppBundle:Report:show.html.twig',
            array('report' => $report)
        );
    }

    public function writeCommentAction(Request $request, $reportId)
    {
        $form = $this->createForm(
            new CreateCommentType(),
            null,
            array(
                'action' => $this->generateUrl('app_user_report_comment', array('reportId' => $reportId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var CreateCommentModel $model */
            $model = $form->getData();

            $this->get('use_cases.report.write_comment')
                ->execute($this->getWriteCommentRequest($model, $reportId));

            $this->addFlash('success', 'app.flash_message.add_comment_success');

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render(
            'AppBundle:Report/partials:comment_create.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @param CreateCommentModel $model
     * @param int                $reportId
     *
     * @return WriteCommentRequest
     */
    private function getWriteCommentRequest(CreateCommentModel $model, $reportId)
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->get('request.report.write_comment_request_builder')
            ->create()
            ->withContent($model->content)
            ->withAuthorId($user->getId())
            ->withReportId($reportId)
            ->build();
    }

    public function studentTutorReportAction(Request $request)
    {
        try {
            $companyTutor = $this->get('use_cases.tutor.get_company_tutor')
                ->execute(new GetCompanyTutorRequestDTO($this->getUser()->getId()));

            $student = $this->get('use_cases.student.get_student')
                ->execute(
                    $this->get('request.student.get_student_request_builder')
                        ->create()
                        ->byCompanyTutorId($companyTutor->getId())
                        ->build()
                );

            $response = $this->get('use_cases.report.get_paginated_reports')
                ->execute($this->getPaginatedReportsRequest($request, $student->getUser()->getId()));

            $pagination = array(
                'page'         => $response->getPage(),
                'route'        => 'app_user_report',
                'pages_count'  => $response->getTotalPages(),
                'route_params' => array()
            );

            $reports = $response->getItems();
        } catch (UserCompanyTutorNotFoundException $e) {
            $reports = array();
            $pagination = array(
                'page'         => 1,
                'route'        => 'app_user_report',
                'pages_count'  => 1,
                'route_params' => array()
            );
        }

        return $this->render(
            'AppBundle:Report:list.html.twig',
            array(
                'reports'    => $reports,
                'pagination' => $pagination
            )
        );
    }

    public function validateReportAction(Request $request, $reportId)
    {
        $this->get('use_cases.report.validate_report')
            ->execute(new ValidateReportRequestDTO($reportId));

        $this->addFlash('success', 'app.flash_message.report_validated_success');

        return $this->redirect($request->headers->get('referer'));
    }

    public function removeCommentAction($reportId, $commentId)
    {
        $comment = $this->get('use_cases.report.get_comment')
            ->execute(new GetCommentRequestDTO($commentId));

        if (!$this->isAuthor($comment->getAuthor())) {
            $this->addFlash('error', 'app.flash_message.comment_remove_author_fail');

            return $this->redirectToRoute('app_user_report_show', array('reportId' => $reportId));
        }

        $this->get('use_cases.report.remove_comment')
            ->execute(new RemoveCommentRequestDTO($commentId));

        $this->addFlash('success', 'app.flash_message.comment_remove_success');

        return $this->redirectToRoute('app_user_report_show', array('reportId' => $reportId));
    }

    public function editCommentAction(Request $request, $reportId, $commentId)
    {
        $comment = $this->get('use_cases.report.get_comment')
            ->execute(new GetCommentRequestDTO($commentId));

        if (!$this->isAuthor($comment->getAuthor())) {
            $this->addFlash('error', 'app.flash_message.comment_edit_author_fail');

            return $this->redirectToRoute('app_user_report_show', array('reportId' => $reportId));
        }

        $model          = new EditCommentModel();
        $model->content = $comment->getContent();

        $form = $this->createForm(
            new EditCommentType(),
            $model,
            array(
                'action' => $this->generateUrl(
                    'app_report_edit_comment',
                    array('reportId' => $reportId, 'commentId' => $commentId)
                )
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EditCommentModel $model */
            $model = $form->getData();

            $this->get('use_cases.report.edit_comment')
                ->execute($this->getEditCommentRequest($comment, $model));

            $this->addFlash('success', 'app.flash_message.comment_edit_success');

            return $this->redirectToRoute('app_user_report_show', array('reportId' => $reportId));
        }

        return new Response(
            $this->renderView(
                'AppBundle:Report/partials:comment_edit.html.twig',
                array('form' => $form->createView())
            )
        );
    }

    /**
     * @param CommentResponse  $comment
     * @param EditCommentModel $model
     *
     * @return EditCommentRequest
     */
    private function getEditCommentRequest(CommentResponse $comment, EditCommentModel $model)
    {
        return $this->get('request.report.edit_comment_request_builder')
            ->create()
            ->withCommentId($comment->getId())
            ->withContent($model->content)
            ->build();
    }

    /**
     * @param User $author
     *
     * @return bool
     */
    private function isAuthor(User $author)
    {
        return $author->getId() === $this->getUser()->getId();
    }

    public function studentProfessorReportAction(Request $request, $studentUserId)
    {
        $response = $this->get('use_cases.report.get_paginated_reports')
            ->execute(
                $this->get('request.report.get_paginated_reports_request_builder')
                    ->create()
                    ->withUserId($studentUserId)
                    ->withIsValidated(true)
                    ->withPage($request->get('page'))
                    ->build()
            );

        $pagination = array(
            'page'         => $response->getPage(),
            'route'        => 'app_user_report',
            'pages_count'  => $response->getTotalPages(),
            'route_params' => array()
        );

        return $this->render(
            'AppBundle:Report:list.html.twig',
            array(
                'reports'    => $response->getItems(),
                'pagination' => $pagination
            )
        );
    }
}
