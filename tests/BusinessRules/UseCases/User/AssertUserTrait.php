<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\PromotionStub1;
use BusinessRules\Responders\User\UserResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
trait AssertUserTrait
{
    /**
     * @param UserResponse $response
     */
    private function assertUser($userStub, UserResponse $response)
    {
        $this->assertEquals($userStub::ID, $response->getId());
        $this->assertEquals($userStub::FIRSTNAME, $response->getFirstName());
        $this->assertEquals($userStub::LASTNAME, $response->getLastName());
        $this->assertEquals($userStub::LOGIN_IDENTIFIER, $response->getLoginIdentifier());
        $this->assertEquals($userStub::EMAIL, $response->getEmail());
    }
}
