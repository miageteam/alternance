<?php

namespace BusinessRules\Requestors\Tutor;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetCompanyTutorRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();
}
