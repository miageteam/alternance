<?php

namespace AppBundle\Controller;

use BusinessRules\Entities\User\User;
use BusinessRules\UseCases\User\DTO\Request\ChangePasswordRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\Model\ChangePasswordModel;
use UserBundle\Form\Type\ChangePasswordType;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class AccountController extends Controller
{
    /**
     * @Security("is_granted('ROLE_USER')")
     */
    public function showAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $formPassword = $this->buildChangePasswordForm($request);

        if (is_null($formPassword)) {
            return $this->redirectToRoute('app_account');
        }

        return $this->render('AppBundle:Account:show.html.twig', array(
                'user' => $user,
                'formPassword' => $formPassword->createView(),
            )
        );
    }


    private function buildChangePasswordForm(Request $request)
    {
        $form = $this->createForm(new ChangePasswordType());

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var ChangePasswordModel $model */
            $model = $form->getData();
            /** @var User $user */
            $user = $this->getUser();
            $this->get('use_cases.user.change_password')
                 ->execute(new ChangePasswordRequestDTO($user->getId(), $model->newPassword));

            $this->addFlash('success', 'app.flash_message.change_password_success');

            return null;
        }

        return $form;
    }
}
