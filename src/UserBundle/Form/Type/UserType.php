<?php

namespace UserBundle\Form\Type;

use BusinessRules\Entities\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use UserBundle\Form\Model\UserModel;
use UserBundle\Repository\GroupRepository;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserType extends AbstractType
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('email', 'email')
            ->add('phoneNumber');

        if ($user->isAdmin()) {
            $builder->add(
                'group',
                'entity',
                array(
                    'class'       => 'UserBundle\Entity\GroupImpl',
                    'placeholder' => ''
                )
            );
        } else {
            $builder->add(
                'group',
                'entity',
                array(
                    'class'         => 'UserBundle\Entity\GroupImpl',
                    'query_builder' => function (GroupRepository $er) {
                        return $er->getAllWithoutAdminAndSecretaryQueryBuilder();
                    },
                    'placeholder'   => ''
                )
            );
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'UserBundle\Form\Model\UserModel',
                'intention'  => 'user',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user';
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
}
