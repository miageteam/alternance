<?php

namespace BusinessRules\Responders\User;

use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GroupsResponse extends UseCaseResponse
{
    /**
     * @return GroupResponse[]
     */
    public function getGroups();
}
