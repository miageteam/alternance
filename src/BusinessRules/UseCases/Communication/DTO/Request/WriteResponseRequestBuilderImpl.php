<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\Exceptions\InvalidWriteResponseRequestParameterException;
use BusinessRules\Requestors\Communication\WriteResponseRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteResponseRequestBuilderImpl implements WriteResponseRequestBuilder
{

    /**
     * @var WriteResponseRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new WriteResponseRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withFlowId($flowid)
    {
        $this->request->flowId = $flowid;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withContent($content)
    {
        $this->request->content = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAuthorId($authorId)
    {
        $this->request->authorId = $authorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withAttachmentPath($attachmentPath)
    {
        $this->request->attachmentPath = $attachmentPath;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $this->CheckIfParameterIsValid();

        return $this->request;
    }

    private function CheckIfParameterIsValid()
    {
        if (empty($this->request->flowId)) {
            throw new InvalidWriteResponseRequestParameterException('The argument "flowId" must be defined.');
        }

        if (empty($this->request->content)) {
            throw new InvalidWriteResponseRequestParameterException('The argument "Content" must be defined.');
        }

        if (empty($this->request->authorId)) {
            throw new InvalidWriteResponseRequestParameterException('The argument "AuthorId" must be defined.');
        }
    }
}
