<?php

namespace BusinessRules\Entities\Communication;

use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub2;
use CommunicationBundle\Entity\FlowImpl;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowStub1 extends FlowImpl
{
    const ID         = 1;
    const TITLE      = 'This Flow is so Cool !';
    const CREATED_AT = '2015-04-20';

    public function __construct()
    {
        $this->participants = array(new UserStub1(), new UserStub2());
        parent::__construct();
        $this->createdAt    = new DateTime(self::CREATED_AT);
    }

    protected $id = self::ID;

    protected $title = self::TITLE;

    /**
     * @param Message[] $messages
     */
    public function setMessages(array $messages)
    {
        $this->messages = $messages;
    }
}
