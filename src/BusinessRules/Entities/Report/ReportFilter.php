<?php

namespace BusinessRules\Entities\Report;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class ReportFilter
{
    const USER = 'user';

    const VALIDATED = 'validated';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
