<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\EditStudentRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditStudentRequestBuilderImpl implements EditStudentRequestBuilder
{

    /**
     * @var EditStudentRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new EditStudentRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withPromotionId($promotionId)
    {
        $this->request->promotionId = $promotionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserSchoolTutorId($userSchoolTutorId)
    {
        $this->request->userSchoolTutorId = $userSchoolTutorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserCompanyTutorId($userCompanyTutorId)
    {
        $this->request->userCompanyTutorId = $userCompanyTutorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
