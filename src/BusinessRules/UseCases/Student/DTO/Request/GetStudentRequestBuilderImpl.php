<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\Exceptions\InvalidGetStudentRequestParameterException;
use BusinessRules\Requestors\Student\GetStudentRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetStudentRequestBuilderImpl implements GetStudentRequestBuilder
{

    /**
     * @var GetStudentRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetStudentRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byUserId($userId)
    {
        $this->request->userId = $userId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byCompanyTutorId($companyTutorId)
    {
        $this->request->companyTutorId = $companyTutorId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        if ($this->countNotEmptyParameter() > 1) {
            throw new InvalidGetStudentRequestParameterException('You should requested only one parameter');
        }

        return $this->request;
    }

    /**
     * @return int
     */
    private function countNotEmptyParameter()
    {
        $arrayRequest = get_object_vars($this->request);

        $countNotEmptyParameter = 0;
        foreach ($arrayRequest as $parameter) {
            if (!empty($parameter)) {
                $countNotEmptyParameter++;
            }
        }

        return $countNotEmptyParameter;
    }
}
