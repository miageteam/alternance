<?php

namespace BusinessRules\Requestors\User;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetUserRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getLoginIdentifier();
}
