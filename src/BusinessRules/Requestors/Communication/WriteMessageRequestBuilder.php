<?php

namespace BusinessRules\Requestors\Communication;

use BusinessRules\Requestors\Communication\Exceptions\InvalidWriteMessageRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteMessageRequestBuilder
{
    /**
     * @return WriteMessageRequestBuilder
     */
    public function create();

    /**
     * @param string $content
     *
     * @return WriteMessageRequestBuilder
     */
    public function withContent($content);

    /**
     * @param int $authorId
     *
     * @return WriteMessageRequestBuilder
     */
    public function withAuthorId($authorId);

    /**
     * @param string $title
     *
     * @return WriteMessageRequestBuilder
     */
    public function withTitle($title);

    /**
     * @param int[] $participantIds
     *
     * @return WriteMessageRequestBuilder
     */
    public function withParticipantIds(array $participantIds);

    /**
     * @param string $attachmentPath
     *
     * @return WriteMessageRequestBuilder
     */
    public function withAttachmentPath($attachmentPath);

    /**
     * @return WriteMessageRequest
     *
     * @throws InvalidWriteMessageRequestParameterException
     */
    public function build();
}
