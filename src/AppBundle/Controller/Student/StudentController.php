<?php

namespace AppBundle\Controller\Student;

use AppBundle\Form\Model\EditUserStudentModel;
use AppBundle\Form\Model\UserStudentModel;
use AppBundle\Form\Type\EditUserStudentType;
use AppBundle\Form\Type\UserStudentType;
use BusinessRules\Gateways\Student\Exceptions\MeetingNotFoundException;
use BusinessRules\Gateways\Student\Exceptions\UserStudentNotFoundException;
use BusinessRules\Requestors\User\GetUserRequest;
use BusinessRules\UseCases\Student\DTO\Request\CreateStudentRequestDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class StudentController extends Controller
{
    public function showAction($userId)
    {
        try {
            $student = $this->get('use_cases.student.get_student')
                ->execute(
                    $this->get('request.student.get_student_request_builder')
                        ->create()
                        ->byUserId($userId)
                        ->build()
                );

            return $this->render('AppBundle:Student/Student:show.html.twig', array('student' => $student));
        } catch (UserStudentNotFoundException $e) {
            return $this->forward('AppBundle:Student/Student:create', array('userId' => $userId));
        }
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function createAction(Request $request, $userId)
    {
        $form = $this->createForm(
            new UserStudentType(),
            null,
            array(
                'action' => $this->generateUrl('app_user_student_create', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var UserStudentModel $model */
            $model = $form->getData();

            $this->get('use_cases.student.create_student')
                ->execute($this->getCreateStudentRequest($model, $userId));

            $user = $this->get('use_cases.user.get_user')->execute($this->getUserRequest($userId));

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $user->getLoginIdentifier())
            );
        }

        return $this->render('AppBundle:Student/Student:create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param UserStudentModel $model
     * @param int              $userId
     *
     * @return CreateStudentRequestDTO
     */
    private function getCreateStudentRequest(UserStudentModel $model, $userId)
    {
        return $this->get('request.student.create_student_request_builder')
            ->create()
            ->withUserId($userId)
            ->withPromotionId($model->promotion)
            ->withUserCompanyTutorId($model->userCompanyTutor)
            ->withUserSchoolTutorId($model->userSchoolTutor)
            ->build();
    }

    /**
     * @param int $userId
     *
     * @return GetUserRequest
     */
    private function getUserRequest($userId)
    {
        return $this->get('request.user.get_user_request_builder')
            ->create()
            ->byId($userId)
            ->build();
    }

    public function getTutorsAction()
    {
        try {
            $student = $this->get('use_cases.student.get_student')
                ->execute(
                    $this->get('request.student.get_student_request_builder')
                        ->create()
                        ->byUserId($this->getUser()->getId())
                        ->build()
                );

            $schoolTutor = $student->getUserSchoolTutor();
            $companyTutor = $student->getUserCompanyTutor();

        } catch (UserStudentNotFoundException $e) {
            $schoolTutor = null;
            $companyTutor = null;
        }

        return $this->render(
            'AppBundle:Student/Student/partials:get_tutors.html.twig',
            array(
                'schoolTutor'  => $schoolTutor,
                'companyTutor' => $companyTutor
            )
        );
    }

    public function getAlternationCalendarAction()
    {
        try {
            $student = $this->get('use_cases.student.get_student')
                ->execute(
                    $this->get('request.student.get_student_request_builder')
                        ->create()
                        ->byUserId($this->getUser()->getId())
                        ->build()
                );

            $alternationCalendarPath = $student->getPromotion()->getAlternationCalendarPath();
        } catch (UserStudentNotFoundException $e) {
            $alternationCalendarPath = null;
        }

        return $this->render(
            'AppBundle:Student/Student/partials:get_alternation_calendar.html.twig',
            array('alternationCalendarPath' => $alternationCalendarPath)
        );
    }

    public function getMeetingAction()
    {
        try {
            $meeting = $this->get('use_cases.student.get_meeting')
                ->execute(
                    $this->get('request.student.get_meeting_request_builder')
                        ->create()
                        ->byUserId($this->getUser()->getId())
                        ->build()
                );

            return $this->render(
                'AppBundle:Student/Student/partials:get_meeting.html.twig',
                array('meeting' => $meeting)
            );
        } catch (MeetingNotFoundException $e) {
            return $this->render('AppBundle:Student/Student/partials:get_meeting.html.twig');
        }
    }

    /**
     * @Security("is_granted('ROLE_TRAINING_MANAGER')")
     */
    public function editAction(Request $request, $userId)
    {
        $student = $this->get('use_cases.student.get_student')
            ->execute(
                $this->get('request.student.get_student_request_builder')
                    ->create()
                    ->byUserId($userId)
                    ->build()
            );

        $model                   = new EditUserStudentModel();
        $model->promotion        = $student->getPromotion();
        $model->userCompanyTutor = $student->getUserCompanyTutor();
        $model->userSchoolTutor  = $student->getUserSchoolTutor();

        $form = $this->createForm(
            new EditUserStudentType(),
            $model,
            array(
                'action' => $this->generateUrl('app_user_student_edit', array('userId' => $userId))
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EditUserStudentModel $model */
            $model = $form->getData();

            $this->get('use_cases.student.edit_student')->execute($this->getEditStudentRequest($model, $userId));

            $this->addFlash('success', 'app.flash_message.edit_student_success');

            return $this->redirectToRoute(
                'user_bundle_show_user',
                array('userIdentifier' => $student->getUser()->getLoginIdentifier())
            );
        }

        return $this->render('AppBundle:Student/Student:edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param EditUserStudentModel $model
     * @param int                  $userId
     *
     * @return CreateStudentRequestDTO
     */
    private function getEditStudentRequest(EditUserStudentModel $model, $userId)
    {
        return $this->get('request.student.edit_student_request_builder')
            ->create()
            ->withUserId($userId)
            ->withPromotionId($model->promotion)
            ->withUserCompanyTutorId($model->userCompanyTutor)
            ->withUserSchoolTutorId($model->userSchoolTutor)
            ->build();
    }
}
