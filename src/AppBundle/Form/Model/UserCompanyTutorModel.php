<?php

namespace AppBundle\Form\Model;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserCompanyTutorModel
{

    /**
     * @var string
     */
    public $companyName;

    /**
     * @var string
     */
    public $street;

    /**
     * @var int
     */
    public $zipCode;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $country;

}
