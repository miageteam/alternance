<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Student\RemoveCommentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class RemoveCommentRequestDTO implements RemoveCommentRequest
{

    /**
     * @var int
     */
    public $commentId;

    /**
     * @param int $commentId
     */
    public function __construct($commentId)
    {
        $this->commentId = $commentId;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommentId()
    {
        return $this->commentId;
    }
}
