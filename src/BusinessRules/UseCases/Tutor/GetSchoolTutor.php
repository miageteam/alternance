<?php

namespace BusinessRules\UseCases\Tutor;

use BusinessRules\Gateways\Tutor\UserSchoolTutorGateway;
use BusinessRules\Requestors\Tutor\GetSchoolTutorRequest;
use BusinessRules\Responders\Tutor\UserSchoolTutorResponse;
use BusinessRules\Responders\Tutor\UserSchoolTutorResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetSchoolTutor implements UseCase
{

    /**
     * @var UserSchoolTutorGateway
     */
    private $userSchoolTutorGateway;

    /**
     * @var UserSchoolTutorResponseFactory
     */
    private $userSchoolTutorResponseFactory;

    /**
     * @param GetSchoolTutorRequest $request
     *
     * @return UserSchoolTutorResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $userSchoolTutor = $this->userSchoolTutorGateway->findByUserId($request->getUserId());

        return $this->userSchoolTutorResponseFactory->createFromUserSchoolTutor($userSchoolTutor);
    }

    /**
     * @param UserSchoolTutorGateway $userSchoolTutorGateway
     */
    public function setUserSchoolTutorGateway(UserSchoolTutorGateway $userSchoolTutorGateway)
    {
        $this->userSchoolTutorGateway = $userSchoolTutorGateway;
    }

    /**
     * @param UserSchoolTutorResponseFactory $userSchoolTutorResponseFactory
     */
    public function setUserSchoolTutorResponseFactory(UserSchoolTutorResponseFactory $userSchoolTutorResponseFactory)
    {
        $this->userSchoolTutorResponseFactory = $userSchoolTutorResponseFactory;
    }
}
