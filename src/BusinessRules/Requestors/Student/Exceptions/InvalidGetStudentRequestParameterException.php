<?php

namespace BusinessRules\Requestors\Student\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InvalidGetStudentRequestParameterException extends \Exception
{

}
