<?php

namespace BusinessRules\Requestors\TrainingManager;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
interface EditTrainingManagerRequestBuilder
{
    /**
     * @return EditTrainingManagerRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return EditTrainingManagerRequestBuilder
     */
    public function withUserId($userId);

    /**
     * @param int $promotionId
     *
     * @return EditTrainingManagerRequestBuilder
     */
    public function withPromotionId($promotionId);

    /**
     * @return EditTrainingManagerRequest
     */
    public function build();
}
