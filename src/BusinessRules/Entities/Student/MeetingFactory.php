<?php

namespace BusinessRules\Entities\Student;

use BusinessRules\Requestors\Student\CreateMeetingRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MeetingFactory
{
    /**
     * @param CreateMeetingRequest $request
     *
     * @return Meeting
     */
    public function createFromRequest(CreateMeetingRequest $request);
}
