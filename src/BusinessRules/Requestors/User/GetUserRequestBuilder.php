<?php

namespace BusinessRules\Requestors\User;

use BusinessRules\Requestors\User\Exceptions\InvalidGetUserRequestParameterException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetUserRequestBuilder
{
    /**
     * @return GetUserRequestBuilder
     */
    public function create();

    /**
     * @param int $id
     *
     * @return GetUserRequestBuilder
     */
    public function byId($id);

    /**
     * @param string $email
     *
     * @return GetUserRequestBuilder
     */
    public function byEmail($email);

    /**
     * @param string $loginIdentifier
     *
     * @return GetUserRequestBuilder
     */
    public function byLoginIdentifier($loginIdentifier);

    /**
     * @return GetUserRequest
     *
     * @throws InvalidGetUserRequestParameterException
     */
    public function build();
}
