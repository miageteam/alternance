<?php

namespace BusinessRules\UseCases\Tutor\DTO\Response;

use BusinessRules\Entities\Student\UserStudent;
use BusinessRules\Entities\User\User;
use BusinessRules\Responders\Tutor\UserSchoolTutorResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserSchoolTutorResponseDTO implements UserSchoolTutorResponse
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $companyName;

    /**
     * @var string
     */
    public $street;

    /**
     * @var int
     */
    public $zipCode;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $country;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * {@inheritdoc}
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * {@inheritdoc}
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        return $this->country;
    }
}
