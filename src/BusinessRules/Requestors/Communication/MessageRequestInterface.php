<?php

namespace BusinessRules\Requestors\Communication;

use Doctrine\Common\Collections\ArrayCollection;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MessageRequestInterface extends UseCaseRequest
{
    /**
     * @return string
     */
    public function getContent();

    /**
     * @return int
     */
    public function getAuthorId();

    /**
     * @return string
     */
    public function getAttachmentPath();
}
