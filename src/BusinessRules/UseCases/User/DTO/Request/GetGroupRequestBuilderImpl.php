<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\Exceptions\InvalidGetGroupRequestParameterException;
use BusinessRules\Requestors\User\GetGroupRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetGroupRequestBuilderImpl implements GetGroupRequestBuilder
{

    /**
     * @var GetGroupRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetGroupRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byId($id)
    {
        $this->request->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byName($name)
    {
        $this->request->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        if ($this->countNotEmptyParameter() > 1) {
            throw new InvalidGetGroupRequestParameterException('You should requested only one parameter');
        }

        return $this->request;
    }

    /**
     * @return int
     */
    private function countNotEmptyParameter()
    {
        $arrayRequest = get_object_vars($this->request);

        $countNotEmptyParameter = 0;
        foreach ($arrayRequest as $parameter) {
            if (!empty($parameter)) {
                $countNotEmptyParameter++;
            }
        }

        return $countNotEmptyParameter;
    }
}
