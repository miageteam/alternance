<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\GetCommentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetCommentRequestDTO implements GetCommentRequest
{

    /**
     * @var int
     */
    public $id;

    /**
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
}
