<?php

namespace BusinessRules\Responders\Communication;

use BusinessRules\Entities\Communication\Message;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MessageResponseFactory
{
    /**
     * @param Message $message
     *
     * @return MessageResponse
     */
    public function createFromMessage(Message $message);

    /**
     * @param Message[] $message
     *
     * @return MessageResponse[]
     */
    public function createFromMessages($message);
}
