<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Gateways\User\GroupGateway;
use BusinessRules\Requestors\User\GetGroupsRequest;
use BusinessRules\Responders\User\GroupResponseFactory;
use BusinessRules\Responders\User\GroupsResponse;
use BusinessRules\UseCases\User\DTO\Response\GroupsResponseDTO;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetGroups implements UseCase
{

    /**
     * @var GroupGateway
     */
    private $groupGateway;

    /**
     * @var GroupResponseFactory
     */
    private $groupResponseFactory;

    /**
     * @param GetGroupsRequest $useCaseRequest
     *
     * @return GroupsResponse
     */
    public function execute(UseCaseRequest $useCaseRequest)
    {
        $groups = $this->groupGateway->findAll();

        return new GroupsResponseDTO($this->groupResponseFactory->createFromGroups($groups));
    }

    /**
     * @param GroupGateway $groupGateway
     */
    public function setGroupGateway(GroupGateway $groupGateway)
    {
        $this->groupGateway = $groupGateway;
    }

    /**
     * @param GroupResponseFactory $groupResponseFactory
     */
    public function setGroupResponseFactory(GroupResponseFactory $groupResponseFactory)
    {
        $this->groupResponseFactory = $groupResponseFactory;
    }
}
