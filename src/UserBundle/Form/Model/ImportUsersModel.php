<?php

namespace UserBundle\Form\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ImportUsersModel
{

    /**
     * @var UploadedFile
     *
     * @Assert\File()
     */
    public $csvFile;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     */
    public $promotion;

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->csvFile->getClientOriginalName();
    }

    public function getWebPath()
    {
        return $this->getUploadDir() . '/' . $this->csvFile->getClientOriginalName();
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'tmp/csv_import';
    }

    public function upload()
    {
        if (null === $this->csvFile) {
            return null;
        }

        $this->csvFile->move($this->getUploadRootDir(), $this->csvFile->getClientOriginalName());
    }

    public function removeFile()
    {
        if (file_exists($this->getAbsolutePath())) {
            unlink($this->getAbsolutePath());
        }
    }
}
