<?php

namespace UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateGroupType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('userRoles', 'entity', array(
                    'class' => 'UserBundle\Entity\UserRoleImpl',
                    'property' => 'label',
                    'read_only' => true,
                    'expanded' => true,
                    'multiple' => true,
                )
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           'data_class' => 'UserBundle\Form\Model\GroupModel',
           'intention'  => 'create_group',
       ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'create_group';
    }
}
