<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\WriteMessageRequest;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteMessageRequestDTO implements WriteMessageRequest
{

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $authorId;

    /**
     * @var string
     */
    public $attachmentPath;

    /**
     * @var string
     */
    public $title;

    /**
     * @var int[]|ArrayCollection
     */
    public $participantIds;

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttachmentPath()
    {
        return $this->attachmentPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function getParticipantIds()
    {
        return $this->participantIds;
    }
}
