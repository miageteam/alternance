<?php
/**
 * Created by PhpStorm.
 * User: Corkyman
 * Date: 15/06/2015
 * Time: 11:24
 */

namespace BusinessRules\UseCases\Student\DTO\Request;


use BusinessRules\Requestors\Student\GetPromotionRequestBuilder;

class GetPromotionRequestBuilderImpl implements GetPromotionRequestBuilder
{

    /**
     * @var GetPromotionRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new GetPromotionRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function byId($id)
    {
        $this->request->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
