<?php

namespace BusinessRules\Gateways\TrainingManager;

use BusinessRules\Entities\TrainingManager\TrainingManager;
use BusinessRules\Gateways\TrainingManager\Exceptions\TrainingManagerNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface TrainingManagerGateway
{
    /**
     * @param TrainingManager $trainingManage
     */
    public function insert(TrainingManager $trainingManager);

    /**
     * @param int $userId
     *
     * @return TrainingManager
     *
     * @throws TrainingManagerNotFoundException
     */
    public function findByUserId($userId);

    /**
     * @param int $promotionId
     *
     * @return TrainingManager[]
     */
    public function findAllByPromotionId($promotionId);

    /**
     * @param TrainingManager $trainingManager
     */
    public function update(TrainingManager $trainingManager);
}
