<?php

namespace BusinessRules\Responders\Tutor;

use BusinessRules\Entities\Student\UserStudent;
use BusinessRules\Entities\User\User;
use OpenClassrooms\UseCase\BusinessRules\Responders\UseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserSchoolTutorResponse extends UseCaseResponse
{

    /**
     * @return int
     */
    public function getId();

    /**
     * @return User
     */
    public function getUser();

    /**
     * @return string
     */
    public function getCompanyName();

    /**
     * @return string
     */
    public function getStreet();

    /**
     * @return int
     */
    public function getZipCode();

    /**
     * @return string
     */
    public function getCity();

    /**
     * @return string
     */
    public function getCountry();
}
