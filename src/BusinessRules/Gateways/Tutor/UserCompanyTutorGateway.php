<?php

namespace BusinessRules\Gateways\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutor;
use BusinessRules\Gateways\Tutor\Exceptions\UserCompanyTutorNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserCompanyTutorGateway
{
    /**
     * @param UserCompanyTutor $userCompanyTutor
     */
    public function insert(UserCompanyTutor $userCompanyTutor);

    /**
     * @param int $id
     *
     * @return UserCompanyTutor
     *
     * @throws UserCompanyTutorNotFoundException
     */
    public function find($id);

    /**
     * @param int $userId
     *
     * @return UserCompanyTutor
     *
     * @throws UserCompanyTutorNotFoundException
     */
    public function findByUserId($userId);

    /**
     * @param UserCompanyTutor $userCompanyTutor
     */
    public function update(UserCompanyTutor $userCompanyTutor);
}
