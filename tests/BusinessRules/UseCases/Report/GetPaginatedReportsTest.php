<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Entities\Report\ReportSort;
use BusinessRules\Entities\Report\ReportStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\Report\InMemoryReportGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Report\GetPaginatedReportsRequestBuilder;
use BusinessRules\Responders\Report\CommentResponse;
use BusinessRules\Responders\Report\ReportResponse;
use BusinessRules\UseCases\Report\DTO\Request\GetPaginatedReportsRequestBuilderImpl;
use BusinessRules\UseCases\Report\DTO\Response\CommentResponseFactoryImpl;
use BusinessRules\UseCases\Report\DTO\Response\ReportResponseFactoryImpl;
use DateTime;
use OpenClassrooms\UseCase\Application\Responder\PaginatedUseCaseResponseBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedReportsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GetPaginatedReports
     */
    private $useCase;

    /**
     * @var GetPaginatedReportsRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function UserNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();

        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withUserId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     */
    public function EmptyList_ReturnEmpty()
    {
        InMemoryReportGateway::$reports = array();

        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->build()
        );

        $this->assertEmpty($response->getItems());
        $this->assertCount(0, $response->getItems());
        $this->assertEquals(0, $response->getTotalItems());
    }

    /**
     * @test
     */
    public function GetReports_ReturnList()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withUserId(UserStub1::ID)
                ->withSorts(array(ReportSort::CREATED_AT => ReportSort::DESC))
                ->withItemsPerPage(10)
                ->withPage(1)
                ->build()
        );

        $this->assertCount(1, $response->getItems());
        $reportStub = 'BusinessRules\Entities\Report\ReportStub';
        foreach ($response->getItems() as $key => $report) {
            $this->assertReport($reportStub . ($key + 1), $report);
        }

    }

    private function assertReport($reportStub, ReportResponse $response)
    {
        $this->assertEquals($reportStub::ID, $response->getId());
        $this->assertEquals($reportStub::TITLE, $response->getTitle());
        $this->assertEquals(UserStub1::ID, $response->getAuthor()->getId());
        $this->assertEquals(new DateTime($reportStub::CREATED_AT), $response->getCreatedAt());
        $this->assertEquals(new DateTime($reportStub::UPDATED_AT), $response->getUpdatedAt());

        $commentStub = 'BusinessRules\Entities\Report\CommentStub';
        foreach ($response->getComments() as $key => $comment) {
            $this->assertComment($commentStub . ($key + 1), $comment);
        }
    }

    private function assertComment($commentStub, CommentResponse $response)
    {
        $this->assertEquals($commentStub::ID, $response->getId());
        $this->assertEquals($commentStub::CONTENT, $response->getContent());
        $this->assertEquals(UserStub1::ID, $response->getAuthor()->getId());
        $this->assertEquals(new DateTime($commentStub::CREATED_AT), $response->getCreatedAt());
        $this->assertEquals(new DateTime($commentStub::UPDATED_AT), $response->getUpdatedAt());
    }

    protected function setUp()
    {
        $this->useCase = new GetPaginatedReports();
        $this->useCase->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));
        $this->useCase->setReportGateway(new InMemoryReportGateway(array(ReportStub1::ID => new ReportStub1())));
        $this->useCase->setReportResponseFactory($this->getReportResponseFactory());

        $this->requestBuilder = new GetPaginatedReportsRequestBuilderImpl();
    }

    /**
     * @return ReportResponseFactoryImpl
     */
    private function getReportResponseFactory()
    {
        $reportResponseFactory = new ReportResponseFactoryImpl();
        $reportResponseFactory->setCommentResponseFactory(new CommentResponseFactoryImpl());
        $reportResponseFactory->setPaginatedUseCaseResponseBuilder(new PaginatedUseCaseResponseBuilderImpl());

        return $reportResponseFactory;
    }
}
