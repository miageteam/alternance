<?php

namespace BusinessRules\UseCases\Student;

use BusinessRules\Gateways\Student\FormationGateway;
use BusinessRules\Requestors\Student\RemoveFormationRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class RemoveFormation implements UseCase
{

    /**
     * @var FormationGateway
     */
    private $formationGateway;

    /**
     * @Transaction
     *
     * @param RemoveFormationRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $formation = $this->formationGateway->find($request->getFormationId());

        $this->formationGateway->remove($formation);
    }

    /**
     * @param FormationGateway $formationGateway
     */
    public function setFormationGateway(FormationGateway $formationGateway)
    {
        $this->formationGateway = $formationGateway;
    }
}
