<?php

namespace CommunicationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class EditMessageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content', 'textarea');

    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'CommunicationBundle\Form\Model\EditMessageModel',
                'validation_groups' => array('Default', 'edit_message')
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'edit_message';
    }
}
