<?php

namespace BusinessRules\Entities\Student;

use BusinessRules\Requestors\Student\CreateFormationRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FormationFactory
{
    /**
     * @param CreateFormationRequest $request
     *
     * @return Formation
     */
    public function createFromRequest(CreateFormationRequest $request);
}
