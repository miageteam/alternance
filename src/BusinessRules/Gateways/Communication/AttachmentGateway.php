<?php

namespace BusinessRules\Gateways\Communication;

use BusinessRules\Entities\Communication\Attachment;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface AttachmentGateway
{
    /**
     * @param Attachment $attachment
     */
    public function insert(Attachment $attachment);
}
