<?php

namespace UserBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Validator\Constraints as MyAssert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserModel
{

    /**
     * @var String
     *
     * @Assert\NotBlank()
     */
    public $firstName;

    /**
     * @var String
     *
     * @Assert\NotBlank()
     */
    public $lastName;

    /**
     * @var String
     *
     * @Assert\NotBlank()
     * @MyAssert\EmailUnique()
     */
    public $email;

    /**
     * @var String
     */
    public $phoneNumber;

    /**
     * @var int
     */
    public $group;

}
