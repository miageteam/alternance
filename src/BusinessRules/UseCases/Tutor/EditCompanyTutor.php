<?php

namespace BusinessRules\UseCases\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutorFactory;
use BusinessRules\Gateways\Tutor\UserCompanyTutorGateway;
use BusinessRules\Requestors\Tutor\EditCompanyTutorRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditCompanyTutor implements UseCase
{

    /**
     * @var UserCompanyTutorGateway
     */
    private $userCompanyTutorGateway;

    /**
     * @var UserCompanyTutorFactory
     */
    private $userCompanyTutorFactory;

    /**
     * @Transaction
     *
     * @param EditCompanyTutorRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $companyTutor = $this->userCompanyTutorGateway->findByUserId($request->getUserId());

        $updatedCompanyTutor = $this->userCompanyTutorFactory->editFromRequest($companyTutor, $request);

        $this->userCompanyTutorGateway->update($updatedCompanyTutor);
    }

    /**
     * @param UserCompanyTutorGateway $userCompanyTutorGateway
     */
    public function setUserCompanyTutorGateway(UserCompanyTutorGateway $userCompanyTutorGateway)
    {
        $this->userCompanyTutorGateway = $userCompanyTutorGateway;
    }

    /**
     * @param UserCompanyTutorFactory $userCompanyTutorFactory
     */
    public function setUserCompanyTutorFactory(UserCompanyTutorFactory $userCompanyTutorFactory)
    {
        $this->userCompanyTutorFactory = $userCompanyTutorFactory;
    }
}
