<?php

namespace BusinessRules\Gateways\User;

use BusinessRules\Entities\User\Group;
use BusinessRules\Gateways\User\Exceptions\GroupAlreadyExistException;
use BusinessRules\Gateways\User\Exceptions\GroupNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryGroupGateway implements GroupGateway
{

    /**
     * @var Group[]
     */
    public static $groups = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @param Group[] $groups
     */
    public function __construct(array $groups = array())
    {
        self::$groups = $groups;
        self::$id     = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(Group $group)
    {
        foreach (self::$groups as $localGroup) {
            if ($localGroup->getName() === $group->getName()) {
                throw new GroupAlreadyExistException();
            }
        }

        self::$groups[++self::$id] = $group;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$groups[$id])) {
            throw new GroupNotFoundException();
        }

        return self::$groups[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function findByName($name)
    {
        foreach (self::$groups as $group) {
            if ($group->getName() === $name) {
                return $group;
            }
        }

        throw new GroupNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        $groups = array();
        foreach (self::$groups as $group) {
            $groups[] = $group;
        }

        return $groups;
    }
}
