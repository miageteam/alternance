<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\ReadFlowRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReadFlowRequestDTO implements ReadFlowRequest
{

    /**
     * @var int
     */
    public $flowId;

    /**
     * @param int $flowId
     */
    public function __construct($flowId)
    {
        $this->flowId = $flowId;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFlowId()
    {
        return $this->flowId;
    }
}
