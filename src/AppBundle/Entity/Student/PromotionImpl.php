<?php

namespace AppBundle\Entity\Student;

use BusinessRules\Entities\Student\Promotion;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PromotionImpl extends Promotion
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFormationName() . ' (' . $this->getYear() . ')';
    }
}
