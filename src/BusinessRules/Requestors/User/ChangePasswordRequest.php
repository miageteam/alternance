<?php

namespace BusinessRules\Requestors\User;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface ChangePasswordRequest extends UseCaseRequest
{

    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return string
     */
    public function getNewPassword();
}
