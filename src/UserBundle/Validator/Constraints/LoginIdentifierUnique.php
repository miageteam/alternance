<?php

namespace UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class LoginIdentifierUnique extends Constraint
{

    /**
     * @var string
     */
    public $message = 'user.login_identifier_already_exist';

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'login_identifier_unique';
    }
}
