<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\GetPaginatedUsersRequest;
use BusinessRules\Requestors\User\GetPaginatedUsersRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedUsersRequestBuilderImpl implements GetPaginatedUsersRequestBuilder
{

    /**
     * @var GetPaginatedUsersRequestDTO
     */
    private $request;

    /**
     * @return GetPaginatedUsersRequestBuilder
     */
    public function create()
    {
        $this->request = new GetPaginatedUsersRequestDTO();

        return $this;
    }

    /**
     * @param array $sorts
     *
     * @return GetPaginatedUsersRequestBuilder
     */
    public function withSorts(array $sorts)
    {
        $this->request->sorts = $sorts;

        return $this;
    }

    /**
     * @param int $page
     *
     * @return GetPaginatedUsersRequestBuilder
     */
    public function withPage($page)
    {
        $this->request->page = $page;

        return $this;
    }

    /**
     * @param int $itemsPerPage
     *
     * @return GetPaginatedUsersRequestBuilder
     */
    public function withItemsPerPage($itemsPerPage)
    {
        $this->request->itemsPerPage = $itemsPerPage;

        return $this;
    }

    /**
     * @return GetPaginatedUsersRequest
     */
    public function build()
    {
        return $this->request;
    }
}
