<?php

namespace BusinessRules\UseCases\Tutor;

use BusinessRules\Gateways\Tutor\UserCompanyTutorGateway;
use BusinessRules\Requestors\Tutor\GetCompanyTutorRequest;
use BusinessRules\Responders\Tutor\UserCompanyTutorResponse;
use BusinessRules\Responders\Tutor\UserCompanyTutorResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetCompanyTutor implements UseCase
{

    /**
     * @var UserCompanyTutorGateway
     */
    private $userCompanyTutorGateway;

    /**
     * @var UserCompanyTutorResponseFactory
     */
    private $userCompanyTutorResponseFactory;

    /**
     * @param GetCompanyTutorRequest $request
     *
     * @return UserCompanyTutorResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $userCompanyTutor = $this->userCompanyTutorGateway->findByUserId($request->getUserId());

        return $this->userCompanyTutorResponseFactory->createFromUserCompanyTutor($userCompanyTutor);
    }

    /**
     * @param UserCompanyTutorGateway $userCompanyTutorGateway
     */
    public function setUserCompanyTutorGateway(UserCompanyTutorGateway $userCompanyTutorGateway)
    {
        $this->userCompanyTutorGateway = $userCompanyTutorGateway;
    }

    /**
     * @param UserCompanyTutorResponseFactory $userCompanyTutorResponseFactory
     */
    public function setUserCompanyTutorResponseFactory(UserCompanyTutorResponseFactory $userCompanyTutorResponseFactory)
    {
        $this->userCompanyTutorResponseFactory = $userCompanyTutorResponseFactory;
    }
}
