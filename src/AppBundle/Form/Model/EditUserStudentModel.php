<?php

namespace AppBundle\Form\Model;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditUserStudentModel
{

    /**
     * @var int
     */
    public $promotion;

    /**
     * @var int
     */
    public $userSchoolTutor;

    /**
     * @var int
     */
    public $userCompanyTutor;

}
