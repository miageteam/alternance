<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class MeetingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plannedAt', 'datetime', array(
                'input'  => 'datetime',
                'widget' => 'choice',
                'empty_value' => array('year' => 'Année', 'month' => 'Mois', 'day' => 'Jour', 'hour' => 'H', 'minute' => 'm'),
            ))
            ->add('place');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'AppBundle\Form\Model\MeetingModel',
                'validation_groups' => array('Default', 'meeting')
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'meeting';
    }
}
