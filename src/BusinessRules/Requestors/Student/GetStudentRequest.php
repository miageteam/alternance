<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetStudentRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return int
     */
    public function getCompanyTutorId();

    /**
     * @return int
     */
    public function getPromotionId();
}
