<?php

namespace UserBundle\Validator\Constraints;

use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use BusinessRules\Requestors\User\GetUserRequestBuilder;
use BusinessRules\UseCases\User\GetUser;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class LoginIdentifierUniqueValidator extends ConstraintValidator
{

    /**
     * @var GetUser
     */
    private $getUser;

    /**
     * @var GetUserRequestBuilder
     */
    private $getUserRequestBuilder;

    /**
     * @param GetUser               $getUser
     * @param GetUserRequestBuilder $getUserRequestBuilder
     */
    public function __construct(UseCase $getUser, GetUserRequestBuilder $getUserRequestBuilder)
    {
        $this->getUser               = $getUser;
        $this->getUserRequestBuilder = $getUserRequestBuilder;
    }


    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LoginIdentifierUnique) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\LoginIdentifierUnique');
        }

        try {
            $request = $this->getUserRequestBuilder
                ->create()
                ->byLoginIdentifier($value)
                ->build();

            $user = $this->getUser->execute($request);

            if (!empty($user)) {
                $this->context->addViolation($constraint->message);
            }
        } catch (UserNotFoundException $unfe) {
        }
    }
}
