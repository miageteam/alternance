<?php

namespace UserBundle\Entity;

use BusinessRules\Entities\User\Group;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupImpl extends Group
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

}
