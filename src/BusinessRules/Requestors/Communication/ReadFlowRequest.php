<?php

namespace BusinessRules\Requestors\Communication;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface ReadFlowRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getFlowId();
}
