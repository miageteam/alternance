<?php

namespace BusinessRules\UseCases\Report\DTO\Request;

use BusinessRules\Requestors\Report\EditCommentRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditCommentRequestBuilderImpl implements EditCommentRequestBuilder
{

    /**
     * @var EditCommentRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new EditCommentRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withCommentId($commentId)
    {
        $this->request->commentId = $commentId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withContent($content)
    {
        $this->request->content = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
