<?php

namespace BusinessRules\Requestors\Communication;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteMessageRequest extends MessageRequestInterface
{
    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return int[]|ArrayCollection
     */
    public function getParticipantIds();
}
