<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Gateways\Report\CommentGateway;
use BusinessRules\Requestors\Report\GetCommentRequest;
use BusinessRules\Responders\Report\CommentResponse;
use BusinessRules\Responders\Report\CommentResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetComment implements UseCase
{

    /**
     * @var CommentGateway
     */
    private $commentGateway;

    /**
     * @var CommentResponseFactory
     */
    private $commentResponseFactory;

    /**
     * @param GetCommentRequest $request
     *
     * @return CommentResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $comment = $this->commentGateway->find($request->getId());

        return $this->commentResponseFactory->createFromComment($comment);
    }

    /**
     * @param CommentGateway $commentGateway
     */
    public function setCommentGateway(CommentGateway $commentGateway)
    {
        $this->commentGateway = $commentGateway;
    }

    /**
     * @param CommentResponseFactory $commentResponseFactory
     */
    public function setCommentResponseFactory(CommentResponseFactory $commentResponseFactory)
    {
        $this->commentResponseFactory = $commentResponseFactory;
    }
}
