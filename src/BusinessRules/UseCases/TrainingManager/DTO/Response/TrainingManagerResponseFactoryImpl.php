<?php

namespace BusinessRules\UseCases\TrainingManager\DTO\Response;

use BusinessRules\Entities\TrainingManager\TrainingManager;
use BusinessRules\Responders\TrainingManager\TrainingManagerResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class TrainingManagerResponseFactoryImpl implements TrainingManagerResponseFactory
{

    /**
     * {@inheritdoc}
     */
    public function createFromTrainingManager(TrainingManager $trainingManager)
    {
        $response            = new TrainingManagerResponseDTO();
        $response->id        = $trainingManager->getId();
        $response->user      = $trainingManager->getUser();
        $response->promotion = $trainingManager->getPromotion();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromTrainingManagers($trainingManagers)
    {
        $trainingManagersResponse = array();
        foreach ($trainingManagers as $trainingManager) {
            $trainingManagersResponse[] = $this->createFromTrainingManager($trainingManager);
        }

        return $trainingManagersResponse;
    }
}
