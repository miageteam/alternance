<?php

namespace BusinessRules\Gateways\User;

use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\User\Exceptions\UserNotFoundException;
use OpenClassrooms\UseCase\Application\Entity\PaginatedCollectionBuilderImpl;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollectionBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class InMemoryUserGateway implements UserGateway
{

    /**
     * @var User[]
     */
    public static $users = array();

    /**
     * @var int
     */
    public static $id = 0;

    /**
     * @var PaginatedCollectionBuilder
     */
    private $paginatedCollectionBuilder;

    /**
     * @param User[] $users
     */
    public function __construct(array $users = array())
    {
        self::$users                      = $users;
        self::$id                         = 0;
        $this->paginatedCollectionBuilder = new PaginatedCollectionBuilderImpl();
    }


    /**
     * {@inheritdoc}
     */
    public function insert(User $user)
    {
        self::$users[self::$id++] = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (empty(self::$users[$id])) {
            throw new UserNotFoundException();
        }

        return self::$users[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail($email)
    {
        foreach (self::$users as $user) {
            if ($user->getEmail() === $email) {
                return $user;
            }
        }

        throw new UserNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllPaginated(array $filters = array(), array $sorts = array(), array $pagination = array())
    {
        $items = array();
        foreach (self::$users as $user) {
            $items[] = $user;
        }

        return $this->paginatedCollectionBuilder
            ->create()
            ->withItems($items)
            ->withTotalItems(count($items))
            ->build();
    }

    /**
     * {@inheritdoc}
     */
    public function findByIds(array $ids)
    {
        $items = array();
        foreach (self::$users as $user) {
            if (in_array($user->getId(), $ids)) {
                $items[] = $user;
            }
        }

        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function findByLogin($loginIdentifier)
    {
        // not use here
    }

    /**
     * {@inheritdoc}
     */
    public function update(User $user)
    {
        self::$users[$user->getId()] = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function findAllLikeLogin($loginIdentifier)
    {
        $items = array();
        foreach (self::$users as $user) {
            if ($loginIdentifier === $user->getLoginIdentifier()) {
                $items[] = $user;
            }
        }

        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function remove(User $user)
    {
        // TODO: Implement this method.
    }
}
