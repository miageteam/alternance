<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
class EditPromotionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year', 'text', array('attr' => array('placeholder' => '2014/2015')))
            ->add('formation', 'entity', array(
                                 'empty_value' => '',
                                 'class' => 'AppBundle:Student\FormationImpl'
                             )
            )
            ->add('alternationCalendar', 'file', array('required' => false));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'AppBundle\Form\Model\EditPromotionModel',
                'validation_groups' => array('Default', 'edit_promotion')
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'edit_promotion';
    }
}
