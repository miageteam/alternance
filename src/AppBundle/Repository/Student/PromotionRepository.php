<?php

namespace AppBundle\Repository\Student;

use BusinessRules\Entities\Student\Promotion;
use BusinessRules\Gateways\Student\Exceptions\PromotionNotFoundException;
use BusinessRules\Gateways\Student\PromotionGateway;
use Doctrine\ORM\EntityRepository;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PromotionRepository extends EntityRepository implements PromotionGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(Promotion $promotion)
    {
        $this->_em->persist($promotion);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        $promotion = parent::find($id);
        if (empty($promotion)) {
            throw new PromotionNotFoundException();
        }

        return $promotion;
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Promotion $promotion)
    {
        $this->_em->remove($promotion);
    }

    /**
     * {@inheritdoc}
     */
    public function update(Promotion $promotion)
    {
        // Do nothing
    }
}
