<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Entities\Communication\AttachmentStub1;
use BusinessRules\Entities\Communication\FlowStub1;
use BusinessRules\Entities\Communication\MessageStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\Communication\InMemoryAttachmentGateway;
use BusinessRules\Gateways\Communication\InMemoryFlowGateway;
use BusinessRules\Gateways\Communication\InMemoryMessageGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Communication\WriteResponseRequestBuilder;
use BusinessRules\UseCases\Communication\DTO\Request\WriteResponseRequestBuilderImpl;
use CommunicationBundle\Entity\AttachmentFactoryImpl;
use CommunicationBundle\Entity\MessageFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteResponse
     */
    private $useCase;

    /**
     * @var WriteResponseRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\Communication\Exceptions\FlowNotFoundException
     */
    public function FlowNotFound_ThrowException()
    {
        InMemoryFlowGateway::$flows = array();

        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withFlowId(FlowStub1::ID)
                ->withContent(MessageStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function AuthorNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();

        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withFlowId(FlowStub1::ID)
                ->withContent(MessageStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     */
    public function WriteMessage_CreateAndInsert()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withFlowId(FlowStub1::ID)
                ->withContent(MessageStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );

        foreach (InMemoryMessageGateway::$messages as $message) {
            $this->assertEquals(FlowStub1::ID, $message->getFlow()->getId());
            $this->assertEquals(MessageStub1::CONTENT, $message->getContent());
            $this->assertEquals(UserStub1::ID, $message->getAuthor()->getId());
        }
    }

    /**
     * @test
     */
    public function WithAttachment_SaveAttachment()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withFlowId(FlowStub1::ID)
                ->withContent(MessageStub1::CONTENT)
                ->withAuthorId(UserStub1::ID)
                ->withAttachmentPath(AttachmentStub1::PATH)
                ->build()
        );

        $this->assertCount(1, InMemoryAttachmentGateway::$attachments);
        foreach (InMemoryAttachmentGateway::$attachments as $attachment) {
            $this->assertEquals(AttachmentStub1::PATH, $attachment->getPath());
            $this->assertEquals(MessageStub1::CONTENT, $attachment->getMessage()->getContent());
        }
    }

    protected function setup()
    {
        $this->useCase = new WriteResponse();
        $this->useCase->setFlowGateway(new InMemoryFlowGateway(array(FlowStub1::ID => new FlowStub1())));
        $this->useCase->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));
        $this->useCase->setMessageFactory(new MessageFactoryImpl());
        $this->useCase->setMessageGateway(new InMemoryMessageGateway());
        $this->useCase->setAttachmentFactory(new AttachmentFactoryImpl());
        $this->useCase->setAttachmentGateway(new InMemoryAttachmentGateway());

        $this->requestBuilder = new WriteResponseRequestBuilderImpl();
    }
}
