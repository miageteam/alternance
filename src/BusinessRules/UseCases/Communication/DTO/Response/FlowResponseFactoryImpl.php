<?php

namespace BusinessRules\UseCases\Communication\DTO\Response;

use BusinessRules\Entities\Communication\Flow;
use BusinessRules\Responders\Communication\FlowResponseFactory;
use BusinessRules\Responders\Communication\MessageResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class FlowResponseFactoryImpl implements FlowResponseFactory
{

    /**
     * @var MessageResponseFactory
     */
    private $messageResponseFactory;

    /**
     * @var PaginatedUseCaseResponseBuilder
     */
    private $paginatedUseCaseResponseBuilder;

    /**
     * {@inheritdoc}
     */
    public function createFromFlow(Flow $flow)
    {
        $response               = new FlowResponseDTO();
        $response->id           = $flow->getId();
        $response->title        = $flow->getTitle();
        $response->messages     = $this->messageResponseFactory->createFromMessages($flow->getMessages());
        $response->firstMessage = $this->messageResponseFactory->createFromMessage($flow->getFirstMessage());
        $response->lastMessage  = $this->messageResponseFactory->createFromMessage($flow->getLastMessage());
        $response->createdAt    = $flow->getCreatedAt();
        $response->participants = $flow->getParticipants();

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromFlows($flows)
    {
        $flowsResponse = array();
        foreach ($flows as $flow) {
            $flowsResponse[] = $this->createFromFlow($flow);
        }

        return $flowsResponse;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromPaginatedCollection(PaginatedCollection $paginatedCollection)
    {
        return $this->paginatedUseCaseResponseBuilder
            ->create()
            ->withItems($this->createFromFlows($paginatedCollection->getItems()))
            ->withItemsPerPage($paginatedCollection->getItemsPerPage())
            ->withPage($paginatedCollection->getPage())
            ->withTotalItems($paginatedCollection->getTotalItems())
            ->build();
    }

    /**
     * @param MessageResponseFactory $messageResponseFactory
     */
    public function setMessageResponseFactory(MessageResponseFactory $messageResponseFactory)
    {
        $this->messageResponseFactory = $messageResponseFactory;
    }

    /**
     * @param PaginatedUseCaseResponseBuilder $builder
     */
    public function setPaginatedUseCaseResponseBuilder(PaginatedUseCaseResponseBuilder $builder)
    {
        $this->paginatedUseCaseResponseBuilder = $builder;
    }
}
