<?php

namespace AppBundle\Repository\Report;

use BusinessRules\Entities\Report\Attachment;
use BusinessRules\Gateways\Report\AttachmentGateway;
use Doctrine\ORM\EntityRepository;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class AttachmentRepository extends EntityRepository implements AttachmentGateway
{

    /**
     * {@inheritdoc}
     */
    public function insert(Attachment $attachment)
    {
        $this->_em->persist($attachment);
    }
}
