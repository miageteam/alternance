<?php

namespace AppBundle\Service\Pagination;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollectionBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class PaginatedCollectionFactoryImpl implements PaginatedCollectionFactory
{

    /**
     * @var PaginatedCollectionBuilder
     */
    private $builder;

    /**
     * {@inheritdoc}
     */
    public function make(QueryBuilder $qb, array $pagination = array())
    {
        $paginator    = new Paginator($qb);
        $itemsPerPage = null;
        $page         = 1;
        if (!empty($pagination)) {
            if (isset($pagination[PaginatedCollection::ITEMS_PER_PAGE])) {
                if (isset($pagination[PaginatedCollection::PAGE])) {
                    $page = $pagination[PaginatedCollection::PAGE];
                }
                $itemsPerPage = $pagination[PaginatedCollection::ITEMS_PER_PAGE];
                $paginator->getQuery()->setFirstResult($itemsPerPage * ($page - 1));
                $paginator->getQuery()->setMaxResults($itemsPerPage);
            }
        }

        return $this->builder
            ->create()
            ->withItems($paginator->getIterator()->getArrayCopy())
            ->withTotalItems($paginator->count())
            ->withItemsPerPage($itemsPerPage)
            ->withPage($page)
            ->build();
    }

    /**
     * @param PaginatedCollectionBuilder $builder
     */
    public function setPaginatedCollectionBuilder(PaginatedCollectionBuilder $builder)
    {
        $this->builder = $builder;
    }
}
