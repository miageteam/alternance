<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Entities\Communication\FlowFilter;
use BusinessRules\Gateways\Communication\FlowGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Communication\GetPaginatedFlowsRequest;
use BusinessRules\Responders\Communication\FlowResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Entities\PaginatedCollection;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;
use OpenClassrooms\UseCase\BusinessRules\Responders\AbstractPaginatedUseCaseResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedFlows implements UseCase
{

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var FlowGateway
     */
    private $flowGateway;

    /**
     * @var FlowResponseFactory
     */
    private $flowResponseFactory;

    /**
     * @param GetPaginatedFlowsRequest $request
     *
     * @return AbstractPaginatedUseCaseResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $flows = $this->flowGateway->findAllPaginated(
            $this->getFilters($request),
            $this->getSorts($request),
            $this->getPagination($request)
        );

        return $this->flowResponseFactory->createFromPaginatedCollection($flows);
    }

    /**
     * @param GetPaginatedFlowsRequest $useCaseRequest
     *
     * @return array
     */
    private function getFilters(GetPaginatedFlowsRequest $useCaseRequest)
    {
        $filters = array();

        if (null !== $useCaseRequest->getUserId()) {
            $user                        = $this->userGateway->find($useCaseRequest->getUserId());
            $filters[FlowFilter::USER] = $user;
        }

        return $filters;
    }

    /**
     * @param GetPaginatedFlowsRequest $useCaseRequest
     *
     * @return array
     */
    private function getSorts(GetPaginatedFlowsRequest $useCaseRequest)
    {
        return $useCaseRequest->getSorts();
    }

    /**
     * @param GetPaginatedFlowsRequest $useCaseRequest
     *
     * @return array
     */
    private function getPagination(GetPaginatedFlowsRequest $useCaseRequest)
    {
        return array(
            PaginatedCollection::PAGE           => $useCaseRequest->getPage(),
            PaginatedCollection::ITEMS_PER_PAGE => $useCaseRequest->getItemsPerPage(),
        );
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param FlowGateway $flowGateway
     */
    public function setFlowGateway(FlowGateway $flowGateway)
    {
        $this->flowGateway = $flowGateway;
    }

    /**
     * @param FlowResponseFactory $flowResponseFactory
     */
    public function setFlowResponseFactory(FlowResponseFactory $flowResponseFactory)
    {
        $this->flowResponseFactory = $flowResponseFactory;
    }
}
