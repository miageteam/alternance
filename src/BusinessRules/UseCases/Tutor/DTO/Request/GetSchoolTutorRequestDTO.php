<?php

namespace BusinessRules\UseCases\Tutor\DTO\Request;

use BusinessRules\Requestors\Tutor\GetSchoolTutorRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetSchoolTutorRequestDTO implements GetSchoolTutorRequest
{

    /**
     * @var int
     */

    public $userId;

    /**
     * @param int $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
