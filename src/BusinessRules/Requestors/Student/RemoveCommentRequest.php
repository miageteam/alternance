<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface RemoveCommentRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getCommentId();
}
