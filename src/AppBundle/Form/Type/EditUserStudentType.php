<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EditUserStudentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'promotion',
                'entity',
                array(
                    'empty_value' => '',
                    'class'       => 'AppBundle:Student\PromotionImpl',
                    'required'    => false
                )
            )
            ->add(
                'userSchoolTutor',
                'entity',
                array(
                    'empty_value' => '',
                    'class'       => 'AppBundle:Tutor\UserSchoolTutorImpl',
                    'required'    => false
                )
            )
            ->add(
                'userCompanyTutor',
                'entity',
                array(
                    'empty_value' => '',
                    'class'       => 'AppBundle:Tutor\UserCompanyTutorImpl',
                    'required'    => false
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'AppBundle\Form\Model\EditUserStudentModel',
                'validation_groups' => array('Default', 'edit_user_student')
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'edit_user_student';
    }
}
