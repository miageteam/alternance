<?php

namespace BusinessRules\Requestors\TrainingManager;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetTrainingManagerRequestBuilder
{
    /**
     * @return GetTrainingManagerRequestBuilder
     */
    public function create();

    /**
     * @param int $userId
     *
     * @return GetTrainingManagerRequestBuilder
     */
    public function byUserId($userId);

    /**
     * @return GetTrainingManagerRequest
     */
    public function build();
}
