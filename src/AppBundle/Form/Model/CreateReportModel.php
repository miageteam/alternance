<?php

namespace AppBundle\Form\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateReportModel
{

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var UploadedFile
     *
     * @Assert\File()
     */
    public $attachment;

    /**
     * @var string
     */
    public $attachmentPath;

    /**
     * @var string
     */
    private $name;

    public function getAbsolutePath()
    {
        return null === $this->attachmentPath ? null : $this->getUploadRootDir() . '/' . $this->name;
    }

    public function getWebPath()
    {
        return null === $this->attachmentPath ? null : $this->getUploadDir() . '/' . $this->name;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/report/attachments';
    }

    public function upload()
    {
        if (null === $this->attachment) {
            return;
        }

        $this->name = sha1(uniqid(mt_rand(), true)).'.'.$this->attachment->guessExtension();

        $this->attachment->move($this->getUploadRootDir(), $this->name);

        $this->attachmentPath = $this->getUploadDir() . '/' . $this->name;

        $this->attachment = null;
    }
}
