<?php

namespace BusinessRules\Entities\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class GroupName
{
    const ADMIN            = 'Administrateur';
    const SECRETARY        = 'Secrétaire';
    const TRAINING_MANAGER = 'Responsable de formation';
    const PROFESSOR        = 'Professeur';
    const TUTOR            = 'Tuteur';
    const STUDENT          = 'Étudiant';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
