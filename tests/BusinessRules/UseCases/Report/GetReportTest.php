<?php

namespace BusinessRules\UseCases\Report;

use BusinessRules\Entities\Report\ReportStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\Report\InMemoryReportGateway;
use BusinessRules\Requestors\Report\GetReportRequest;
use BusinessRules\Responders\Report\CommentResponse;
use BusinessRules\Responders\Report\ReportResponse;
use BusinessRules\UseCases\Report\DTO\Request\GetReportRequestDTO;
use BusinessRules\UseCases\Report\DTO\Response\CommentResponseFactoryImpl;
use BusinessRules\UseCases\Report\DTO\Response\ReportResponseFactoryImpl;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetReportTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GetReport
     */
    private $useCase;

    /**
     * @var GetReportRequest
     */
    private $request;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\Report\Exceptions\ReportNotFoundException
     */
    public function reportNotFound_ThrowException()
    {
        InMemoryReportGateway::$reports = array();
        $this->useCase->execute($this->request);
    }

    /**
     * @test
     */
    public function getReport()
    {
        $response = $this->useCase->execute($this->request);

        $reportStub1 = 'BusinessRules\Entities\Report\ReportStub1';
        $this->assertReport($reportStub1, $response);
    }

    private function assertReport($reportStub, ReportResponse $response)
    {
        $this->assertEquals($reportStub::ID, $response->getId());
        $this->assertEquals($reportStub::TITLE, $response->getTitle());
        $this->assertEquals(UserStub1::ID, $response->getAuthor()->getId());
        $this->assertEquals(new DateTime($reportStub::CREATED_AT), $response->getCreatedAt());
        $this->assertEquals(new DateTime($reportStub::UPDATED_AT), $response->getUpdatedAt());

        $commentStub = 'BusinessRules\Entities\Report\CommentStub';
        foreach ($response->getComments() as $key => $comment) {
            $this->assertComment($commentStub . ($key + 1), $comment);
        }
    }

    private function assertComment($commentStub, CommentResponse $response)
    {
        $this->assertEquals($commentStub::ID, $response->getId());
        $this->assertEquals($commentStub::CONTENT, $response->getContent());
        $this->assertEquals(UserStub1::ID, $response->getAuthor()->getId());
        $this->assertEquals(new DateTime($commentStub::CREATED_AT), $response->getCreatedAt());
        $this->assertEquals(new DateTime($commentStub::UPDATED_AT), $response->getUpdatedAt());
    }

    protected function setUp()
    {
        $this->useCase = new GetReport();
        $this->useCase->setReportGateway(new InMemoryReportGateway(array(ReportStub1::ID => new ReportStub1())));
        $this->useCase->setReportResponseFactory($this->getReportResponseFactory());

        $this->request = new GetReportRequestDTO(ReportStub1::ID);
    }

    /**
     * @return ReportResponseFactoryImpl
     */
    private function getReportResponseFactory()
    {
        $reportResponseFactory = new ReportResponseFactoryImpl();
        $reportResponseFactory->setCommentResponseFactory(new CommentResponseFactoryImpl());

        return $reportResponseFactory;
    }
}
