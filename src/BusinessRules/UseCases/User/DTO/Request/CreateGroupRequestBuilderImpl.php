<?php

namespace BusinessRules\UseCases\User\DTO\Request;

use BusinessRules\Requestors\User\CreateGroupRequestBuilder;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateGroupRequestBuilderImpl implements CreateGroupRequestBuilder
{

    /**
     * @var CreateGroupRequestDTO
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->request = new CreateGroupRequestDTO();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withName($name)
    {
        $this->request->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserRoles(array $userRoles)
    {
        $this->request->userRoles = $userRoles;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        return $this->request;
    }
}
