<?php

namespace UserBundle\Validator\Constraints;

use BusinessRules\Entities\User\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CheckPasswordValidator extends ConstraintValidator
{

    /**
     * @var PasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param PasswordEncoderInterface $encoder
     * @param TokenStorageInterface    $tokenStorage
     */
    public function __construct(PasswordEncoderInterface $encoder, TokenStorageInterface $tokenStorage)
    {
        $this->encoder      = $encoder;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CheckPassword) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\ValidPassword');
        }

        $user = $this->getCurrentUser();
        if ($user) {
            $passwordValid = $this->encoder->isPasswordValid(
                $user->getPassword(),
                $value,
                $user->getSalt()
            );

            if (!$passwordValid) {
                $this->context->addViolation($constraint->message);
            }
        }
    }

    /**
     * @return User|null
     */
    private function getCurrentUser()
    {
        $token = $this->tokenStorage->getToken();

        if ($token && $token->getUser() instanceof User) {
            return $token->getUser();
        }
    }
}
