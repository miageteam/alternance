<?php

namespace BusinessRules\Gateways\Communication;

use BusinessRules\Entities\Communication\Message;
use BusinessRules\Gateways\Communication\Exceptions\MessageNotFoundException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
interface MessageGateway
{
    /**
     * @param Message $message
     */
    public function insert(Message $message);

    /**
     * @param int $id
     *
     * @return Message
     *
     * @throws MessageNotFoundException
     */
    public function find($id);

    /**
     * @param Message $message
     */
    public function update(Message $message);
}
