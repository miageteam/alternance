<?php

namespace BusinessRules\Requestors\User;

/**
 * @author Kevin LECONTE
 */
interface ModifyUserRequestBuilder
{
    /**
     * @return ModifyUserRequestBuilder
     */
    public function create();

    /**
     * @param int $id
     *
     * @return ModifyUserRequestBuilder
     */
    public function withId($id);

    /**
     * @param string $firstName
     *
     * @return ModifyUserRequestBuilder
     */
    public function withFirstName($firstName);

    /**
     * @param string $lastName
     *
     * @return ModifyUserRequestBuilder
     */
    public function withLastName($lastName);

    /**
     * @param string $email
     *
     * @return ModifyUserRequestBuilder
     */
    public function withEmail($email);

    /**
     * @param string $phoneNumber
     *
     * @return ModifyUserRequestBuilder
     */
    public function withPhoneNumber($phoneNumber);

    /**
     * @param int $groupId
     *
     * @return ModifyUserRequestBuilder
     */
    public function withGroupId($groupId);

    /**
     * @return string $password
     *
     * @return ModifyUserRequestBuilder
     */
    public function withPassword($password);

    /**
     * @return ModifyUserRequest
     */
    public function build();
}
