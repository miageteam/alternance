<?php

namespace BusinessRules\Entities\Communication;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
final class FlowFilter
{
    const USER = 'user';

    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }
}
