<?php

namespace BusinessRules\Requestors\TrainingManager;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface GetTrainingManagersRequestBuilder
{

    /**
     * @return GetTrainingManagersRequestBuilder
     */
    public function create();

    /**
     * @param int $promotionId
     *
     * @return GetTrainingManagersRequestBuilder
     */
    public function byPromotionId($promotionId);

    /**
     * @return GetTrainingManagersRequest
     */
    public function build();
}
