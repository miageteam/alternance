<?php

namespace BusinessRules\Requestors\Report;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface WriteCommentRequest extends UseCaseRequest
{
    /**
     * @return int
     */
    public function getReportId();

    /**
     * @return string
     */
    public function getContent();

    /**
     * @return int
     */
    public function getAuthorId();
}
