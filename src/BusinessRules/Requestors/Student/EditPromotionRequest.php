<?php

namespace BusinessRules\Requestors\Student;

use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author David Dieu <daviddieu80@gmail.com>
 */
interface EditPromotionRequest extends UseCaseRequest
{

    /**
     * @return int
     */
    public function getPromotionId();

    /**
     * @return String
     */
    public function getAlternationCalendarPath();

    /**
     * @return String
     */
    public function getYear();

    /**
     * @return int
     */
    public function getFormationId();

}
