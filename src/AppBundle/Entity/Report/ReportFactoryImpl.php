<?php

namespace AppBundle\Entity\Report;

use BusinessRules\Entities\Report\Attachment;
use BusinessRules\Entities\Report\ReportFactory;
use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReportFactoryImpl implements ReportFactory
{
    /**
     * {@inheritdoc}
     */
    public function create($title, User $author, Attachment $attachment = null)
    {
        $report = new ReportImpl();

        $report->setTitle($title);
        $report->setAuthor($author);
        $report->setAttachment($attachment);

        return $report;
    }
}
