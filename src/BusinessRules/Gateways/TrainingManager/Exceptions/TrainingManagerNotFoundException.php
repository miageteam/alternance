<?php

namespace BusinessRules\Gateways\TrainingManager\Exceptions;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class TrainingManagerNotFoundException extends \Exception
{

}
