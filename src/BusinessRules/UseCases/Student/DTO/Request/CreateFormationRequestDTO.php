<?php

namespace BusinessRules\UseCases\Student\DTO\Request;

use BusinessRules\Requestors\Student\CreateFormationRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CreateFormationRequestDTO implements CreateFormationRequest
{

    /**
     * @var string
     */
    public $name;

    /**
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    /**
     * {@inheritdoc]
     */
    public function getName()
    {
        return $this->name;
    }
}
