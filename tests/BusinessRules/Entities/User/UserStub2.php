<?php

namespace BusinessRules\Entities\User;

use UserBundle\Entity\UserImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserStub2 extends UserImpl
{
    const ID = 2;

    const FIRSTNAME = 'Paul';

    const LASTNAME = 'Dubois';

    const LOGIN_IDENTIFIER = 'pauldubois';

    const EMAIL = 'paul.dubois@mail.com';

    public function __construct()
    {
        $this->group = new GroupStub1();
    }

    protected $id = self::ID;

    protected $firstName = self::FIRSTNAME;

    protected $lastName = self::LASTNAME;

    protected $loginIdentifier = self::LOGIN_IDENTIFIER;

    protected $email = self::EMAIL;

}
