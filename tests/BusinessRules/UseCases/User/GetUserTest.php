<?php

namespace BusinessRules\UseCases\User;

use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\User\GetUserRequestBuilder;
use BusinessRules\UseCases\User\DTO\Request\GetUserRequestBuilderImpl;
use BusinessRules\UseCases\User\DTO\Response\UserResponseFactoryImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetUserTest extends \PHPUnit_Framework_TestCase
{
    use AssertUserTrait;

    /**
     * @var GetUser
     */
    private $useCase;

    /**
     * @var GetUserRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function UserNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->byId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     */
    public function UserFoundById_ReturnResponse()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->byId(UserStub1::ID)
                ->build()
        );

        $this->assertUser('BusinessRules\Entities\User\UserStub1', $response);
    }

    /**
     * @test
     */
    public function UserFoundByEmail_ReturnResponse()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->byEmail(UserStub1::EMAIL)
                ->build()
        );

        $this->assertUser('BusinessRules\Entities\User\UserStub1', $response);
    }

    protected function setup()
    {
        $this->useCase = new GetUser();
        $this->useCase->setUserGateway(
            new InMemoryUserGateway(
                array(UserStub1::ID => new UserStub1())
            )
        );
        $this->useCase->setUserResponseFactory(new UserResponseFactoryImpl());

        $this->requestBuilder = new GetUserRequestBuilderImpl();
    }
}
