<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Entities\Communication\AttachmentFactory;
use BusinessRules\Entities\Communication\Flow;
use BusinessRules\Entities\Communication\Message;
use BusinessRules\Entities\Communication\MessageFactory;
use BusinessRules\Entities\User\User;
use BusinessRules\Gateways\Communication\AttachmentGateway;
use BusinessRules\Gateways\Communication\FlowGateway;
use BusinessRules\Gateways\Communication\MessageGateway;
use BusinessRules\Gateways\User\UserGateway;
use BusinessRules\Requestors\Communication\WriteResponseRequest;
use OpenClassrooms\UseCase\Application\Annotations\Transaction;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteResponse implements UseCase
{

    /**
     * @var FlowGateway
     */
    private $flowGateway;

    /**
     * @var UserGateway
     */
    private $userGateway;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var MessageGateway
     */
    private $messageGateway;

    /**
     * @var AttachmentGateway
     */
    private $attachmentGateway;

    /**
     * @var AttachmentFactory
     */
    private $attachmentFactory;

    /**
     * @Transaction
     *
     * @param WriteResponseRequest $request
     */
    public function execute(UseCaseRequest $request)
    {
        $flow   = $this->flowGateway->find($request->getFlowId());
        $author = $this->userGateway->find($request->getAuthorId());

        $message = $this->createAndInsertMessage($request, $flow, $author);

        $attachmentPath = $request->getAttachmentPath();
        if (!empty($attachmentPath)) {
            $this->createAndInsertAttachments($request, $message);
        }
    }

    private function createAndInsertMessage(WriteResponseRequest $request, Flow $flow, User $author)
    {
        $message = $this->messageFactory->create($request->getContent(), $flow, $author);
        $this->messageGateway->insert($message);

        return $message;
    }

    private function createAndInsertAttachments(WriteResponseRequest $request, Message $message)
    {
        $attachment = $this->attachmentFactory->create($request->getAttachmentPath(), $message);
        $this->attachmentGateway->insert($attachment);
    }

    /**
     * @param FlowGateway $flowGateway
     */
    public function setFlowGateway(FlowGateway $flowGateway)
    {
        $this->flowGateway = $flowGateway;
    }

    /**
     * @param UserGateway $userGateway
     */
    public function setUserGateway(UserGateway $userGateway)
    {
        $this->userGateway = $userGateway;
    }

    /**
     * @param MessageFactory $messageFactory
     */
    public function setMessageFactory(MessageFactory $messageFactory)
    {
        $this->messageFactory = $messageFactory;
    }

    /**
     * @param MessageGateway $messageGateway
     */
    public function setMessageGateway(MessageGateway $messageGateway)
    {
        $this->messageGateway = $messageGateway;
    }

    /**
     * @param AttachmentGateway $attachmentGateway
     */
    public function setAttachmentGateway(AttachmentGateway $attachmentGateway)
    {
        $this->attachmentGateway = $attachmentGateway;
    }

    /**
     * @param AttachmentFactory $attachmentFactory
     */
    public function setAttachmentFactory(AttachmentFactory $attachmentFactory)
    {
        $this->attachmentFactory = $attachmentFactory;
    }
}
