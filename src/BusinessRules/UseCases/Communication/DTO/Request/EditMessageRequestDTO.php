<?php

namespace BusinessRules\UseCases\Communication\DTO\Request;

use BusinessRules\Requestors\Communication\EditMessageRequest;

/**
 * @author Paul DUBOIS <dubois.paul@live.fr>
 */
class EditMessageRequestDTO implements EditMessageRequest
{

    /**
     * @var int
     */
    public $messageId;

    /**
     * @var string
     */
    public $content;

    /**
     * {@inheritdoc}
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }
}
