<?php

namespace BusinessRules\UseCases\User\DTO\Response;

use BusinessRules\Entities\User\Group;
use BusinessRules\Responders\User\GroupsResponse;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GroupsResponseDTO implements GroupsResponse
{

    /**
     * @var Group[]
     */
    public $groups;

    /**
     * @param array $groups
     */
    public function __construct(array $groups = array())
    {
        $this->groups = $groups;
    }

    /**
     * @return Group[]
     */
    public function getGroups()
    {
        return $this->groups;
    }
}
