<?php

namespace BusinessRules\Responders\Communication;

use BusinessRules\Entities\Communication\Flow;
use OpenClassrooms\UseCase\BusinessRules\Responders\PaginatedUseCaseResponseFactory;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface FlowResponseFactory extends PaginatedUseCaseResponseFactory
{
    /**
     * @param Flow $flow
     *
     * @return FlowResponse
     */
    public function createFromFlow(Flow $flow);

    /**
     * @param Flow[] $flows
     *
     * @return FlowResponse[]
     */
    public function createFromFlows($flows);
}
