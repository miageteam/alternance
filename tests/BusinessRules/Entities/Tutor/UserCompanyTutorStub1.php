<?php

namespace BusinessRules\Entities\Tutor;

use AppBundle\Entity\Tutor\UserCompanyTutorImpl;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub2;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class UserCompanyTutorStub1 extends UserCompanyTutorImpl
{
    const ID          = 1;
    const COMPANYNAME = 'Miage';
    const STREET      = '20 rue du moulin';
    const ZIPCODE     = '80000';
    const CITY        = 'Amiens';
    const COUNTRY     = 'France';

    public function __construct()
    {
        $this->user    = new UserStub2();
        $this->student = new UserStub1();
    }

    protected $id = self::ID;

    protected $companyName = self::COMPANYNAME;

    protected $street = self::STREET;

    protected $zipCode = self::ZIPCODE;

    protected $city = self::CITY;

    protected $country = self::COUNTRY;

}
