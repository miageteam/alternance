<?php

namespace BusinessRules\Entities\Report;

use BusinessRules\Entities\User\User;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface CommentFactory
{
    /**
     * @param string $content
     * @param User   $author
     * @param Report $report
     *
     * @return Comment
     */
    public function create($content, User $author, Report $report);
}
