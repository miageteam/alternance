<?php

namespace BusinessRules\UseCases\Report;

use AppBundle\Entity\Report\AttachmentFactoryImpl;
use AppBundle\Entity\Report\ReportFactoryImpl;
use BusinessRules\Entities\Report\AttachmentStub1;
use BusinessRules\Entities\Report\CommentStub1;
use BusinessRules\Entities\Report\ReportStub1;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Entities\User\UserStub2;
use BusinessRules\Gateways\Report\InMemoryAttachmentGateway;
use BusinessRules\Gateways\Report\InMemoryReportGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Report\WriteReportRequestBuilder;
use BusinessRules\UseCases\Report\DTO\Request\WriteReportRequestBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class WriteReportTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WriteReport
     */
    private $useCase;

    /**
     * @var WriteReportRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function AuthorNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withTitle(ReportStub1::TITLE)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     */
    public function WriteReport()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withTitle(ReportStub1::TITLE)
                ->withAuthorId(UserStub1::ID)
                ->build()
        );

        $this->assertCount(1, InMemoryReportGateway::$reports);
        foreach (InMemoryReportGateway::$reports as $report) {
            $this->assertEquals(ReportStub1::TITLE, $report->getTitle());
            $this->assertEquals(UserStub1::ID, $report->getAuthorId());
        }
    }

    /**
     * @test
     */
    public function WithAttachment_SaveAttachment()
    {
        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withTitle(ReportStub1::TITLE)
                ->withAuthorId(UserStub1::ID)
                ->withAttachmentPath(AttachmentStub1::PATH)
                ->build()
        );

        $this->assertCount(1, InMemoryAttachmentGateway::$attachments);
        foreach (InMemoryAttachmentGateway::$attachments as $attachment) {
            $this->assertEquals(AttachmentStub1::PATH, $attachment->getPath());
        }
    }

    protected function setUp()
    {
        $this->useCase = new WriteReport();
        $this->useCase->setUserGateway(
            new InMemoryUserGateway(array(UserStub1::ID => new UserStub1(), UserStub2::ID => new UserStub2()))
        );
        $this->useCase->setReportFactory(new ReportFactoryImpl());
        $this->useCase->setReportGateway(new InMemoryReportGateway());
        $this->useCase->setAttachmentGateway(new InMemoryAttachmentGateway());
        $this->useCase->setAttachmentFactory(new AttachmentFactoryImpl());

        $this->requestBuilder = new WriteReportRequestBuilderImpl();
    }
}
