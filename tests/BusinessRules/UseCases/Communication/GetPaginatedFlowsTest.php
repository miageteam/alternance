<?php

namespace BusinessRules\UseCases\Communication;

use BusinessRules\Entities\Communication\FlowSort;
use BusinessRules\Entities\Communication\FlowStub1;
use BusinessRules\Entities\Communication\MessageStub1;
use BusinessRules\Entities\User\User;
use BusinessRules\Entities\User\UserStub1;
use BusinessRules\Gateways\Communication\InMemoryFlowGateway;
use BusinessRules\Gateways\User\InMemoryUserGateway;
use BusinessRules\Requestors\Communication\GetPaginatedFlowsRequestBuilder;
use BusinessRules\Responders\Communication\FlowResponse;
use BusinessRules\UseCases\Communication\DTO\Request\GetPaginatedFlowsRequestBuilderImpl;
use BusinessRules\UseCases\Communication\DTO\Request\GetPaginatedPaginatedFlowsRequestDTO;
use BusinessRules\UseCases\Communication\DTO\Response\FlowResponseFactoryImpl;
use BusinessRules\UseCases\Communication\DTO\Response\MessageResponseFactoryImpl;
use DateTime;
use OpenClassrooms\UseCase\Application\Responder\PaginatedUseCaseResponseBuilderImpl;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetPaginatedFlowsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var GetPaginatedFlows
     */
    private $useCase;

    /**
     * @var GetPaginatedFlowsRequestBuilder
     */
    private $requestBuilder;

    /**
     * @test
     *
     * @expectedException \BusinessRules\Gateways\User\Exceptions\UserNotFoundException
     */
    public function UserNotFound_ThrowException()
    {
        InMemoryUserGateway::$users = array();

        $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withUserId(UserStub1::ID)
                ->build()
        );
    }

    /**
     * @test
     */
    public function EmptyList_ReturnEmpty()
    {
        InMemoryFlowGateway::$flows = array();

        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->build()
        );

        $this->assertEmpty($response->getItems());
        $this->assertCount(0, $response->getItems());
        $this->assertEquals(0, $response->getTotalItems());
    }

    /**
     * @test
     */
    public function GetFlows_ReturnList()
    {
        $response = $this->useCase->execute(
            $this->requestBuilder
                ->create()
                ->withUserId(UserStub1::ID)
                ->withSorts(array(FlowSort::CREATED_AT => FlowSort::DESC))
                ->withItemsPerPage(10)
                ->withPage(1)
                ->build()
        );

        $this->assertCount(1, $response->getItems());
        $flowStub = 'BusinessRules\Entities\Communication\FlowStub';
        foreach ($response->getItems() as $key => $message) {
            $this->assertFlow($flowStub . ($key + 1), $message);
        }

    }

    private function assertFlow($flowStub, FlowResponse $response)
    {
        $this->assertEquals($flowStub::ID, $response->getId());
        $this->assertEquals($flowStub::TITLE, $response->getTitle());
        $this->assertEquals(new DateTime($flowStub::CREATED_AT), $response->getCreatedAt());
        $this->assertEquals(MessageStub1::ID, $response->getFirstMessage()->getId());

        $userStub = 'BusinessRules\Entities\User\UserStub';
        foreach ($response->getParticipants() as $key => $participant) {
            $this->assertParticipant($userStub . ($key + 1), $participant);
        }
    }

    private function assertParticipant($userStub, User $participant)
    {
        $this->assertEquals($userStub::ID, $participant->getId());
    }

    protected function setUp()
    {
        $this->useCase = new GetPaginatedFlows();
        $this->useCase->setUserGateway(new InMemoryUserGateway(array(UserStub1::ID => new UserStub1())));
        $flowStub1 = new FlowStub1();
        $flowStub1->setMessages(array(new MessageStub1()));
        $this->useCase->setFlowGateway(new InMemoryFlowGateway(array(FlowStub1::ID => $flowStub1)));
        $this->useCase->setFlowResponseFactory($this->getFlowResponseFactory());

        $this->requestBuilder = new GetPaginatedFlowsRequestBuilderImpl();
    }

    /**
     * @return FlowResponseFactoryImpl
     */
    private function getFlowResponseFactory()
    {
        $flowResponseFactory = new FlowResponseFactoryImpl();
        $flowResponseFactory->setMessageResponseFactory(new MessageResponseFactoryImpl());
        $flowResponseFactory->setPaginatedUseCaseResponseBuilder(new PaginatedUseCaseResponseBuilderImpl());

        return $flowResponseFactory;
    }
}
