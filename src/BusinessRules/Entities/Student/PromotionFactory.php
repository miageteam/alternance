<?php

namespace BusinessRules\Entities\Student;

use BusinessRules\Requestors\Student\CreatePromotionRequest;
use BusinessRules\Requestors\Student\EditPromotionRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface PromotionFactory
{
    /**
     * @param CreatePromotionRequest $request
     *
     * @return Promotion
     */
    public function createFromRequest(CreatePromotionRequest $request);

    /**
     * @param Promotion        $promotion
     * @param EditPromotionRequest $request
     *
     * @return Promotion
     */
    public function editFromRequest(Promotion $promotion, EditPromotionRequest $request);
}
