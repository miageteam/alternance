<?php

namespace BusinessRules\Entities\Student;

use BusinessRules\Requestors\Student\CreateStudentRequest;
use BusinessRules\Requestors\Student\EditStudentRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserStudentFactory
{
    /**
     * @param CreateStudentRequest $request
     *
     * @return UserStudent
     */
    public function createFromRequest(CreateStudentRequest $request);

    /**
     * @param UserStudent        $userStudent
     * @param EditStudentRequest $request
     *
     * @return UserStudent
     */
    public function editFromRequest(UserStudent $userStudent, EditStudentRequest $request);
}
