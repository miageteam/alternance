<?php

namespace BusinessRules\Responders\Student;

use BusinessRules\Entities\Student\Meeting;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface MeetingResponseFactory
{
    /**
     * @param Meeting $meeting
     *
     * @return MeetingResponse
     */
    public function createFromMeeting(Meeting $meeting);
}
