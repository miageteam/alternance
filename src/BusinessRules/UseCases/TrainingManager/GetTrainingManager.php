<?php

namespace BusinessRules\UseCases\TrainingManager;

use BusinessRules\Gateways\TrainingManager\TrainingManagerGateway;
use BusinessRules\Requestors\TrainingManager\GetTrainingManagerRequest;
use BusinessRules\Responders\TrainingManager\TrainingManagerResponse;
use BusinessRules\Responders\TrainingManager\TrainingManagerResponseFactory;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCase;
use OpenClassrooms\UseCase\BusinessRules\Requestors\UseCaseRequest;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class GetTrainingManager implements UseCase
{

    /**
     * @var TrainingManagerGateway
     */
    private $trainingManagerGateway;

    /**
     * @var TrainingManagerResponseFactory
     */
    private $trainingManagerResponseFactory;

    /**
     * @param GetTrainingManagerRequest $request
     *
     * @return TrainingManagerResponse
     */
    public function execute(UseCaseRequest $request)
    {
        $trainingManager = $this->trainingManagerGateway->findByUserId($request->getUserId());

        return $this->trainingManagerResponseFactory->createFromTrainingManager($trainingManager);
    }

    /**
     * @param TrainingManagerGateway $trainingManagerGateway
     */
    public function setTrainingManagerGateway(TrainingManagerGateway $trainingManagerGateway)
    {
        $this->trainingManagerGateway = $trainingManagerGateway;
    }

    /**
     * @param TrainingManagerResponseFactory $trainingManagerResponseFactory
     */
    public function setTrainingManagerResponseFactory(TrainingManagerResponseFactory $trainingManagerResponseFactory)
    {
        $this->trainingManagerResponseFactory = $trainingManagerResponseFactory;
    }
}
