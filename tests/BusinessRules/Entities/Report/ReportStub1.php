<?php

namespace BusinessRules\Entities\Report;

use AppBundle\Entity\Report\ReportImpl;
use BusinessRules\Entities\User\UserStub1;
use DateTime;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class ReportStub1 extends ReportImpl
{
    const ID         = 1;
    const TITLE      = 'Report Title';
    const CREATED_AT = '2015-04-20';
    const UPDATED_AT = '2015-04-20';

    public function __construct()
    {
        $this->author    = new UserStub1();
        $this->comments  = array(new CommentStub1());
        $this->createdAt = new DateTime(self::CREATED_AT);
        $this->updatedAt = new DateTime(self::UPDATED_AT);
    }

    protected $id = self::ID;

    protected $title = self::TITLE;

}
