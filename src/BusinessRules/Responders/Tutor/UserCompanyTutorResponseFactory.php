<?php

namespace BusinessRules\Responders\Tutor;

use BusinessRules\Entities\Tutor\UserCompanyTutor;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
interface UserCompanyTutorResponseFactory
{
    /**
     * @param UserCompanyTutor $userCompanyTutor
     *
     * @return UserCompanyTutorResponse
     */
    public function createFromUserCompanyTutor(UserCompanyTutor $userCompanyTutor);
}
