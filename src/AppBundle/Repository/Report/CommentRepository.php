<?php

namespace AppBundle\Repository\Report;

use BusinessRules\Entities\Report\Comment;
use BusinessRules\Gateways\Report\CommentGateway;
use BusinessRules\Gateways\Report\Exceptions\CommentNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class CommentRepository extends EntityRepository implements CommentGateway
{
    /**
     * {@inheritdoc}
     */
    public function insert(Comment $comment)
    {
        $this->_em->persist($comment);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        try {
            return $this->createQueryBuilder('c')
                ->andWhere('c.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult();

        } catch (NoResultException $nre) {
            throw new CommentNotFoundException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove(Comment $comment)
    {
        $this->_em->remove($comment);
    }

    /**
     * {@inheritdoc}
     */
    public function update(Comment $comment)
    {
        // do nothing
    }
}
