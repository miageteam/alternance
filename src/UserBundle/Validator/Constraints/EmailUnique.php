<?php

namespace UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Arnaud Lefèvre <arnaud.h.lefevre@gmail.com>
 */
class EmailUnique extends Constraint
{

    /**
     * @var string
     */
    public $message = 'user.email_already_exist';

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'email_unique';
    }
}
